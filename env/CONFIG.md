# (TraMod) WebApp Client Configuration

WebApp client is semi-modular system. There is currently no system provided to easily add custom traffic modules of application (other then editing source code) but existing modules have lot of configuration options.

For configuration [Vite's .env and modes](https://vitejs.dev/guide/env-and-mode.html) are utilized.

By default you can use _.env_, _.env.development_ and _.env.production_ but you can provide own modes like _dev-pilsen -> .env.dev-pilsen_ used by default serve command.
Note that .env files are not provided by repo but you can use .example variant as reasonable default.

## Config options list

### App modules activation and defaults (all options required)

- **VITE_ENABLED_DATA_MODES**

  - options: _model_, _comparison_
  - data modes selectable in data mode switcher

- **VITE_ENABLED_ROUTE_LINKS**

  - options: _user_, _scenarios_, _models_, _settings_
  - main (left) menu accessible entries

- **VITE_ENABLED_MODEL_TYPES**

  - options: _DTA_
  - allowed model types, for their usage, traffic model of that type must be present in backend DB

- **VITE_ENABLED_WIDGETS**

  - options: _calendar_, _comparison_, _legend_, _eventList_, _mapPopup_, _timeline_, _mapSettings_
  - widgets allowed to be used in app (some widgets are connected to specific data modes)

- **VITE_DEFAULT_MAP_MODE**

  - examples: _modelDTA_, (see useMapModes)[../src/composables/useMapModes.ts]
  - default map mode to open app if not provided by url, only one selectable, and might by constrained by data mode availability

- **VITE_DEFAULT_TRAFFIC_MODEL_TYPE**

  - options: _DTA_

### App global settings

- **VITE_API_HOST**

  - path to WebApp Backend service

- **VITE_ENABLED_LANGUAGES**

  - options: _EN_, _CS_
  - languages selectable in app, only EN/CS possible to select, other languages are not translated in locales and configured in plugins

- **VITE_GOOGLE_ANALYTICS_ID**

  - GA_ID, required for google analytics activation

- **VITE_TEXT_APP_TITLE_CS**
- **VITE_TEXT_APP_TITLE_EN**

  - html title to use dynamically

- **VITE_TEXT_APP_DESCRIPTION_CS**
- **VITE_TEXT_APP_DESCRIPTION_EN**

  - html meta description to use dynamically

- **VITE_TEXT_COOKIE_CONTACT_CS**
- **VITE_TEXT_COOKIE_CONTACT_EN**

  - localized text for cookie contact to use dynamically

- **VITE_TEXT_ABOUT_CS**
- **VITE_TEXT_ABOUT_EN**
  - localized about text (available in help panel) to use dynamically

### Map options

- **VITE_DEFAULT_MAP_BASE**

- **VITE_API_OSM**

  - url for OSM basemap service

- **VITE_DEFAULT_MAP_ZOOM**

- **VITE_DEFAULT_MAP_CENTER**

- **VITE_MAP_STYLING_MODE**

  - set to _custom_ to enable more granular style config for links (color, width, text) and not only presets (activated only for modelDTA)

- **VITE_TRAFFIC_SIGNS_STYLE**
  - set to true, to enable traffic sign style setting for model link type modelSigns on map

### Model Data Modes options

- **VITE_API_MODEL_DTA**

  - Required if data mode enabled, Url of traffic-modeller model API endpoint (for relevant modelType)

- **VITE_API_MODEL_DTA**

  - Required if data mode enabled, ModelName used for relevant modelType

- **VITE_MODEL_CACHE_PREFIX**

  - Required, prefix set by backend for model caches grouping, to correctly infer default cache name

- **VITE_COMPUTATION_REQUEST_FREQUENCY_DTA**
  - in miliseconds, timeout between periodical checks for model computation result, 20000ms DTA. In most cases default should be used.
