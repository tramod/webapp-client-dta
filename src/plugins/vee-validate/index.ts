import { defaultLocale } from '@plugins/i18n'; // default locale logic decided at i18n using store
import { configureVeeValidate } from './config';

configureVeeValidate(defaultLocale);
