import { createVueMap } from 'vuemap';
import type { App } from 'vue';

export default function registerVuemap(app: App) {
  app.use(createVueMap());
}
