import type { TimeLocaleDefinition } from 'd3';

const d3Locales: Record<TmLang, TimeLocaleDefinition> = {
  cs: {
    // decimal: ',',
    // thousands: ' ',
    dateTime: '%A,%e.%B %Y, %X',
    date: '%-d.%-m.%Y',
    time: '%H:%M:%S',
    periods: ['AM', 'PM'],
    days: ['Neděle', 'Pondělí', 'Úterý', 'Středa', 'Čvrtek', 'Pátek', 'Sobota'],
    shortDays: ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
    months: [
      'Leden',
      'Únor',
      'Březen',
      'Duben',
      'Květen',
      'Červen',
      'Červenec',
      'Srpen',
      'Září',
      'Říjen',
      'Listopad',
      'Prosinec',
    ],
    shortMonths: ['Led', 'Úno', 'Břez', 'Dub', 'Kvě', 'Čer', 'Červ', 'Srp', 'Zář', 'Říj', 'List', 'Pros'],
  },

  en: {
    // decimal: '.',
    // thousands: ',',
    dateTime: '%a %b %e %X %Y',
    date: '%m/%d/%Y',
    time: '%H:%M:%S',
    periods: ['AM', 'PM'],
    days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    shortDays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    months: [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ],
    shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  },
};

export default d3Locales;
