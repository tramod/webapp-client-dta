import { toJsDate } from '@utils/tm-date';

type ModelSection = { key: string; dateFrom: TmDate; dateTo: TmDate; codeList: string };

export function getScenarioModelSectionByDate(scenario: TmScenario, date: TmDate) {
  const modelSections: ModelSection[] = scenario.modelSections || [];
  const selectedSection = hasOneNullishSection(modelSections)
    ? modelSections[0]
    : modelSections.find((section: ModelSection) => isCurrentSection(section, date));
  return selectedSection ? { sectionKey: selectedSection.key, extraFeaturesCodeList: selectedSection.codeList } : {};
}

function hasOneNullishSection(sections: ModelSection[]) {
  if (sections.length !== 1) return false;
  const { dateFrom, dateTo } = sections[0];
  return dateFrom === null && dateTo === null;
}

function isCurrentSection(section: ModelSection, date: TmDate) {
  const currentDate = toJsDate(date);
  const dateFrom = toJsDate(section.dateFrom);
  const dateTo = toJsDate(section.dateTo);

  if (!currentDate || (!dateFrom && !dateTo)) return false;
  if (dateFrom && dateTo) return currentDate < dateTo && currentDate >= dateFrom;
  if (!dateFrom && dateTo) return currentDate < dateTo;
  if (dateFrom && !dateTo) return currentDate >= dateFrom;
}
