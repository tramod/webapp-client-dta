import type { RouteParams } from 'vue-router';

const stringToNumber = (p: string) => Number.parseInt(p, 10);

export const parseRouteParam = (routeParam?: string | string[]) => {
  if (!routeParam) return;
  const singleStringParam = Array.isArray(routeParam) ? routeParam[0] : routeParam;
  return stringToNumber(singleStringParam);
};

export const getScenarioItemIdsFromRouteParams = (params: RouteParams): TmItemIds => {
  return {
    scenarioId: parseRouteParam(params.id),
    eventId: parseRouteParam(params.evId),
    modificationId: parseRouteParam(params.modId),
  };
};
