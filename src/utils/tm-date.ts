import { add, sub } from 'date-fns';

export const today = ({ stripHours = false, stripMins = true } = {}) => {
  const jsDate = new Date();
  if (stripHours) jsDate.setHours(0, 0, 0, 0);
  if (stripMins) jsDate.setMinutes(0, 0, 0);
  return jsDate.toISOString();
};

export const tomorrow = ({ stripHours = false } = {}) => {
  const jsDate = add(new Date(), { days: 1 });
  if (stripHours) jsDate.setHours(0, 0, 0, 0);

  return jsDate.toISOString();
};

export const yesterday = ({ stripHours = false } = {}) => {
  const jsDate = sub(new Date(), { days: 1 });
  if (stripHours) jsDate.setHours(0, 0, 0, 0);

  return jsDate.toISOString();
};

export const getModelMonday = ({ hour = null }: { hour?: number | null } = {}) => {
  // hard-set day of the traffic model - probably just temporary
  const jsDate = new Date('2022-01-03');
  if (hour) jsDate.setHours(hour, 0, 0, 0);

  return jsDate.toISOString();
};

/* TmDate helpers, expect date iso string, returns same */
export const getTmDateHours = (tmDate: TmDate) => {
  if (!tmDate) return 0;

  const date = new Date(tmDate);
  return date.getHours();
};

export const setTmDateHours = (tmDate: TmDate, hours: number) => {
  if (!tmDate) return tmDate;

  const date = new Date(tmDate);
  date.setHours(hours);
  return date.toISOString();
};

export const addTmDateHours = (
  tmDate: TmDate,
  hours: number,
  {
    minHour = 0,
    maxHour = null,
  }: {
    minHour?: number;
    maxHour?: number | null;
  } = {},
) => {
  if (!tmDate) return tmDate;
  const date = new Date(tmDate);
  const currentHours = date.getHours();
  const currentMinutes = date.getMinutes();
  let hourDecimal = currentHours + hours + currentMinutes / 60;
  if (maxHour && hourDecimal > maxHour) hourDecimal = minHour;
  date.setHours(getHoursFromHoursNumber(hourDecimal), getMinutesFromHoursNumber(hourDecimal));
  return date.toISOString();
};

export const subtractTmDateHours = (tmDate: TmDate, hours: number) => {
  if (!tmDate) return tmDate;
  const date = new Date(tmDate);
  const currentHours = date.getHours();
  date.setHours(currentHours - hours);
  return date.toISOString();
};

export const isAfter = (date: TmDate, compareDate: TmDate) => {
  const dateD = toJsDate(date);
  const compareDateD = toJsDate(compareDate);
  if (!dateD || !compareDateD) return false;
  return dateD.valueOf() - compareDateD.valueOf() < 0;
};

export const toJsDate = (tmDate: TmDate, { stripHours = false } = {}) => {
  if (!tmDate) return null;
  const jsDate = new Date(tmDate);
  if (stripHours) jsDate.setHours(0, 0, 0, 0);
  return jsDate;
};

export const toTmDate = (date: TmCommonDate) => {
  const jsDate = new Date(date);
  return jsDate.toISOString();
};

export const formatScenarioDate = (
  tmDate: TmDate | undefined,
  i18n: () => { t: (phrase: string) => string; d: (date: TmDate, format: string) => string },
) => {
  const { t, d } = i18n();
  switch (typeof tmDate) {
    case 'undefined':
      return 'n/a';
    case 'object':
      // meaning null
      return t('scenarios.unlimited');
    case 'string':
      return d(tmDate, 'default');
  }
};

export const formatScenarioDatePeriod = (
  dateFrom: TmDate | undefined,
  dateTo: TmDate | undefined,
  i18n: () => { t: (phrase: string) => string; d: (date: TmDate, phrase: string) => string },
) => {
  return formatScenarioDate(dateFrom, i18n) + ' - ' + formatScenarioDate(dateTo, i18n);
};

export const formatAsApiDate = (tmDate: TmDate, round = true) => {
  if (!tmDate) return '';
  const date = new Date(tmDate);
  if (round) {
    date.setHours(date.getHours() + Math.round(date.getMinutes() / 60));
    date.setMinutes(0, 0, 0);
  }
  return date.toISOString().split('.')[0]; // strip milliseconds
};

const HOUR_MILLISECONDS = 1000 * 60 * 60;
// Expect hour with decimal minutes/seconds
export const formatTravelTime = (hourPart: number) => {
  const date = new Date(HOUR_MILLISECONDS * hourPart);
  const hours = date.getUTCHours();
  const minutes = date.getUTCMinutes();
  const seconds = date.getSeconds();
  return `${hours ? hours + 'h ' : ''} ${minutes ? minutes + 'm ' : ''} ${seconds}s`;
};

// Hours number = float hours.decimal minutes 3:15 -> 3.25
export const getTmDateHoursNumber = (tmDate: TmDate, { stripMins = false } = {}) => {
  if (!tmDate) return 0;
  const date = new Date(tmDate);
  const h = date.getHours();
  const m = stripMins ? 0 : date.getMinutes();
  return h + m / 60;
};

export const getHoursFromHoursNumber = (n: number) => Math.trunc(n);

export const getMinutesFromHoursNumber = (n: number) => 60 * (n % 1);

export const dateIntervalIntersection = (date: TmDate) => {
  const testDate = toJsDate(date);
  if (testDate == null) return true;

  const evaluateDate = ({ dateFrom, dateTo }: { dateFrom: TmDate; dateTo: TmDate }) => {
    // DateFrom match included in intersection, DateTo match excluded (eg. for date 12:00, item starting at 12:00 is included, item ending at 12:00 excluded)
    const jsDateFrom = toJsDate(dateFrom);
    const jsDateTo = toJsDate(dateTo);
    return (
      (dateFrom === null || jsDateFrom === null || jsDateFrom <= testDate) &&
      (dateTo === null || jsDateTo === null || jsDateTo > testDate)
    );
  };

  return evaluateDate;
};
