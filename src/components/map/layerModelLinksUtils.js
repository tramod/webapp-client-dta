import { createLineStringFeature } from './layerCommonUtils';

// utility to get full vector feature instead of tiled one (might be simplified, or cropped)
export function createFeatureFromId(featureId, fetchFeatureFunction) {
  // Get data from our pre-tile cache, if server tiles will be used we need to query model server here
  const featureData = fetchFeatureFunction(featureId);
  return createLineStringFeature(featureData, { idProperty: 'edge_id' });
}

export function createFeatureFromFeature(feature, fetchFeatureFunction) {
  if (!feature || !feature.getId()) return null;
  return createFeatureFromId(feature.getId(), fetchFeatureFunction);
}

export function createFeatureFromPoints(points) {
  if (points.length < 2) throw Error('Cannot create lineString from less than 2 points');
  return createLineStringFeature({
    geometry: { coordinates: points.map((point) => point.getGeometry().getCoordinates()) },
  });
}

export function createFeatureFromCoordinates(coordinates) {
  return createLineStringFeature({
    geometry: { coordinates },
  });
}
