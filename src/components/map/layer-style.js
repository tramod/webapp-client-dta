import Style from 'ol/style/Style';
import Stroke from 'ol/style/Stroke';
import Text from 'ol/style/Text';
import Circle from 'ol/style/Circle';
import LineString from 'ol/geom/LineString';
import MultiLineString from 'ol/geom/MultiLineString';
import Fill from 'ol/style/Fill';
import Icon from 'ol/style/Icon';
import { getLength } from 'ol/sphere';

import { MP_MODEL_DTA_VIEWER, MP_MODEL_DTA_COMPARISON } from '@keys/index';

const styleConfig = {
  general: {
    vehiclesCountDisplayed: false,
    trafficSignsStyle: false,
    modelLinkOffset: false,
    width: 'volume',
    color: 'volume',
    text: 'volume',
  },
  [MP_MODEL_DTA_VIEWER]: {
    output: 'outflow-capacity',
    width: 'outFlow',
    color: 'outFlowIntensity',
    text: 'outFlow',
  },
  [MP_MODEL_DTA_COMPARISON]: {
    width: 'outFlowDiff',
    color: 'outFlowDiff',
    text: 'outFlowDiff',
  },
};

export const setStyleConfig = (updatedConfig) => {
  const updatedKeys = Object.keys(updatedConfig);
  for (const configKey of updatedKeys) {
    const oldVal = JSON.stringify(styleConfig[configKey]);
    Object.assign(styleConfig[configKey], updatedConfig[configKey]);
    if (configKey !== 'general' && oldVal !== JSON.stringify(styleConfig[configKey])) updateLinkDataGetter(configKey);
  }
};

const configSetProps = ['width', 'color', 'text'];
const getConfigSet = (configKey) => {
  const usedConfig = styleConfig[configKey] || styleConfig.general;
  return configSetProps.reduce((set, property) => {
    if (property === 'text' && !styleConfig.general.vehiclesCountDisplayed) return set;
    set[property] = usedConfig[property];
    return set;
  }, {});
};

/*
  App style configs
  Currently static (might become configurable later)
*/
const COLORS = {
  PRIMARY: '#0094d2',
  DISABLED: '#66bfe4',
  HIGHLIGHT: '#007CAF',
  SELECTED: '#F8F004',
  SOURCE: '#3EC71D',
  TARGET: '#F05A28',
  CLOSED: '#000',
  CHANGED: '#BE1E2D',
  TRAFFIC_PALETTE: ['#009345', '#F9EC31', '#FAAF40', '#F05A28', '#BE1E2D'],
  TRAFFIC_DIFF_PALETTE: ['#2166AC', '#67A9CF', '#D1E5F0', '#F7F7F7', '#FDDBC7', '#EF8A62', '#BE1E2D'],
  TRAFFIC_DIFF_LONG_PALETTE: [
    '#5e4fa2',
    '#3288bd',
    '#66c2a5',
    '#abdda4',
    '#e6f598',
    '#ffffbf',
    '#fee08b',
    '#fdae61',
    '#f46d43',
    '#d53e4f',
    '#9e0142',
  ],
  TRAFFIC_DIFF_EXTRA_LONG_PALETTE: [
    '#7B3014',
    '#9C3206',
    '#C53E01',
    '#E35408',
    '#F4711F',
    '#FD8E3F',
    '#FDAA65',
    '#D2D2D2',
    '#7AC7E2',
    '#6AB1D6',
    '#4993C0',
    '#2D7DB4',
    '#1C6AA8',
    '#1F5591',
    '#26456E',
  ],
  TRAFFIC_DIFF_EXTRA_LONG_PALETTE2: [
    '#7B3014',
    '#9C3206',
    '#C53E01',
    '#E35408',
    '#F4711F',
    '#FD8E3F',
    '#FDAA65',
    '#96BC85',
    '#7AC7E2',
    '#6AB1D6',
    '#4993C0',
    '#2D7DB4',
    '#1C6AA8',
    '#1F5591',
    '#26456E',
  ],
  SINGLE_COLOR_PALETTE: ['#ffffd4', '#fed98e', '#fe9929', '#d95f0e', '#993404'],
};

const COLOR_SETS = {
  INTENSITY: { levels: [0.45, 0.65, 0.85, 1], colorMap: COLORS.TRAFFIC_PALETTE },
  DIFF: { levels: [-500, -250, -50, 50, 250, 500], colorMap: COLORS.TRAFFIC_DIFF_PALETTE },
  SPEED: { levels: [31, 51, 71, 91], colorMap: COLORS.SINGLE_COLOR_PALETTE },
  VOLUMES: { levels: [200, 500, 1000, 2000], colorMap: COLORS.SINGLE_COLOR_PALETTE },
  TRAVEL_TIMES: {
    levels: [0.001, 0.01, 0.1, 0.2],
    colorMap: COLORS.SINGLE_COLOR_PALETTE,
  },
  DELAY_TIMES: {
    levels: [0.001, 0.005, 0.05, 0.2],
    colorMap: COLORS.TRAFFIC_PALETTE,
  },
};

// GENERAL / COMMON
/* 
  General mode agnostic traffic link style function
  Optional highlighting and vehiclesCountDisplayed modes
*/
export function commonLinkStyle(feature, resolution, { highlight = false, context = {} } = {}) {
  const { vehiclesCountDisplayed } = styleConfig.general;
  const linkData = _getLinkData({ feature, context, resolution, highlight, vehiclesCountDisplayed });
  const props = _getLinkStyleProps(linkData);
  return _getCommonLinkStyle(feature, resolution, props);
}

// MODEL
/*
  Model link viewing styles
*/
export function modelLinkStyle(feature, resolution, { highlight = false, context = {} } = {}, styleType) {
  const { vehiclesCountDisplayed, modelLinkOffset } = styleConfig.general;
  const linkData = _getLinkData({ feature, context, resolution, highlight, vehiclesCountDisplayed }, styleType);
  const baseProps = _getLinkStyleProps(linkData, styleType);
  const modelProps = _getModelLinkStyleProps(linkData, baseProps);

  const baseStyle = _getCommonLinkStyle(feature, resolution, {
    ...baseProps,
    ...modelProps,
    lineOffset: modelLinkOffset,
  });
  if (modelProps.isExtra && vehiclesCountDisplayed && baseStyle.length)
    baseStyle.unshift(...createLineSegmentTexts(feature, baseProps.text));
  return baseStyle;
}

export const modelDTALinkStyle = (...args) => modelLinkStyle(...args, MP_MODEL_DTA_VIEWER);

export function modelShortestPathStyle(feature, resolution, { context } = {}) {
  const { modelLinkOffset } = styleConfig.general;
  const linkData = _getLinkData({ feature, context, resolution, highlight: true, minVolume: 100 });
  const props = _getLinkStyleProps(linkData);
  return _getCommonLinkStyle(feature, resolution, {
    ...linkData,
    ...props,
    lineOffset: modelLinkOffset,
  });
}

/*
  Model link style for node neighbors, for node traffic details
*/
export function modelNeighboringLinksStyle(
  feature,
  resolution,
  options = {}, // isLinkTo, isLinkFrom, isSelectedLink, isDirectionLink, isTabled, isTabledHighlighted, isHovered
) {
  const { isSelectedLink, isDirectionLink, isTabled, isHovered } = options;
  const { modelLinkOffset } = styleConfig.general;
  if (!isValid(feature) || !(isSelectedLink || isDirectionLink || isTabled)) return [];

  const capacity = parseInt(feature.get('capacity'), 10);
  const styleProps = _getNLStyleProps(capacity, options);

  const lineOffsetMargin = modelLinkOffset ? 3 : 0;
  const styles = _getCommonLinkStyle(feature, resolution, {
    resolution,
    ...styleProps,
    lineOffset: modelLinkOffset,
    lineOffsetMargin,
  });
  if (isSelectedLink || isTabled) {
    const highlightColor = _getNLHighlightColor(options);
    if (isSelectedLink)
      styles.unshift(
        _getHighlightLinkStyle(feature, resolution, {
          color: highlightColor,
          width: (capacity / 800) * 3 + 6,
          zIndex: capacity - 1,
          lineOffset: modelLinkOffset,
        }),
      );

    if (!isHovered)
      styles.unshift(
        _getNLTextStyle(feature, resolution, {
          fId: feature.getId(),
          highlightColor,
          capacity,
          width: styleProps.width,
          lineOffset: modelLinkOffset,
          lineOffsetMargin,
        }),
      );
  }
  return styles;
}

/*
  Model items edit interaction styles
  Set of selector/active/disabled styles for each item type (link/node/generator)
*/
export function modelLinksSelectorStyle(feature, resolution, { context } = {}) {
  const isHovered = !!feature.get('isHovered');
  const isSelected = !!feature.get('isSelected');
  return _linkSelectorStyle(feature, resolution, { hover: isHovered, selected: isSelected, context });
}

export function modelLinksSelectedStyle(feature, resolution, { context } = {}) {
  return _linkSelectorStyle(feature, resolution, { selected: true, showText: true, context });
}

export const modelLinksDisabledStyle = _linkDisabledStyle;

export function modelLinksActiveStyle() {
  return lineActiveStyle;
}

export function modelNodesViewerStyle() {
  return _nodeIconCache;
}

export function modelNodesPrimaryStyle(feature) {
  const isHovered = feature.get('isHovered');
  return isHovered ? circleHighlightStyle : circlePrimaryStyle;
}

export function modelNodesDisabledStyle() {
  return circleDisabledStyle;
}

const nodesSelectedColors = {
  source: COLORS.SOURCE,
  target: COLORS.TARGET,
  selected: COLORS.SELECTED,
  generatorSelected: COLORS.SOURCE,
};

export function modelNodesSelectedStyle(feature, resolution) {
  const type = feature.get('type');
  const isHovered = feature.get('isHovered');
  const mainColor = nodesSelectedColors[type];
  const fillColor = isHovered ? COLORS.PRIMARY : COLORS.HIGHLIGHT;
  const label = resolution < 16 ? `ID ${feature.getId()}` : null;
  return getCircleSelectedStyle({
    mainColor,
    fillColor,
    label,
    ...(type === 'selected' && { textStrokeColor: '#000' }),
  });
}

export function modelNodeCreatedStyle(feature) {
  const isHovered = feature.get('isHovered');

  const mainColor = COLORS.SELECTED;
  const fillColor = isHovered ? COLORS.PRIMARY : COLORS.HIGHLIGHT;

  return getCircleSelectedStyle({
    mainColor,
    fillColor,
  });
}

const maxTrips = 1000;
const sizeStep = (maxTrips - 10) / 12; // Diff between 6 a 18
const iconScaleStep = (3 - 0.3) / 12;
export function modelGeneratorViewerStyle(feature) {
  const { incoming_trips: inTrips, outgoing_trips: outTrips } = feature.getProperties();
  const tripsAvg = (inTrips + outTrips) / 2;
  const stepCount = Math.round((tripsAvg > maxTrips ? maxTrips : tripsAvg) / sizeStep);
  // const radius = 6 + stepCount;
  const iconInc = stepCount * iconScaleStep;
  const iconScale = (0.7 + iconInc).toFixed(1);
  return getGeneratorViewStyleCached({ iconScale });
}

export function modelGeneratorPrimaryStyle(feature) {
  const isHovered = feature.get('isHovered');
  const color = isHovered ? COLORS.HIGHLIGHT : COLORS.PRIMARY;
  return getCircleSelectedStyle({
    mainColor: color,
    fillColor: color,
  });
}

export function modelGeneratorCreatedStyle(feature) {
  const isHovered = feature.get('isHovered');
  const fillColor = COLORS.PRIMARY;
  const mainColor = isHovered ? COLORS.PRIMARY : COLORS.HIGHLIGHT;
  return getCircleSelectedStyle({
    mainColor,
    fillColor,
  });
}

export function modelGeneratorSelectedStyle(feature, resolution) {
  const isHovered = feature.get('isHovered');
  const fillColor = isHovered ? COLORS.PRIMARY : COLORS.HIGHLIGHT;
  const mainColor = COLORS.SELECTED;
  const label = resolution < 16 && !isHovered ? `ID ${feature.getId()}` : null;
  return getCircleSelectedStyle({
    mainColor,
    fillColor,
    label,
    textStrokeColor: '#000',
  });
}

/*
  DTA visualisations styles
  outflow - Outflow - Šířkou outflow, barva ponechat RT modrou všude
  inflow - Inflow - Šířkou inflow, barva ponechat RT modrou všude
  speed-width - Rychlost (délka úseku / Travel time) šířkou úseku - Šířkou délka úseku / Travel time, barva ponechat RT modrou všude
  speed-color - Rychlost (délka úseku / Travel time) barvou úseku - Šířkou nechat jednotně (vyber nějakou střední hodnotu), barva délka úseku / Travel time
  outflow-speed - Outflow a Rychlost - Šířkou outflow, barva délka úseku / Travel time
  outflow-capacity - Outflow a Stupeň dopravy (ouflow/kapacita) - Šířkou outflow, barva je poměr ouflow/kapacita
  outflow-inflow - Outflow vs. Inflow (Outflow - Inflow) - Šířkou Outflow - Inflow, barvou podle toho, jestli je výsledek kladný nebo záporný (ala rozdílová mapa)
  speed-free - Spočtená rychlost vs. free speed ((délka úseku / Travel time) - free speed) - šířka konstantní, barvou (délka úseku / Travel time) - free speed
  speed-critical - Spočtená rychlost vs. critical_speed ((délka úseku / Travel time) - critical_speed) - šířka konstantní, barvou (délka úseku / Travel time) - critical_speed
  delay-free - Zpoždění (free speed) - šířka konstantní, barvou Travel time - (délka úseku / free speed)
  delay-critical - Zpoždění (critical speed) - šířka konstantní, barvou Travel time - (délka úseku / critical speed)
  outflow-delay - Outflow a Zpoždění 1 - Šířkou outflow, barvou Travel time - (délka úseku / free speed)

  Feature
   - length (km)
   - free_speed (kph)
   - critical_speed (kph)
   - capacity (veh/hour)
  Context 
   - outflow (veh/hour)
   - inflow (veh/hour)
   - travelTime (hour)
  Calc
   - speed (length(km)/travelTime(h))
   - outFlow intensity (outFlow/capacity*magic coef)
   - flowDiff (outFlow - inFlow)
   - freeSpeedDiff (speed - freeSpeed)
   - criticalSpeedDiff (speed - criticalSpeed)
   - freeSpeedTime (length/freeSpeed)
   - criticalSpeedTime (length/criticalSpeed)
   - delayFree (travelTime - freeSpeedTime)
   - delayCritical (travelTime - criticalSpeedTime)
  Width variant
   - standard width (3?)
   - outflow
   - inflow
  Color variant
   - plain color RT
  Other?
*/
const featurePropGetter =
  (property) =>
  ({ feature }) =>
    feature.get(property);

const contextPropGetter =
  (contextPath, defaultValue) =>
  ({ context }) =>
    getFromPath(context, contextPath) ?? defaultValue;
const getFromPath = (obj, path) => {
  if (!obj) return null;
  if (path.length === 1) return obj[path];
  return getFromPath(obj[path[0]], path.slice(1));
};

const contextOrFeatureGetter =
  (contextPath, featureProperty) =>
  ({ feature, context }) =>
    getFromPath(context, contextPath) ?? feature.get(featureProperty);

const linkDataGetters = {};

const LINK_DATA_PROPERTIES = {
  [MP_MODEL_DTA_VIEWER]: [
    { key: 'length', getter: ({ feature }) => feature.get('length') ?? getLength(feature.getGeometry()) / 1000 }, // get from geom if not on feature props
    { key: 'freeSpeed', getter: featurePropGetter('free_speed') },
    { key: 'criticalSpeed', getter: featurePropGetter('critical_speed') },
    { key: 'outFlow', getter: contextPropGetter(['outFlow']) },
    { key: 'inFlow', getter: contextPropGetter(['inFlow']) },
    { key: 'volume', getter: ({ outFlow, inFlow }) => (outFlow + inFlow) / 2, deps: ['outFlow', 'inFlow'] },
    { key: 'travelTime', getter: contextPropGetter(['travelTime']) },
    { key: 'capacity', getter: ({ context, feature }) => context?.capacity ?? feature.get('capacity') },
    { key: 'travelSpeed', getter: ({ length, travelTime }) => length / travelTime, deps: ['length', 'travelTime'] },
    { key: 'outFlowIntensity', getter: ({ outFlow, capacity }) => outFlow / capacity, deps: ['outFlow', 'capacity'] },
    { key: 'freeSpeedTime', getter: ({ length, freeSpeed }) => length / freeSpeed, deps: ['length', 'freeSpeed'] },
    {
      key: 'criticalSpeedTime',
      getter: ({ length, criticalSpeed }) => length / criticalSpeed,
      deps: ['length', 'criticalSpeed'],
    },
    { key: 'flowDiff', getter: ({ outFlow, inFlow }) => outFlow - inFlow, deps: ['outFlow', 'inFlow'] },
    {
      key: 'freeSpeedDiff',
      getter: ({ travelSpeed, freeSpeed }) => travelSpeed - freeSpeed,
      deps: ['travelSpeed', 'freeSpeed'],
    },
    {
      key: 'criticalSpeedDiff',
      getter: ({ travelSpeed, criticalSpeed }) => travelSpeed - criticalSpeed,
      deps: ['travelSpeed', 'criticalSpeed'],
    },
    {
      key: 'delayFreeSpeed',
      getter: ({ travelTime, freeSpeedTime }) => travelTime - freeSpeedTime,
      deps: ['travelTime', 'freeSpeedTime'],
    },
    {
      key: 'delayCriticalSpeed',
      getter: ({ travelTime, criticalSpeedTime }) => travelTime - criticalSpeedTime,
      deps: ['travelTime', 'criticalSpeedTime'],
    },
  ],
  [MP_MODEL_DTA_COMPARISON]: [
    { key: 'length', getter: ({ feature }) => feature.get('length') ?? getLength(feature.getGeometry()) / 1000 }, // get from geom if not on feature props
    { key: 'freeSpeedSource', getter: contextOrFeatureGetter(['source', 'speed'], 'free_speed') },
    { key: 'freeSpeedTarget', getter: contextOrFeatureGetter(['target', 'speed'], 'free_speed') },
    {
      key: 'freeSpeedDiff',
      getter: ({ freeSpeedSource, freeSpeedTarget }) => freeSpeedSource - freeSpeedTarget,
      deps: ['freeSpeedSource', 'freeSpeedTarget'],
    },
    { key: 'capacitySource', getter: contextOrFeatureGetter(['source', 'capacity'], 'capacity') },
    { key: 'capacityTarget', getter: contextOrFeatureGetter(['target', 'capacity'], 'capacity') },
    {
      key: 'capacityDiff',
      getter: ({ capacitySource, capacityTarget }) => capacitySource - capacityTarget,
      deps: ['capacitySource', 'capacityTarget'],
    },
    { key: 'outFlowSource', getter: contextPropGetter(['source', 'outFlow'], 0) },
    { key: 'outFlowTarget', getter: contextPropGetter(['target', 'outFlow'], 0) },
    {
      key: 'outFlowDiff',
      getter: ({ outFlowSource, outFlowTarget }) => outFlowSource - outFlowTarget,
      deps: ['outFlowSource', 'outFlowTarget'],
    },
    { key: 'inFlowSource', getter: contextPropGetter(['source', 'inFlow'], 0) },
    { key: 'inFlowTarget', getter: contextPropGetter(['target', 'inFlow'], 0) },
    {
      key: 'inFlowDiff',
      getter: ({ inFlowSource, inFlowTarget }) => inFlowSource - inFlowTarget,
      deps: ['inFlowSource', 'inFlowTarget'],
    },
    { key: 'travelTimeSource', getter: contextPropGetter(['source', 'travelTime'], 1) },
    { key: 'travelTimeTarget', getter: contextPropGetter(['target', 'travelTime'], 1) },
    {
      key: 'travelTimeDiff',
      getter: ({ travelTimeSource, travelTimeTarget }) => travelTimeSource - travelTimeTarget,
      deps: ['travelTimeSource', 'travelTimeTarget'],
    },
    {
      key: 'travelSpeedSource',
      getter: ({ length, travelTimeSource }) => length / travelTimeSource,
      deps: ['length', 'travelTimeSource'],
    },
    {
      key: 'travelSpeedTarget',
      getter: ({ length, travelTimeTarget }) => length / travelTimeTarget,
      deps: ['length', 'travelTimeTarget'],
    },
    {
      key: 'travelSpeedDiff',
      getter: ({ travelSpeedSource, travelSpeedTarget }) => travelSpeedSource - travelSpeedTarget,
      deps: ['travelSpeedSource', 'travelSpeedTarget'],
    },
    {
      key: 'outIntensitySource',
      getter: ({ outFlowSource, capacitySource }) => outFlowSource / capacitySource,
      deps: ['outFlowSource', 'capacitySource'],
    },
    {
      key: 'outIntensityTarget',
      getter: ({ outFlowTarget, capacityTarget }) => outFlowTarget / capacityTarget,
      deps: ['outFlowTarget', 'capacityTarget'],
    },
    {
      key: 'outIntensityDiff',
      getter: ({ outIntensitySource, outIntensityTarget }) => outIntensitySource - outIntensityTarget,
      deps: ['outIntensitySource', 'outIntensityTarget'],
    },
  ],
};

const _parseGetterDeps = (getterSet, dataList, key) => {
  if (key === 'none') return;
  const dataItem = dataList.find((getter) => getter.key === key);
  if (dataItem.deps)
    for (const property of dataItem.deps) {
      _parseGetterDeps(getterSet, dataList, property);
    }
  getterSet.add(dataItem);
};

function createLinkDataGetterSet(configKey) {
  const props = Object.values(getConfigSet(configKey));
  if (configKey === MP_MODEL_DTA_VIEWER) props.unshift('volume');
  const getterSet = new Set();
  for (const property of props) {
    _parseGetterDeps(getterSet, LINK_DATA_PROPERTIES[configKey], property);
  }
  return Array.from(getterSet);
}

function updateLinkDataGetter(configKey) {
  linkDataGetters[configKey] = createLinkDataGetterSet(configKey);
}

for (const configKey of Object.keys(styleConfig)) {
  if (configKey !== 'general') updateLinkDataGetter(configKey);
}

const formatRound = (value) => Math.round(value).toString();
const formatFixed = (value) => value.toFixed(2).toString();
const MS_IN_HOUR = 1000 * 60 * 60;
const formatHour = (hourPart) => {
  const isNegative = hourPart < 0;
  const positiveValue = Math.abs(hourPart);
  const date = new Date(MS_IN_HOUR * positiveValue);
  const hours = date.getUTCHours();
  const minutes = date.getUTCMinutes();
  const seconds = date.getSeconds();
  return `${isNegative ? '- ' : ''}${hours ? hours + ':' : ''} ${minutes ? minutes : ''}:${seconds}`; // Debug with [${hourPart.toFixed(5)}]
};

const getLinkColor = (linkData, type, property) => {
  if (linkData.highlight === true) return COLORS.HIGHLIGHT;
  if (property === 'none') return COLORS.PRIMARY;
  return _getClassifiedColor({
    value: linkData[property],
    ...LINK_STYLES[type].COLOR[property],
  });
};

const getLinkWidth = (linkData, type, property) => {
  if (property === 'none') return 3;
  const transformationFn = LINK_STYLES[type].WIDTH[property];
  const value = transformationFn ? transformationFn(linkData[property]) : linkData[property];
  return _getValueWidth(value, linkData.resolution);
};

const getLinkText = (linkData, type, property) => {
  const textValue = linkData[property];
  if (typeof textValue === 'undefined' || textValue === null) return '';
  const formatFn = LINK_STYLES[type].TEXT?.[property] ?? LINK_STYLES_TEXT_DEFAULT;
  return formatFn(textValue);
};

const LINK_STYLES = {
  general: {
    WIDTH: {},
    COLOR: {
      volume: COLOR_SETS.DIFF,
    },
  },
  [MP_MODEL_DTA_VIEWER]: {
    // Get intensity width formula has experimental resolution recalc tailored to volume numbers (expected to be 0-3600 in pilsen)
    // as other widths are experimental for now, temp solution is to convert its value to volume like number (with similar min-max)
    // in many cases formula improvement might be needed if that prop will be used for final viz
    WIDTH: {
      length: (value) => value * 800,
      freeSpeed: (value) => (value - 20) * 30,
      criticalSpeed: (value) => (value - 20) * 30,
      travelTime: (value) => value * 60 * 800,
      travelSpeed: (value) => (value - 20) * 30,
      outFlowIntensity: (value) => value * 4 * 800,
      freeSpeedTime: (value) => value * 60 * 800,
      criticalSpeedTime: (value) => value * 60 * 800,
      flowDiff: (value) => Math.abs(value) * 10,
      freeSpeedDiff: (value) => Math.abs(value) * 50,
      criticalSpeedDiff: (value) => Math.abs(value) * 50,
      delayFreeSpeed: (value) => value * 10000,
      delayCriticalSpeed: (value) => value * 10000,
    },
    COLOR: {
      length: { levels: [0.2, 0.5, 1, 2], colorMap: COLORS.SINGLE_COLOR_PALETTE },
      freeSpeed: COLOR_SETS.SPEED,
      criticalSpeed: COLOR_SETS.SPEED,
      outFlow: COLOR_SETS.VOLUMES,
      inFlow: COLOR_SETS.VOLUMES,
      travelTime: COLOR_SETS.TRAVEL_TIMES,
      capacity: COLOR_SETS.VOLUMES,
      travelSpeed: COLOR_SETS.SPEED,
      outFlowIntensity: COLOR_SETS.INTENSITY,
      freeSpeedTime: COLOR_SETS.TRAVEL_TIMES,
      criticalSpeedTime: COLOR_SETS.TRAVEL_TIMES,
      flowDiff: {
        levels: [-250, -100, -20, 20, 100, 250],
        colorMap: COLORS.TRAFFIC_DIFF_PALETTE,
      },
      freeSpeedDiff: {
        levels: [-30, -15, -5, 5, 15, 30],
        colorMap: COLORS.TRAFFIC_DIFF_PALETTE,
      },
      criticalSpeedDiff: {
        levels: [-30, -15, -5, 5, 15, 30],
        colorMap: COLORS.TRAFFIC_DIFF_PALETTE,
      },
      delayFreeSpeed: COLOR_SETS.DELAY_TIMES,
      delayCriticalSpeed: COLOR_SETS.DELAY_TIMES,
    },
    TEXT: {
      length: formatFixed,
      travelTime: formatHour,
      travelSpeed: formatRound,
      outFlowIntensity: formatFixed,
      freeSpeedTime: formatHour,
      criticalSpeedTime: formatHour,
      freeSpeedDiff: formatRound,
      criticalSpeedDiff: formatRound,
      delayFreeSpeed: formatHour,
      delayCriticalSpeed: formatHour,
    },
  },
  [MP_MODEL_DTA_COMPARISON]: {
    COLOR: {
      freeSpeedDiff: { levels: [-30, -15, -5, 5, 15, 30], colorMap: COLORS.TRAFFIC_DIFF_PALETTE },
      capacityDiff: COLOR_SETS.DIFF,
      outFlowDiff: COLOR_SETS.DIFF,
      inFlowDiff: COLOR_SETS.DIFF,
      travelTimeDiff: { levels: [-0.05, -0.02, -0.005, 0.005, 0.02, 0.05], colorMap: COLORS.TRAFFIC_DIFF_PALETTE },
      travelSpeedDiff: { levels: [-30, -15, -5, 5, 15, 30], colorMap: COLORS.TRAFFIC_DIFF_PALETTE },
      outIntensityDiff: { levels: [-0.4, -0.2, -0.1, 0.1, 0.2, 0.4], colorMap: COLORS.TRAFFIC_DIFF_PALETTE },
    },
    TEXT: {
      default: (value) => value.toString(),
      travelTimeDiff: formatHour,
      travelSpeedDiff: formatRound,
      outIntensityDiff: formatFixed,
    },
  },
};
const LINK_STYLES_TEXT_DEFAULT = (value) => value.toString();

/*
  Traffic link compare styles
  Common variant and model enhanced one
*/
export function modelCompareStyle(feature, resolution, { highlight = false, context, styleType } = {}) {
  const diff = context?.diff ?? context;
  if (typeof diff !== 'number') return [];
  const { vehiclesCountDisplayed, modelLinkOffset } = styleConfig.general;
  const linkData = _getLinkData({ feature, context, resolution, highlight, vehiclesCountDisplayed }, styleType);
  const baseProps = _getCompareStyleProps(linkData, styleType);
  const isExtra = isExtraFeature(feature);
  const baseStyle = _getCommonLinkStyle(feature, resolution, {
    ...baseProps,
    ...(isExtra && { text: '' }),
    lineOffset: modelLinkOffset,
  });
  if (isExtra && vehiclesCountDisplayed && baseStyle.length)
    baseStyle.unshift(...createLineSegmentTexts(feature, baseProps.text));
  return baseStyle;
}

export function commonCompareStyle(feature, resolution, { highlight = false, context, styleType } = {}) {
  const diff = typeof context === 'number' ? context : context?.diff;
  if (typeof diff !== 'number' || Number.isNaN(diff)) return [];
  const { vehiclesCountDisplayed } = styleConfig.general;
  const hasLinkDataGetter = LINK_DATA_PROPERTIES[styleType] !== undefined;
  const linkData = hasLinkDataGetter
    ? _getLinkData({ feature, context, resolution, highlight, vehiclesCountDisplayed }, styleType)
    : { volume: diff, highlight, vehiclesCountDisplayed };
  const props = _getCompareStyleProps(linkData, styleType);
  return _getCommonLinkStyle(feature, resolution, props);
}

// TODO make backwards compatible
function _getCompareStyleProps(linkData, type) {
  const configSet = getConfigSet(type);
  const absNumber = Math.abs(linkData[configSet.width]);
  const width = absNumber > 50 ? 8 : 3;
  const color = getLinkColor(linkData, type, configSet.color);
  const text = configSet.text !== 'none' ? getLinkText(linkData, type, configSet.text) : '';
  return {
    width,
    color,
    zIndex: absNumber,
    text,
  };
}

/*
  Signs style functions
*/
export function signsStyle(f, r, { highlight = false } = {}) {
  const { type, mode, isClosed, isRestricted } = f.getProperties();
  const { trafficSignsStyle } = styleConfig.general;
  if (trafficSignsStyle && (isClosed || isRestricted)) return _getTrafficIconStyle({ isClosed, highlight });
  return _getModIconStyle({ type, mode, highlight });
}

function _getModIconStyle({ type, mode, highlight = false } = {}) {
  return [
    new Style({
      image: new Circle({
        radius: 16,
        fill: new Fill({ color: highlight ? COLORS.HIGHLIGHT : COLORS.PRIMARY }),
        ...(mode === 'existing' && { stroke: new Stroke({ color: COLORS.SELECTED, width: 3 }) }),
      }),
    }),
    new Style({
      image: new Icon({
        scale: 0.7,
        offset: [0, 0],
        src: `${import.meta.env.BASE_URL}icons/${type}.svg`,
      }),
    }),
  ];
}

const _nodeIconCache = _getNodeViewIconStyle();
function _getNodeViewIconStyle({ highlight = false } = {}) {
  const iconSrc = `${import.meta.env.BASE_URL}icons/node-${highlight ? 'active.svg' : 'map.png'}`;
  return [
    new Style({
      image: new Icon({
        scale: highlight ? 0.8 : 0.4, // TODO Different sizes in formats
        offset: [0, 0],
        src: iconSrc,
      }),
    }),
  ];
}

const generatorViwCaches = {};
const generatorViewKey = (opt) => `${opt.iconScale}`;
function getGeneratorViewStyleCached(options) {
  const base = getCachedStyle(generatorViwCaches, generatorViewKey, _getGeneratorViewIconStyle, options);
  return base;
}
function _getGeneratorViewIconStyle({ iconScale, highlight = false } = {}) {
  const iconSrc = `${import.meta.env.BASE_URL}icons/generator-${highlight ? 'active.svg' : 'map.png'}`;
  return [
    new Style({
      image: new Icon({
        scale: highlight ? iconScale : iconScale / 2, // TODO Different sizes in formats
        offset: [0, 0],
        src: iconSrc,
      }),
    }),
  ];
}

function _getTrafficIconStyle({ isClosed, highlight } = {}) {
  const icon = isClosed ? 'sign-closed' : 'sign-restricted';
  return [
    new Style({
      image: new Icon({
        scale: 0.08,
        offset: [0, 0],
        src: `${import.meta.env.BASE_URL}icons/${icon}.svg`,
        ...(highlight && { color: COLORS.HIGHLIGHT }),
      }),
    }),
  ];
}

/*
  Common traffic style helpers
  _getLinkData - Extract volume/capacity/intensity from context/feature
  _getLinkStyleProps - calculate basic link style props for standard use-case
  _getCommonLinkStyle - return base link style, generally expect props from traffic comp, which could be overriden for other use-cases
*/
function _getLinkData(initData, type = MP_MODEL_DTA_VIEWER) {
  const linkData = { ...initData }; // feature, context, addons
  return linkDataGetters[type].reduce((data, { key, getter }) => {
    data[key] = getter(data);
    return data;
  }, linkData);
}

function _getLinkStyleProps(linkData, type = MP_MODEL_DTA_VIEWER) {
  const configSet = getConfigSet(type);
  const width = getLinkWidth(linkData, type, configSet.width);
  const color = getLinkColor(linkData, type, configSet.color);
  const { highlight, volume } = linkData;
  const text = configSet.text !== 'none' ? getLinkText(linkData, type, configSet.text) : '';
  const zIndex = volume + (highlight ? 5000 : 0);
  return {
    width,
    color,
    text,
    zIndex,
  };
}

function _getCommonLinkStyle(f, r, { color, width, text = '', zIndex, lineDash, lineOffset, lineOffsetMargin }) {
  if (!width) return [];
  return [
    adjustCommonLinkStyleCached(f, r, {
      width,
      color,
      zIndex,
      text,
      lineDash,
      lineOffset,
      lineOffsetMargin,
    }),
  ];
}

/* 
  Common style props calc helpers
*/
function _widthResolutionFormula(volume, resolution) {
  return (Math.sqrt(volume) + 1) / Math.sqrt(resolution + 2);
}

function _getValueWidth(volume, resolution) {
  const calcVolume = volume < 1 ? 1 : volume;
  const res = _widthResolutionFormula(calcVolume, resolution);
  return res >= 0 ? res : 0;
}

function _getClassifiedColor({ value, levels, colorMap } = {}) {
  const level = levels.findIndex((level) => value < level);
  return colorMap.at(level);
}

function _getLineDashLength(resolution, width) {
  const resFactor = 1 / resolution + 0.5; // Adjustment to enlarge dashes in small scales (could be improved?)
  const x = Math.round(12 * resFactor);
  const y = Math.round(8 * resFactor + width);
  return [x, y];
}

/*
  Model traffic style helpers
*/
function _getModelLinkStyleProps({ feature, resolution, context, volume } = {}, { width } = {}) {
  const { isClosed, isChanged } = context;
  const isExtra = isExtraFeature(feature);
  return {
    isExtra,
    ...(isClosed && { color: COLORS.CLOSED }),
    ...(isChanged && { lineDash: _getLineDashLength(resolution, width) }),
    ...(volume === 0 && { width: 0 }),
    ...((isChanged || isClosed) && volume < 100 && { volume: 100, width: _getValueWidth(100, resolution) }),
    ...(isExtra && { text: '' }),
  };
}

function _linkSelectorStyle(feature, resolution, { selected = false, hover = false, showText = false, context } = {}) {
  if (!isValid(feature)) return [];
  const { modelLinkOffset } = styleConfig.general;
  const { capacity } = _getLinkData({ feature, context, baseCapacity: true });
  const width = (capacity / 800) * 3;
  const color = isLinkToggling(hover, selected) ? COLORS.HIGHLIGHT : COLORS.PRIMARY;
  const zIndex = parseInt(capacity, 10);
  const lineOffsetMargin = selected ? 3 : 0;
  const styles = _getCommonLinkStyle(feature, resolution, {
    resolution,
    width,
    color,
    zIndex,
    lineOffset: modelLinkOffset,
    lineOffsetMargin,
  });

  // Selection outline
  if (selected)
    styles.unshift(
      _getHighlightLinkStyle(feature, resolution, {
        color: COLORS.SELECTED,
        width: width + 6,
        zIndex: zIndex - 1,
        lineOffset: modelLinkOffset,
      }),
    );

  // Selected text
  if (showLinkSelectorText(resolution, showText)) {
    const geometry = modelLinkOffset ? _lineOffset(feature, resolution, width, { lineOffsetMargin }) : null;
    // TODO cache texts?
    styles.unshift(
      adjustPlainTextCached({
        geometry,
        text: `ID ${feature.getId()}`,
        zIndex: zIndex + 10000,
        variant: 'lSelector',
        textStyleProps: {
          placement: 'line',
          font: 'bold 12px Arial',
          fill: new Fill({ color: COLORS.SELECTED }),
          stroke: new Stroke({ color: '#000', width: 3 }),
        },
      }),
    );
  }

  return styles;
}

const isExtraFeature = (f) => typeof f.getId() === 'string';
const isValid = (f) => f.get('isValid') !== false;
const isLinkToggling = (hover, selected) => (hover && !selected) || (!hover && selected);
const showLinkSelectorText = (resolution, showText) => showText && resolution < 16;

function _linkDisabledStyle(feature, resolution, { context } = {}) {
  if (!isValid(feature)) return [];
  const { modelLinkOffset } = styleConfig.general;
  const { capacity } = _getLinkData({ feature, context });
  return _getCommonLinkStyle(feature, resolution, {
    resolution,
    width: (capacity / 800) * 3,
    color: COLORS.DISABLED,
    zIndex: parseInt(capacity, 10),
    lineOffset: modelLinkOffset,
  });
}

function _getHighlightLinkStyle(f, r, { width, zIndex, color = COLORS.HIGHLIGHT, lineOffset, lineOffsetMargin }) {
  return adjustCommonLinkStyleCached(f, r, { color, width, zIndex, lineOffset, lineOffsetMargin });
}

function createLineSegmentTexts(feature, volume) {
  const geometry = feature.getGeometry();
  const text = volume.toString();
  const styles = [];
  geometry.forEachSegment(function (start, end) {
    styles.push(
      adjustPlainTextCached({
        text,
        geometry: new LineString([start, end]),
        variant: 'lSegment',
        textStyleProps: {
          stroke: new Stroke({
            color: '#fff',
            width: 3,
          }),
          font: 'bold 12px sans-serif',
          placement: 'line',
        },
      }),
    );
  });
  return styles;
}

/*
  node neighboring link helpers
*/
function _getNLStyleProps(capacity, options) {
  return {
    volume: 10,
    width: _getNLStrokeWidth(capacity, options),
    color: _getNLStrokeColor(options),
    zIndex: capacity,
  };
}

function _getNLStrokeColor({ isTabledHighlighted, isTabled, isHovered, isDirectionLink, isSelectedLink } = {}) {
  if (isTabledHighlighted) return COLORS.HIGHLIGHT;
  if (isTabled) return COLORS.DISABLED;
  if (isSelectedLink) return isHovered ? COLORS.PRIMARY : COLORS.HIGHLIGHT;
  if (isDirectionLink) return isHovered ? COLORS.HIGHLIGHT : COLORS.PRIMARY;
}

function _getNLHighlightColor({ isTabled, isLinkFrom } = {}) {
  if (isTabled) return COLORS.HIGHLIGHT;
  return isLinkFrom ? COLORS.SOURCE : COLORS.TARGET;
}

function _getNLStrokeWidth(capacity, { isHovered, isSelectedLink } = {}) {
  const base = (capacity / 800) * 3;
  return isHovered && !isSelectedLink ? base + 6 : base;
}

function _getNLTextStyle(f, r, { fId, highlightColor, capacity, width, lineOffset, lineOffsetMargin } = {}) {
  const geometry = lineOffset ? _lineOffset(f, r, width, { lineOffsetMargin }) : null;
  return adjustPlainTextCached({
    geometry,
    text: `ID ${fId}`,
    zIndex: capacity + 1,
    variant: 'lNL',
    fillColor: highlightColor,
    textStyleProps: {
      placement: 'line',
      font: 'bold 12px Arial',
      fill: new Fill({ color: highlightColor }),
      stroke: new Stroke({ color: '#fff', width: 3 }),
    },
  });
}

/*
  Circle (node/generator) helpers
*/
function getCirclePlainStyle({ fillColor, strokeColor } = {}) {
  return new Style({
    image: new Circle({
      radius: 6,
      fill: new Fill({ color: fillColor }),
      ...(strokeColor && { stroke: new Stroke({ color: strokeColor, width: 2 }) }),
    }),
  });
}

function getCircleSelectedStyle(options) {
  return adjustCircleTextCached(options);
}

const circlePrimaryStyle = getCirclePlainStyle({ fillColor: COLORS.PRIMARY, strokeColor: COLORS.PRIMARY });
const circleHighlightStyle = getCirclePlainStyle({
  fillColor: COLORS.HIGHLIGHT,
  strokeColor: COLORS.HIGHLIGHT,
});
const circleDisabledStyle = getCirclePlainStyle({ fillColor: COLORS.DISABLED });

/* 
  Style caches
*/
function createBaseLineStyle({ color }) {
  return new Style({
    stroke: new Stroke({ color }),
    text: new Text({
      text: '',
      stroke: new Stroke({
        color: '#fff',
        width: 3,
      }),
      font: 'bold 12px sans-serif',
      placement: 'line',
    }),
  });
}

const lineActiveStyle = new Style({
  stroke: new Stroke({
    color: COLORS.PRIMARY,
    width: 5,
  }),
});

function getCachedStyle(cache, keyFn, styleFn, opt) {
  const key = keyFn(opt);
  if (!cache[key]) {
    const style = styleFn(opt);
    cache[key] = style;
  }
  return cache[key];
}

const commonLinkCaches = {};
const commonLinkKey = (opt) => `${opt.color}.${opt.hasAddon}`;
function adjustCommonLinkStyleCached(
  f,
  r,
  { color, width, zIndex, text = '', lineDash = null, lineOffset = false, lineOffsetMargin } = {},
) {
  const hasAddon = text !== '' || lineDash !== null;
  const base = getCachedStyle(commonLinkCaches, commonLinkKey, createBaseLineStyle, { color, hasAddon });
  base.setGeometry(lineOffset ? _lineOffset(f, r, width, { lineOffsetMargin }) : null);
  base.getStroke().setWidth(width);
  base.setZIndex(zIndex);
  if (hasAddon) {
    base.getStroke().setLineDash(lineDash);
    base.getText().setText(text);
  }
  return base;
}

function createPlainTextStyle({ textStyleProps } = {}) {
  return new Style({
    text: new Text({
      text: '',
      ...textStyleProps,
    }),
  });
}

const pTCaches = {};
const pTKey = ({ variant, fillColor, strokeColor }) => `${variant}.${fillColor}.${strokeColor}`;
function adjustPlainTextCached(options) {
  const base = getCachedStyle(pTCaches, pTKey, createPlainTextStyle, options);
  const { text, strokeColor, zIndex, fillColor, geometry } = options;
  base.getText().setText(text);
  if (strokeColor) base.getText().getStroke().setColor(strokeColor);
  if (zIndex) base.setZIndex(zIndex);
  if (fillColor) base.getText().getFill().setColor(fillColor);
  if (geometry) {
    // Geo set doesnt seem to work with caching
    const geoClone = base.clone();
    geoClone.setGeometry(geometry);
    return geoClone;
  }
  return base;
}

function createCircleTextStyle({
  mainColor = COLORS.HIGHLIGHT,
  textStrokeColor = '#fff',
  fillColor = COLORS.HIGHLIGHT,
  hasLabel = false,
} = {}) {
  return new Style({
    image: new Circle({
      radius: 10,
      fill: new Fill({ color: fillColor }),
      stroke: new Stroke({ color: mainColor, width: 4 }),
    }),
    ...(hasLabel && {
      text: new Text({
        text: '',
        textAlign: 'right',
        textBaseline: 'top',
        offsetX: -15,
        font: 'bold 14px Arial',
        fill: new Fill({ color: mainColor }),
        stroke: new Stroke({ color: textStrokeColor, width: 2 }),
      }),
    }),
  });
}

const cTCaches = {};
const cTKey = ({ mainColor, fillColor, textStrokeColor, hasLabel }) =>
  `${mainColor}.${fillColor}.${textStrokeColor}.${hasLabel}`;
function adjustCircleTextCached(options) {
  const { label } = options;
  const hasLabel = label && label !== '';
  const base = getCachedStyle(cTCaches, cTKey, createCircleTextStyle, { ...options, hasLabel });
  if (hasLabel) base.getText().setText(label);
  return base;
}

/*
  LineOffset solution inspired by 
  SO comment + mathJS https://stackoverflow.com/questions/57421223/openlayers-3-offset-stroke-style
  Turf JS lineOffset https://github.com/Turfjs/turf/tree/master/packages/turf-line-offset
  Turf JS parts are simpler as its intersect is less complex and doesnt duplicate coords
  SO parts are used to skip transform into TURF 4326 projection + degrees
  Final solution should be fastest but might skip some edge cases
*/
function _lineOffset(feature, resolution, width, { lineOffsetMargin = 0 } = {}) {
  /*
    width - map units/pixels on screen
    resolution - how many meters (in map projection/EPSG 3857) is one map unit/pixel 
    dist - should be half of line (+ optional extra bit for gap between lines)
  */
  const dist = (width / 1.8 + lineOffsetMargin) * resolution;
  const geom = feature.getGeometry();
  // TODO add multilinestring support ?
  if (typeof geom.forEachSegment !== 'function') {
    // is multiLineString
    const outLineStrings = geom.getLineStrings().map((ls) => processLineString(ls, dist));
    return new MultiLineString(outLineStrings);
  }

  return processLineString(geom, dist);
}

function processLineString(geom, dist) {
  const srcCoords = geom.getCoordinates();
  const outCoords = [];

  if (srcCoords.length === 2) {
    const newCoords = processSegment(srcCoords[0], srcCoords[1], dist);
    outCoords.push(...newCoords);
  } else {
    const segments = [];
    for (let i = 0; i < srcCoords.length - 1; i++) {
      const newCoords = processSegment(srcCoords[i], srcCoords[i + 1], dist);
      segments.push(newCoords);
      if (i > 0) {
        const prevCoords = segments[i - 1];
        const intersection = intersect(prevCoords, newCoords);

        if (intersection) {
          prevCoords[1] = intersection;
          newCoords[0] = intersection;
        }

        outCoords.push(prevCoords[0]);
        if (i === srcCoords.length - 2) {
          outCoords.push(newCoords[0]);
          outCoords.push(newCoords[1]);
        }
      }
    }
  }
  return new LineString(outCoords);
}

function processSegment(from, to, dist) {
  const angle = Math.atan2(to[1] - from[1], to[0] - from[0]);
  const newFrom = [Math.sin(angle) * dist + from[0], -Math.cos(angle) * dist + from[1]];
  const newTo = [Math.sin(angle) * dist + to[0], -Math.cos(angle) * dist + to[1]];
  return [newFrom, newTo];
}

/**
 * Intersection helpers by
 * https://github.com/rook2pawn/node-intersection
 *
 * Author @rook2pawn
 */

/**
 * AB
 *
 * @private
 * @param {Array<Array<number>>} segment - 2 vertex line segment
 * @returns {Array<number>} coordinates [x, y]
 */
function ab(segment) {
  var start = segment[0];
  var end = segment[1];
  return [end[0] - start[0], end[1] - start[1]];
}

/**
 * Cross Product
 *
 * @private
 * @param {Array<number>} v1 coordinates [x, y]
 * @param {Array<number>} v2 coordinates [x, y]
 * @returns {Array<number>} Cross Product
 */
function crossProduct(v1, v2) {
  return v1[0] * v2[1] - v2[0] * v1[1];
}

/**
 * Add
 *
 * @private
 * @param {Array<number>} v1 coordinates [x, y]
 * @param {Array<number>} v2 coordinates [x, y]
 * @returns {Array<number>} Add
 */
function add(v1, v2) {
  return [v1[0] + v2[0], v1[1] + v2[1]];
}

/**
 * Sub
 *
 * @private
 * @param {Array<number>} v1 coordinates [x, y]
 * @param {Array<number>} v2 coordinates [x, y]
 * @returns {Array<number>} Sub
 */
function sub(v1, v2) {
  return [v1[0] - v2[0], v1[1] - v2[1]];
}

/**
 * scalarMult
 *
 * @private
 * @param {number} s scalar
 * @param {Array<number>} v coordinates [x, y]
 * @returns {Array<number>} scalarMult
 */
function scalarMult(s, v) {
  return [s * v[0], s * v[1]];
}

/**
 * Intersect Segments
 *
 * @private
 * @param {Array<number>} a coordinates [x, y]
 * @param {Array<number>} b coordinates [x, y]
 * @returns {Array<number>} intersection
 */
function intersectSegments(a, b) {
  var p = a[0];
  var r = ab(a);
  var q = b[0];
  var s = ab(b);

  var cross = crossProduct(r, s);
  var qmp = sub(q, p);
  var numerator = crossProduct(qmp, s);
  var t = numerator / cross;
  var intersection = add(p, scalarMult(t, r));
  return intersection;
}

/**
 * Is Parallel
 *
 * @private
 * @param {Array<number>} a coordinates [x, y]
 * @param {Array<number>} b coordinates [x, y]
 * @returns {boolean} true if a and b are parallel (or co-linear)
 */
function isParallel(a, b) {
  var r = ab(a);
  var s = ab(b);
  return crossProduct(r, s) === 0;
}

/**
 * Intersection
 *
 * @private
 * @param {Array<number>} a coordinates [x, y]
 * @param {Array<number>} b coordinates [x, y]
 * @returns {Array<number>|boolean} true if a and b are parallel (or co-linear)
 */
function intersect(a, b) {
  if (isParallel(a, b)) return false;
  return intersectSegments(a, b);
}
