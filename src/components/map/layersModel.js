import { defineLayer, vectorLayer, vectorFeaturesPlugin } from 'vuemap';
import useModelSigns from '@composables/useModelSigns';
import { LID_SIGNS, LID_MODIFICATIONS } from '@keys/index';

export const useModelSignsLayer = defineLayer(LID_SIGNS, modelSignsSetup);
function modelSignsSetup() {
  const { signFeatures } = useModelSigns();

  return {
    layerType: vectorLayer,
    lazyLoad: true,
    layerOptions: {
      className: 'signs-layer',
    },
    sourceOptions: {
      type: 'Vector',
    },
    plugins: [[vectorFeaturesPlugin, { featuresVModel: signFeatures }]],
  };
}

export const useModificationLayer = defineLayer(LID_MODIFICATIONS, () => ({
  layerType: vectorLayer,
  lazyLoad: true,
  sourceOptions: {
    type: 'Vector',
  },
  plugins: [vectorFeaturesPlugin],
}));
