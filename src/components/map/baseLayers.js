import { defineLayer, tileLayer } from 'vuemap';

export const useOSM = defineLayer('OSM', {
  layerType: tileLayer,
  layerOptions: { className: 'tm-layer-pastel' },
  sourceOptions: { type: 'OSM', url: import.meta.env.VITE_API_OSM },
});

export const useOSMStandard = defineLayer('OSM-standard', {
  layerType: tileLayer,
  sourceOptions: { type: 'OSM', url: import.meta.env.VITE_API_OSM },
});
