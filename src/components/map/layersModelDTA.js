import { reactive, markRaw, computed } from 'vue';
import { defineLayer, vectorTileLocalLayer, layerContextPlugin } from 'vuemap';
import useLayerSourceData from '@composables/useLayerSourceData';
import useTrafficData from '@composables/useTrafficData';
import { createLineStringFeature, createPointFeature } from './layerCommonUtils';
import { LID_DTA_LINKS, LID_DTA_NODES, LID_DTA_GENERATORS, MP_MODEL_DTA_VIEWER } from '@keys/index';
import getWorkerMessenger from './tileWorker';

export const useDTALinksLayer = defineLayer(LID_DTA_LINKS, dtaLinksSetup);
function dtaLinksSetup() {
  const { fetchSourceData, sourceData, extraFeaturesData } = useLayerSourceData(LID_DTA_LINKS, { autoFetch: false });
  const extraFeatures = computed(() =>
    extraFeaturesData.value.map((featureData) =>
      markRaw(createLineStringFeature(featureData, { idProperty: 'edge_id' })),
    ),
  );
  const { fetchTrafficData } = useTrafficData(MP_MODEL_DTA_VIEWER);

  const sourceOptions = reactive({
    dataProjection: 'EPSG:3857',
    promoteId: 'edge_id',
  });

  return {
    layerType: vectorTileLocalLayer,
    lazyLoad: true,
    sourceOptions,
    typeOptions: {
      extraFeatures,
      workerMessenger: getWorkerMessenger(),
    },
    plugins: [layerContextPlugin],
    preLoadFn: async () => {
      await Promise.all([await fetchSourceData(), await fetchTrafficData()]);
      sourceOptions.data = sourceData.value;
      console.log('model dta source data loaded (links)');
    },
  };
}

export const useDTANodesLayer = defineLayer(LID_DTA_NODES, dtaNodesSetup);
function dtaNodesSetup() {
  const { fetchSourceData, sourceData, extraFeaturesData } = useLayerSourceData(LID_DTA_NODES, { autoFetch: false });
  const extraFeatures = computed(() =>
    extraFeaturesData.value.map((featureData) => markRaw(createPointFeature(featureData, { idProperty: 'node_id' }))),
  );
  const sourceOptions = reactive({
    dataProjection: 'EPSG:3857',
    promoteId: 'node_id',
  });

  return {
    layerType: vectorTileLocalLayer,
    lazyLoad: true,
    layerOptions: {
      className: 'model-points',
      zIndex: 11,
      declutter: false,
    },
    sourceOptions,
    typeOptions: {
      extraFeatures,
      workerMessenger: getWorkerMessenger(),
    },
    plugins: [layerContextPlugin],
    preLoadFn: async () => {
      await fetchSourceData();
      sourceOptions.data = sourceData.value;
      console.log('model dta source data loaded (nodes)');
    },
  };
}

export const useDTAGeneratorsLayer = defineLayer(LID_DTA_GENERATORS, dtaGeneratorsSetup);
function dtaGeneratorsSetup() {
  const { fetchSourceData, sourceData, extraFeaturesData } = useLayerSourceData(LID_DTA_GENERATORS, {
    autoFetch: false,
  });
  const extraFeatures = computed(() =>
    extraFeaturesData.value.map((featureData) => markRaw(createPointFeature(featureData, { idProperty: 'zone_id' }))),
  );
  const sourceOptions = reactive({
    dataProjection: 'EPSG:3857',
    promoteId: 'zone_id',
  });

  return {
    layerType: vectorTileLocalLayer,
    lazyLoad: true,
    layerOptions: {
      className: 'model-points',
      zIndex: 12,
      declutter: false,
    },
    sourceOptions,
    typeOptions: {
      extraFeatures,
      workerMessenger: getWorkerMessenger(),
    },
    plugins: [layerContextPlugin],
    preLoadFn: async () => {
      await fetchSourceData();
      sourceOptions.data = sourceData.value;
      console.log('model dta source data loaded (generators)');
    },
  };
}
