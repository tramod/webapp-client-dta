import { createPointFeature } from './layerCommonUtils';

// utility to get full vector feature instead of tiled one (might be simplified, or cropped)
export function createFeatureFromId(featureId, fetchFeatureFunction) {
  // Get data from our pre-tile cache, if server tiles will be used we need to query model server here
  const featureData = fetchFeatureFunction(featureId);
  return createPointFeature(featureData, { idProperty: 'zone_id' });
}
