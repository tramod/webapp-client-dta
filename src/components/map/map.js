import { defaults as interactions } from 'ol/interaction';
import { defineMap, mapManagerPlugin } from 'vuemap';
import { useStore } from 'vuex';
import { MAP_ID } from '@keys/index';

function mainMapSetup() {
  const store = useStore();
  const zoom = store.state.map.zoom || import.meta.env.VITE_DEFAULT_MAP_ZOOM || 0;
  const center = store.state.map.center ||
    import.meta.env.VITE_DEFAULT_MAP_CENTER?.split(', ').map((value) => parseFloat(value)) || [0, 0];

  return {
    mapOptions: {
      interactions: interactions({ altShiftDragRotate: false, pinchRotate: false }),
      controls: [], // display no controls
    },
    viewOptions: { zoom, center },
    plugins: [mapManagerPlugin],
  };
}

export const useMainMap = defineMap(MAP_ID, mainMapSetup);
