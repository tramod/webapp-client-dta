import { WorkerMessenger } from 'vuemap';

let messenger;

export default function getWorkerMessenger() {
  if (messenger) return messenger;

  const workerMessenger = createWorker();
  messenger = workerMessenger;

  return workerMessenger;
}

function createWorker() {
  // TODO test alternative usages (load from node_modules/src, non-minified es script) with next Vite version
  const worker = new Worker(`${import.meta.env.BASE_URL}scripts/tileWorker.e571122d.js`, { type: 'classic' });
  // Log error from worker, will be caught and logged in all layers
  worker.addEventListener(
    'error',
    (event) => {
      console.log(event.message);
    },
    false,
  );

  return new WorkerMessenger(worker);
}
