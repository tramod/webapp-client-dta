import LineString from 'ol/geom/LineString';
import MultiLineString from 'ol/geom/MultiLineString';
import Point from 'ol/geom/Point';
import Feature from 'ol/Feature';

const LineConstructors = {
  LineString,
  MultiLineString,
};

export function createLineStringFeature(jsonData, { dataProjection, idProperty = null } = {}) {
  if (!jsonData) return null;
  const GeometryConstructor = LineConstructors[jsonData.geometry.type] ?? LineConstructors.LineString;
  const lineString = new GeometryConstructor(jsonData.geometry.coordinates);
  if (dataProjection && dataProjection !== 'EPSG:3857') lineString.transform(dataProjection, 'EPSG:3857');
  const feature = new Feature({
    geometry: lineString,
    ...jsonData.properties,
  });
  if (idProperty) feature.setId(jsonData.properties[idProperty]);
  return feature;
}

export function createPointFeature(jsonData, { dataProjection, idProperty = null } = {}) {
  if (!jsonData) return null;
  const point = new Point(jsonData.geometry.coordinates);
  if (dataProjection && dataProjection !== 'EPSG:3857') point.transform(dataProjection, 'EPSG:3857');
  const feature = new Feature({
    geometry: point,
    ...jsonData.properties,
  });
  if (idProperty) feature.setId(jsonData.properties[idProperty]);
  return feature;
}
