<template>
  <tm-card>
    <template #header>
      <tm-heading :text="$t('widgets.comparison widget')" widget-name="comparison" />
      <tm-divider />
    </template>

    <template #content>
      <tm-selection
        v-if="comparisonModeOptions.length"
        v-model:selected="selectedComparisonMode"
        :options="comparisonModeOptions"
        option-value="name"
        type="radio"
        class="p-mb-3"
      />

      <template v-if="mapMode === MP_MODEL_DTA_COMPARISON">
        <div class="p-d-flex p-ai-center p-jc-between">
          <span>{{ $t('widgets.compared output') }}</span>
          <tm-selection
            v-model:selected="dtaModelOutput"
            :options="dtaComparisonOutputOptions"
            class="switcher"
            type="dropdown"
          />
        </div>
        <tm-divider class="p-mb-3" />
      </template>

      <template v-if="['model', 'combined'].includes(selectedComparisonMode)">
        <tm-selection
          v-model:selected="sourceScenario"
          :label="$t('scenarios.scenario')"
          :options="$isMobile() ? availableScenarios : filteredScenarios"
          :option-value="$isMobile() ? null : 'name'"
          :placeholder="$t('widgets.pick scenario')"
          class="p-mb-2"
          :type="$isMobile() ? 'dropdown' : 'autocomplete-dropdown'"
          @autocomplete="searchForScenario"
          @update:selected="setComparedScenarios()"
        />
      </template>

      <tm-calendar
        v-model:selected="sourceScenarioDate"
        v-tooltip="$t('modifications.value defined by model')"
        :disabled-date="true"
        :time-options="getTimeOptions()"
        :popup="true"
        :timeslider="true"
        @update:selected="endPlayMode()"
      />

      <tm-divider class="p-mb-3" />

      <template v-if="selectedComparisonMode === 'model'">
        <tm-selection
          v-model:selected="targetScenario"
          :label="$t('scenarios.scenario')"
          :options="$isMobile() ? availableScenarios : filteredScenarios"
          :option-value="$isMobile() ? null : 'name'"
          :placeholder="$t('widgets.pick scenario')"
          class="p-mb-2"
          :type="$isMobile() ? 'dropdown' : 'autocomplete-dropdown'"
          @autocomplete="searchForScenario"
          @update:selected="setComparedScenarios()"
        />
      </template>

      <tm-calendar
        v-model:selected="targetScenarioDate"
        v-tooltip="$t('modifications.value defined by model')"
        :disabled-date="true"
        :disabled="isLocked"
        :time-options="getTimeOptions()"
        :popup="true"
        :timeslider="true"
        @update:selected="endPlayMode()"
      />

      <tm-divider class="p-mb-3" />

      <div class="p-d-flex p-jc-center p-mb-2">
        <tm-button
          type="create"
          :tooltip="$t(`widgets.${isLocked ? 'unlock' : 'lock'} calendar`)"
          :icon="isLocked ? 'ri-lock-line' : 'ri-lock-unlock-line'"
          @click="isLocked ? unlockCalendar() : lockCalendar()"
        />

        <tm-button
          class="p-ml-3"
          type="create"
          :tooltip="$t(`widgets.${isPlayModeActive ? 'stop' : 'start'} calendar play mode`)"
          :icon="isPlayModeActive ? 'ri-stop-fill' : 'ri-play-fill'"
          @click="isPlayModeActive ? endPlayMode() : startPlayMode()"
        />
      </div>
    </template>
  </tm-card>
</template>

<script setup lang="ts">
import { ref, computed, watch, toRef } from 'vue';
import { useStore } from 'vuex';
import { useI18n } from 'vue-i18n';
import useFullScenario from '@composables/useFullScenario';
import useFilters from '@composables/useFilters';
import useModels, { dtaComparisonOutputOptions } from '@composables/useModels';
import usePlayMode from '@composables/usePlayMode';
import { MP_MODEL_DTA_COMPARISON } from '@keys/index';

type ComparedScenario = Omit<TmScenario, 'id'> & { id: 'base' | number };

const props = defineProps<{
  mapMode: TmMapMode; // Currently active map mode
}>();

const store = useStore();
const { t } = useI18n();
const {
  scenariosOverview: scenarios,
  fetchScenariosOverview: fetchScenarios,
  activeScenario,
  activeScenarioId,
} = useFullScenario();
const { getModelCalendarSetting } = useModels();
const { isPlayModeActive, startPlayMode, endPlayMode } = usePlayMode(toRef(props, 'mapMode'));

const baseScenario: ComparedScenario = {
  id: 'base',
  name: t('widgets.base model'),
  description: 'base, basic, základní, zakladni, model',
};

const isLocked = ref(false);

const dtaModelOutput = computed<string>({
  get: () => store.state.map.mapStyle[MP_MODEL_DTA_COMPARISON].color,
  set: (newState) => {
    store.commit('map/SET_MAP_STYLE', { property: 'color', group: MP_MODEL_DTA_COMPARISON, newState });
    store.commit('map/SET_MAP_STYLE', { property: 'text', group: MP_MODEL_DTA_COMPARISON, newState });
  },
});

const sourceScenarioDate = computed<TmDate>({
  get: () => store.getters['map/getCalendarDate']({ startDateOnly: true }),
  set: (tmDate) => {
    const sourceDate = tmDate;
    const targetDate = isLocked.value ? tmDate : targetScenarioDate.value;
    store.dispatch('map/setCalendarDate', { tmDate: [sourceDate, targetDate] });
  },
});

const targetScenarioDate = computed<TmDate>({
  get: () => store.getters['map/getCalendarDate']({ endDateOnly: true }),
  set: (tmDate) => store.dispatch('map/setCalendarDate', { tmDate: [sourceScenarioDate.value, tmDate] }),
});

const { filteredItems: sortedScenarios } = useFilters<TmScenario>({
  items: scenarios,
  cacheKey: 'scenarios',
});

const availableScenarios = computed<ComparedScenario[]>(() => {
  const displayedScenario =
    activeScenario.value.id && !sortedScenarios.value.find((sc: TmScenario) => sc.id == activeScenario.value.id)
      ? [activeScenario.value]
      : [];

  return [baseScenario, ...displayedScenario, ...sortedScenarios.value] as ComparedScenario[];
});

const sourceScenario = ref<ComparedScenario>();
const targetScenario = ref<ComparedScenario>();
const filteredScenarios = ref<ComparedScenario[]>([]);

const setDefaultScenarios = () => {
  const sourceScId = store.state.map.comparison?.[0] || activeScenarioId.value || baseScenario.id;
  sourceScenario.value = availableScenarios.value.find((sc) => sc.id == sourceScId) || baseScenario;

  const targetScId = store.state.map.comparison?.[1] || baseScenario.id;
  targetScenario.value = availableScenarios.value.find((sc) => sc.id == targetScId) || baseScenario;
};

const searchForScenario = (query: string) => {
  const filtered = availableScenarios.value.filter((el) => el.name?.includes(query) || el.description?.includes(query));
  filteredScenarios.value = filtered || [];
};

const setComparedScenarios = () => {
  if (!sourceScenario.value?.id || !targetScenario.value?.id) return;

  store.dispatch('map/setComparedScenarios', {
    scenarios: [sourceScenario.value.id, targetScenario.value.id],
  });
};

const comparisonModeOptions = computed(() => {
  const modeOptions = [
    {
      name: 'model',
      label: 'widgets.scenarios comparison',
      mapMode: MP_MODEL_DTA_COMPARISON,
    },
  ];
  return modeOptions;
});

const selectedComparisonMode = computed({
  get: () => {
    const opt = comparisonModeOptions.value.find((o) => o.mapMode === props.mapMode);
    return opt ? opt.name : 'model';
  },
  set: (name) => {
    const opt = comparisonModeOptions.value.find((o) => o.name === name);
    if (opt) store.dispatch('map/setMapMode', { mode: opt.mapMode });
  },
});

const isLoggedUser = computed<boolean>(() => store.getters['auth/isLogged']);

const getTimeOptions = () => getModelCalendarSetting();

const unlockCalendar = () => (isLocked.value = false);

const lockCalendar = () => {
  const commonDate = sourceScenarioDate.value;
  store.dispatch('map/setCalendarDate', {
    tmDate: [commonDate, commonDate],
  });
  isLocked.value = true;
};

watch(
  [() => props.mapMode, isLoggedUser],
  async () => {
    isLocked.value = false;
    await fetchScenarios();
    setDefaultScenarios();
    setComparedScenarios();
  },
  { immediate: true },
);
</script>

<style scoped>
::v-deep(.tm-slider),
::v-deep(.timeslider) {
  margin-top: 0.5rem !important;
}
.switcher {
  max-width: 50%;
}
</style>
