const year = new Date().getFullYear();
const month = new Date().getMonth();
const day = new Date().getDate();
const minZoomRange = 172800000000; // cca 6 years range
const maxZoomRange = 21600000; // cca 12 hours range
const maxDate = new Date(year + 10, month, day); // end date of infinite event
const minDate = new Date(year - 10, month, day); // start date of infinite event
const defaultMaxDate = new Date(year + 1, month, day); // default timeline range end (in case all events are infinite)
const defaultMinDate = new Date(year - 1, month, day); // default timeline range start (in case all events are infinite)
const eventHeight = 20;
const eventSpacing = 2;
const minEventWidth = 1;
const eventColor = '#0094d2';
const lineColor = '#dee2e6';
const axisTickColor = '#0094d2';
const chartMargin = {
  top: 0,
  bottom: 30,
  left: 15,
  right: 15,
};

const getEventStart = (e: TmTimelineEvent, min = minDate) => {
  const start = e.dateFrom;
  if (!start && min) return min;
  if (!start && !min) return null;
  const startDate = new Date(start as string);
  if (!min) return startDate;
  if (min > startDate) return min;
  return startDate;
};

const getEventEnd = (e: TmTimelineEvent, max = maxDate) => {
  const end = e.dateTo;
  if (!end && max) return max;
  if (!end && !max) return null;
  const endDate = new Date(end as string);
  if (!max) return endDate;
  if (max < endDate) return max;
  return endDate;
};

export {
  defaultMaxDate,
  defaultMinDate,
  eventHeight,
  eventSpacing,
  minEventWidth,
  chartMargin,
  getEventStart,
  getEventEnd,
  eventColor,
  lineColor,
  axisTickColor,
  minZoomRange,
  maxZoomRange,
};
