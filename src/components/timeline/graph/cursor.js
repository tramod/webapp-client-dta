import { lineColor } from './../config';

export default ({ timeScale, height }) =>
  (selection) => {
    const now = timeScale(Date.now());
    const cursor = selection.selectAll('.cursor').data((d) => d);
    const g = cursor.enter().append('g').classed('cursor', true).attr('transform', `translate(${now})`);

    g.append('line').attr('x1', 0).attr('y1', 0).attr('x2', 0).attr('y2', height).attr('stroke', lineColor);
    cursor.attr('transform', `translate(${now})`);
  };
