import { set } from 'lodash-es';
import { getEventStart, getEventEnd } from './../config';

const intersects = (event1, event2) => {
  const s1 = getEventStart(event1);
  const e1 = getEventEnd(event1);
  const s2 = getEventStart(event2);
  const e2 = getEventEnd(event2);
  if ((s2 < s1 && s1 < e2) || (s1 < s2 && s2 < e1) || s1.getTime() == s2.getTime() || e1.getTime() == e2.getTime())
    return true;
  return false;
};

export default {
  generate(data) {
    data.forEach((e) => (e.duration = getEventEnd(e) - getEventStart(e)));
    data.sort((a, b) => b.duration - a.duration);

    const placedEvents = {};
    let maxLevel = 1;

    data.forEach((e) => {
      e.level = 0;
      for (const [level, eventsOnThisLevel] of Object.entries(placedEvents)) {
        e.level = parseInt(level);
        let intersectsWithLevel = false;
        for (const [, e2] of Object.entries(eventsOnThisLevel)) {
          if (intersects(e, e2)) {
            e.level++;
            intersectsWithLevel = true;
            break;
          }
        }
        if (!intersectsWithLevel) break;
      }
      if (e.level >= maxLevel) maxLevel = e.level + 1;
      set(placedEvents, `${e.level}.${e.id}`, e);
    });

    data.forEach((e) => (e.position = maxLevel - e.level));
  },
};
