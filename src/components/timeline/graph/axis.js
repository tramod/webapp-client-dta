import * as d3 from 'd3';
import { axisTickColor, lineColor } from './../config';

const format = (date) => {
  if (d3.timeDay(date) < date) return d3.timeFormat('%I:%M')(date);
  if (d3.timeMonth(date) < date) return d3.timeFormat('%b %d')(date);
  if (d3.timeYear(date) < date) return d3.timeFormat('%B')(date);
  return d3.timeFormat('%Y')(date);
};

export default ({ timeScale, height }) =>
  (selection) => {
    const axe = selection.selectAll('.axe').data((d) => d);

    const ay = d3
      .axisBottom()
      .scale(timeScale)
      .tickSize(0)
      .tickPadding(15)
      .tickFormat((d) => format(d));

    const updateTicks = (axis) => {
      const path = axis.select('path');
      path.style('stroke', lineColor);
      path.style('stroke-width', 2);
      const ticks = axis.selectAll('.tick');
      ticks.selectAll('line').remove(); // line is not visible thanks to tickSize above, but we can remove it anyway
      ticks.each(function () {
        d3.select(this).append('circle').attr('r', 5).attr('stroke', axisTickColor).attr('fill', '#FFFFFF');
      });
    };

    axe
      .enter()
      .append('g')
      .attr('transform', `translate(0, ${height + 5})`)
      .classed('axe', true)
      .call(ay)
      .call(updateTicks);

    // when zooming -> this triggers
    axe.call(ay).call(updateTicks);
  };
