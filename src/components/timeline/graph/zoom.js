import * as d3 from 'd3';
import { minZoomRange, maxZoomRange } from './../config';

export default ({ timeScale, view, draw, defaultTimelineDuration }) => {
  const minZoom = defaultTimelineDuration / minZoomRange;
  const maxZoom = defaultTimelineDuration / maxZoomRange;

  return (
    d3
      .zoom()
      .scaleExtent([minZoom, maxZoom])
      // .translateExtent([[ -1200, -700 ], [ 800, 800 ]])
      .on('zoom', () => {
        const { k, x, y } = d3.event.transform;
        const scale = d3.zoomIdentity.translate(x, y).scale(k).rescaleX(timeScale);
        view.call(draw(scale));
      })
  );
};
