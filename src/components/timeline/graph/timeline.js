import * as d3 from 'd3';
import events from './events';
import axis from './axis';
import zoom from './zoom';
import cursor from './cursor';
import layout from './layout';
import d3Locales from '@locales/plugins/d3';
import { defaultLocale } from '@plugins/i18n';
import {
  chartMargin,
  eventHeight,
  eventSpacing,
  getEventStart,
  getEventEnd,
  defaultMinDate,
  defaultMaxDate,
} from './../config';

let redraw;

export const setLocale = (lang) => {
  d3.timeFormatDefaultLocale(d3Locales[lang || defaultLocale]);
  redraw();
};

export const redrawTimeline = () => {
  redraw();
};

const init = (onEventClick, selection) => {
  selection.selectAll('svg').remove();

  const data = selection.data();
  const events = data[0];
  layout.generate(events);
  // console.table(events);
  const eventRows = d3.max(events.map((e) => e.level));
  const width = (window.innerWidth - chartMargin.right - chartMargin.left) * 0.8;
  const height = eventRows * (eventHeight + eventSpacing) + 60;
  const defaultTimelineStart = d3.min(events.map((e) => getEventStart(e, null))) || defaultMinDate;
  const defaultTimelineEnd = d3.max(events.map((e) => getEventEnd(e, null))) || defaultMaxDate;
  const defaultTimelineDuration = defaultTimelineEnd - defaultTimelineStart;

  const svg = selection
    .append('svg')
    .datum(data)
    .attr('width', width + chartMargin.right + chartMargin.left)
    .attr('height', height + chartMargin.top + chartMargin.bottom);
  const timeScale = d3.scaleTime().domain([defaultTimelineStart, defaultTimelineEnd]).range([0, width]);
  const graph = svg
    .append('g')
    .classed('graph', true)
    .attr('transform', `translate(${chartMargin.left},${chartMargin.top})`);
  const view = graph.append('g').classed('view', true);

  svg
    .call(zoom({ timeScale, view, draw, defaultTimelineDuration }))
    .on('mousewheel.zoom', (ev) => ev.preventDefault())
    .on('mousewheel', (ev) => ev.preventDefault());
  view.call(draw(timeScale, onEventClick, height));
};

const draw = (timeScale, onEventClick, height) => {
  return (selection) => {
    selection
      .data(selection.data())
      .call(events({ timeScale, onEventClick }))
      .call(axis({ timeScale, height }))
      .call(cursor({ timeScale, height }));
  };
};

export default ({ onEventClick, lang }) =>
  (selection) => {
    redraw = () => init(onEventClick, selection);
    setLocale(lang);
    redraw();
  };
