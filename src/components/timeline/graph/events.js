import {
  getEventStart,
  getEventEnd,
  eventHeight,
  eventSpacing,
  minEventWidth,
  chartMargin,
  eventColor,
} from './../config';

const getWidth =
  (timeScale, innerObject = false) =>
  (e) => {
    const substract = innerObject ? getNegativeX(timeScale)(e) : 0;
    const width = timeScale(getEventEnd(e)) - timeScale(getEventStart(e)) - eventSpacing - substract;
    return Math.max(width, minEventWidth);
  };

const getTransform = (timeScale) => (e) =>
  `translate(${timeScale(getEventStart(e))} ${e.position * (eventHeight + eventSpacing)})`;

const getNegativeX = (timeScale) => (e) => {
  const start = timeScale(getEventStart(e));
  const end = timeScale(getEventEnd(e));
  return start < 0 && end > 0 ? Math.abs(start) - chartMargin.left : 0;
};

export default ({ timeScale, onEventClick }) =>
  (selection) => {
    const events = selection.selectAll('g.event').data(selection.data()[0][0]);

    const g = events
      .enter()
      .append('g')
      .classed('event', true)
      .attr('transform', getTransform(timeScale))
      .style('cursor', 'pointer')
      .on('click', onEventClick);

    g.append('rect')
      .attr('width', getWidth(timeScale))
      .attr('height', eventHeight)
      .attr('fill', eventColor)
      .attr('ry', 7); // rounded corners

    g.append('foreignObject')
      .attr('width', getWidth(timeScale, true))
      .attr('height', eventHeight)
      .attr('x', getNegativeX(timeScale))
      .attr('class', 'event')
      .append('xhtml:span')
      .attr('class', 'event-text')
      .attr('data-test', (d) => d.id)
      .text((d) => d.name);

    // when zooming -> this triggers and updates disaplyed event RECT
    events.attr('transform', getTransform(timeScale)).selectAll('rect').attr('width', getWidth(timeScale));
    // when zooming -> this triggers and updates displayed event TEXT (as fireignObject)
    events.selectAll('foreignObject').attr('width', getWidth(timeScale, true)).attr('x', getNegativeX(timeScale));

    // events.exit().on('click', null).remove();
  };
