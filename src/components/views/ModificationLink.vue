<template>
  <div :class="{ 'p-disabled': disabled }">
    <map-interaction-wrapper
      :interaction="interaction"
      v-bind="interactionProps"
      :disabled="disabled"
      @[newModeEventKey]="onPropUpdate"
      @[existingModeEventKey]="updateModificationAndPresets($event)"
    />

    <tm-divider />

    <tm-date-range
      v-model:from="linkModification.dateFrom"
      v-model:to="linkModification.dateTo"
      v-tooltip="$t('modifications.value defined by model')"
      :disabled="true"
      class="p-mb-2"
    />

    <tm-selection
      v-model:selected="selectedLinkPreset"
      :options="linkPresets"
      option-value="name"
      type="radio"
      class="p-mt-3"
      @update:selected="applyLinkPreset"
    />

    <tm-slider
      v-model:selected="linkModification.speed"
      :label="$t('modifications.speed')"
      :range="{ min: 0, max: 150 }"
      :placeholder="$t('modifications.original')"
      :direct-input="true"
      suffix="km/h"
    />
    <tm-slider
      v-model:selected="linkModification.capacity"
      :label="$t('modifications.capacity')"
      :range="{ min: 0, max: 5000 }"
      :placeholder="$t('modifications.original')"
      :direct-input="true"
      suffix="v/h"
    />

    <tm-input
      v-model="linkModification.lanes"
      name="lanes"
      type="number"
      :label="$t('modifications.lanes')"
      :borderless="false"
      :min="1"
      :max="6"
      :placeholder="$t('modifications.original')"
    />

    <div v-if="modMode === 'new'">
      <tm-selection
        v-model:selected="linkModification.twoWay"
        :options="[
          { isTwoWay: false, label: 'modifications.one way' },
          { isTwoWay: true, label: 'modifications.two way' },
        ]"
        option-value="isTwoWay"
        type="radio"
        class="p-mt-3"
      />

      <div v-if="linkModification.twoWay">
        <tm-slider
          v-model:selected="linkModification.twoWaySpeed"
          :label="$t('modifications.speed')"
          :range="{ min: 0, max: 150 }"
          :direct-input="true"
          suffix="km/h"
        />
        <tm-slider
          v-model:selected="linkModification.twoWayCapacity"
          :label="$t('modifications.capacity')"
          :range="{ min: 0, max: 5000 }"
          :direct-input="true"
          suffix="v/h"
        />
        <tm-input
          v-model="linkModification.twoWayLanes"
          name="two-way-lanes"
          type="number"
          :label="$t('modifications.lanes')"
          :borderless="false"
          :min="1"
          :max="6"
        />
      </div>
    </div>
  </div>
</template>

<script>
import { ref, computed, shallowReactive } from 'vue';
import MapInteractionWrapper from '@components/map/MapInteractionWrapper.vue';
import useDate from '@composables/useDate';
import { useModificationLayer } from '@components/map/layersModel';
import { useDTALinksLayer, useDTANodesLayer } from '@components/map/layersModelDTA';
import ModificationLinksSelector from './ModificationLinksSelector.vue';
import ModificationLinksCreator from './ModificationLinksCreator.vue';
import useModificationView, { useInteractionProps } from '@composables/useModificationView';

export default {
  components: { MapInteractionWrapper },
  props: {
    /**
     * Fetched modification object
     */
    modification: {
      type: Object,
      required: true,
    },
    /**
     * Mode of modification - whether to modify existing or new modification
     */
    modMode: {
      type: String,
      required: true,
      validator: (value) => ['new', 'existing'].includes(value),
    },
    /**
     * Whether the interaction with form fields should be disabled
     */
    disabled: {
      type: Boolean,
      default: false,
    },
  },
  /**
   * update:modification event - updates the original modification model
   *
   * @event update:modification
   * @property {Object} updated modification model
   */
  emits: ['update:modification'],
  setup(props, { emit }) {
    const useLinksLayer = useDTALinksLayer;
    const useNodesLayer = useDTANodesLayer;
    const interactions = shallowReactive({
      new: {
        component: ModificationLinksCreator,
        layers: [
          { layer: useLinksLayer(), key: 'linksLayer' },
          { layer: useNodesLayer(), key: 'nodesLayer' },
          { layer: useModificationLayer(), key: 'newLinkLayer' },
        ],
        loadingMessageKey: 'modifications.preparing creator',
      },
      existing: {
        component: ModificationLinksSelector,
        layers: [{ layer: useLinksLayer(), key: 'linksLayer' }],
        loadingMessageKey: 'modifications.preparing selector',
      },
    });

    const interaction = computed(() => interactions[props.modMode]);

    const linkModification = ref({});
    const { watchModification, updateModification, onPropUpdate } = useModificationView(linkModification, emit, props);
    const interactionProps = useInteractionProps(props, interaction);
    const { getModelMonday } = useDate();

    const linkModificationDefaults = {
      dateFrom: getModelMonday({ hour: 7 }),
      dateTo: getModelMonday({ hour: 10 }),
    };

    const newModeDefaults = {
      ...linkModificationDefaults,
      source: null,
      target: null,
      coordinates: [],
      twoWay: false,
      speed: 25,
      capacity: 250,
      lanes: 1,
      twoWaySpeed: 25,
      twoWayCapacity: 250,
      twoWayLanes: 1,
    };

    const existingModeDefaults = {
      ...linkModificationDefaults,
      speed: null,
      capacity: null,
      linkId: [],
      lanes: null,
    };

    const existingLinkPresets = [
      { name: 'closure', label: 'modifications.closure', speed: 0, capacity: 0, lanes: null }, // 'lanes' default is updated by selecting a link
      { name: 'limit', label: 'modifications.limit', speed: 50, capacity: 1000, lanes: null }, // 'lanes' default is updated by selecting a link
      { name: 'original', label: 'modifications.original' }, // default values are added by selecting a link
      { name: 'custom', label: 'modifications.custom' },
    ];

    const newLinkPresets = [
      { name: 'calm', label: 'modifications.calm', speed: 25, capacity: 250, lanes: 1 },
      { name: 'service', label: 'modifications.service', speed: 40, capacity: 900, lanes: 1 },
      { name: 'collecting', label: 'modifications.collecting', speed: 50, capacity: 1500, lanes: 1 },
      { name: 'road', label: 'modifications.road', speed: 90, capacity: 2000, lanes: 1 },
      { name: 'custom', label: 'modifications.custom' },
    ];

    const linkPresets = computed(() => (props.modMode === 'new' ? newLinkPresets : existingLinkPresets));
    const selectedLinkPreset = ref(_getCalculatedLinkPreset());

    function _getCalculatedLinkPreset(updatedModification = {}) {
      const exactLinkPreset = linkPresets.value.find(
        (preset) =>
          preset.speed === updatedModification.speed &&
          preset.capacity === updatedModification.capacity &&
          preset.lanes === updatedModification.lanes &&
          (!updatedModification.twoWay || preset.speed === updatedModification.twoWaySpeed) &&
          (!updatedModification.twoWay || preset.capacity === updatedModification.twoWayCapacity) &&
          (!updatedModification.twoWay || preset.lanes === updatedModification.twoWayLanes),
      );
      return exactLinkPreset ? exactLinkPreset.name : 'custom';
    }

    const applyLinkPreset = (type) => {
      if (type === 'custom') return;
      linkModification.value.speed = linkPresets.value.find((el) => el.name === type).speed;
      linkModification.value.capacity = linkPresets.value.find((el) => el.name === type).capacity;
      linkModification.value.lanes = linkPresets.value.find((el) => el.name === type).lanes;
      if (props.modMode === 'new') {
        // opposite direction (only relevant if the modification also has twoWay prop set to true)
        linkModification.value.twoWaySpeed = linkPresets.value.find((el) => el.name === type).speed;
        linkModification.value.twoWayCapacity = linkPresets.value.find((el) => el.name === type).capacity;
        linkModification.value.twoWayLanes = linkPresets.value.find((el) => el.name === type).lanes;
      }
    };

    const existingModeEventKey = computed(() => (props.modMode === 'existing' ? 'feature:selected' : null));
    const newModeEventKey = computed(() => (props.modMode === 'new' ? 'update' : null));

    const updateModificationAndPresets = ({ lastSpeed, lastCapacity, lastLanes, count, linkId }) => {
      // set to specific values only if single feature is selected - otherwise set to null
      const speed = count === 1 ? lastSpeed : null;
      const capacity = count === 1 ? lastCapacity : null;
      const lanes = count === 1 ? lastLanes : null;
      // iterate through link presets and update its default values
      for (const preset of linkPresets.value) {
        if (preset.name === 'custom') continue;
        // update speed & capacity in the 'original' link preset
        if (preset.name == 'original') {
          preset.speed = speed;
          preset.capacity = capacity;
        }
        // update lanes in every link preset
        preset.lanes = lanes;
      }
      // in case the 'original' preset is currently selected, also update currently displayed values
      if (selectedLinkPreset.value === 'original') {
        linkModification.value.speed = speed;
        linkModification.value.capacity = capacity;
        linkModification.value.lanes = lanes;
      }
      // patch linkId in the end to correctly merge changes
      linkModification.value.linkId = linkId;
    };

    watchModification((updatedModification) => {
      updateModification(updatedModification, props.modMode === 'new' ? newModeDefaults : existingModeDefaults);
      selectedLinkPreset.value = _getCalculatedLinkPreset(updatedModification);
    });

    return {
      linkModification,
      interaction,
      interactionProps,
      onPropUpdate,
      linkPresets,
      selectedLinkPreset,
      applyLinkPreset,
      updateModificationAndPresets,
      newModeEventKey,
      existingModeEventKey,
    };
  },
};
</script>

<style scoped></style>
