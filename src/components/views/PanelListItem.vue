<template>
  <prime-panel
    :collapsed="!isActive"
    class="p-mb-3"
    :class="{ 'active-item': isActive, 'mobile-view': $isMobile() }"
    @click="$emit('clicked', item)"
    @mouseenter="showActions = true"
    @mouseleave="showActions = false"
  >
    <template #header>
      <div v-if="fields.switchInput" class="p-mr-3">
        <tm-switch v-model="switchState" :disabled="fields.switchInput.disabled === true" @click.stop />
      </div>
      <div v-if="fields.checkbox" class="p-mr-3">
        <prime-checkbox v-model="selectedItems" :value="item" @click.stop />
      </div>
      <div v-if="type === 'scenarios'">
        <tm-scenario-state-icon :scenario="item" :use-link="true" class="p-mr-3" />
      </div>
      <div v-if="type === 'modifications'" class="p-mr-3">
        <tm-button
          type="create"
          :icon="fields.modIcon.icon"
          :border-color="fields.modIcon.border"
          class="p-mr-1 panel-icon"
        />
      </div>
      <div>
        <span class="item-heading">{{ getItemName() }}</span>
      </div>
      <div
        v-if="['basic', 'scenarios', 'events', 'modifications', 'imports'].includes(type)"
        class="p-ml-auto item-date"
        :class="{ transparent: showActions }"
      >
        {{ $tmdate.formatScenarioDatePeriod(item[fields.dates.from], item[fields.dates.to]) }}
      </div>
      <div v-show="showActions" class="p-py-3 p-pl-3 item-actions">
        <a
          v-for="action in fields.actions"
          :key="action.action"
          v-tooltip="$t(`scenarios.${action.action}`)"
          class="p-mr-3 item-action"
          ><i
            :class="action.icon + ' pi'"
            :data-type="action.action"
            @click.stop="$emit('action:clicked', action.action)"
        /></a>
      </div>
    </template>

    <div class="p-d-flex">
      <div v-for="(opt, i) in fields.content" :key="i" class="p-mr-2" :class="opt.class">
        <span v-if="opt.field">{{ $t(opt.name) }}: </span>
        <strong v-else>{{ $t(opt.name) }}</strong>
        <strong v-if="opt.field">{{
          item[opt.field] === true
            ? $t('scenarios.yes')
            : item[opt.field] === false
            ? $t('scenarios.no')
            : item[opt.field]
        }}</strong>
        <strong v-if="opt.field2">/{{ item[opt.field2] }}</strong>
        <span v-if="opt.units">&nbsp;{{ $t(opt.units) }}</span>
      </div>
      <div v-if="['scenarios', 'events'].includes(type)" class="p-ml-auto">
        <span>{{ $t('scenarios.last edit') }}: </span>
        <strong>{{ $d(new Date(item[fields.dates.lastEdit]), 'default') }}</strong>
      </div>
    </div>
  </prime-panel>
</template>

<script>
import { ref, computed } from 'vue';
import useAbility from '@composables/useAbility';
import { types as modificationTypes } from '@composables/useModifications';
import PrimePanel from 'primevue/panel';
import PrimeCheckbox from 'primevue/checkbox';
import TmScenarioStateIcon from '@components/views/ScenarioStateIcon.vue';

export default {
  components: {
    PrimePanel,
    PrimeCheckbox,
    TmScenarioStateIcon,
  },
  props: {
    /**
     * Type representing the view to which the item belongs
     */
    type: {
      type: String,
      default: 'basic',
      validator: (value) => {
        return ['scenarios', 'events', 'modifications', 'imports', 'basic'].includes(value);
      },
    },
    /**
     * Item object to display
     */
    item: {
      type: Object,
      default: () => {},
    },
    /**
     * Whether the item is in 'active' mode and should display details
     */
    isActive: {
      type: Boolean,
      default: false,
    },
    /**
     * User access to this item - affects disaplyed action icons
     */
    access: {
      type: String,
      default: '',
    },
    /**
     * Event object - only needed for type 'modifications' and used for authorization
     * (modifications use same rules as events, so we need to pass event object to can() function)
     */
    accessEventObject: {
      type: Object,
      default: () => {},
    },
    /**
     * Whether to display the item in watch mode (without any action icons)
     */
    watchMode: {
      type: Boolean,
      default: true,
    },
    /**
     * List of selected items
     */
    selected: {
      type: Array,
      default: () => [],
    },
  },
  /**
   * clicked event - signals, that the item was clicked on
   *
   * @event clicked
   * @property {String} Id of the clicked item
   *
   * checked event - signals, that the item was checked
   *
   * @event checked
   * @property {Boolean} checked state of the switchInput
   *
   * update:selected event - updates original selected model
   *
   * @event update:selected
   * @property {Array} list od selected items
   *
   * action:clicked event - signals, that the item action was checked
   *
   * @event action:clicked
   * @property {String} name of the item action that was clicked on
   */
  emits: ['checked', 'clicked', 'action:clicked', 'update:selected'],
  setup(props, { emit }) {
    const { can } = useAbility();

    const getCustomScenarioFields = () => ({
      actions: [
        ...(props.watchMode || can('enter', { scenario: props.item })
          ? [{ action: 'enter', icon: 'pi-directions' }]
          : []),
        ...(!props.watchMode && can('update', { scenario: props.item }) && _isItemValid(props.item)
          ? [{ action: 'share', icon: 'pi-share-alt' }]
          : []),
        ...(!props.watchMode && _isItemValid(props.item) ? [{ action: 'copy', icon: 'pi-copy' }] : []),
        ...(!props.watchMode && can('delete', { scenario: props.item }) && _isItemValid(props.item)
          ? [{ action: 'delete', icon: 'pi-trash' }]
          : []),
      ],
    });

    const getCustomEventFields = () => ({
      actions: [
        ...(props.watchMode || can('enter', { scenario: { level: props.access, events: [props.item] } })
          ? [{ action: 'enter', icon: 'pi-directions' }]
          : []),
        ...(!props.watchMode &&
        can('create:event', { scenario: { level: props.access, events: [props.item] } }) &&
        _isItemValid(props.item)
          ? [{ action: 'copy', icon: 'pi-copy' }]
          : []),
        ...(!props.watchMode &&
        can('delete:event', { scenario: { level: props.access, events: [props.item] } }) &&
        _isItemValid(props.item)
          ? [{ action: 'delete', icon: 'pi-trash' }]
          : []),
      ],
      switchInput: {
        field: 'included',
        disabled:
          props.watchMode || !can('update:event:included', { scenario: { level: props.access, events: [props.item] } }),
      },
    });

    const getCustomModificationFields = () => ({
      content: [
        ...(props.item.type == 'link' && props.item.mode == 'new'
          ? [
              {
                field: 'capacity',
                field2: props.item.twoWay ? 'twoWayCapacity' : null,
                name: 'modifications.capacity',
                units: 'map.vehicles per hour',
              },
              {
                field: 'speed',
                field2: props.item.twoWay ? 'twoWaySpeed' : null,
                name: 'modifications.speed',
                units: 'map.kilometers per hour',
              },
            ]
          : []),
        ...(props.item.type == 'link' &&
        props.item.mode == 'existing' &&
        (props.item.capacity > 0 || props.item.speed > 0)
          ? [
              {
                field: 'capacity',
                name: 'modifications.capacity',
                units: 'map.vehicles per hour',
              },
              {
                field: 'speed',
                name: 'modifications.speed',
                units: 'map.kilometers per hour',
              },
            ]
          : []),
        ...(props.item.type == 'link' && props.item.mode == 'existing' && !props.item.capacity && !props.item.speed
          ? [{ name: 'modifications.closure' }]
          : []),
        ...(props.item.type == 'node' && props.item.mode == 'existing' && props.item.cost < 0
          ? [{ name: 'modifications.closure' }]
          : []),
        ...(props.item.type == 'node' && props.item.mode == 'existing' && props.item.cost > 0
          ? [
              {
                field: 'cost',
                name: 'modifications.delay',
                units: 'modifications.seconds',
              },
            ]
          : []),
        ...(props.item.type == 'generator'
          ? [
              {
                field: 'inTraffic',
                field2: 'outTraffic',
                name: 'modifications.target source traffic',
                units: 'map.vehicles per hour',
              },
            ]
          : []),
      ],
      actions: [
        ...(props.watchMode || can('enter', { scenario: { level: props.access, events: [props.accessEventObject] } })
          ? [{ action: 'enter', icon: 'pi-directions' }]
          : []),
        ...(!props.watchMode &&
        can('update:event', { scenario: { level: props.access, events: [props.accessEventObject] } }) &&
        _isItemValid(props.item)
          ? [{ action: 'copy', icon: 'pi-copy' }]
          : []),
        ...(!props.watchMode &&
        can('delete:event', { scenario: { level: props.access, events: [props.accessEventObject] } }) &&
        _isItemValid(props.item)
          ? [{ action: 'delete', icon: 'pi-trash' }]
          : []),
      ],
      modIcon: modificationTypes.find((el) => el.type == props.item.type && el.mode == props.item.mode),
    });

    const getCustomImportFields = () => ({
      content: [
        { field: 'scenarioName', name: 'scenarios.scenario' },
        { field: 'included', name: 'scenarios.included', class: 'p-ml-auto' },
      ],
      checkbox: true,
    });

    const fields = computed(() => ({
      header: 'name',
      dates: { from: 'dateFrom', to: 'dateTo', lastEdit: 'updatedAt' },
      content: [{ field: 'owner', name: 'scenarios.owner' }],
      actions: [],
      ...(props.type === 'scenarios' ? getCustomScenarioFields() : {}),
      ...(props.type === 'events' ? getCustomEventFields() : {}),
      ...(props.type === 'modifications' ? getCustomModificationFields() : {}),
      ...(props.type === 'imports' ? getCustomImportFields() : {}),
    }));

    const switchState = computed({
      get: () => fields.value.switchInput && !!props.item[fields.value.switchInput.field],
      set: (newState) => emit('checked', newState),
    });
    const showActions = ref(false);
    const getItemName = () => {
      if (props.type !== 'modifications') return props.item[fields.value.header];
      // custom name for modifications
      return props.item.name || `${props.item.type}-${props.item.id}`;
    };

    const selectedItems = computed({
      get: () => fields.value.checkbox && props.selected,
      set: (selected) => emit('update:selected', selected),
    });

    const _isItemValid = (item) => [undefined, true].includes(item.hasModelSectionsValid);

    return {
      switchState,
      fields,
      showActions,
      getItemName,
      selectedItems,
    };
  },
};
</script>

<style scoped>
.item-actions {
  white-space: nowrap;
  position: absolute;
  right: 0;
  background-color: rgb(248, 249, 250);
}
.item-action {
  cursor: pointer;
  color: #0094d2;
}
.p-panel-header:hover .item-heading,
.item-action:hover i,
.active-item .item-heading {
  font-weight: bold;
}
.p-panel-header .item-date.transparent {
  opacity: 0.5;
}
.tm-button.panel-icon ::v-deep(.p-button) {
  font-size: 0.8rem;
  height: 1.8rem !important;
  width: 1.8rem !important;
  pointer-events: none;
}
.mobile-view .item-date,
.mobile-view .item-actions,
.mobile-view ::v-deep(.p-toggleable-content) {
  display: none;
}
</style>
