<template>
  <div class="tm-calendar" :class="{ labeled: label, clearable: selectedDay && clearable }">
    <div :class="{ input: isPopup }">
      <span v-if="isPopup && label" class="p-button calendar-label" :class="{ 'p-disabled': disabled }">{{
        $t(label)
      }}</span>
      <prime-calendar
        v-model="selectedDay"
        :inline="!isPopup"
        :show-button-bar="showButtonBar"
        selection-mode="single"
        :min-date="limits.minDate"
        :max-date="limits.maxDate"
        :manual-input="false"
        :append-to="isPopup ? 'body' : null"
        :show-time="isPopup && showTime"
        :select-other-months="true"
        :touch-u-i="$isMobile()"
        :hide-on-date-time-select="$isMobile()"
        :disabled="disabled || disabledDate"
        @date-select="datetimeUpdated"
      />
      <i
        v-if="isPopup && clearable && selectedDay && !disabled"
        class="p-button p-button-icon-only calendar-icon pi-times pi"
        @click="clearDate()"
      />
    </div>

    <div v-if="timeslider" class="timeslider" :class="{ separated: isPopup }">
      <tm-slider
        v-model:selected="selectedHour"
        :step="timeOptions.step"
        :range="{ min: timeOptions.min, max: timeOptions.max }"
        :disabled="disabled"
        :suffix="'h'"
        :label="isPopup ? $t('widgets.time') : ''"
        :input="isPopup"
        :value-as-time-range="isPopup"
        @slideend="datetimeUpdated"
        @click="datetimeUpdated"
      ></tm-slider>
    </div>
  </div>
</template>

<script>
import PrimeCalendar from 'primevue/calendar';
import useDate from '@composables/useDate';
import { ref, computed, watch } from 'vue';
import useMobile from '@composables/useMobile';

/**
 * Calendar component for picking date and time
 * @version 1.1.0
 * @author [roadtwin]
 */
export default {
  components: {
    PrimeCalendar,
  },
  props: {
    /**
     * Whether the calendar should display time slider
     */
    timeslider: {
      type: Boolean,
      default: false,
    },
    /**
     * Date limits
     */
    limits: {
      type: Object,
      default: () => ({
        minDate: null, // the minimum selectable date as JS Date()
        maxDate: null, // the maximum selectable date as JS Date()
        // (cannot be passed as tmDate since converting to JS Date here would trigger reactivity for prime-calendar watches/focuses)
      }),
    },
    /**
     * Pre-selected default date as iso string (tmDate) - can be null if the date is not set
     */
    selected: {
      type: String,
      default: null,
    },
    /**
     * Whether should calendar display as input and only enable date picking on the popup window
     */
    popup: {
      type: Boolean,
      default: false,
    },
    /**
     * Whether should calendar display icon over its input to enable clearing the date (only for popup version)
     */
    clearable: {
      type: Boolean,
      default: false,
    },
    /**
     * Label to display with (before) the input (only for popup version)
     */
    label: {
      type: String,
      default: 'scenarios.date',
    },
    /**
     * Whether to display time with the popup calendar
     */
    showTime: {
      type: Boolean,
      default: false,
    },
    /**
     * Whether to display additional button bar with 'today' and 'clear' options
     */
    showButtonBar: {
      type: Boolean,
      default: false,
    },
    /**
     * Whether to disable date & time editing
     */
    disabled: {
      type: Boolean,
      default: false,
    },
    /**
     * Whether to disable only date and keep the time slider enabled
     * (probably just a temporary prop, only utilized by DTA to represent model monday)
     */
    disabledDate: {
      type: Boolean,
      default: false,
    },
    /**
     * Time slider options
     */
    timeOptions: {
      type: Object,
      default: () => ({
        min: 0,
        max: 23,
        step: 1,
      }),
    },
  },
  /**
   * update:selected event - updates original selection model
   *
   * @event update:selected
   * @property {String} selected date as iso string (tmDate)
   */
  emits: ['update:selected'],
  setup(props, { emit }) {
    const { toJsDate, toTmDate, getTmDateHoursNumber, getHoursFromHoursNumber, getMinutesFromHoursNumber } = useDate();
    const isMobile = useMobile();
    const selectedDay = ref(props.selected ? toJsDate(props.selected) : null);
    const selectedHour = ref(props.selected ? getTmDateHoursNumber(props.selected) : null);

    const isPopup = computed(() => props.popup || isMobile());

    watch(
      () => props.selected,
      (selection) => {
        selectedDay.value = selection ? toJsDate(selection) : null;
        selectedHour.value = selection ? getTmDateHoursNumber(selection) : null;
      },
    );

    const datetimeUpdated = () => {
      const day = selectedDay.value;
      const hour = selectedHour.value;
      if (day) day.setSeconds(0, 0);
      if (day && props.timeslider) day.setHours(getHoursFromHoursNumber(hour), getMinutesFromHoursNumber(hour));
      emit('update:selected', day ? toTmDate(day) : null);
    };

    const clearDate = () => {
      selectedDay.value = null;
      datetimeUpdated();
    };

    return {
      selectedDay,
      selectedHour,
      datetimeUpdated,
      clearDate,
      isPopup,
    };
  },
};
</script>

<style scoped>
.p-calendar {
  width: 100%;
}
.timeslider {
  background-color: #f8f9fa;
  border: 1px solid #ced4da;
  border-top: 0px;
}
.timeslider ::v-deep(.tm-slider) {
  margin-top: 0px !important;
  margin-bottom: 0px !important;
}
.timeslider.separated {
  margin-top: 1rem;
  background-color: initial;
  border: none;
}
::v-deep(thead) {
  display: none;
}
::v-deep(.p-datepicker) {
  background-color: #f8f9fa;
  width: 100%;
}
::v-deep(.p-datepicker-header) {
  background-color: #f8f9fa;
}
::v-deep(.p-datepicker table td) {
  padding-top: 0px;
  padding-bottom: 0px;
}
::v-deep(.p-datepicker-group-container) {
  width: 100%;
}
::v-deep(.p-slider-range) {
  display: none;
}
.tm-calendar .p-button {
  overflow: visible;
}
.tm-calendar .input {
  width: auto;
  display: flex;
}
.tm-calendar.labeled ::v-deep(.p-calendar .p-inputtext) {
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
}
.tm-calendar.clearable ::v-deep(.p-calendar .p-inputtext) {
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
}
.calendar-label {
  cursor: default;
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
}
.calendar-icon {
  cursor: pointer;
  padding-top: 0.7rem;
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
}
.calendar-icon:hover {
  background-color: #007aad;
}
</style>
