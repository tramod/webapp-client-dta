<template>
  <tm-card>
    <template #header>
      <tm-breadcrumb
        :names="{
          scenarios: scenario.name,
          events: scEvent.name,
          ...(isEditing ? { modifications: modification.name || `${modification.type}-${modification.id}` } : {}),
        }"
      />
      <tm-header-form
        v-model:resource="modification"
        v-model:valid="isFormValid"
        :ready="isFormDataReady"
        :has-editor-access="canBeEditing"
        :rules="{ name: '' }"
        back-to-route="scenarios.events.modifications"
        class="p-mb-3"
      >
        <template #icon>
          <tm-button
            type="create"
            :icon="modificationIcon?.icon"
            :border-color="modificationIcon?.border"
            class="p-button-secondary"
          />
        </template>
      </tm-header-form>
    </template>

    <template #content>
      <component
        :is="modificationTypeComponent || 'div'"
        v-if="isFormDataReady"
        :modification="modification"
        :mod-mode="modMode"
        :disabled="watchMode"
        @update:modification="updateWithTypeOptions"
      />
    </template>

    <template #footer>
      <div class="p-grid">
        <div class="p-col-6"></div>
        <div class="p-col-6">
          <tm-split-button
            v-if="!watchMode"
            :disabled="isSaveDisabled"
            :label="isEditing ? $t('modifications.update and leave') : $t('modifications.add and leave')"
            :options="splitButtonOptions"
            @default:clicked="submitModification({ leaveAfter: true })"
          />
          <tm-button
            v-else
            type="cancel"
            :label="$t('modifications.leave')"
            @click="$router.push({ name: 'scenarios.events.modifications' })"
          />
        </div>
      </div>
    </template>
  </tm-card>
</template>

<script setup lang="ts">
import { ref, watch, computed, onMounted, provide, defineAsyncComponent } from 'vue';
import { useRouter } from 'vue-router';
import { useStore } from 'vuex';
import { useI18n } from 'vue-i18n';
import { useForm } from 'vee-validate';
import useFullScenario from '@composables/useFullScenario';
import useUnfinishedGuard from '@composables/useUnfinishedGuard';
import useMapItems from '@composables/useMapItems';
import useExtraFeatures from '@composables/useExtraFeatures';
import TmHeaderForm from '@components/views/HeaderForm.vue';

const props = defineProps<{
  scenarioId: number;
  eventId: number;
  modificationId?: number; // present only in case of editing existing modification
  modType: TmModificationType; // Type of the modification - each represented by specific child component
  modMode: TmModificationMode; // Mode of modification - whether to modify existing or new modification
}>();

provide('scenarioId', props.scenarioId);
provide('eventId', props.eventId);

const isEditing = computed(() => !!props.modificationId);
const canBeEditing = computed(() => !isEditing.value || scenario.value.level === 'editor');
const isFormDataReady = computed(() => !isEditing.value || !!modification.value.id);
const isSaveDisabled = computed(() => {
  // TODO: temporary disabled save button for DTA generators
  return (isEditing.value && !isModelChanged.value) || props.modType === 'generator';
});

const {
  scenario,
  event: scEvent,
  modification: formerModification,
  activeScenario,
  createModification,
  updateModification,
  types: modificationTypes,
} = useFullScenario(props);
const modificationIcon = computed(() =>
  modificationTypes.find((el) => el.type == props.modType && el.mode == props.modMode),
);
const router = useRouter();
const store = useStore();
const { t } = useI18n();
const watchMode = computed(() => !canBeEditing.value || store.getters['scenarios/isWatchModeActive'](props.scenarioId));
const { meta, validate } = useForm();
const { fitMapToItem } = useMapItems();
// TODO: remove any Type after useExtraFeatures is transformed to TS
const { preloadExtraFeatures } = useExtraFeatures({ activeScenario } as any);

// Must be defined statically, just point this in computed
const componentDefs = {
  link: defineAsyncComponent(() => import('@components/views/ModificationLink.vue')),
  node: defineAsyncComponent(() => import('@components/views/ModificationNode.vue')),
  generator: defineAsyncComponent(() => import('@components/views/ModificationGenerator.vue')),
  matrix: null,
};

const modificationTypeComponent = computed(() => {
  // in case of editing - wait for modification data before mounting the component
  if (isEditing.value && !modification.value.id) return null;

  return componentDefs[props.modType];
});

const modification = ref<TmModification>({
  type: props.modType,
  mode: props.modMode,
  name: '',
  description: '',
  note: '',
});

onMounted(preloadExtraFeatures);

watch(
  formerModification,
  ({ id }) => {
    if (id == modification.value.id) return;
    // in case the active modification is changed during the component lifetime -> update data
    modification.value = { ...modification.value, ...formerModification.value };
    fitMapToItem(modification.value);
  },
  { immediate: true },
);

const submitModification = async ({ leaveAfter = false } = {}) => {
  await validate();
  if (!meta.value.valid) return;

  isEditing.value
    ? await updateModification(modification.value, { leaveAfter })
    : await createModification(modification.value, { leaveAfter });
};

const { forcedFormAbandonment, isModelChanged } = useUnfinishedGuard({
  formerModel: formerModification,
  newModel: modification,
  modelType: 'modification',
  submitAction: submitModification,
});

const cancelModification = ({ leaveAfter = false } = {}) => {
  if (leaveAfter) {
    forcedFormAbandonment.value = true;
    router.push({ name: 'scenarios.events.modifications' });
  } else {
    // only restore modification data if the user intends to stay on this view
    modification.value = isEditing.value
      ? { ...formerModification.value }
      : { type: props.modType, mode: props.modMode };
  }
};

const updateWithTypeOptions = (newOptions: TmModification) => {
  // update modification obj with values from dynamic child component
  modification.value = { ...modification.value, ...newOptions };
};

const splitButtonOptions = computed(() => [
  {
    label: isEditing.value ? t('modifications.leave without updating') : t('modifications.leave without adding'),
    command: () => cancelModification({ leaveAfter: true }),
  },
  {
    label: t('modifications.delete changes'),
    disabled: isSaveDisabled.value,
    command: cancelModification,
  },
  {
    label: isEditing.value ? t('modifications.update') : t('modifications.add'),
    disabled: isSaveDisabled.value,
    command: submitModification,
  },
]);

const isFormValid = ref(false);
</script>
