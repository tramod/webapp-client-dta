export {}; // export to ensure the .d.ts file is treated as a module

import { tmDate } from '@plugins/tm-date';

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $tmdate: typeof tmDate; // types for template property
    $cc: CookieConsent;
    $tmability: { can: (action: any, model: any) => boolean }; // TODO:
    $isMobile: () => boolean;
    $store: TmStore;
  }
}
