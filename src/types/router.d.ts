export {}; // export to ensure the .d.ts file is treated as a module

import type { Component } from 'vue';

declare module 'vue-router' {
  interface RouteMeta {
    // custom tm properties
    layout?: Component;
    requiresAuth?: boolean;
  }

  interface Router {
    // custom prop for test purposes only
    mocked: boolean;
  }
}
