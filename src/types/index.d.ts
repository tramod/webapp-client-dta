export {}; // export to ensure the .d.ts file is treated as a module

import 'vite/client'; // fix for import.meta ts bug (Property 'env' does not exist on type 'ImportMeta')

declare global {
  const __BUILD_TIME__: string;
  const __APP_VERSION__: string;
  const __BRANCH_NAME__: string;
  const __COMMIT_MESSAGE__: string;

  // TODO: env variables always return string, but might be further specified with regex using custom Tm Types below
  interface ImportMetaEnv {
    VITE_API_HOST?: string;
    VITE_ENABLED_DATA_MODES?: string;
    VITE_ENABLED_ROUTE_LINKS?: string;
    VITE_ENABLED_MODEL_TYPES?: string;
    VITE_ENABLED_WIDGETS?: string;
    VITE_ENABLED_LANGUAGES?: string;
    VITE_DEFAULT_MAP_MODE?: string;
    VITE_DEFAULT_MAP_BASE?: string;
    VITE_DEFAULT_MAP_ZOOM?: string;
    VITE_DEFAULT_MAP_CENTER?: string;
    VITE_MAP_STYLING_MODE?: string;
    VITE_COMPUTATION_REQUEST_FREQUENCY_DTA?: string;
    VITE_MODEL_CACHE_PREFIX?: string;
    VITE_GOOGLE_ANALYTICS_ID?: string;
    VITE_TEXT_APP_TITLE_CS?: string;
    VITE_TEXT_APP_TITLE_EN?: string;
    VITE_TEXT_APP_DESCRIPTION_CS?: string;
    VITE_TEXT_APP_DESCRIPTION_EN?: string;
    VITE_TEXT_COOKIE_CONTACT_CS?: string;
    VITE_TEXT_COOKIE_CONTACT_EN?: string;
    VITE_TEXT_ABOUT_CS?: string;
    VITE_TEXT_ABOUT_EN?: string;
  }

  type TmLang = string; // TODO: can be more precise, regex?
  type TmDataMode = string; // TODO: can be more precise, regex?
  type TmDate = string | null; // TODO: can be more precise, regex?
  type TmCommonDate = string | number | Date;
  type TmModelType = 'DTA';
  type TmStoreState = { [key: string]: any };
  type TmStoreGetters = { [key: string]: any };
  // dummy store object - TODO: try to get proper vuex type? or wait until Pinia?
  type TmStore = {
    state: TmStoreState;
    getters: TmStoreGetters;
    commit: (key: string, arg: any) => void;
    dispatch: (key: string, arg?: any) => void;
  };
  type TmMapMode = string; // TODO: maybe use explicit map mode names from the keys..
  type TmAccessLevel = 'editor' | 'viewer' | 'inserter';

  type TmScenario = {
    id?: number;
    dateFrom?: TmDate;
    dateTo?: TmDate;
    level?: TmAccessLevel;
    isPublic?: boolean;
    isInPublicList?: boolean;
    modelSections?: any[]; // TS_TODO:
    name?: string;
    description?: string;
    note?: string;
    owner?: string;
    updatedAt?: TmDate;
    hasModelSectionsValid?: boolean;
    isPending?: boolean;
    events?: TmEvent[];
    modifications?: TmModification[];
  };

  type TmEvent = {
    id?: number;
    scenarioId?: number;
    dateFrom?: TmDate;
    dateTo?: TmDate;
    included?: boolean;
    name?: string;
    description?: string;
    note?: string;
    type?: string;
    modifications?: TmModification[];
    isPending?: boolean;
  };

  type TmModificationType = 'link' | 'node' | 'generator' | 'matrix';
  type TmModificationMode = 'new' | 'existing';

  type TmModification = {
    id?: number;
    dateFrom?: TmDate;
    dateTo?: TmDate;
    type?: TmModificationType;
    mode?: TmModificationMode;
    name?: string;
    description?: string;
    note?: string;
    linkId?: any; // TODO: set types for optional data attributes, maybe split modification Type by its type/mode?
    speed?: any;
    capacity?: any;
    lanes?: any;
    coordinates?: any;
    source?: any;
    target?: any;
    twoWay?: any;
    twoWaySpeed?: any;
    twoWayCapacity?: any;
    twoWayLanes?: any;
    node?: any;
    linkFrom?: any;
    linkTo?: any;
    cost?: any;
    generator?: any;
    nodes?: any;
    inTraffic?: any;
    outTraffic?: any;
    isPending?: boolean;
    eventId?: number;
  };

  type TmEditSession = {
    computationState: 'notStarted' | 'inProgress' | 'computed' | 'error';
    hasChange: boolean;
    hasComputableChange: boolean;
    computationTimestamp: TmDate | null;
    computationWarning: boolean;
    scenarioId: number;
    flags?: {
      saveOnComputed?: boolean;
    };
  };

  type TmTrafficModel = {
    id: number;
    type: TmModelType;
    isDefault: boolean;
    isValid: boolean; // wether the base-model is computed
    timestamp: number; // base-model version
  };

  type TmApiResponse = {
    status: number;
    data: {
      status: string; // custom TM code as string e.g. 'EDIT_CONTINUED'
      editSession?: Partial<TmEditSession>; // some requests return edit session info
      trafficModel?: Partial<TmTrafficModel>; // some requests return traffic model info
      [key: string]: any;
    };
    message?: string; // optional custom TM message as string
    code?: string; // optional custom TM code as string e.g. 'EDIT_NOT_FOUND' (TODO: unify status/code keywords?)
  };

  type TmTimelineEvent = {
    id: number;
    name: string;
    dateFrom: TmDate;
    dateTo: TmDate;
  };

  type TmUser = {
    id?: number;
    username?: string;
    email?: string;
    organization?: TmOrganization['name'] | null;
    roles?: number[];
    oldPassword?: string;
    newPassword?: string;
  };

  type TmOrganization = {
    id: number;
    name: string;
  };

  type TmItemIds = { scenarioId?: number; eventId?: number; modificationId?: number };
}
