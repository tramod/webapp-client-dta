import {
  getSynchronizedModuleState,
  getDataModeByMapMode,
  getWidgetStateIndex,
  getCalendarDate,
  updateCalendarDate,
} from '@store/helpers';
import { MODEL_DTA_LIDS, MP_MODEL_DTA_VIEWER, MP_MODEL_DTA_COMPARISON, getModelModeByModelType } from '@keys/index';
import type { Commit, Dispatch } from 'vuex';

type MapItem = [scenarioId?: number | null, eventId?: number | null, modificationId?: number | null];
type ComparisonItems = [sourceScenarioId?: number | 'base', targetScenarioId?: number | 'base'];
type CalendarState = { date: TmDate; mapMode: TmMapMode; modelType: TmModelType };
type WidgetState = { name: string; dataMode: TmDataMode; modelType: TmModelType };
type ItemLevel = 'scenario' | 'event' | 'modification';
type Center = string[];
type Zoom = string;

const moduleState = {
  mapMode: <TmMapMode>import.meta.env.VITE_DEFAULT_MAP_MODE,
  center: <Center | null>null,
  zoom: <Zoom | null>null,
  item: <MapItem>[],
  comparison: <ComparisonItems>[],
  base: <string>import.meta.env.VITE_DEFAULT_MAP_BASE,
  mapStyle: {
    general: {
      links: true,
      nodes: false,
      generators: false,
      vehiclesCountDisplayed: false,
      trafficSignsStyle: import.meta.env.VITE_TRAFFIC_SIGNS_STYLE === 'true' || false,
      modelLinkOffset: import.meta.env.VITE_MODEL_LINK_OFFSET === 'true' || false,
    },
    [MP_MODEL_DTA_VIEWER]: {
      output: 'outflow-capacity',
      width: 'outFlow',
      color: 'outFlowIntensity',
      text: 'outFlow',
    },
    [MP_MODEL_DTA_COMPARISON]: {
      color: 'outFlowDiff',
      width: 'outFlowDiff',
    },
  },
  calendar: <CalendarState[]>[],
  widgets: <WidgetState[]>[
    { name: 'calendar', dataMode: 'model', modelType: 'DTA' },
    { name: 'comparison', dataMode: 'comparison', modelType: 'DTA' },
  ],
  brokenMapModes: <string[]>[],
  // TODO Temp solution
  sourceLoaded: <{ [key: string]: boolean }>{},
};

export const getters = {
  getCalendarDate:
    (state: typeof moduleState, getters: TmStoreGetters, rootState: TmStoreState) =>
    ({
      mapMode = state.mapMode,
      modelType = rootState.scenarios.model.type,
      startDateOnly = false,
      endDateOnly = false,
    } = {}) => {
      let date = getCalendarDate({ calendarState: state.calendar, mapMode, modelType });
      if (startDateOnly && Array.isArray(date)) date = date[0];
      if (endDateOnly && Array.isArray(date)) date = date[1];
      return date;
    },
  getActiveItemId:
    (state: typeof moduleState) =>
    ({ itemLevel }: { itemLevel: ItemLevel }) => {
      const stateItem = Array.isArray(state.item) ? state.item : [state.item];

      switch (itemLevel) {
        case 'scenario':
          return stateItem[0] ?? null;
        case 'event':
          return stateItem[1] ?? null;
        case 'modification':
          return stateItem[2] ?? null;
      }
    },
  getDataMode: (state: typeof moduleState) => getDataModeByMapMode(state.mapMode),
  getWidgets:
    (state: typeof moduleState, getters: TmStoreGetters, rootState: TmStoreState) =>
    ({ dataMode = getters.getDataMode, modelType = rootState.scenarios.model.type } = {}) =>
      state.widgets.reduce((widgets, props) => {
        if (dataMode == props.dataMode && modelType == props.modelType) widgets.push(props.name);
        return widgets;
      }, <string[]>[]),
};

export const actions = {
  async setMapMode(
    { commit, dispatch }: { commit: Commit; dispatch: Dispatch },
    {
      mode,
      openWidgetPanel = false,
      closeWidgetPanel = false,
    }: { mode: TmMapMode; openWidgetPanel: boolean; closeWidgetPanel: boolean },
  ) {
    commit('SET_MAP_MODE', { mode });
    if (openWidgetPanel) dispatch('layout/openPanel', { panelSide: 'right' }, { root: true });
    if (closeWidgetPanel) dispatch('layout/closePanel', { panelSide: 'right' }, { root: true });
  },
  async setCalendarDate(
    { commit, state, rootState }: { commit: Commit; state: typeof moduleState; rootState: TmStoreState },
    {
      tmDate,
      mapMode = state.mapMode,
      modelType = rootState.scenarios.model.type,
    }: { tmDate: TmDate; mapMode: TmMapMode; modelType: TmModelType },
  ) {
    commit('SET_CALENDAR', { tmDate, mapMode, modelType });
  },
  async activateItem(
    { commit, rootState }: { commit: Commit; rootState: TmStoreState },
    { scenarioId, eventId, modificationId, date }: TmItemIds & { date?: TmDate } = {},
  ) {
    const modelType = rootState.scenarios.model.type;
    const mapMode = getModelModeByModelType();
    commit('SET_MAP_MODE', { mode: mapMode }); // map item should always be displayed in its variant model mode
    commit('SET_ITEM', { scenarioId, eventId, modificationId });
    commit('SET_COMPARISON', [scenarioId || 'base', 'base']);
    if (date) commit('SET_CALENDAR', { tmDate: date, mapMode, modelType }); // also update calendar date if set
  },
  async setPosition({ commit }: { commit: Commit }, { center, zoom }: { center: Center; zoom: Zoom }) {
    const parsedCenter = Array.isArray(center) ? [parseFloat(center[0]), parseFloat(center[1])] : null;
    commit('SET_CENTER', { center: parsedCenter });
    commit('SET_ZOOM', { zoom: parseFloat(zoom) });
  },
  async zoomIn({ commit, state }: { commit: Commit; state: typeof moduleState }) {
    commit('SET_ZOOM', { zoom: parseFloat(state.zoom || '0') + 1 });
  },
  async zoomOut({ commit, state }: { commit: Commit; state: typeof moduleState }) {
    commit('SET_ZOOM', { zoom: parseFloat(state.zoom || '0') - 1 });
  },
  async setBase({ commit }: { commit: Commit }, { baseId }: { baseId: string }) {
    commit('SET_BASE', { baseId });
  },
  async setComparedScenarios({ commit }: { commit: Commit }, { scenarios }: { scenarios: ComparisonItems }) {
    commit('SET_COMPARISON', scenarios);
  },
  async addBrokenMapMode(
    { commit, state }: { commit: Commit; state: typeof moduleState },
    { mapModeKey = null, layerKey = null } = {},
  ) {
    const brokenMapModes = state.brokenMapModes;

    if (mapModeKey) brokenMapModes.push(mapModeKey);

    if (layerKey && Object.values(MODEL_DTA_LIDS).includes(layerKey)) {
      brokenMapModes.push(MP_MODEL_DTA_VIEWER);
    }

    commit('SET_BROKEN_MODES', [...new Set(brokenMapModes)]);
  },
  addSourceLoaded({ commit }: { commit: Commit }, { sourceKey, status }: { sourceKey: string; status: boolean }) {
    // TODO Temp solution
    commit('SET_SOURCE_LOADED', { sourceKey, status });
  },
  toggleWidget(
    {
      commit,
      state,
      rootState,
      getters,
    }: { commit: Commit; state: typeof moduleState; rootState: TmStoreState; getters: TmStoreGetters },
    {
      name,
      dataMode = getters.getDataMode,
      modelType = rootState.scenarios.model.type,
    }: {
      name: string;
      dataMode: TmDataMode;
      modelType: TmModelType;
    },
  ) {
    const activeWidgetIndex = getWidgetStateIndex({ widgetsState: state.widgets, name, dataMode, modelType });
    if (activeWidgetIndex >= 0) commit('HIDE_WIDGET', { widgetIndex: activeWidgetIndex });
    else commit('SHOW_WIDGET', { name, dataMode, modelType });
  },
  showWidget(
    {
      commit,
      state,
      rootState,
      getters,
    }: { commit: Commit; state: typeof moduleState; rootState: TmStoreState; getters: TmStoreGetters },
    {
      name,
      dataMode = getters.getDataMode,
      modelType = rootState.scenarios.model.type,
    }: { name: string; dataMode: TmDataMode; modelType: TmModelType },
  ) {
    const activeWidgetIndex = getWidgetStateIndex({ widgetsState: state.widgets, name, dataMode, modelType });
    if (activeWidgetIndex < 0) commit('SHOW_WIDGET', { name, dataMode, modelType });
  },
  hideWidget(
    {
      commit,
      state,
      rootState,
      getters,
    }: { commit: Commit; state: typeof moduleState; rootState: TmStoreState; getters: TmStoreGetters },
    {
      name,
      dataMode = getters.getDataMode,
      modelType = rootState.scenarios.model.type,
    }: { name: string; dataMode: TmDataMode; modelType: TmModelType },
  ) {
    const activeWidgetIndex = getWidgetStateIndex({ widgetsState: state.widgets, name, dataMode, modelType });
    if (activeWidgetIndex >= 0) commit('HIDE_WIDGET', { widgetIndex: activeWidgetIndex });
  },
};

export const mutations = {
  SET_MAP_MODE(state: typeof moduleState, { mode }: { mode: TmMapMode }) {
    state.mapMode = mode;
  },
  SET_CALENDAR(
    state: typeof moduleState,
    { tmDate, mapMode, modelType }: { tmDate: TmDate; mapMode: TmMapMode; modelType: TmModelType },
  ) {
    updateCalendarDate({ calendarState: state.calendar, date: tmDate, mapMode, modelType });
  },
  SET_ITEM(state: typeof moduleState, { scenarioId, eventId, modificationId }: TmItemIds = {}) {
    if (!scenarioId && !eventId && !modificationId) return (state.item = []); // keep empty array, unless there is at least one item active
    // set array values one by one to avoid triggering unexpected ref changes during watches
    state.item[0] = scenarioId || null;
    state.item[1] = eventId || null;
    state.item[2] = modificationId || null;
  },
  SET_CENTER(state: typeof moduleState, { center }: { center: Center }) {
    state.center = center;
  },
  SET_ZOOM(state: typeof moduleState, { zoom }: { zoom: Zoom }) {
    state.zoom = zoom;
  },
  SET_BASE(state: typeof moduleState, { baseId }: { baseId: string }) {
    state.base = baseId;
  },
  SET_COMPARISON(state: typeof moduleState, [sourceScenarioId, targetScenarioId]: ComparisonItems) {
    state.comparison = [sourceScenarioId, targetScenarioId];
  },
  SET_BROKEN_MODES(state: typeof moduleState, brokenMapModes: string[]) {
    state.brokenMapModes = brokenMapModes;
  },
  SET_MAP_STYLE(
    state: typeof moduleState,
    {
      newState,
      group,
      property,
    }: {
      newState: never;
      group: keyof typeof moduleState.mapStyle;
      property: never; // TODO type this
    },
  ) {
    state.mapStyle[group][property] = newState;
  },
  SET_SOURCE_LOADED(
    state: typeof moduleState,
    { sourceKey, status }: { sourceKey: keyof typeof moduleState.sourceLoaded; status: boolean },
  ) {
    // TODO Temp solution
    state.sourceLoaded[sourceKey] = status;
  },
  SHOW_WIDGET(
    state: typeof moduleState,
    { name, dataMode, modelType }: { name: string; dataMode: TmDataMode; modelType: TmModelType },
  ) {
    state.widgets.push({ name, dataMode, modelType });
  },
  HIDE_WIDGET(state: typeof moduleState, { widgetIndex }: { widgetIndex: number }) {
    state.widgets.splice(widgetIndex, 1);
  },
};

export default {
  namespaced: true,
  state: getSynchronizedModuleState({
    module: 'map',
    state: moduleState,
  }),
  getters,
  actions,
  mutations,
};
