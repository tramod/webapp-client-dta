import { set, get } from 'lodash-es';
import { today, getModelMonday } from '@utils/tm-date';
import { getDataModeByMapMode } from '@composables/useMapModes';
import {
  COMPARISON_MAP_MODES,
  INTERACTION_MAP_MODES,
  MP_MODEL_DTA_VIEWER,
  MP_MODEL_DTA_COMPARISON,
  getModelModeByModelType,
} from '@keys/index';
import type { Router } from 'vue-router';

type ExpectedQueryParam = {
  key: string;
  path: string;
  isArray?: boolean;
  valueType?: NumberConstructor;
  queryTransformFn?: (...args: any[]) => any;
  stateTransformFn?: (...args: any[]) => any;
  conditionalDisplayFn?: (arg: any) => boolean;
};

const expectedQueryParams: ExpectedQueryParam[] = [
  { key: 'mapmode', path: 'map.mapMode' },
  { key: 'mapitem', path: 'map.item', isArray: true },
  { key: 'mapzoom', path: 'map.zoom', valueType: Number },
  { key: 'mapcenter', path: 'map.center', isArray: true, valueType: Number },
  {
    key: 'maptime',
    path: 'map.calendar',
    queryTransformFn: (date: TmDate, query) => {
      // additional 'query to state' transformation specific for this param
      const mapMode = query.get('mapmode') || import.meta.env.VITE_DEFAULT_MAP_MODE;
      const modelType = query.get('model') || 'DTA';
      return [{ date, mapMode: _getCalendarMapMode({ mapMode }), modelType }];
    },
    stateTransformFn: (calendarState, state) => {
      // additional 'state to query' transformation specific for this param
      return getCalendarDate({ calendarState, mapMode: state.map.mapMode, modelType: state.scenarios.model.type });
    },
  },
  {
    key: 'comparison',
    path: 'map.comparison',
    isArray: true,
    conditionalDisplayFn: (state) => COMPARISON_MAP_MODES.includes(get(state, 'map.mapMode')),
  },
  {
    key: 'base',
    path: 'map.base',
    conditionalDisplayFn: (state) => get(state, 'map.base') !== 'OSM',
  },
  {
    key: 'lang',
    path: 'locale.lang',
    conditionalDisplayFn: (state) => get(state, 'locale.lang') !== defaultAppLang,
  },
  {
    key: 'widget',
    path: 'layout.panels.right',
    conditionalDisplayFn: (state) => get(state, 'layout.panels.right') == 'opened',
  },
  {
    key: 'model',
    path: 'scenarios.model.type',
    conditionalDisplayFn: (state) => get(state, 'scenarios.model.type') === 'DTA',
  },
];

export const queryAffectingMutations = [
  'layout/SET_PANEL_STATE',
  'locale/SET_LANG',
  'map/SET_MAP_MODE',
  'map/SET_CALENDAR',
  'map/SET_ITEM',
  'map/SET_COMPARISON',
  'map/SET_CENTER',
  'map/SET_ZOOM',
  'map/SET_BASE',
  'scenarios/SET_MODEL',
];

export const getSynchronizedModuleState = ({ module, state }: { module: string; state: TmStoreState }) => {
  const query = _getUrlQueryParams();
  query.forEach((val, key) => {
    const expectedParam = expectedQueryParams.find((qp) => qp.key == key);
    if (!expectedParam) return; // ignore unexpected query params

    const statePath = _getStatePath({ expectedParam, module });
    if (!statePath) return; // ignore query params not relevant to the module

    const paramValue = _parseParamValue({ expectedParam, query });
    set(state, statePath, paramValue);
  });

  return state;
};

export const synchronizeUrlWithModuleState = ({ state, router }: { state: TmStoreState; router: Router }) => {
  const oldQuery = { ...router.currentRoute.value.query }; // don't update router query directly, just make copy and update via .replace() to properly propagate URL change
  const newQuery: { [key: string]: any } = {};

  expectedQueryParams.forEach((expectedParam) => {
    const statePath = _getStatePath({ expectedParam });
    if (!statePath) return;
    const shouldDisplay = _shouldBeDisplayedQuery({ expectedParam, state });
    let value = get(state, statePath, null);
    if (value && expectedParam.stateTransformFn) value = expectedParam.stateTransformFn(value, state);
    if (value && shouldDisplay) newQuery[expectedParam.key] = value;
    else delete oldQuery[expectedParam.key];
  });

  const query = { ...oldQuery, ...newQuery };
  router.replace({ query });
};

export { getDataModeByMapMode };

export const getCalendarDate = ({
  calendarState,
  mapMode,
  modelType,
}: {
  calendarState: TmStoreState;
  mapMode: TmMapMode;
  modelType: TmModelType;
}) => {
  const calendarMapMode = _getCalendarMapMode({ mapMode });
  const calendar = calendarState.find((c: TmStoreState) => c.mapMode == calendarMapMode && c.modelType == modelType);
  return calendar ? calendar.date : _getDefaultCalendarDate({ mapMode: calendarMapMode });
};

export const updateCalendarDate = ({
  calendarState,
  date,
  mapMode,
  modelType,
}: {
  calendarState: TmStoreState;
  date: TmDate;
  mapMode: TmMapMode;
  modelType: TmModelType;
}) => {
  const calendarMapMode = _getCalendarMapMode({ mapMode });
  const stateIndex = calendarState.findIndex(
    (i: TmStoreState) => i.mapMode == calendarMapMode && i.modelType == modelType,
  );
  if (stateIndex >= 0) calendarState[stateIndex].date = date;
  else calendarState.push({ date, mapMode: calendarMapMode, modelType });
};

export const getWidgetStateIndex = ({
  widgetsState,
  name,
  dataMode,
  modelType,
}: {
  widgetsState: TmStoreState;
  name: string;
  dataMode: string;
  modelType: TmModelType;
}) => {
  return widgetsState.findIndex(
    (w: TmStoreState) => w.name == name && w.dataMode == dataMode && w.modelType == modelType,
  );
};

const _getStatePath = ({
  expectedParam,
  module = null,
}: {
  expectedParam: ExpectedQueryParam;
  module?: string | null;
}) => {
  let path = expectedParam.path;
  if (module) {
    const [moduleMismatch, modulePath] = path.split(`${module}.`);
    if (moduleMismatch) return null;
    path = modulePath;
  }

  return path;
};

const _parseParamValue = ({ expectedParam, query }: { expectedParam: ExpectedQueryParam; query: URLSearchParams }) => {
  let value: any = query.getAll(expectedParam.key);
  if (expectedParam.valueType) value = value.map(expectedParam.valueType); // re-type values
  if (!expectedParam.isArray && value.length === 1) value = value[0]; // extract non-array value
  if (expectedParam.queryTransformFn) value = expectedParam.queryTransformFn(value, query);

  return value;
};

const _getUrlQueryParams = () => {
  // get query from window since store initializes before router
  const query = window.location.search;
  return new URLSearchParams(query);
};

const _shouldBeDisplayedQuery = ({
  expectedParam,
  state,
}: {
  expectedParam: ExpectedQueryParam;
  state: TmStoreState;
}) => {
  const { conditionalDisplayFn } = expectedParam;
  if (!conditionalDisplayFn) return true;
  return conditionalDisplayFn(state);
};

const _getDefaultAppLang = (): TmLang => {
  const enabledLangs = import.meta.env.VITE_ENABLED_LANGUAGES?.split(', ') || [];
  const defaultLocale = enabledLangs[0] || 'cs';

  const availableBrowserLangs = navigator.languages;
  if (!availableBrowserLangs) return defaultLocale;

  const formatLang = (l: string) => l.split('-')[0];
  const preferredBrowserLang = availableBrowserLangs.find((lang) => enabledLangs.includes(formatLang(lang)));
  if (!preferredBrowserLang) return defaultLocale;

  return formatLang(preferredBrowserLang);
};

const _getCalendarMapMode = ({ mapMode }: { mapMode: TmMapMode }) => {
  // some map modes are considered as just an alias of another map mode for the purpose of storing/retrieving calendar date
  if (INTERACTION_MAP_MODES.includes(mapMode)) return getModelModeByModelType();
  return mapMode;
};

const _getDefaultCalendarDate = ({ mapMode }: { mapMode: TmMapMode }) => {
  const date = [MP_MODEL_DTA_VIEWER, MP_MODEL_DTA_COMPARISON].includes(mapMode) ? getModelMonday({ hour: 7 }) : today();
  const isComparison = COMPARISON_MAP_MODES.includes(mapMode);
  return isComparison ? [date, date] : date;
};

export const defaultAppLang = _getDefaultAppLang();
