// LAYER IDS
export const MAP_ID = 'mainMap';
// View layers
export const LID_DTA_LINKS = 'modelDTALinks';
export const LID_DTA_NODES = 'modelDTANodes';
export const LID_DTA_GENERATORS = 'modelDTAGenerators';
export const LID_SIGNS = 'modelSigns';
export const LID_MODIFICATIONS = 'modelModifications';

// Model layers groups
export const MODEL_DTA_LIDS = {
  link: LID_DTA_LINKS,
  node: LID_DTA_NODES,
  generator: LID_DTA_GENERATORS,
};

// MAP PRESET KEYS
// viewer modes
export const MP_MODEL_DTA_VIEWER = 'modelDTA';
export const MP_MODEL_DTA_COMPARISON = 'modelDTA-comparison';
// data interaction modes
export const MP_GENERATOR_DRAW = 'model-generator_new_draw';
export const MP_GENERATOR_NEW_NODES = 'model-generator_new_nodes_select';
export const MP_GENERATOR_SELECT = 'model-generator_existing_select';
export const MP_GENERATOR_EXIST_NODES = 'model-generator_existing_nodes_select';
export const MP_LINK_NODES = 'model-link_new_nodes_select';
export const MP_LINK_MODIFY = 'model-link_new_modification';
export const MP_LINK_SELECT = 'model-link_existing_select';
export const MP_NODE_DRAW = 'model-node_new_draw';
export const MP_NODE_SELECT = 'model-node_existing_selection';
export const MP_NODE_LINKS = 'model-node_existing_linking';

export const MP_SHORTEST_SELECT = 'model-shortest_path_select';
export const MP_SHORTEST_VIEW = 'model-shortest_path_viewer';

// every model interaction mode key in a list (simplifies imports)
export const INTERACTION_MAP_MODES = [
  MP_GENERATOR_DRAW,
  MP_GENERATOR_NEW_NODES,
  MP_GENERATOR_SELECT,
  MP_GENERATOR_EXIST_NODES,
  MP_LINK_NODES,
  MP_LINK_MODIFY,
  MP_LINK_SELECT,
  MP_NODE_DRAW,
  MP_NODE_SELECT,
  MP_NODE_LINKS,
];

// every comparison map mode in a list
export const COMPARISON_MAP_MODES = [MP_MODEL_DTA_COMPARISON];

// every model map mode in a list
export const MODEL_MAP_MODES = [MP_MODEL_DTA_VIEWER, MP_MODEL_DTA_COMPARISON];

// helper function (place elsewhere?)
export const getModelModeByModelType = () => {
  return MP_MODEL_DTA_VIEWER;
};
