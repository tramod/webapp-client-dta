import { unset } from 'lodash-es';
import modelDTATrafficInterval from './useModelDTATrafficInterval';

const DTAIntervalCache = {};

export const invalidateModelTrafficInterval = (scenarioId) => {
  unset(DTAIntervalCache, `${scenarioId}`);
};

export const useModelDTATrafficInterval = () => modelDTATrafficInterval(DTAIntervalCache);
