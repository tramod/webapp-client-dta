import { computed } from 'vue';
import { useStore } from 'vuex';
import WidgetTimeline from '@components/layouts/widgets/WidgetTimeline.vue';
import useMobile from '@composables/useMobile';
import { INTERACTION_MAP_MODES } from '@keys/index';
import type { Component } from 'vue';

type TmWidget = {
  name: string;
  dataModes?: TmDataMode[];
  modelTypes?: TmModelType[];
  excludedMapModes?: TmMapMode[];
  isForMobile?: boolean;
  icon: string;
  buttonComponent?: Component;
};

const widgets: TmWidget[] = [
  {
    name: 'mapPopup',
    dataModes: ['model', 'comparison'],
    modelTypes: ['DTA'],
    excludedMapModes: [...INTERACTION_MAP_MODES],
    isForMobile: true,
    icon: 'ri-information-line',
  },
  {
    name: 'calendar',
    dataModes: ['model'],
    modelTypes: ['DTA'],
    excludedMapModes: [...INTERACTION_MAP_MODES],
    isForMobile: true,
    icon: 'ri-calendar-line',
  },
  {
    name: 'comparison',
    dataModes: ['comparison'],
    modelTypes: ['DTA'],
    excludedMapModes: [],
    isForMobile: true,
    icon: 'ri-equalizer-line',
  },
  {
    name: 'eventList',
    dataModes: ['model'],
    modelTypes: ['DTA'],
    excludedMapModes: [...INTERACTION_MAP_MODES],
    isForMobile: true,
    icon: 'ri-timer-line',
  },
  {
    name: 'timeline',
    dataModes: ['model'],
    modelTypes: ['DTA'],
    excludedMapModes: [...INTERACTION_MAP_MODES],
    isForMobile: true,
    icon: 'ri-bar-chart-horizontal-fill',
    buttonComponent: WidgetTimeline,
  },
  {
    name: 'mapSettings',
    dataModes: ['model', 'comparison'],
    modelTypes: ['DTA'],
    excludedMapModes: [...INTERACTION_MAP_MODES],
    isForMobile: true,
    icon: 'ri-map-line',
  },
  {
    name: 'legend',
    dataModes: ['model'],
    modelTypes: ['DTA'],
    excludedMapModes: [...INTERACTION_MAP_MODES],
    isForMobile: true,
    icon: 'ri-list-check',
  },
];

export default function useWidgets() {
  const store = useStore();
  const isMobile = useMobile();

  const modelType = computed(() => store.state.scenarios.model.type);
  const dataMode = computed(() => store.getters['map/getDataMode']);
  const mapMode = computed(() => store.state.map.mapMode);

  const widgetEnabledList = import.meta.env.VITE_ENABLED_WIDGETS?.split(', ') || [];
  const widgetActiveList = computed(() => store.getters['map/getWidgets']());

  const isWidgetEnabled = (name: string) => widgetEnabledList.includes(name);
  const isWidgetActive = (name: string) => isWidgetEnabled(name) && widgetActiveList.value.includes(name);

  const widgets = computed(() => _getWidgets(dataMode.value, modelType.value, mapMode.value, isMobile()));

  const openWidgetsForDataMode = (dataMode: TmDataMode) => {
    const widgetsToBeOpened = _getWidgets(dataMode, modelType.value, mapMode.value, isMobile()).reverse(); // get widgets in reverse order to keep the original display order
    const mapModeOpenedByDefault = isWidgetActive('mapPopup');
    widgetsToBeOpened.forEach(({ name }) => {
      store.dispatch('map/hideWidget', { name }); // hide widgets displayed by default first (to keep the original order)
      store.dispatch('map/showWidget', { name });
    });
    if (!mapModeOpenedByDefault) store.dispatch('map/hideWidget', { name: 'mapPopup' });
  };

  return {
    widgets,
    enabledWidgets: computed(() => widgets.value.filter((w) => isWidgetEnabled(w.name))),
    activeWidgets: computed(() =>
      widgets.value
        .filter((w) => isWidgetActive(w.name))
        .sort((w1, w2) => widgetActiveList.value.indexOf(w2.name) - widgetActiveList.value.indexOf(w1.name)),
    ),
    isWidgetEnabled,
    isWidgetActive,
    openWidgetsForDataMode,
  };
}

function _getWidgets(
  dataMode = <TmDataMode | null>null,
  modelType = <TmModelType | null>null,
  mapMode = <TmMapMode | null>null,
  isMobile = false,
) {
  const matchesModelType = (modelTypes?: TmModelType[]) => !modelType || modelTypes?.includes(modelType);
  const matchesDataMode = (dataModes?: TmDataMode[]) => !dataMode || dataModes?.includes(dataMode);
  const matchesMapMode = (exclusions?: TmMapMode[]) => !mapMode || !exclusions?.includes(mapMode);
  const matchesDevice = (isForMobile?: boolean) => !isMobile || isForMobile;

  return widgets.reduce((modes: TmWidget[], props) => {
    if (
      matchesModelType(props.modelTypes) &&
      matchesDataMode(props.dataModes) &&
      matchesMapMode(props.excludedMapModes) &&
      matchesDevice(props.isForMobile)
    ) {
      modes.push({ name: props.name, icon: props.icon, buttonComponent: props.buttonComponent });
    }
    return modes;
  }, []);
}
