import { ref } from 'vue';
import useMainApiRequest from '@composables/useMainApiRequest';
import { useI18n } from 'vue-i18n';

type ScenarioAccess = { id: number; userId: number; access: TmAccessLevel };

export default function useScenarioAccesses(defScenarioId?: number) {
  const accesses = ref<ScenarioAccess[]>([]);
  const { t } = useI18n();
  const { makeRequest } = useMainApiRequest();

  async function fetchAccesses(scenarioId = defScenarioId) {
    await makeRequest({
      url: `scenarios/${scenarioId}/access`,
      method: 'get',
      message: {
        error: {
          404: { summary: t('accesses.no accesses found'), severity: 'info' },
          default: t('accesses.error while fetching accesses'),
        },
      },
      onSuccess: (result: { accesses: ScenarioAccess[] }) => {
        accesses.value = result.accesses;
      },
    });
  }

  async function createAccess(
    { id: userId, level, username }: { id: number; username: string; level: TmAccessLevel },
    scenarioId = defScenarioId,
  ) {
    await makeRequest({
      url: `scenarios/${scenarioId}/access/${userId}`,
      method: 'post',
      data: { level },
      message: {
        success: t('accesses.access created', { name: username }),
        error: t('accesses.failed to create access'),
      },
      onSuccess: (result: { access: ScenarioAccess }) => {
        accesses.value.push(result.access);
      },
    });
  }

  async function updateAccess(userId: number, level: TmAccessLevel, scenarioId = defScenarioId) {
    await makeRequest({
      url: `scenarios/${scenarioId}/access/${userId}`,
      method: 'patch',
      data: { level },
      message: {
        success: t('accesses.access updated'),
        error: t('accesses.failed to update access'),
      },
      onSuccess: (result: { access: ScenarioAccess }) => {
        const resultedAccess = result.access;

        accesses.value = accesses.value.map((access) => {
          return access.id == resultedAccess.id ? resultedAccess : access;
        });
      },
    });
  }

  async function deleteAccess(userId: number, scenarioId = defScenarioId) {
    await makeRequest({
      url: `scenarios/${scenarioId}/access/${userId}`,
      method: 'delete',
      message: {
        success: t('accesses.access deleted'),
        error: t('accesses.failed to delete access'),
      },
      onSuccess: () => {
        accesses.value = accesses.value.filter((access) => access.userId != userId);
      },
    });
  }

  return {
    accesses,
    fetchAccesses,
    createAccess,
    updateAccess,
    deleteAccess,
  };
}
