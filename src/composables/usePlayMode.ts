import { ref, watch, onUnmounted } from 'vue';
import useModels from '@composables/useModels';
import { useStore } from 'vuex';
import useDate from '@composables/useDate';
import { COMPARISON_MAP_MODES } from '@keys/index';
import type { Ref } from 'vue';

export default function usePlayMode(mapMode: Ref<string>): {
  isPlayModeActive: Ref<boolean>;
  startPlayMode: () => void;
  endPlayMode: () => void;
} {
  const store: TmStore = useStore();
  const { getModelCalendarSetting } = useModels();
  const { addTmDateHours } = useDate();

  const isPlayModeActive = ref(false);
  let playInterval: NodeJS.Timeout;

  const autoUpdateCalendarDate = () => {
    const isComparison = COMPARISON_MAP_MODES.includes(mapMode.value);
    const { step = 1, min, max } = getModelCalendarSetting();
    const calendarDate = store.getters['map/getCalendarDate']();
    const updatedDate = isComparison
      ? [
          addTmDateHours(calendarDate[0], step, { minHour: min, maxHour: max }),
          addTmDateHours(calendarDate[1], step, { minHour: min, maxHour: max }),
        ]
      : addTmDateHours(calendarDate, step, { minHour: min, maxHour: max });
    store.dispatch('map/setCalendarDate', { tmDate: updatedDate });
  };

  const startPlayMode = () => {
    if (isPlayModeActive.value) return;
    isPlayModeActive.value = true;
    playInterval = setInterval(autoUpdateCalendarDate, 2 * 1000);
  };

  const endPlayMode = () => {
    if (!isPlayModeActive.value) return;
    isPlayModeActive.value = false;
    clearInterval(playInterval);
  };

  watch(mapMode, endPlayMode);
  onUnmounted(endPlayMode);

  return {
    isPlayModeActive,
    startPlayMode,
    endPlayMode,
  };
}
