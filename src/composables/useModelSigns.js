import { ref, computed, markRaw, watch } from 'vue';
import { useStore } from 'vuex';
import LineString from 'ol/geom/LineString';

import { createPointFeature } from '@components/map/layerCommonUtils';
import {
  useLayerSourceDataGroup,
  isLayerSourceGroupLoaded,
  loadLayerSourceGroup,
} from '@composables/useLayerSourceData';
import useFullScenario from '@composables/useFullScenario';
import useModelModifications, {
  includedEventsFilter,
  linkExistingFilter,
  dateIntersectionFilterFn,
  getBaseModData,
  getLEModClassification,
} from '@composables/useModelModifications';
import { MODEL_DTA_LIDS } from '@keys/index';

export default function useModelSigns() {
  const applyFilters = ref(true); // TODO: this might be a user option later

  const store = useStore();
  const calendarDate = computed(() => store.getters['map/getCalendarDate']());
  const trafficSignsMode = computed(() => store.state.map.mapStyle.general.trafficSignsStyle);

  const layerDataDTA = useLayerSourceDataGroup(MODEL_DTA_LIDS);
  const layerSources = computed(() => layerDataDTA);
  const layerSourceLink = computed(() => layerSources.value?.link);

  const { activeScenario } = useFullScenario();
  const { extractMods } = useModelModifications();

  // Get filtered modification data
  const modificationData = computed(() => {
    const filters = { event: [], mods: [] };
    if (applyFilters.value) {
      filters.event.push(includedEventsFilter);
      filters.mods.push(dateIntersectionFilterFn(calendarDate.value));
    }

    return extractMods({
      scenario: activeScenario.value,
      eventFilters: filters.event,
      modFilters: filters.mods,
      listReducer: (list, item) => {
        const baseData = getBaseModData(item);
        if (trafficSignsMode.value && linkExistingFilter(item.modification) && layerSourceLink.value?.state.isLoaded)
          Object.assign(baseData, getLEModClassification(item, layerSourceLink.value.getFeatureData));
        list.push(baseData);
        return list;
      },
    });
  });

  // Filter out layer sources needed by modification
  const modificationLayerSources = computed(() => {
    const typesInMods = modificationData.value.reduce((types, { type }) => types.add(type), new Set());
    return Array.from(typesInMods).map((type) => layerSources.value[type]);
  });
  const areLayerSourcesLoaded = computed(() => isLayerSourceGroupLoaded(modificationLayerSources.value));

  // Refresh layer sources if some data missing
  watch(
    areLayerSourcesLoaded,
    () => {
      loadLayerSourceGroup(modificationLayerSources.value);
    },
    { immediate: true },
  );

  // Build sign features on modData change when layer sources are ready
  const signFeatures = computed(() => {
    if (!areLayerSourcesLoaded.value) return [];
    return buildSignFeatures(modificationData.value, layerSources.value);
  });

  return {
    signFeatures,
    applyFilters, // exported for testing
  };
}

function buildSignFeatures(modsData, getFeatureDataFns) {
  return modsData
    .map((mod) => {
      try {
        const featureData = _getBaseJson(mod);
        featureData.geometry.coordinates = _getSignCoordinates(mod, getFeatureDataFns);
        return markRaw(createPointFeature(featureData, { idProperty: 'node_id' }));
      } catch (error) {
        if (error.message === 'FEATURE_NOT_FOUND')
          console.warn(`Feature used by modification ${mod.modId} not found in base model`);
        else throw error;
      }
    })
    .filter((feat) => feat !== undefined);
}

function _getBaseJson(props) {
  return {
    geometry: {
      coordinates: null,
    },
    properties: { ...props },
  };
}

const newModCoordKey = {
  link: 'coordinates',
  node: 'node',
  generator: 'generator',
};
const featureIdDict = {
  link: 'linkId',
  node: 'node',
  generator: 'generator',
};

function _getSignCoordinates(modification, getFeatureDataFns) {
  const { mode, type } = modification;
  let coords;
  if (mode === 'new') {
    const coordKey = newModCoordKey[type];
    coords = modification[coordKey];
  } else {
    const { getFeatureData } = getFeatureDataFns[type];
    if (!getFeatureData) throw Error('Invalid type');
    const idProperty = featureIdDict[type];
    const featureIds = Array.isArray(modification[idProperty]) ? modification[idProperty] : [modification[idProperty]];

    let idIndex = featureIds.length - 1;
    let feature;

    // Try all features for coords from last to first
    while (!feature && idIndex >= 0) {
      const featureId = featureIds[idIndex];
      feature = getFeatureData(featureId);
      idIndex--;
    }

    if (!feature) throw Error('FEATURE_NOT_FOUND');
    coords = feature.geometry.coordinates;
  }
  return type === 'link' ? _getSingleLinkCoord(coords) : coords;
}

// Compute center of link coordinate
function _getSingleLinkCoord(coords) {
  // Partial multilinestring compat, doesnt have proper way to get center of line for multilinestring
  // For multilinestring center of first segment is extracted
  const geometry = new LineString(Array.isArray(coords[0][0]) ? coords[0] : coords);
  return geometry.getCoordinateAt(0.5);
}
