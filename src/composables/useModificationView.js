import { watch, computed } from 'vue';

export default function useModificationView(modification, emit, props) {
  const watchModification = (updateModFunction) => {
    // watch and emit local changes
    watch(
      () => ({ ...modification.value }),
      () => emit('update:modification', modification.value),
      { immediate: true }, // emit immediately - so it is possible to save default values instantly without any local changes
    );

    // watch props and update local state accordingly (after modification is fetched or changes discarded)
    watch(
      () => props.modification,
      (updatedModification) => updateModFunction(updatedModification), // update one prop at a time - so it triggers the watch above and does not cycle infinitely
      { immediate: true },
    );
  };

  const updateModification = (updatedModification, defaults) => {
    for (const [field, defaultVal] of Object.entries(defaults)) {
      modification.value[field] = updatedModification[field] !== undefined ? updatedModification[field] : defaultVal;
    }
  };

  const onPropUpdate = (updated) => {
    emit('update:modification', { ...modification.value, ...updated });
  };

  return {
    watchModification,
    updateModification,
    onPropUpdate,
  };
}

export const useInteractionProps = function (props, interactionComponent) {
  return computed(() => {
    const propsKeys = Object.keys(interactionComponent.value.component.props);

    const base = {
      ...(propsKeys.includes('modId') && { modId: props.modification.id }),
    };

    return propsKeys.reduce(
      (modification, key) => ({
        ...modification,
        ...(key in props.modification && { [key]: props.modification[key] }),
      }),
      base,
    );
  });
};
