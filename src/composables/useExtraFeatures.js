import { computed, watch } from 'vue';
import { useStore } from 'vuex';
import { useLayerSourceDataGroup } from './useLayerSourceData';
import { toJsDate } from '@utils/tm-date';
import { MODEL_DTA_LIDS } from '@keys/index';

// TODO Review extra features integration in app
// Helper flag to track extra features
let hasLoaded = false;

export default function useExtraFeatures({ activeScenario = null } = {}) {
  const store = useStore();

  const { link: lsLink, node: lsNode, generator: lsGenerator } = useLayerSourceDataGroup(MODEL_DTA_LIDS);
  let watcherHandler;

  const setExtraFeatures = ({ link = [], node = [], generator = [] } = {}) => {
    if (!hasLoaded) hasLoaded = true;
    if (lsLink) lsLink.setExtraFeaturesData(link);
    if (lsNode) lsNode.setExtraFeaturesData(node);
    if (lsGenerator) lsGenerator.setExtraFeaturesData(generator);
  };

  const activeScenarioModifications = computed(() => {
    const modifications = activeScenario?.value?.modifications;
    if (!modifications || !modifications.length) return [];
    return _filterNewModifications(modifications);
  });

  const extraFeatures = computed(() => _getFeaturesData(activeScenarioModifications.value, 'DTA'));

  const dateFilteredExtraFeatures = computed(() => {
    const calendarDate = store.getters['map/getCalendarDate']();
    return {
      link: _filterExtraFeaturesByDate(extraFeatures.value.link, calendarDate),
      node: _filterExtraFeaturesByDate(extraFeatures.value.node, calendarDate),
      generator: _filterExtraFeaturesByDate(extraFeatures.value.generator, calendarDate),
    };
  });

  const setModelDiffExtraFeatures = ({ sourceOptions, targetOptions } = {}) => {
    const [sourceMods, targetMods] = [sourceOptions, targetOptions].map(({ modifications, date }) => {
      if (!modifications.length) return [];
      const newMods = _filterNewModifications(modifications);
      if (!newMods.length) return [];
      return _filterExtraFeaturesByDate(newMods, date, { modificationFormat: true });
    });
    const uniqueModifications = [...new Set([...sourceMods, ...targetMods])];
    const featuresData = _getFeaturesData(uniqueModifications, 'DTA');
    setExtraFeatures(featuresData);
  };

  const startWatcher = () => {
    watcherHandler = watch(
      extraFeatures,
      (efs, oldEfs) => {
        const isPending = activeScenario?.value?.isPending;
        if (isPending) return;
        const hasChanges = JSON.stringify(efs) !== JSON.stringify(oldEfs);
        const hasNoFeatures = !efs.generator.length && !efs.link.length && !efs.node.length;
        if (hasChanges || hasNoFeatures) setExtraFeatures(efs);
      },
      { immediate: true },
    );
  };

  const endWatcher = () => {
    if (typeof watcherHandler === 'function') watcherHandler();
  };

  const preloadExtraFeatures = () => {
    if (hasLoaded) return;
    setExtraFeatures(extraFeatures.value);
  };

  return {
    dateFilteredExtraFeatures,
    setModelDiffExtraFeatures,
    preloadExtraFeatures,
    startWatcher,
    endWatcher,
    activeScenarioModifications, // exported for testing
    extraFeatures, // exported for testing
  };
}

function _filterNewModifications(modifications) {
  return modifications.filter((mod) => mod.mode === 'new');
}

function _filterExtraFeaturesByDate(features, date, { modificationFormat = false } = {}) {
  const testDate = toJsDate(date);
  return features.filter((f) => {
    const { dateFrom, dateTo } = modificationFormat ? f : f.properties.modification;
    return (dateFrom === null || toJsDate(dateFrom) <= testDate) && (dateTo === null || toJsDate(dateTo) >= testDate);
  });
}

function _getFeaturesData(extraFeatureModifications, modelType) {
  return extraFeatureModifications.reduce(
    (features, modification) => {
      const modificationType = modification.type;
      const featureData = _transformModification(modification, modelType);
      features[modificationType].push(featureData);
      if (modificationType === 'link' && modification.twoWay)
        features.link.push(_transformModification(modification, modelType, true));
      return features;
    },
    { link: [], node: [], generator: [] },
  );
}

function _transformModification(modification, modelType, isTwoWay) {
  const data = transformers[modelType][modification.type](modification, isTwoWay);
  data.properties.modification = modification;
  return data;
}

const transformers = {
  DTA: {
    link: _transformDTALink,
    node: _transformDTANode,
    generator: _transformDTAGenerator,
  },
};

function _getBaseJson() {
  return {
    geometry: {
      coordinates: null,
    },
    properties: {},
  };
}

function _extractCapacitySpeed(link, isTwoWay) {
  return isTwoWay
    ? { capacity: link.twoWayCapacity, speed: link.twoWaySpeed }
    : { capacity: link.capacity, speed: link.speed };
}

function _extractLanes(link, isTwoWay) {
  return isTwoWay ? link.twoWayLanes : link.lanes;
}

function _extractLinkCoords({ coordinates }, isTwoWay) {
  return isTwoWay ? [...coordinates].reverse() : [...coordinates];
}

function _extractLinkNodes({ source, target }, isTwoWay) {
  return isTwoWay ? { source: target, target: source } : { source, target };
}

function _transformDTALink(link, isTwoWay = false) {
  const featureData = _getBaseJson();
  featureData.geometry.coordinates = _extractLinkCoords(link, isTwoWay);
  const { source, target } = _extractLinkNodes(link, isTwoWay);
  const { capacity, speed } = _extractCapacitySpeed(link, isTwoWay);
  const lanes = _extractLanes(link, isTwoWay);
  const id = `extra-${link.id}${isTwoWay ? '-b' : ''}`;
  featureData.properties = {
    id,
    from_node_id: source,
    to_node_id: target,
    capacity,
    free_speed: speed,
    critical_speed: (speed / 6) * 5,
    lanes,
    edge_id: id,
    // length, missing - calculated from geometry during styling, if needed elsewhere, move it here (would require geometry creation)
    isvalid: true,
  };

  return featureData;
}

function _transformDTANode(node) {
  const featureData = _getBaseJson();
  featureData.geometry.coordinates = node.node;
  const id = `extra-${node.id}`;
  featureData.properties = { id, node_id: id };
  featureData.properties.ctrl_type = node.ctrlType ?? 'none'; // Currently nothing saved on node
  featureData.properties.use_movement = featureData.properties.ctrl_type !== 'none';
  return featureData;
}

function _transformDTAGenerator(generator) {
  const featureData = _getBaseJson();
  featureData.geometry.coordinates = generator.generator;
  const id = `extra-${generator.id}`;
  featureData.properties = {
    id,
    zone_id: id,
    node_id: generator.nodes,
    incoming_trips: generator.inTraffic,
    outgoing_trips: generator.outTraffic,
  };

  return featureData;
}
