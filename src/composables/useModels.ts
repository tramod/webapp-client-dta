import { ref, computed } from 'vue';
import useMainApiRequest from '@composables/useMainApiRequest';
// import { registerCacheInvalidation } from '@composables/useCache';
import { useI18n } from 'vue-i18n';
import { useStore } from 'vuex';
import { useToast } from 'primevue/usetoast';
import useFullScenario from '@composables/useFullScenario';

const cachedModels = ref<TmTrafficModel[]>([]);
// registerCacheInvalidation('models', invalidateModels); // TODO: no need to invalidate cache on user login/logout for now

export const ENABLED_MODEL_TYPES = import.meta.env.VITE_ENABLED_MODEL_TYPES?.split(', ') || [];

export default function useModels() {
  const { t } = useI18n();
  const { makeRequest } = useMainApiRequest();
  const store = useStore();
  const toast = useToast();
  const { invalidateFullScenario } = useFullScenario();

  const activeModel = computed(() => {
    const activeModelState: Partial<TmTrafficModel> = store.state.scenarios.model;
    if (!cachedModels.value.length) return activeModelState; // dummy return of default model state if we have no models fetched yet (only before the app is loaded)
    // with no id and only type specified - return default model of that type
    return getModel(activeModelState);
  });

  const computeRequestFrequency = computed(() => {
    const dtaFrequency = import.meta.env.VITE_COMPUTATION_REQUEST_FREQUENCY_DTA || 2000;
    return Number(dtaFrequency);
  });

  async function fetchModels() {
    if (isCacheAvailable()) return;

    await makeRequest({
      url: 'models',
      method: 'get',
      message: {
        error: {
          404: { summary: t('models.no models found'), severity: 'info' },
          default: t('models.error while fetching models'),
        },
      },
      onSuccess: (result: { trafficModels: TmTrafficModel[] }) => {
        cachedModels.value = result.trafficModels;
        store.commit('scenarios/SET_MODEL', activeModel.value);
      },
    });
  }

  async function refreshModel(modelType: TmModelType) {
    await makeRequest({
      url: `models/refresh`,
      method: 'post',
      data: { modelType },
      message: { error: t('models.error while trying to refresh the model') },
      onSuccess: () => {
        _trackActiveModelState();
        // invalidate scenarios to update their 'hasModelSectionsValid' state
        invalidateFullScenario();
      },
    });
  }

  async function switchActiveModel(modelId: number) {
    invalidateFullScenario();
    const newActiveModel = getModel({ id: modelId });
    store.commit('scenarios/SET_MODEL', newActiveModel);
    store.dispatch('map/activateItem', {}); // deactivate all items
  }

  function getModel({ id, type }: { id?: number; type?: TmModelType }): TmTrafficModel {
    let model;
    if (id) model = cachedModels.value.find((m) => m.id === id);
    if (type) model = cachedModels.value.find((m) => m.type === type && m.isDefault);
    if (!model) throw new Error('Traffic model is not available!');
    return model;
  }

  async function _getActiveModelState() {
    const modelType = activeModel.value.type;
    if (!modelType) throw new Error('No active model');

    let modelState = false;

    await makeRequest({
      url: `models/${modelType}`, // TODO: to be replaced with established form (models/:modelId)
      method: 'get',
      onSuccess: (result: { trafficModel: TmTrafficModel }) => {
        modelState = result.trafficModel.isValid;
      },
    });

    return modelState;
  }

  async function _trackActiveModelState() {
    store.dispatch('layout/startLoading');
    store.commit('scenarios/SET_MODEL', { isValid: false });

    let isModelStateValid = false;
    while (!isModelStateValid) {
      isModelStateValid = await _getActiveModelState();
      await new Promise((resolve) => setTimeout(resolve, computeRequestFrequency.value));
    }

    toast.add({
      severity: 'success',
      summary: t('models.computing traffic model'),
      detail: t('models.model restored'),
      life: 4000,
    });

    store.commit('scenarios/SET_MODEL', { isValid: true });
    store.dispatch('layout/finishLoading');
  }

  return {
    models: computed(() => cachedModels.value),
    activeModel,
    fetchModels,
    refreshModel,
    switchActiveModel,
    invalidateModels,
    getModelCalendarSetting,
    dtaModelOutputOptions,
  };
}

function getModelCalendarSetting() {
  const dtaSettings = { min: 7, max: 9.75, step: 0.25 };
  return dtaSettings;
}

function isCacheAvailable() {
  return !!cachedModels.value.length;
}

function invalidateModels() {
  cachedModels.value = [];
}

export const dtaModelOutputOptions = [
  { name: 'outflow', label: 'widgets.outflow', viewerPreset: { width: 'outFlow', color: 'none', text: 'outFlow' } },
  { name: 'inflow', label: 'widgets.inflow', viewerPreset: { width: 'inFlow', color: 'none', text: 'inFlow' } },
  {
    name: 'speed-width',
    label: 'widgets.speed as width',
    viewerPreset: { width: 'travelSpeed', color: 'none', text: 'outFlow' },
  },
  {
    name: 'speed-color',
    label: 'widgets.speed as color',
    viewerPreset: { width: 'none', color: 'travelSpeed', text: 'outFlow' },
  },
  {
    name: 'outflow-speed',
    label: 'widgets.outflow and speed',
    viewerPreset: { width: 'outFlow', color: 'travelSpeed', text: 'outFlow' },
  },
  {
    name: 'outflow-capacity',
    label: 'widgets.outflow and capacity',
    viewerPreset: { width: 'outFlow', color: 'outFlowIntensity', text: 'outFlow' },
  },
  {
    name: 'outflow-inflow',
    label: 'widgets.outflow vs inflow',
    viewerPreset: { width: 'flowDiff', color: 'flowDiff', text: 'flowDiff' },
  },
  {
    name: 'speed-free',
    label: 'widgets.speed vs free speed',
    viewerPreset: { width: 'none', color: 'freeSpeedDiff', text: 'outFlow' },
  },
  {
    name: 'speed-critical',
    label: 'widgets.speed vs critical speed',
    viewerPreset: { width: 'none', color: 'criticalSpeedDiff', text: 'outFlow' },
  },
  {
    name: 'delay-free',
    label: 'widgets.delay free speed',
    viewerPreset: { width: 'none', color: 'delayFreeSpeed', text: 'outFlow' },
  },
  {
    name: 'delay-critical',
    label: 'widgets.delay critical speed',
    viewerPreset: { width: 'none', color: 'delayCriticalSpeed', text: 'outFlow' },
  },
  {
    name: 'outflow-delay',
    label: 'widgets.outflow and delay',
    viewerPreset: { width: 'outFlow', color: 'delayFreeSpeed', text: 'outFlow' },
  },
];

export const dtaComparisonOutputOptions = [
  // source data
  'freeSpeedDiff',
  'capacityDiff',
  // section/context data
  'outFlowDiff',
  'inFlowDiff',
  'travelTimeDiff',
  // derived data
  'travelSpeedDiff',
  'outIntensityDiff',
];

export const dtaDataProperties = [
  // source data
  'length',
  'freeSpeed',
  'criticalSpeed',
  'capacity',
  // section/context data
  'outFlow',
  'inFlow',
  'travelTime',
  // derived data
  'travelSpeed',
  'outFlowIntensity',
  'freeSpeedTime',
  'criticalSpeedTime',
  'flowDiff',
  'freeSpeedDiff',
  'criticalSpeedDiff',
  'delayFreeSpeed',
  'delayCriticalSpeed',
];
