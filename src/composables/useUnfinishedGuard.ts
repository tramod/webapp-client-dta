import { ref, computed } from 'vue';
import { useRouter, onBeforeRouteLeave } from 'vue-router';
import { useStore } from 'vuex';
import useDialog from '@composables/useDialog';
import useEditMode from '@composables/useEditMode';
import type { Ref } from 'vue';
import type { RouteLocationNormalized } from 'vue-router';
import { useCurrentRouteParams } from '@composables/useCommonHelpers';

const ITEM_CHANGED_EXCLUDED_PROPS = ['id', 'type', 'updatedAt'];

type TmModelItem = { [key: string]: unknown };

type TmUnfinishedGuardObj = {
  newModel?: Ref<TmModelItem>;
  formerModel?: Ref<TmModelItem>;
  modelType?: 'scenario' | 'event' | 'modification';
  submitAction?: () => Promise<void> | void;
};

export default function useUnfinishedGuard({
  newModel,
  formerModel,
  modelType,
  submitAction,
}: TmUnfinishedGuardObj = {}) {
  const router = useRouter();
  const store = useStore();
  const dialog = useDialog();
  const { getScenarioId } = useCurrentRouteParams();
  const scenarioId = getScenarioId();
  const { computeScenario, saveScenario, exitEditSession } = useEditMode(scenarioId);

  const isScenarioToBeComputed = computed<boolean>(() => store.getters['scenarios/isScenarioToBeComputed'](scenarioId));
  const isScenarioToBeSaved = computed<boolean>(() => store.getters['scenarios/isScenarioToBeSaved'](scenarioId));
  const isOpenEditSession = computed<boolean>(() => store.getters['scenarios/isOpenEditSession'](scenarioId));
  const isUsingChangedScenario = computed<boolean>(() => store.getters['scenarios/isUsingChangedScenario'](scenarioId));
  const hasScenarioComputationReady = computed<boolean>(() =>
    store.getters['scenarios/hasScenarioComputationReady'](scenarioId),
  );

  const forcedScenarioAbandonment = ref(false);
  const forcedFormAbandonment = ref(false);

  const isModelChanged = computed(() => !!newModel && !!formerModel && _isChanged(newModel.value, formerModel.value));

  // this if prevents warnings when testing components in isolation and without router (since onBeforeRouteLeave hook checks for current route)
  if (!router.mocked) onBeforeRouteLeave(async (to) => await guardRouteLeave(to));

  async function guardRouteLeave(to: RouteLocationNormalized) {
    if (_shouldDisplayUnfinishedFormDialog()) _displayUnfinishedFormDialog({ to });
    else if (_shouldDisplayUnfinishedScenarioDialog(to)) _displayUnfinishedScenarioDialog({ to });
    else {
      if (_shouldExitEditSession(to)) await exitEditSession(scenarioId);
      // continue with the redirect
      return true;
    }

    // cancel the navigation and stay on the same route
    return false;
  }

  function _displayUnfinishedFormDialog({ to }: { to: RouteLocationNormalized }) {
    dialog.show({
      type: 'unfinished',
      data: { modelType },
      callback: {
        onSave: async () => {
          if (submitAction) await submitAction();
          router.push(to); // try to leave the route again
          dialog.close();
        },
        onLeave: () => {
          forcedFormAbandonment.value = true;
          router.push(to); // try to leave the route again
          dialog.close();
        },
      },
    });
  }

  function _displayUnfinishedScenarioDialog({ to }: { to: RouteLocationNormalized }) {
    dialog.show({
      type: 'unfinished',
      callback: {
        onCompute: async () => {
          await computeScenario(scenarioId);
          router.push(to); // try to leave again
          dialog.close();
        },
        onSave: async () => {
          if (isScenarioToBeComputed.value) await computeScenario(scenarioId, { saveAfter: true });
          else await saveScenario(scenarioId);
          router.push(to); // try to leave again
          dialog.close();
        },
        onLeave: async () => {
          forcedScenarioAbandonment.value = true;
          await exitEditSession(scenarioId);
          router.push(to); // try to leave again
          dialog.close();
        },
        onCancel: () => {
          dialog.close();
          // user can change the form after he canceled the scenario dialog
          // we should ask again even if he forced to leave the form before
          forcedFormAbandonment.value = false;
        },
      },
    });
  }

  function _shouldDisplayUnfinishedFormDialog() {
    if (forcedFormAbandonment.value) return false;
    if (!isOpenEditSession.value) return false;
    if (!isUsingChangedScenario.value) return false;
    return isModelChanged.value;
  }

  function _shouldDisplayUnfinishedScenarioDialog(to: RouteLocationNormalized) {
    if (forcedScenarioAbandonment.value) return false;
    if (!isOpenEditSession.value) return false;
    if (!isUsingChangedScenario.value) return false;
    if (hasScenarioComputationReady.value) return false;
    return (
      (isScenarioToBeComputed.value || isScenarioToBeSaved.value) &&
      ![
        'scenarios.events',
        'scenarios.events.import',
        'scenarios.events.create',
        'scenarios.events.edit',
        'scenarios.events.modifications',
        'scenarios.events.modifications.create',
        'scenarios.events.modifications.edit',
      ].includes(to.name as string)
    );
  }

  function _shouldExitEditSession(to: RouteLocationNormalized) {
    return (
      isOpenEditSession.value &&
      !_shouldDisplayUnfinishedFormDialog() &&
      !_shouldDisplayUnfinishedScenarioDialog(to) &&
      ![
        'scenarios.create',
        'scenarios.edit',
        'scenarios.events',
        'scenarios.events.import',
        'scenarios.events.create',
        'scenarios.events.edit',
        'scenarios.events.modifications',
        'scenarios.events.modifications.create',
        'scenarios.events.modifications.edit',
      ].includes(to.name as string)
    );
  }

  function _isChanged(newItem: TmModelItem, oldItem: TmModelItem) {
    let changed = false;
    for (const [key, value] of Object.entries(newItem)) {
      if (
        oldItem[key] !== undefined &&
        !ITEM_CHANGED_EXCLUDED_PROPS.includes(key) &&
        JSON.stringify(oldItem[key]) !== JSON.stringify(value) // handle primitive values as well as arrays (order is significant though)
      ) {
        changed = true;
      }
    }
    return changed;
  }

  return {
    forcedScenarioAbandonment,
    forcedFormAbandonment,
    isModelChanged,
    guardRouteLeave, // exported for testing
    dialog, // exported for testing
  };
}
