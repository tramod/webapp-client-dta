import { inject } from 'vue';
import { pluginKey } from '@plugins/cookie-consent';

export default function useDate() {
  const cc = inject(pluginKey);
  if (!cc) throw new Error('COOKIE CONSENT INJECT NOT READY');

  return cc;
}
