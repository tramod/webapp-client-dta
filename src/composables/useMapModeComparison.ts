import { onActivated, onDeactivated, onMounted, watch, computed } from 'vue';
import { addStyleOptions, onVmItemsReady } from 'vuemap';
import { useSelectInteraction } from '@composables/useFeatureSelection';

import type { WatchSource } from 'vue';

type ScenarioId = 'base' | number;
type HistoricalProps = { dates: TmDate[] };
type ModelProps = HistoricalProps & { scenarios: ScenarioId[] };

type LoadDiffDataOptions = {
  layer: any; // TS_TODO vuemap type,
  props: HistoricalProps | ModelProps;
  compareDataProvider: Function; // TS_TODO improve function signature
  sourceOptionsProvider: Function; // TS_TODO improve function signature
  targetOptionsProvider: Function; // TS_TODO improve function signature
  useScenario: any; // TS_TODO fix useScenario object type
  contextPreloadFn?: Function; // TS_TODO improve function signature
};
type GetLoadDiffDataOptions = Omit<LoadDiffDataOptions, 'layer'>;

type UseMapModeComparisonOptions = {
  useLayer: any; // TS_TODO vuemap useLayer
  props: any; // TS_TODO Props definition of components?
  layerStyle: any; // TS_TODO layer-style type defs?
  createFeatureFromFeature: any; //TS_TODO layer utils type defs?
  watchSource: WatchSource;
  watchCallback: Function; // TS_TODO improve function signature
  watchOnMount: boolean;
};

const parseScId = (scId: ScenarioId) => (scId === 'base' ? null : scId);

export async function getHistoricalOptions(props: HistoricalProps, position: number) {
  return { date: props.dates[position] };
}

// TS_TODO fix useScenario object type
export async function getModelOptions(
  props: ModelProps,
  position: number,
  { fetchScenario, getScenario, getModifications }: any,
) {
  const sourceScenarioId = parseScId(props.scenarios[position]);
  if (sourceScenarioId) await fetchScenario(sourceScenarioId);
  return {
    date: props.dates[position],
    scenario: getScenario(sourceScenarioId),
    modifications: getModifications(sourceScenarioId),
  };
}

async function loadDiffDataFn({
  layer,
  props,
  compareDataProvider,
  sourceOptionsProvider,
  targetOptionsProvider,
  useScenario,
  contextPreloadFn,
}: LoadDiffDataOptions) {
  const sourceOptions = await sourceOptionsProvider(props, 0, useScenario);
  const targetOptions = await targetOptionsProvider(props, 1, useScenario);
  const compareCtxData = await compareDataProvider({ sourceOptions, targetOptions });

  if (contextPreloadFn) contextPreloadFn({ sourceOptions, targetOptions });
  // TODO: update key pattern (model-sourceSectionKey-sourceDate-targetSectionKey-targetDate)
  onVmItemsReady(layer, () => layer.context.add('comparison', compareCtxData));
}

export function getLoadDiffDataFn(options: GetLoadDiffDataOptions) {
  return async (layer: any) => loadDiffDataFn({ layer, ...options });
}

export function useMapModeComparison({
  useLayer,
  props,
  layerStyle,
  createFeatureFromFeature,
  watchSource,
  watchCallback,
  watchOnMount,
}: UseMapModeComparisonOptions) {
  const layer = useLayer();
  const styleFn = addStyleOptions(
    layerStyle,
    computed(() => layer.context.getByFeature),
    { styleType: props.modeKey },
  );
  props.registerMode(props.modeKey, {
    layers: [
      {
        layer,
        style: styleFn,
      },
    ],
  });

  const { activateSelect, select } = useSelectInteraction({
    modeKey: props.modeKey,
    layers: [
      {
        layer,
        selectReplacerFn: createFeatureFromFeature,
        selectStyle: styleFn,
      },
    ],
  }) as { activateSelect: any; select: any };

  let watcherHandler: Function | undefined;

  function startWatcher() {
    watcherHandler = watch(watchSource, (...args) => watchCallback(layer, ...args), {
      immediate: true,
    });
  }

  function endWatcher() {
    if (typeof watcherHandler === 'function') watcherHandler();
  }

  // For some reason, onActivated doesnt fire here during first load, while in other modes does, maybe because of nesting?
  if (watchOnMount)
    onMounted(() => {
      activateSelect(select); // Manually activate select in these cases, same activated skip applies there
      startWatcher();
    });
  onActivated(() => startWatcher());

  onDeactivated(() => endWatcher());

  return {};
}
