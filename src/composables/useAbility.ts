import { inject } from 'vue';
import { pluginKey } from '@plugins/tm-ability/keys';

export default function useAbility() {
  const tmAbility = inject(pluginKey);
  if (!tmAbility) throw new Error('TMABILITY INJECT NOT READY');

  return tmAbility;
}
