import { inject } from 'vue';
import { pluginKey } from '@plugins/tm-date';

export default function useDate() {
  const tmDate = inject(pluginKey);
  if (!tmDate) throw new Error('TMDATE INJECT NOT READY');

  return tmDate;
}
