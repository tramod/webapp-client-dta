import { ref, computed, markRaw, reactive } from 'vue';
import { useStore } from 'vuex';

import useForeignApiRequest from '@composables/useForeignApiRequest';
import { transformGeojson } from '@utils/geojson';
import { LID_DTA_LINKS, LID_DTA_NODES, LID_DTA_GENERATORS } from '@keys/index';

const API_MODEL_DTA = import.meta.env.VITE_API_MODEL_DTA;
const MODEL_NAME_DTA = import.meta.env.VITE_MODEL_NAME_DTA;

const SOURCE_URLS = {
  [LID_DTA_LINKS]: `${API_MODEL_DTA}/edges/${MODEL_NAME_DTA}`,
  [LID_DTA_NODES]: `${API_MODEL_DTA}/nodes/${MODEL_NAME_DTA}`,
  [LID_DTA_GENERATORS]: `${API_MODEL_DTA}/zones/${MODEL_NAME_DTA}`,
};
const SOURCE_ID_KEYS = {
  [LID_DTA_LINKS]: 'edge_id',
  [LID_DTA_NODES]: 'node_id',
  [LID_DTA_GENERATORS]: 'zone_id',
};
const SOURCE_PARAMS = {};

const dtaCoordsTransformer = (data) => transformGeojson(data, { sourceProj: 'EPSG:4326', targetProj: 'EPSG:3857' });
const DATA_PARSER = {
  [LID_DTA_LINKS]: dtaCoordsTransformer,
  [LID_DTA_NODES]: dtaCoordsTransformer,
  [LID_DTA_GENERATORS]: dtaCoordsTransformer,
};

const sourceCache = ref({});
const extraFeaturesStore = ref({});
const sourceFeaturesCache = {};
const loadingCache = ref({});

export default function useLayerSourceData(layerKey, { autoFetch = true, clearOldData = false } = {}) {
  if (!layerKey) throw Error('SourceKey not provided');
  if (!SOURCE_URLS[layerKey]) throw Error(`SourceKey ${layerKey} not supported`);
  if (sourceCache.value[layerKey] === undefined) {
    sourceFeaturesCache[layerKey] = new Map();
    sourceCache.value[layerKey] = null;
    extraFeaturesStore.value[layerKey] = [];
    loadingCache.value[layerKey] = false;
  }
  const store = useStore();
  const { makeRequest } = useForeignApiRequest();

  const sourceData = computed(() => sourceCache.value[layerKey]);
  const isLoaded = computed(() => sourceCache.value[layerKey]?.features?.length > 0);
  const isLoading = computed(() => loadingCache.value[layerKey]);

  async function fetchSourceData() {
    if (isLoading.value) return isLoading.value;
    loadingCache.value[layerKey] = makeRequest({
      url: SOURCE_URLS[layerKey],
      method: 'get',
      ...(SOURCE_PARAMS[layerKey] && { params: SOURCE_PARAMS[layerKey]() }),
      message: { error: `FAILED TO LOAD MAP SOURCE DATA (${layerKey})` },
      onSuccess: (result) => {
        sourceCache.value[layerKey] = markRaw(DATA_PARSER[layerKey] ? DATA_PARSER[layerKey](result) : result);
        store.dispatch('map/addSourceLoaded', { sourceKey: layerKey, status: true });
      },
      onFailure: (err) => {
        console.log(err);
        store.dispatch('map/addBrokenMapMode', { layerKey });
        store.dispatch('map/addSourceLoaded', { sourceKey: layerKey, status: false });
      },
    });
    await isLoading.value;
  }

  const extraFeaturesData = computed(() => extraFeaturesStore.value[layerKey]);
  const setExtraFeaturesData = (features) => (extraFeaturesStore.value[layerKey] = markRaw(features));

  const features = sourceFeaturesCache[layerKey];
  function getFeatureData(featureId) {
    if (!isLoaded.value) throw Error(`Source data for ${layerKey} are not available`);
    const isExtraFeature = typeof featureId === 'string';
    return isExtraFeature ? _getExtraFeatureData(featureId) : _getSourceFeatureData(featureId);
  }

  function _getExtraFeatureData(featureId) {
    const feature = extraFeaturesData.value.find(
      (feature) => feature.properties[SOURCE_ID_KEYS[layerKey] || 'id'] === featureId,
    );
    return feature;
  }

  function _getSourceFeatureData(featureId) {
    if (features.has(featureId)) return features.get(featureId);
    const feature = sourceData.value.features.find(
      (feature) => feature.properties[SOURCE_ID_KEYS[layerKey] || 'id'] === featureId,
    );
    if (feature) features.set(featureId, feature);
    return feature;
  }

  function clearSourceData() {
    sourceFeaturesCache[layerKey] = new Map();
    loadingCache.value[layerKey] = false;
    sourceCache.value[layerKey] = null;
  }

  if (clearOldData) clearSourceData();
  if (autoFetch && !isLoading.value) fetchSourceData();

  return {
    fetchSourceData,
    getFeatureData,
    clearSourceData,
    setExtraFeaturesData,
    sourceData,
    extraFeaturesData,
    state: reactive({
      layerKey,
      isLoading,
      isLoaded,
    }),
  };
}

const nodeNeighborLinksCache = ref({
  [LID_DTA_LINKS]: new Map(),
});
export function getNodeNeighboringLinksData(nodeId) {
  const layerKey = LID_DTA_LINKS;
  if (!sourceCache.value[layerKey]) throw Error(`Source data for modelLinks are not available`);
  const propNamesProfile = 'DTA';
  return [
    ..._getNodeNeighboringSourceLinks(nodeId, layerKey, propNamesProfile),
    ..._getNodeNeighboringExtraLinks(nodeId, layerKey, propNamesProfile),
  ];
}

function _getNodeNeighboringSourceLinks(nodeId, layerKey, profile) {
  if (nodeNeighborLinksCache.value[layerKey].has(nodeId)) return nodeNeighborLinksCache.value[layerKey].get(nodeId);
  const features = _extractNeighboringLinks(sourceCache.value[layerKey].features, nodeId, profile);
  if (features) nodeNeighborLinksCache.value[layerKey].set(nodeId, features);
  return features;
}

function _getNodeNeighboringExtraLinks(nodeId, layerKey, profile) {
  return _extractNeighboringLinks(extraFeaturesStore.value[layerKey], nodeId, profile);
}

// TODO pull this to app wide config?
const propNames = {
  DTA: {
    source: 'from_node_id',
    target: 'to_node_id',
  },
};

function _extractNeighboringLinks(dataSource, nodeId, profile) {
  return dataSource.filter((feature) => {
    const source = feature.properties[propNames[profile].source];
    const target = feature.properties[propNames[profile].target];
    return source === nodeId || target === nodeId;
  });
}

/*
    opt {} outKey: layerKey
*/
export function useLayerSourceDataGroup(groupOpt) {
  const group = Object.entries(groupOpt).reduce((out, [outKey, layerKey]) => {
    out[outKey] = useLayerSourceData(layerKey, { autoFetch: false });
    return out;
  }, {});

  const groupList = Object.values(group);
  const isLoaded = computed(() => isLayerSourceGroupLoaded(groupList));
  const load = async () => loadLayerSourceGroup(groupList);

  return { load, isLoaded, ...group };
}

export function isLayerSourceGroupLoaded(lsGroup) {
  return lsGroup.every((item) => item.state.isLoaded === true);
}

export async function loadLayerSourceGroup(lsGroup) {
  if (lsGroup.length === 0 || isLayerSourceGroupLoaded(lsGroup)) return true;
  await Promise.all(
    lsGroup.map(async (lsItem) => {
      await lsItem.fetchSourceData();
    }),
  );
}
