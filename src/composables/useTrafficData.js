import { shallowRef, computed, markRaw, ref, readonly } from 'vue';
import { useStore } from 'vuex';
import useForeignApiRequest from '@composables/useForeignApiRequest';
import { MP_MODEL_DTA_VIEWER, MODEL_MAP_MODES, getModelModeByModelType } from '@keys/index';

const API_MODEL_DTA = import.meta.env.VITE_API_MODEL_DTA;
const MODEL_NAME_DTA = import.meta.env.VITE_MODEL_NAME_DTA;
const MODEL_CACHE_PREFIX = import.meta.env.VITE_MODEL_CACHE_PREFIX;
const BASE_MODEL_CACHE_CODE = `${MODEL_CACHE_PREFIX}-default`;
const DTA_TIME_INTERVALS_NUM = 12; // Might get dynamic later?, Or parse from result

const TRAFFIC_URLS = {
  [MP_MODEL_DTA_VIEWER]: (cacheCode = BASE_MODEL_CACHE_CODE) =>
    `${API_MODEL_DTA}/caches/${MODEL_NAME_DTA}/${cacheCode}`,
};
const TRAFFIC_PARSER = {
  [MP_MODEL_DTA_VIEWER]: (data) => {
    const trafficData = data.result;
    const output = {};
    for (const item of trafficData.result) {
      output[item.edgeId] = item;
    }
    output.modelInfo = { ...trafficData.modelInfo, numOfTimeIntervals: DTA_TIME_INTERVALS_NUM };
    return output;
  },
};

const TRAFFIC_PARAMS = {};

const trafficCache = shallowRef({});

export default function useTrafficData(mapModeKey) {
  if (!mapModeKey) throw Error('Map mode key not provided');
  if (!TRAFFIC_URLS[mapModeKey]) throw Error(`Data for map mode key ${mapModeKey} are not supported`);

  const store = useStore();
  const { makeRequest } = useForeignApiRequest();
  const isLoading = ref(false); // Stores last loading data until fetch is finished

  async function fetchTrafficData({ params, urlKey, cacheKey = mapModeKey, parserOptions } = {}) {
    isLoading.value = cacheKey;
    await makeRequest({
      url: TRAFFIC_URLS[mapModeKey](urlKey),
      method: 'get',
      ...(TRAFFIC_PARAMS[mapModeKey] && { params: TRAFFIC_PARAMS[mapModeKey](params) }),
      message: { error: `FAILED TO LOAD MAP TRAFFIC DATA (${cacheKey})` },
      onSuccess: (result) => {
        const data = TRAFFIC_PARSER[mapModeKey](result, parserOptions);
        trafficCache.value[cacheKey] = markRaw(data);
      },
      onFailure: (err) => {
        console.log(err);
        const isBaseModelError = !urlKey && MODEL_MAP_MODES.includes(mapModeKey);
        if (isBaseModelError) store.dispatch('map/addBrokenMapMode', { mapModeKey });
      },
    });
    if (isLoading.value === cacheKey) isLoading.value = false;
  }

  const getTrafficDataByKey = (cacheKey = mapModeKey) => trafficCache.value[cacheKey];
  const trafficData = computed(() => getTrafficDataByKey(mapModeKey));

  const areTrafficDataAvailable = (cacheKey = mapModeKey) => {
    const trafficData = getTrafficDataByKey(cacheKey);
    return !!trafficData && Object.keys(trafficData).length !== 1; // ModelInfo doesnot count
  };

  function clearTrafficData(cacheKey = mapModeKey) {
    trafficCache.value[cacheKey] = undefined;
  }

  return {
    fetchTrafficData,
    getTrafficDataByKey,
    areTrafficDataAvailable,
    clearTrafficData,
    trafficData,
    isLoading: readonly(isLoading),
  };
}

// Cache of cacheKey + scenarioId data entries
// 'COMMON' means no codeList entries so pull baseData from common traffic data
// cacheKey: []{}codeListString, traffic
const modelTrafficCache = {
  DTA: shallowRef({}),
};

export function useModelTrafficData(modelType) {
  if (!['DTA'].includes(modelType)) throw Error(`Data for model type '${modelType}' are not supported`);

  const mapModeKey = getModelModeByModelType(modelType);
  const modelTypeCache = modelTrafficCache[modelType];
  const commonTrafficData = useTrafficData(mapModeKey);

  const getTrafficDataByKey = ({ cacheKey = mapModeKey, codeList } = {}) => {
    const trafficData = commonTrafficData.getTrafficDataByKey(cacheKey);
    if (!codeList || Object.keys(codeList).length === 0) return trafficData;

    const modelCacheEntry =
      getCacheItem({ cacheKey, codeList }) ?? parseCodeListTrafficData({ cacheKey, codeList, trafficData });
    return { ...trafficData, ...modelCacheEntry };
  };

  function parseCodeListTrafficData({ cacheKey, codeList, trafficData } = {}) {
    const codeListData = {};
    for (const [modelId, extraId] of Object.entries(codeList)) {
      codeListData[extraId] = trafficData[modelId];
    }

    setCacheItem({ cacheKey, codeList, traffic: codeListData });
    return codeListData;
  }

  function getCacheItem({ cacheKey, codeList }) {
    const entries = modelTypeCache.value[cacheKey];
    if (!entries || entries.length === 0) return null;

    const codeListString = JSON.stringify(codeList);
    const codeListEntry = entries.find((entry) => entry.codeListString === codeListString);
    return codeListEntry?.traffic;
  }

  function setCacheItem({ cacheKey, traffic, codeList }) {
    const codeListString = JSON.stringify(codeList);
    if (!modelTypeCache.value[cacheKey]) modelTypeCache.value[cacheKey] = [];
    modelTypeCache.value[cacheKey].push({ traffic, codeListString });
  }

  return { ...commonTrafficData, getTrafficDataByKey };
}

// Full clear state for testing
export const clearTrafficDataState = () => {
  trafficCache.value = {};
};
