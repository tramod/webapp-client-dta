import { inject } from 'vue';
import { pluginKey } from '@plugins/tm-mobile';

export default function useMobile() {
  const isMobile = inject(pluginKey);
  if (!isMobile) throw new Error('MOBILE INJECT NOT READY');

  return isMobile;
}
