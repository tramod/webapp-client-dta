import { intersection } from 'lodash-es';
import { computed } from 'vue';
import { useStore } from 'vuex';
import useMapModes from '@composables/useMapModes';

export type TmDataModeObj = {
  name: TmDataMode;
  icon: string;
  tooltip: string;
  isBroken?: boolean;
  mapModes?: TmMapMode[];
};

const defaultDataModes: TmDataModeObj[] = [
  { name: 'model', icon: 'ri-send-plane-2-line', tooltip: 'model' },
  { name: 'comparison', icon: 'ri-tools-line', tooltip: 'comparison' },
];

export const ENABLED_DATA_MODES = import.meta.env.VITE_ENABLED_DATA_MODES?.split(', ') || [];

export default function useDataModes(dataModes = defaultDataModes) {
  const store = useStore();
  const { getMapModes, brokenMapModes } = useMapModes();
  const currentDataMode = computed<TmDataMode>(() => store.getters['map/getDataMode']);

  const availableDataModes = computed(() => {
    const enabledModes = dataModes.filter((dm) => ENABLED_DATA_MODES.includes(dm.name));

    return enabledModes.reduce((activeModes: TmDataModeObj[], dataMode) => {
      const mapModes = getMapModes(dataMode.name);
      if (!mapModes.length) return activeModes;
      const isBroken = intersection(brokenMapModes.value, mapModes).length > 0;
      activeModes.push({ ...dataMode, isBroken, mapModes });
      return activeModes;
    }, []);
  });

  const isActiveDataMode = (dataMode: TmDataModeObj) => dataMode.name === currentDataMode.value;

  const setDataMode = (dataMode: TmDataModeObj) => {
    const defaultMapMode = dataMode.mapModes?.[0]; // the first map mode in the list is considered the default one to navigate to
    if (isActiveDataMode(dataMode)) store.dispatch('layout/togglePanel', { panelSide: 'right' });
    else store.dispatch('map/setMapMode', { mode: defaultMapMode, openWidgetPanel: true });
  };

  return {
    enabledDataModes: ENABLED_DATA_MODES,
    currentDataMode,
    availableDataModes,
    isActiveDataMode,
    setDataMode,
  };
}
