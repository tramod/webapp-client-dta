import { shallowRef, reactive, computed, watch, onActivated, onDeactivated, onUnmounted } from 'vue';
import { useStore } from 'vuex';
import { useSelectPaging, addStyleOptions } from 'vuemap';
import { useMainMap } from '@components/map/map';
import useLayerSourceData from '@composables/useLayerSourceData';
import useMobile from '@composables/useMobile';

const selects = shallowRef([]);
const activeSelect = shallowRef(null);
const selectedData = shallowRef(null);

const addSelect = (selectObj) => selects.value.push(selectObj);
const removeSelect = (selectObj) => {
  deactivateActiveSelect(selectObj);
  selects.value = selects.value.filter((item) => item !== selectObj);
};
const deactivateActiveSelect = (selectObj) => {
  if (!activeSelect.value || (selectObj !== undefined && selectObj !== activeSelect.value)) return;
  deselect();
  activeSelect.value.setActive(false);
  activeSelect.value = null;
};
const activateSelect = (selectObj) => {
  deactivateActiveSelect();
  activeSelect.value = selectObj;
  activeSelect.value.setActive(true);
};
const deselect = () => {
  if (!activeSelect.value) return;
  activeSelect.value.clearSelect();
};

export function useSelectInteraction({
  modeKey,
  layers /* []{
  layer,
  selectStyle,
  selectReplacerFn,
  extraDataFn,
  otherLayers,
  */,
} = {}) {
  const store = useStore();
  const isMobile = useMobile();
  const map = useMainMap();
  const { layersConfig, selectConfig } = parseLayersConfig();

  // From viewerPopup
  const selectOptions = {
    mode: 'replacer',
    multiLayer: selectConfig,
    hovering: true,
    hoverOptions: {
      useHoverLayer: false,
      hoveredProperty: false,
    },
    olSelectOptions: {
      hitTolerance: isMobile() ? 10 : 0, // assess size on mobile dev
    },
  };

  const selectObj = useSelectPaging(map, selectOptions);
  addSelect(selectObj);

  // From viewerPopup
  function onselectFeatureTransform({ olLayer, feature }) {
    const fId = feature.getId();
    const lId = olLayer.get('id');
    const { layer, extraDataFn } = layersConfig[lId];
    const hasContext = layer.context !== undefined;
    return {
      fId,
      lId,
      feature,
      olLayer,
      properties: feature.getProperties(),
      extraData: reactive({
        ...(hasContext && { context: computed(() => layer.context.contextData[fId]) }),
        ...(extraDataFn && { ...extraDataFn(feature) }),
      }),
    };
  }

  // From vuemap OlSelectPopup
  watch(
    () => selectObj.paging.selectedItem,
    (item) => {
      if (!item.feature) return updateState();
      updateState(onselectFeatureTransform(item));
    },
  );

  function updateState(item = null) {
    selectedData.value = item;
    if (item) store.dispatch('layout/openPanel', { panelSide: 'right' }); // open widget panel in case it is closed or folded
    store.dispatch(`map/${item ? 'show' : 'hide'}Widget`, { name: 'mapPopup', mode: modeKey });
  }

  function resolveReplaceFn(fn, layer) {
    const { getFeatureData } = useLayerSourceData(layer.id, { autoFetch: false });
    return (f) => fn(f, getFeatureData);
  }
  function parseLayersConfig() {
    const selectConfig = layers.map((layerConfig) => {
      const { layer, selectStyle, selectReplacerFn } = layerConfig;
      return {
        layer,
        style: addStyleOptions(selectStyle, { highlight: true }),
        ...(selectReplacerFn && { replacerFn: resolveReplaceFn(selectReplacerFn, layer) }),
      };
    });

    const layersConfig = layers.reduce((config, layerConfig) => {
      config[layerConfig.layer.id] = layerConfig;
      return config;
    }, {});

    return { layersConfig, selectConfig };
  }

  function _activateSelect() {
    activateSelect(selectObj);
  }
  onActivated(_activateSelect);

  onDeactivated(() => {
    updateState();
    deactivateActiveSelect(selectObj);
  });

  onUnmounted(() => {
    if (selectedData.value) updateState();
    removeSelect(selectObj);
    selectObj.destroy();
  });

  return {
    activateSelect: _activateSelect, // For manual overrides
    select: selectObj,
  };
}

export function useSelectManager() {
  return {
    selectedData,
    deselect,
    activeSelect,
  };
}
