import useFullScenario from '@composables/useFullScenario';
import useLayerSourceData from '@composables/useLayerSourceData';
import { dateIntervalIntersection } from '@utils/tm-date';

export default function useModelModifications() {
  const { getEvents, getModifications } = useFullScenario();

  function extractMods({ scenarioId, scenario, eventFilters, modFilters, listReducer, listReducerFormat = [] } = {}) {
    if (!scenarioId && (!scenario || Object.keys(scenario).length === 0)) return [];
    const srcEvents = scenario?.events ?? getEvents(scenarioId);
    const events = applyFilters(srcEvents, eventFilters);
    const mods = events.reduce((output, event) => {
      const srcMods = event?.modifications ?? getModifications(scenarioId, event.id);
      const mods = applyFilters(srcMods, modFilters);
      for (const mod of mods) {
        output.push({ event, modification: mod });
      }
      return output;
    }, []);
    return listReducer ? mods.reduce(listReducer, listReducerFormat) : mods;
  }

  return { extractMods };
}

// Special use-case might be reusable? (or move to MMViewer later)
export function useLinksModificationContext(layerKey) {
  const { extractMods } = useModelModifications();
  const { getFeatureData: getModelLink } = useLayerSourceData(layerKey);

  return ({ scenarioId, date }) =>
    extractMods({
      scenarioId,
      eventFilters: [includedEventsFilter],
      modFilters: [linkExistingFilter, dateIntersectionFilterFn(date)],
      listReducer: (ctx, { modification }) => {
        const { capacity, speed, linkId } = modification;
        const linkFlags = getLEModClassification({ modification }, getModelLink);
        for (const id of linkId) {
          ctx[id] = mergeLEContext({ capacity, speed, ...linkFlags }, ctx[id]);
        }
        return ctx;
      },
      listReducerFormat: {},
    });
}

// Merge with restrictive strategy
const mergeLEContext = (target, source) => {
  if (!source) return target;
  const isClosed = target.isClosed || source.isClosed;
  const isChanged = !isClosed;
  const isRestricted = !isClosed && (target.isRestricted || source.isRestricted);
  const capacity = getLower(source, target, 'capacity');
  const speed = getLower(source, target, 'speed');
  return { isClosed, isChanged, isRestricted, capacity, speed };
};

const getLower = (a, b, property) => (a[property] < b[property] ? a[property] : b[property]);

function applyFilters(list, filters) {
  if (!filters) return list;
  return list.filter((item) => filters.every((filterFn) => filterFn(item)));
}

// TODO move these reusable helpers to something like utils?
// Reusable event/mod filters
export const includedEventsFilter = ({ included }) => included === true;
export const linkExistingFilter = ({ mode, type }) => type === 'link' && mode === 'existing';
export const dateIntersectionFilterFn = dateIntervalIntersection;

// Reusable reducer helpers
export const getBaseModData = ({ event, modification }) => {
  return {
    eventId: event.id,
    modId: modification.id,
    ...modification,
  };
};

const getLinkFeatures = (linkIds, featGetter) => linkIds.map((id) => featGetter(id));
const findLinkMaxes = (links) =>
  links.reduce(
    (max, link) => {
      // TODO remove later, temp as code very occasionally bugs here???
      if (!link) {
        console.warn('Invalid max input');
        console.log(links);
        return max;
      }
      const speed = Math.round(link.properties.free_speed);
      const capacity = Math.round(link.properties.capacity);
      if (speed > max.originalSpeed) max.originalSpeed = speed;
      if (capacity > max.originalCapacity) max.originalCapacity = capacity;
      return max;
    },
    { originalSpeed: null, originalCapacity: null },
  );
export const getLEModClassification = ({ modification }, featGetter) => {
  const { capacity, speed, linkId } = modification;
  const { originalCapacity, originalSpeed } = findLinkMaxes(getLinkFeatures(linkId, featGetter));
  const isClosed = speed === 0 || capacity === 0;
  const isChanged = !isClosed;
  const isRestricted = !isClosed && (speed < originalSpeed || capacity < originalCapacity);
  return { isClosed, isChanged, isRestricted };
};

// TODO move to utils?
// Expect two object of objects
export function mergeCtxs(a, b) {
  for (const bKey of Object.keys(b)) {
    const bVal = b[bKey];
    if (!a[bKey]) a[bKey] = bVal;
    else a[bKey] = { ...a[bKey], ...bVal };
  }
  return a;
}
