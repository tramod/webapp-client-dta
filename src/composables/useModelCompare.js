import { useModelDTATrafficInterval } from '@composables/useModelTrafficInterval';
import { getScenarioModelSectionByDate } from '@utils/model-sections';
import { useLinksModificationContext, mergeCtxs } from '@composables/useModelModifications';
import { LID_DTA_LINKS } from '@keys/index';

export default function useModelCompare() {
  const { getDataset, getCompareDataById } = useModelCompareData();

  const provideCompareData = async ({ sourceOptions, targetOptions } = {}) => {
    const [sourceData, targetData] = await Promise.all([sourceOptions, targetOptions].map(getDataset));

    const featureIds = [...new Set([...Object.keys(sourceData), ...Object.keys(targetData)])];

    const compareData = featureIds.reduce((data, fId) => {
      data[fId] = getCompareDataById(fId, sourceData, targetData);
      return data;
    }, {});

    return compareData;
  };

  return {
    provideCompareData,
  };
}

export function useModelCompareData() {
  const useModelTrafficInterval = useModelDTATrafficInterval;
  const { getLinksIntervalCtx } = useModelTrafficInterval();
  const getLinksModificationCtx = useLinksModificationContext(LID_DTA_LINKS);

  const compareProp = 'outFlow';

  const getDataset = async ({ scenario, date } = {}) => {
    const { sectionKey, extraFeaturesCodeList } = getScenarioModelSectionByDate(scenario, date);
    const hourVolumes = await getLinksIntervalCtx({
      date,
      modelKey: sectionKey,
      scenarioId: scenario.id,
      extraFeaturesCodeList,
    });
    const modificationData = getLinksModificationCtx({ scenarioId: scenario.id, date });
    return mergeCtxs(hourVolumes, modificationData);
  };

  const getCompareDataById = (fId, sourceData, targetData) => {
    const sourceTraffic = getVolume(sourceData, fId);
    const targetTraffic = getVolume(targetData, fId);
    return {
      source: sourceData[fId],
      target: targetData[fId],
      // TODO Temp compat for MapInfo
      // Ideally change MapInfo to extract needed stuff in similar way to layer-styling from general context data
      diff: sourceTraffic - targetTraffic,
      sourceTraffic,
      targetTraffic,
    };
  };

  const getVolume = (dateset, id) => dateset[id]?.[compareProp] ?? 0;

  return { getDataset, getCompareDataById, getVolume };
}
