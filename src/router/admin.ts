import AdminLayout from '@layouts/Admin.vue';
import store from '@store/index';
import type { RouteRecordRaw } from 'vue-router';

const LoginView = () => import('@views/admin/Login.vue');
const UsersView = () => import('@views/admin/Users.vue');

const routes: RouteRecordRaw[] = [
  {
    path: '/admin',
    name: 'admin.login',
    component: LoginView,
    meta: {
      layout: AdminLayout,
    },
  },
  {
    path: '/admin/users',
    name: 'admin.users',
    component: UsersView,
    meta: {
      requiresAuth: true,
      layout: AdminLayout,
    },
    beforeEnter: () => store.getters['auth/hasRole'](100), // admin only
  },
];

export default routes;
