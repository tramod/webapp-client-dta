import { createRouter, createWebHistory } from 'vue-router';
import errors from './errors';
import home from './home';
import settings from './settings';
import scenarios from './scenarios';
import user from './user';
import admin from './admin';
import models from './models';
import store from '@store/index';

// 1.1 compatibility redirects
const redirects = [
  { path: '/public', redirect: { name: 'home' } },
  { path: '/editor', redirect: { name: 'home' } },
];

const routes = [...errors, ...home, ...settings, ...scenarios, ...user, ...admin, ...redirects, ...models];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

// route guards
router.beforeEach((to, from) => {
  // Authentication check; Authorization is at Per-Route Guard
  if (to.matched.some((record) => record.meta.requiresAuth) && !store.state.auth.user.id) {
    // redirect unauthenticated user to the login view
    const routeName = to.name as string;
    const loginRouteName = routeName.startsWith('admin.') ? 'admin.login' : 'user.login';
    router.push({ name: loginRouteName });
  } else {
    // Preserve query param between routes
    if (Object.keys(to.query).length === 0 && Object.keys(from.query).length !== 0) {
      // this also doubles every other route guard check - maybe try older vue-router approach using 'next()'?
      router.push({ ...to, query: from.query });
    } else {
      // route is validated
      return true;
    }
  }
});

export { routes };
export default router;
