import { reactive } from 'vue';
import { mockScenarioEndpoint } from './mockAxios.js';
import useFullScenario from '@composables/useFullScenario';
import { flushPromises } from '@vue/test-utils';

const testRouter = reactive({
  currentRoute: { value: { name: '', params: { id: null } } },
});

const testRoute = reactive({
  params: { id: null },
});

const testStore = reactive({
  dispatch: () => {},
  getters: {
    'map/getActiveItemId': () => null,
    'scenarios/isCacheExpired': () => false,
    'scenarios/isUsingChangedScenario': () => true,
  },
  state: { scenarios: { cache: { scenarios: { enabled: true } } } },
});

let fsUnmount, fsToast, fsWrapper;

const fsMount = async () => {
  const { mount, globalToastMock } = await import('./mountComposable.js');
  fsToast = globalToastMock;
  fsWrapper = await mount(useFullScenario, {
    router: testRouter,
    route: testRoute,
    store: testStore,
  });
  fsUnmount = fsWrapper.unmount;
  await flushPromises();
};

const activateScenario = async (scenario, { code = 200, evId = null, modId = null, localStore = null } = {}) => {
  const id = scenario.id;
  mockScenarioEndpoint({ response: { scenario }, scenarioId: id, code });
  fsWrapper.invalidateFullScenario(id);
  testRouter.currentRoute.value.params = { id, evId, modId };
  testRoute.params = { id, evId, modId };
  const getterMock = ({ itemLevel }) => {
    switch (itemLevel) {
      case 'scenario':
        return id;
      case 'event':
        return evId;
      case 'modifications':
        return modId;
    }
  };
  testStore.getters['map/getActiveItemId'] = getterMock;
  if (localStore) localStore.getters['map/getActiveItemId'] = getterMock;
  if (id == fsWrapper.activeScenarioId.value) await fsWrapper.fetchScenario(id); // tests might want to activate already active scenario
  await flushPromises();
};

const deactivateScenario = async () => {
  const previouslyActiveScenarioId =
    testRouter.currentRoute.value.params.id || testStore.getters['map/getActiveItemId']();
  testRouter.currentRoute.value.params.id = null;
  testRoute.params.id = null;
  testStore.getters['map/getActiveItemId'] = () => null;
  if (previouslyActiveScenarioId) fsWrapper.invalidateFullScenario(previouslyActiveScenarioId);
  await flushPromises();
};

export { fsMount, fsUnmount, fsToast, activateScenario, deactivateScenario };
