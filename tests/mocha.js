import 'mocha/mocha.js'; // global mocha
import 'mocha/mocha.css'; // mocha css
import { expect } from 'chai';
import sinon from 'sinon';
import { restoreAxiosMock, resetAxiosRequestHistory } from './mockAxios';
import { fsMount, fsUnmount } from './scenarioSetup.js';

mocha.setup('bdd'); //  BDD interface
mocha.setup({ timeout: 5000 });
// mocha.checkLeaks(); // TODO: Error: global leak(s) detected: '__VUE__
window.expect = expect;
window.sinon = sinon;
window.sleep = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

before(async () => {
  // mount useFullScenario before all tests
  // so we can observe and force the active scenario fetching in tests
  await fsMount();
});

afterEach(() => {
  // clear axios mock request history after each test
  // (not before since some requests are made on mount)
  resetAxiosRequestHistory();
  // reset history of all spies
  sinon.resetHistory();
});

after(() => {
  // unmount useFullScenario
  fsUnmount();
  // restore axios mock after all tests
  restoreAxiosMock();
});

Promise.all([import('./App.spec.js'), import('./store'), import('./components'), import('./composables')]).then(() => {
  mocha.run();
});
