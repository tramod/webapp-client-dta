import InputSelector from '@components/core/InputSelector.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;
const testName = 'testName';
const testValue = 101;
const testLabel = 'test label';

describe('InputSelector.spec.js', () => {
  it('displays input with pre-selected value', async () => {
    wrapper = mount(InputSelector, {
      props: { selected: testValue, name: testName },
    });
    await flushPromises();

    const input = wrapper.find(`input[name=${testName}]`);
    expect(input.attributes('name')).to.equal(testName);
    expect(input.element.value).to.equal(testValue.toString());
  });

  it('updates and emits its value if directly editable', async () => {
    const changedValue = 200;

    wrapper = mount(InputSelector, {
      props: { selected: testValue, name: testName, directInput: true },
    });
    await flushPromises();

    const input = wrapper.find(`input[name=${testName}]`);
    expect(input.element.value).to.equal(testValue.toString());
    input.setValue(changedValue.toString());
    // blur is needed here probably because of the separate validation watch
    input.trigger('blur');
    await flushPromises();

    expect(input.element.value).to.equal(changedValue.toString());

    expect(wrapper.emitted()).to.have.property('update:selected');
    expect(wrapper.emitted()['update:selected']).to.have.length(1);
    expect(wrapper.emitted()['update:selected'][0][0]).to.deep.equal(changedValue);
  });

  it('accepts and eforces validation rules', async () => {
    const testRules = 'required';

    wrapper = mount(InputSelector, {
      props: { selected: testValue, name: testName, rules: testRules, directInput: true },
    });
    await flushPromises();

    const input = wrapper.find(`input[name=${testName}]`);
    expect(input.element.value).to.equal(testValue.toString());

    const validationError = wrapper.find(`small#help-input-${testName}`);
    expect(validationError.exists()).to.equal(true);

    await wrapper.setProps({ selected: null });
    await flushPromises();
    expect(validationError.text()).to.equal(`Pole ${testName} je povinné`);

    await wrapper.setProps({ selected: 200 });
    await flushPromises();
    expect(validationError.text()).to.equal('');
  });

  it('has label and emits label click', async () => {
    wrapper = mount(InputSelector, {
      props: { selected: testValue, name: testName, label: testLabel },
    });
    await flushPromises();

    const input = wrapper.find(`input[name=${testName}]`);
    expect(input.exists()).to.equal(true);

    const label = wrapper.find('.button-label');
    expect(label.exists()).to.equal(true);
    expect(label.text()).to.equal(testLabel);

    label.trigger('click');
    await flushPromises();
    expect(wrapper.emitted()).to.have.property('label:clicked');
  });

  it('has clear button and emits clear click', async () => {
    wrapper = mount(InputSelector, {
      props: { selected: testValue, name: testName, clearable: true },
    });
    await flushPromises();

    const input = wrapper.find(`input[name=${testName}]`);
    expect(input.exists()).to.equal(true);

    const clearButton = wrapper.find('.clear-icon');
    expect(clearButton.exists()).to.equal(true);

    await clearButton.trigger('click'); // TODO: this is not propagating in vitest - why?
    await flushPromises();
    expect(wrapper.emitted()).to.have.property('clear:clicked');
  });

  it('accepts array as value and displays it as string', async () => {
    const testValue = ['asd', 'sdf'];

    wrapper = mount(InputSelector, {
      props: { selected: testValue, name: testName, type: 'array' },
    });
    await flushPromises();

    const input = wrapper.find(`input[name=${testName}]`);
    expect(input.exists()).to.equal(true);
    expect(input.element.value).to.equal('asd, sdf');
  });

  it('displays suffix string in case of Number value', async () => {
    const testSuffix = 'asdf';

    wrapper = mount(InputSelector, {
      props: { selected: testValue, suffix: testSuffix, name: testName },
    });
    await flushPromises();

    const input = wrapper.find(`input[name=${testName}]`);
    expect(input.exists()).to.equal(true);
    expect(input.element.value).to.equal(`${testValue} ${testSuffix}`);
  });

  it('emits nothing when disabled', async () => {
    wrapper = mount(InputSelector, {
      props: { selected: testValue, label: testLabel, clearable: true, name: testName, disabled: true },
    });
    await flushPromises();

    const clearButton = wrapper.find('.clear-icon');
    expect(clearButton.exists()).to.equal(true);
    expect(clearButton.attributes('disabled')).to.equal(''); // not undefined
    clearButton.trigger('click');
    await flushPromises();
    expect(wrapper.emitted()).to.deep.equal({});

    const label = wrapper.find('.button-label');
    expect(label.exists()).to.equal(true);
    // emit is disabled only using css here, so I can not test .trigger('click') here
    expect(label.classes()).to.include.members(['disabled']);
  });

  it('has optional border color classes', async () => {
    const testBorderColor = 'red';

    wrapper = mount(InputSelector, {
      props: { selected: testValue, name: testName, label: testLabel, borderColor: testBorderColor },
    });
    await flushPromises();

    const label = wrapper.find('.button-label');
    expect(label.exists()).to.equal(true);
    expect(label.classes()).to.include.members([`border-color-${testBorderColor}`]);
  });

  it('can change its active state', async () => {
    wrapper = mount(InputSelector, {
      props: { selected: testValue, name: testName, label: testLabel, active: true },
    });
    await flushPromises();

    const label = wrapper.find('.button-label');
    expect(label.exists()).to.equal(true);
    expect(label.classes()).to.include.members(['active']);

    await wrapper.setProps({ active: false });
    await flushPromises();
    expect(label.classes()).to.not.include.members(['active']);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
