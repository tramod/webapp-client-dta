import Button from '@components/core/Button.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;
const classesByButtonTypes = {
  basic: [],
  cancel: ['tm-fullwidth'],
  confirm: ['tm-fullwidth'],
  back: ['p-button-rounded', 'tm-back'],
  navigation: ['p-button-rounded', 'tm-navigation', 'p-button-icon-only'],
  create: ['p-button-rounded'],
  toggle: ['p-button-secondary', 'tm-navigation', 'tm-fullwidth'],
  action: ['p-button-rounded'],
};

describe('Button.spec.js', () => {
  it('displays button label', async () => {
    wrapper = mount(Button, {
      props: { label: 'test', type: 'basic' },
    });
    await flushPromises();

    const buttonLabel = wrapper.find('.p-button-label');
    expect(buttonLabel.text()).to.equal('test');
  });

  it('displays prepended icon', async () => {
    wrapper = mount(Button, {
      props: { type: 'basic', icon: 'pi-plus' },
    });
    await flushPromises();

    const plusIcon = wrapper.find('.pi.pi-plus.p-button-icon');
    expect(plusIcon.exists()).to.equal(true);
  });

  describe('sets proper button classes', () => {
    for (const [type, classes] of Object.entries(classesByButtonTypes)) {
      it('type ' + type, async () => {
        wrapper = mount(Button, {
          props: { type: type },
        });
        await flushPromises();

        const button = wrapper.find('button');
        expect(button.classes()).to.include.members(classes);
      });
    }
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
