import SplitButton from '@components/core/SplitButton.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

const opt1Command = sinon.spy();
const opt2Command = sinon.spy();
const opt3Command = sinon.spy();
const testOptions = [
  {
    label: 'opt 1',
    command: opt1Command,
  },
  {
    label: 'opt 2',
    disabled: true,
    command: opt2Command,
  },
  {
    label: 'opt 3',
    command: opt3Command,
  },
];

describe('SplitButton.spec.js', () => {
  it('displays split button as default & menu', async () => {
    const testLabel = 'test label';
    wrapper = mount(SplitButton, {
      props: { label: testLabel, options: testOptions },
    });
    await flushPromises();

    const defButton = wrapper.find('.p-button-label');
    expect(defButton.exists()).to.equal(true);
    expect(defButton.text()).to.equal(testLabel);

    const menuButton = wrapper.find('.p-splitbutton-menubutton');
    expect(menuButton.exists()).to.equal(true);
  });

  it('displays menu options on click', async () => {
    wrapper = mount(SplitButton, {
      props: { options: testOptions },
    });
    await flushPromises();

    const menuButton = wrapper.get('.p-splitbutton-menubutton');
    menuButton.trigger('click');
    await flushPromises();

    // get dropdown menu directly using querySelector since it is attached to the body and therefore can not be obtained via vue wrapper
    const dropdownMenuElement =
      wrapper.element.parentElement.parentElement.parentElement.querySelector('.p-menu-overlay');
    expect(dropdownMenuElement).to.not.equal(null);

    const menuItems = dropdownMenuElement.querySelectorAll('.p-menuitem');
    expect(menuItems).to.not.equal(null);
    expect(menuItems.length).to.equal(3);

    menuItems.forEach((item, index) => {
      const optionItem = testOptions[index];

      expect(item.innerHTML.includes(optionItem.label)).to.equal(true);
      expect(item.classList.contains('p-disabled')).to.equal(!!optionItem.disabled);
    });
  });

  it('calls action on option click', async () => {
    wrapper = mount(SplitButton, {
      props: { options: testOptions },
    });
    await flushPromises();

    const menuButton = wrapper.get('.p-splitbutton-menubutton');

    let index = 0;
    for (const { command } of testOptions) {
      menuButton.trigger('click');
      await flushPromises();

      const dropdownMenuElement =
        wrapper.element.parentElement.parentElement.parentElement.querySelector('.p-menu-overlay');
      const item = dropdownMenuElement.querySelectorAll('.p-menuitem')[index];
      const isDisabled = item.classList.contains('p-disabled');
      const link = item.querySelector('.p-menuitem-link');
      await link.click();
      await flushPromises();

      expect(command.calledOnce).to.equal(!isDisabled); // called only for non-disabled options
      index++;

      if (isDisabled) {
        // clicking the disabled option did not close menu -> do it manually
        menuButton.trigger('click');
        await flushPromises();
      }
    }
  });

  it('emits default button click event', async () => {
    wrapper = mount(SplitButton, {
      props: { options: testOptions },
    });
    await flushPromises();

    const defButton = wrapper.find('.p-button-label');
    defButton.trigger('click');
    await flushPromises();

    expect(wrapper.emitted()).to.have.property('default:clicked');
    expect(wrapper.emitted()['default:clicked']).to.have.length(1);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
