import Breadcrumb from '@components/core/Breadcrumb.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

describe('Breadcrumb.spec.js', () => {
  it('displays one link per rout level + home', async () => {
    wrapper = mount(Breadcrumb, {
      props: { names: {} },
      mocks: { route: { name: 'scenarios.events.modifications' } },
      stubs: ['RouterLink'], // primeVue breadcrumb uses router-link that needs not-mocked router
    });
    await flushPromises();

    const breadcrumbLinks = wrapper.findAll('router-link-stub');
    expect(breadcrumbLinks).to.have.lengthOf(4); // home + each level

    expect(wrapper.vm.routeItems).to.have.lengthOf(3); // items without home
  });

  it('display default route level names', async () => {
    wrapper = mount(Breadcrumb, {
      props: { names: {} },
      mocks: { route: { name: 'scenarios.events.modifications' } },
    });
    await flushPromises();

    const breadcrumbItems = wrapper.vm.routeItems;
    expect(breadcrumbItems).to.have.lengthOf(3);

    expect(breadcrumbItems[0].label).to.equal('navigation.scenarios');
    expect(breadcrumbItems[1].label).to.equal('navigation.scenarios events');
    expect(breadcrumbItems[2].label).to.equal('navigation.scenarios events modifications');
  });

  it('display custom route level name', async () => {
    wrapper = mount(Breadcrumb, {
      props: { names: { events: 'test event' } },
      mocks: { route: { name: 'scenarios.events.modifications' } },
    });
    await flushPromises();

    const breadcrumbItems = wrapper.vm.routeItems;
    expect(breadcrumbItems).to.have.lengthOf(3);

    expect(breadcrumbItems[0].label).to.equal('navigation.scenarios');
    expect(breadcrumbItems[1].label).to.equal('navigation.scenarios events');
    expect(breadcrumbItems[2].label).to.equal('test event'); // custom name displays one level higher since it represents detail of the previous route level
  });

  it('watches change of custom route level name', async () => {
    wrapper = mount(Breadcrumb, {
      props: { names: {} },
      mocks: { route: { name: 'scenarios.events' } },
    });
    await flushPromises();

    const breadcrumbItems = wrapper.vm.routeItems;
    expect(breadcrumbItems).to.have.lengthOf(2);
    expect(breadcrumbItems[0].label).to.equal('navigation.scenarios');
    expect(breadcrumbItems[1].label).to.equal('navigation.scenarios events');

    await wrapper.setProps({ names: { scenarios: 'test scenario' } });
    await flushPromises();

    const updatedItems = wrapper.vm.routeItems;
    expect(updatedItems).to.have.lengthOf(2);
    expect(updatedItems[0].label).to.equal('navigation.scenarios');
    expect(updatedItems[1].label).to.equal('test scenario');
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
