import Card from '@components/core/Card.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper, testSLot;

describe('Card.spec.js', () => {
  describe('Without slot', () => {
    before(async () => {
      wrapper = mount(Card);
      await flushPromises();
    });

    it('displays card header', async () => {
      const header = wrapper.find('.header');
      expect(header.exists()).to.equal(true);
    });

    it('displays empty content', async () => {
      const content = wrapper.find('.content');
      expect(content.exists()).to.equal(true);
      expect(content.text()).to.equal('');
    });

    it('does not have minimal height content', async () => {
      const content = wrapper.find('.content');
      expect(content.classes()).to.not.include.members(['min-height']);
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('With content slot', () => {
    before(async () => {
      testSLot = 'test slot';
      wrapper = mount(Card, { slots: { content: () => testSLot } });
      await flushPromises();
    });

    it('displays card header', async () => {
      const header = wrapper.find('.header');
      expect(header.exists()).to.equal(true);
    });

    it('displays content', async () => {
      const content = wrapper.find('.content');
      expect(content.exists()).to.equal(true);
      expect(content.text()).to.equal(testSLot);
    });

    it('has minimal height content', async () => {
      const content = wrapper.find('.content');
      expect(content.classes()).to.include.members(['min-height']);
    });

    after(() => {
      wrapper.unmount();
    });
  });
});
