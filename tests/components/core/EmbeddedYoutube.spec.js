import EmbeddedYoutube from '@components/core/EmbeddedYoutube.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';
import { getStoreMock } from '../../helpers.js';

let wrapper, testStore;

describe('EmbeddedYoutube.spec.js', () => {
  it('displays thumbnail with link', async () => {
    const testCode = 'test-yt-code';

    wrapper = mount(EmbeddedYoutube, { props: { code: testCode } });
    await flushPromises();

    const link = wrapper.find(`a[href$="https://www.youtube.com/watch?v=${testCode}"]`);
    expect(link.exists()).to.equal(true);

    const thumbnail = wrapper.find(`img[src$="https://img.youtube.com/vi/${testCode}/0.jpg"]`);
    expect(thumbnail.exists()).to.equal(true);

    const frame = wrapper.find(`iframe[src$="https://www.youtube.com/embed/${testCode}"]`);
    expect(frame.exists()).to.equal(false);
  });

  it('displays video iframe', async () => {
    testStore = getStoreMock();
    testStore.state.layout.analyticsEnabled = true;
    const testCode = 'test-yt-code';

    wrapper = mount(EmbeddedYoutube, { props: { code: testCode }, mocks: { store: testStore } });
    await flushPromises();

    const frame = wrapper.find(`iframe[src$="https://www.youtube.com/embed/${testCode}"]`);
    expect(frame.exists()).to.equal(true);

    const link = wrapper.find(`a[href$="https://www.youtube.com/watch?v=${testCode}"]`);
    expect(link.exists()).to.equal(false);

    const thumbnail = wrapper.find(`img[src$="https://img.youtube.com/vi/${testCode}/0.jpg"]`);
    expect(thumbnail.exists()).to.equal(false);
  });

  it('changes link on prop update', async () => {
    const testCode = 'test-yt-code';
    const updatedTestCode = 'updated-yt-code';

    wrapper = mount(EmbeddedYoutube, { props: { code: testCode } });
    await flushPromises();

    const link = wrapper.find(`a[href$="https://www.youtube.com/watch?v=${testCode}"]`);
    expect(link.exists()).to.equal(true);

    await wrapper.setProps({ code: updatedTestCode });
    await flushPromises();

    const updatedLink = wrapper.find(`a[href$="https://www.youtube.com/watch?v=${updatedTestCode}"]`);
    expect(updatedLink.exists()).to.equal(true);

    const oldLink = wrapper.find(`a[href$="https://www.youtube.com/watch?v=${testCode}"]`);
    expect(oldLink.exists()).to.equal(false);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
