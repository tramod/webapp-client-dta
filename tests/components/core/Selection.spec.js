import Selection from '@components/core/Selection.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;
const testOptions = [
  { id: 1, label: 'scenarios.own', name: 'opt1' },
  { id: 2, label: 'scenarios.shared', name: 'opt2' },
  { id: 3, label: 'scenarios.public', name: 'opt3' },
];
const testSelected = testOptions[2];
const testOptionValue = 'name';

describe('Selection.spec.js', () => {
  it('displays select options as checkboxes', async () => {
    wrapper = mount(Selection, {
      props: { type: 'checkbox', options: testOptions, selected: testSelected, optionValue: testOptionValue },
    });
    await flushPromises();

    const checkboxes = wrapper.findAll('input[type=checkbox]');
    expect(checkboxes.length).to.equal(3);
    expect(wrapper.vm.selection).to.deep.equal(testSelected);
  });

  it('displays select options as radio buttons', async () => {
    wrapper = mount(Selection, {
      props: { type: 'radio', options: testOptions, selected: testSelected, optionValue: testOptionValue },
    });
    await flushPromises();

    const buttons = wrapper.findAll('div[role=radio]');
    expect(buttons.length).to.equal(3);
    expect(wrapper.vm.selection).to.deep.equal(testSelected);
  });

  it('displays select options as a dropdown', async () => {
    wrapper = mount(Selection, {
      props: { type: 'dropdown', options: testOptions, selected: testSelected, optionValue: testOptionValue },
    });
    await flushPromises();

    const dropdownIcon = wrapper.find('span.p-dropdown-label');
    expect(dropdownIcon.exists()).to.equal(true);

    dropdownIcon.trigger('click');
    await flushPromises();

    // get dropdown menu directly using querySelector since it is attached to the body and therefore can not be obtained via vue wrapper
    const dropdownMenuElement =
      wrapper.element.parentElement.parentElement.parentElement.querySelector('.p-dropdown-panel');
    expect(dropdownMenuElement).to.not.equal(null);

    const dropdownMenuItems = dropdownMenuElement.querySelector('.p-dropdown-item');
    expect(dropdownMenuItems).to.not.equal(null);

    // const dropdownOptions = wrapper.findAll('li.p-dropdown-item');
    // expect(dropdownOptions.length).to.equal(3);

    expect(wrapper.vm.selection).to.deep.equal(testSelected);
  });

  it('updates and emits its value on selected option', async () => {
    wrapper = mount(Selection, {
      props: { type: 'radio', options: testOptions, selected: testSelected, optionValue: testOptionValue },
    });
    await flushPromises();

    const buttons = wrapper.findAll('div[role=radio]');
    expect(buttons.length).to.equal(3);

    const button = buttons[0];
    button.trigger('click');
    await flushPromises();

    expect(wrapper.emitted()).to.have.property('update:selected');
    expect(wrapper.emitted()['update:selected']).to.have.length(1);
    expect(wrapper.emitted()['update:selected'][0][0]).to.deep.equal(testOptions[0][testOptionValue]);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
