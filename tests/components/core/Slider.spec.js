import Slider from '@components/core/Slider.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

describe('Slider.spec.js', () => {
  it('displays slider with label and input', async () => {
    const testLabel = 'test label';
    const testNumber = 4;
    wrapper = mount(Slider, {
      props: { label: testLabel, selected: testNumber },
    });
    await flushPromises();

    const label = wrapper.find('span.p-button');
    expect(label.exists()).to.equal(true);
    expect(label.text()).to.equal(testLabel);
    const slider = wrapper.find('.p-slider');
    expect(slider.exists()).to.equal(true);
    const input = wrapper.find('input');
    expect(input.exists()).to.equal(true);
    expect(input.element.value).to.equal(testNumber.toString());
  });

  it('updates and emits selected value', async () => {
    const testNumber = 4;
    wrapper = mount(Slider, {
      props: { selected: testNumber },
    });
    await flushPromises();

    expect(wrapper.vm.selectedNumber).to.deep.equal(testNumber);

    const slider = wrapper.get('.p-slider');
    slider.trigger('click');
    await flushPromises();

    expect(wrapper.emitted()).to.have.property('update:selected');
    expect(wrapper.emitted()['update:selected']).to.have.length(1);
    expect(wrapper.emitted()['update:selected'][0][0]).to.deep.equal(1);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
