import TabPanel from '@components/core/TabPanel.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

describe('TabPanel.spec.js', () => {
  it('displays default slot', async () => {
    const testSLot = 'test slot';

    wrapper = mount(TabPanel, {
      slots: { default: () => testSLot }, // TODO: this is probably a bug in current vue-test-utils - slots should not need to be a function
    });
    await flushPromises();

    expect(wrapper.text()).to.equal(testSLot);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
