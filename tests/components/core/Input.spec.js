import Input from '@components/core/Input.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;
const testPlaceholder = 'test placeholder';
const testName = 'test_name';
const testValue = 'test value';
const testType = 'text';

describe('Input.spec.js', () => {
  it('displays input with its properties and value', async () => {
    wrapper = mount(Input, {
      props: { modelValue: testValue, placeholder: testPlaceholder, name: testName, type: testType },
    });
    await flushPromises();

    const input = wrapper.find(`input[name=${testName}]`);
    expect(input.attributes('name')).to.equal(testName);
    expect(input.attributes('placeholder')).to.equal(testPlaceholder);
    expect(input.attributes('type')).to.equal(testType);
    expect(input.element.value).to.equal(testValue);
  });

  it('updates and emits its value on text input', async () => {
    const changedValue = 'test value 2';

    wrapper = mount(Input, {
      props: { modelValue: testValue, placeholder: testPlaceholder, name: testName, type: testType },
    });
    await flushPromises();

    const input = wrapper.find(`input[name=${testName}]`);
    expect(input.element.value).to.equal(testValue);
    await input.setValue(changedValue);
    await flushPromises();

    expect(input.element.value).to.equal(changedValue);
    expect(wrapper.emitted()).to.have.property('update:modelValue');
    expect(wrapper.emitted()['update:modelValue']).to.have.length(2); // the same emit logged twice - validation bug/feature? (introduced since vee-validate 4.6.6)
    expect(wrapper.emitted()['update:modelValue'][0][0]).to.deep.equal(changedValue);
  });

  it('accepts and enforces validation rules', async () => {
    const testRules = 'required|email';

    wrapper = mount(Input, {
      props: { modelValue: testValue, placeholder: testPlaceholder, name: testName, type: testType, rules: testRules },
    });
    await flushPromises();

    const input = wrapper.find(`input[name=${testName}]`);
    expect(input.element.value).to.equal(testValue);

    const validationError = wrapper.find('small#help-input-test_name');
    expect(validationError.exists()).to.equal(true);
    expect(validationError.text()).to.equal('');

    input.setValue('');
    await flushPromises();
    expect(validationError.text()).to.equal(`Pole ${testName} je povinné`);

    input.setValue('asd');
    await flushPromises();
    expect(validationError.text()).to.equal(`Pole ${testName} musí být validní email`);

    input.setValue('test@email.com');
    await flushPromises();
    expect(validationError.text()).to.equal('');
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
