import Calendar from '@components/core/Calendar.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';
import { isRanByVitest } from '../../helpers.js';

let wrapper;

describe('Calendar.spec.js', () => {
  it('displays default inline calendar without time slider', async () => {
    wrapper = mount(Calendar);
    await flushPromises();

    const input = wrapper.find('.p-inputtext');
    expect(input.exists()).to.equal(false);

    const calendar = wrapper.find('.p-datepicker');
    expect(calendar.exists()).to.equal(true);

    const timeslider = wrapper.find('.timeslider');
    expect(timeslider.exists()).to.equal(false);
  });

  it('displays inline calendar with time slider', async () => {
    wrapper = mount(Calendar, {
      props: { popup: false, timeslider: true },
    });
    await flushPromises();

    const input = wrapper.find('.p-inputtext');
    expect(input.exists()).to.equal(false);

    const calendar = wrapper.find('.p-datepicker');
    expect(calendar.exists()).to.equal(true);

    const timeslider = wrapper.find('.timeslider');
    expect(timeslider.exists()).to.equal(true);
  });

  it('displays popup calendar with time slider', async () => {
    wrapper = mount(Calendar, {
      props: { popup: true, timeslider: true },
    });
    await flushPromises();

    const input = wrapper.find('.p-inputtext');
    expect(input.exists()).to.equal(true);

    const calendar = wrapper.find('.p-datepicker');
    expect(calendar.exists()).to.equal(false);

    const timeslider = wrapper.find('.timeslider');
    expect(timeslider.exists()).to.equal(true);
  });

  it('displays date input with label & popup calendar on focus', async () => {
    wrapper = mount(Calendar, {
      props: { popup: true, showTime: true, showButtonBar: true }, // button bar allows us to close the popup, it is difficult wihtout the buttons since the popup is attached to document body
    });
    await flushPromises();

    const inlineCalendar = wrapper.find('.p-datepicker');
    expect(inlineCalendar.exists()).to.equal(false);

    const timeslider = wrapper.find('.timeslider');
    expect(timeslider.exists()).to.equal(false);

    const input = wrapper.find('.p-inputtext');
    expect(input.exists()).to.equal(true);
    await input.trigger('focus');

    // get datepicker directly using querySelector since it is attached to the body and therefore can not be obtained via vue wrapper
    const datepickerElement = wrapper.element.parentElement.parentElement.parentElement.querySelector('.p-datepicker');
    expect(datepickerElement).to.not.equal(null);
    const clearButtonElement = datepickerElement.querySelector('.p-datepicker-buttonbar button:nth-child(2)');
    expect(clearButtonElement).to.not.equal(null);
    await clearButtonElement.click();
    await flushPromises();
    const datepickerElement2 = wrapper.element.parentElement.parentElement.parentElement.querySelector('.p-datepicker');
    expect(datepickerElement2).to.equal(null);

    // expect(wrapper.find('.p-datepicker').exists()).to.equal(true);
    // // we need to close the popup before unounting since it is attached to document body - to prevent child node error
    // const clearButt = wrapper.find('.p-datepicker-buttonbar button:nth-child(2)');
    // await clearButt.trigger('click');
    // expect(wrapper.find('.p-datepicker').exists()).to.equal(false);
  });

  it('emits changed date on date or time selection', async () => {
    wrapper = mount(Calendar, {
      props: { popup: false, showTime: true, timeslider: true, selected: '2021-06-01T00:00:00.000Z' },
    });
    await flushPromises();

    expect(wrapper.vm.selectedDay.toISOString()).to.equal('2021-06-01T00:00:00.000Z');

    const calendar = wrapper.find('.p-datepicker-calendar');
    const dayButton = calendar.find('tbody tr:nth-child(3) td:nth-child(4) span');
    await dayButton.trigger('click');

    expect(wrapper.emitted()).to.have.property('update:selected');
    expect(wrapper.emitted()['update:selected']).to.have.length(1);
    expect(wrapper.emitted()['update:selected'][0][0]).to.deep.equal('2021-06-16T00:00:00.000Z');

    const slider = wrapper.find('.p-slider');
    await slider.trigger('click'); // this slider click sets time to 0:00 - 1:00 -> which translates to the previous date minus two hours
    const expectedDate = isRanByVitest() ? '2021-06-15T23:00:00.000Z' : '2021-06-15T22:00:00.000Z'; // random slider works different in vitest

    expect(wrapper.emitted()).to.have.property('update:selected');
    expect(wrapper.emitted()['update:selected']).to.have.length(2);
    expect(wrapper.emitted()['update:selected'][1][0]).to.deep.equal(expectedDate);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
