import Switch from '@components/core/Switch.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

describe('Switch.spec.js', () => {
  it('displays switch handle', async () => {
    const testSwitchState = true;
    wrapper = mount(Switch, {
      props: { modelValue: testSwitchState },
    });
    await flushPromises();

    const switchHandle = wrapper.find('.p-inputswitch-slider');
    expect(switchHandle.exists()).to.equal(true);

    const checkedHandle = wrapper.find('.p-inputswitch-checked');
    expect(checkedHandle.exists()).to.equal(testSwitchState);

    expect(wrapper.vm.checked).to.equal(testSwitchState);
  });

  it('displays disabled switch handle', async () => {
    const testSwitchState = true;
    wrapper = mount(Switch, {
      props: { modelValue: testSwitchState, disabled: true },
    });
    await flushPromises();

    const switchHandle = wrapper.find('.p-inputswitch');
    expect(switchHandle.classes()).to.contain('p-disabled');
  });

  it('updates and emits switch state', async () => {
    const testSwitchState = true;
    wrapper = mount(Switch, {
      props: { modelValue: testSwitchState },
    });
    await flushPromises();

    const switchdHandle = wrapper.get('.p-inputswitch-slider');

    expect(wrapper.vm.checked).to.equal(testSwitchState);

    switchdHandle.trigger('click');
    await flushPromises();

    expect(wrapper.emitted()).to.have.property('update:modelValue');
    expect(wrapper.emitted()['update:modelValue']).to.have.length(1);
    expect(wrapper.emitted()['update:modelValue'][0][0]).to.deep.equal(!testSwitchState);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
