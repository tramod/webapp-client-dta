import Divider from '@components/core/Divider.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

describe('Divider.spec.js', () => {
  it('displays default divider', async () => {
    wrapper = mount(Divider);
    await flushPromises();

    const hrDivider = wrapper.find('hr');
    expect(hrDivider.exists()).to.equal(false);

    const primeDivider = wrapper.find('.p-divider');
    expect(primeDivider.exists()).to.equal(true);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
