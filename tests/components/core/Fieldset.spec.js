import Fieldset from '@components/core/Fieldset.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

describe('Fieldset.spec.js', () => {
  it('displays fieldset', async () => {
    wrapper = mount(Fieldset);
    await flushPromises();

    const fieldset = wrapper.find('fieldset');
    expect(fieldset.exists()).to.equal(true);
  });

  it('displays legend', async () => {
    const testLegend = 'test legend';

    wrapper = mount(Fieldset, {
      props: { legend: testLegend },
    });
    await flushPromises();

    const fieldset = wrapper.find('fieldset');
    expect(fieldset.exists()).to.equal(true);

    const legend = wrapper.find('legend');
    expect(legend.exists()).to.equal(true);
    expect(legend.text()).to.equal(testLegend);
  });

  it('displays default slot', async () => {
    const testSLot = 'test slot';

    wrapper = mount(Fieldset, {
      slots: { default: () => testSLot }, // TODO: this is probably a bug in current vue-test-utils - slots should not need to be a function
    });
    await flushPromises();

    const fieldset = wrapper.find('fieldset');
    expect(fieldset.exists()).to.equal(true);

    const slot = wrapper.find('.p-fieldset-content');
    expect(slot.exists()).to.equal(true);
    expect(slot.text()).to.equal(testSLot);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
