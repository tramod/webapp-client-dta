import Textarea from '@components/core/Textarea.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;
const testPlaceholder = 'test placeholder';
const testName = 'test_name';
const testValue = 'test value';
const testRows = 5;

describe('Textarea.spec.js', () => {
  it('displays textarea with its properties and value', async () => {
    wrapper = mount(Textarea, {
      props: { modelValue: testValue, placeholder: testPlaceholder, name: testName, rows: testRows },
    });
    await flushPromises();

    const textarea = wrapper.find(`textarea[name=${testName}]`);
    expect(textarea.attributes('name')).to.equal(testName);
    expect(textarea.attributes('placeholder')).to.equal(testPlaceholder);
    expect(textarea.attributes('rows')).to.equal(testRows.toString());
    expect(textarea.element.value).to.equal(testValue);
  });

  it('updates and emits its value on text input', async () => {
    const changedValue = 'test value 2';

    wrapper = mount(Textarea, {
      props: { modelValue: testValue, placeholder: testPlaceholder, name: testName, rows: testRows },
    });
    await flushPromises();

    const textarea = wrapper.find(`textarea[name=${testName}]`);
    expect(textarea.element.value).to.equal(testValue);
    textarea.setValue(changedValue);
    await flushPromises();

    expect(textarea.element.value).to.equal(changedValue);

    expect(wrapper.emitted()).to.have.property('update:modelValue');
    expect(wrapper.emitted()['update:modelValue']).to.have.length(2); // the same emit logged twice - validation bug/feature? (introduced since vee-validate 4.6.6)
    expect(wrapper.emitted()['update:modelValue'][0][0]).to.deep.equal(changedValue);
  });

  it('accepts and eforces validation rules', async () => {
    const testRules = 'required|min:10';

    wrapper = mount(Textarea, {
      props: { modelValue: testValue, placeholder: testPlaceholder, name: testName, rows: testRows, rules: testRules },
    });
    await flushPromises();

    const textarea = wrapper.find(`textarea[name=${testName}]`);
    expect(textarea.element.value).to.equal(testValue);

    const validationError = wrapper.find('small#help-input-test_name');
    expect(validationError.exists()).to.equal(true);
    expect(validationError.text()).to.equal('');

    textarea.setValue('');
    await flushPromises();
    expect(validationError.text()).to.equal(`Pole ${testName} je povinné`);

    textarea.setValue('asd');
    await flushPromises();
    expect(validationError.text()).to.equal(`Pole ${testName} musí obsahovat alespoň 10 znaků`);

    textarea.setValue('asdf ghjkl asdfd aksldad');
    await flushPromises();
    expect(validationError.text()).to.equal('');
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
