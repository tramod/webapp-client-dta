import DateRange from '@components/core/DateRange.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

describe('DateRange.spec.js', () => {
  it('displays two date inputs with labels', async () => {
    wrapper = mount(DateRange);
    await flushPromises();

    const inputs = wrapper.findAll('.tm-calendar .input');
    expect(inputs).to.have.lengthOf(2);

    const labels = wrapper.findAll('.tm-calendar .input .calendar-label');
    expect(labels).to.have.lengthOf(2);
  });

  it('displays dates from props', async () => {
    const testDates = [
      { tmDate: '2021-03-09T08:28:27.150Z', expectedInputValue: '03/09/2021 09:28' },
      { tmDate: '2022-04-10T09:28:27.150Z', expectedInputValue: '04/10/2022 11:28' },
    ];
    wrapper = mount(DateRange, {
      props: { from: testDates[0]['tmDate'], to: testDates[1]['tmDate'] },
    });
    await flushPromises();

    const inputs = wrapper.findAll('.tm-calendar .input input');
    expect(inputs).to.have.lengthOf(2);

    inputs.forEach((el, i) => {
      expect(el.element.value).to.equal(testDates[i]['expectedInputValue']);
    });

    const labels = wrapper.findAll('.tm-calendar .input .calendar-label');
    expect(labels).to.have.lengthOf(2);
  });

  it('changes dates on props update', async () => {
    const testDates = [
      { tmDate: '2021-03-09T08:28:27.150Z', expectedInputValue: '03/09/2021 09:28' },
      { tmDate: '2022-04-10T09:28:27.150Z', expectedInputValue: '04/10/2022 11:28' },
      { tmDate: '2023-03-09T08:28:27.150Z', expectedInputValue: '03/09/2023 09:28' },
      { tmDate: '2024-04-10T09:28:27.150Z', expectedInputValue: '04/10/2024 11:28' },
    ];
    wrapper = mount(DateRange, {
      props: { from: testDates[0]['tmDate'], to: testDates[1]['tmDate'] },
    });
    await flushPromises();

    const inputs = wrapper.findAll('.tm-calendar .input input');
    inputs.forEach((el, i) => {
      expect(el.element.value).to.equal(testDates[i]['expectedInputValue']);
    });

    await wrapper.setProps({ from: testDates[2]['tmDate'], to: testDates[3]['tmDate'] });
    await flushPromises();

    const updatedInputs = wrapper.findAll('.tm-calendar .input input');
    updatedInputs.forEach((el, i) => {
      expect(el.element.value).to.equal(testDates[i + 2]['expectedInputValue']);
    });
  });

  it('changes date on popup calendar click', async () => {
    const testDates = [
      { tmDate: '2021-03-09T08:28:27.150Z', expectedInputValue: '03/09/2021 09:28' },
      { tmDate: '2022-04-10T09:28:27.150Z', expectedInputValue: '04/10/2022 11:28' },
    ];
    wrapper = mount(DateRange, {
      props: { from: testDates[0]['tmDate'], to: testDates[1]['tmDate'] },
    });
    await flushPromises();

    const input = wrapper.find('.tm-calendar .input input');
    expect(input.element.value).to.equal(testDates[0]['expectedInputValue']);
    await input.trigger('focus');

    // get datepicker directly using querySelector since it is attached to the body and therefore can not be obtained via vue wrapper
    const datepickerElement = wrapper.element.parentElement.parentElement.parentElement.querySelector('.p-datepicker');
    expect(datepickerElement).to.not.equal(null);

    const dayButton = datepickerElement.querySelector('tbody tr td span:not([class^="p-disabled"])');

    await dayButton.click();
    await flushPromises();

    const updatedInput = wrapper.find('.tm-calendar .input input');
    expect(updatedInput.element.value).to.not.equal(testDates[0]['expectedInputValue']);
  });

  it('emits date from date', async () => {
    const testDates = [
      { tmDate: '2021-03-09T08:28:27.150Z', expectedInputValue: '03/09/2021 09:28' },
      { tmDate: '2022-04-10T09:28:27.150Z', expectedInputValue: '04/10/2022 11:28' },
    ];
    wrapper = mount(DateRange, {
      props: { from: testDates[0]['tmDate'], to: testDates[1]['tmDate'] },
    });
    await flushPromises();

    const fromInput = wrapper.find('.tm-calendar .input input');
    expect(fromInput.element.value).to.equal(testDates[0]['expectedInputValue']);
    await fromInput.trigger('focus');

    // get datepicker directly using querySelector since it is attached to the body and therefore can not be obtained via vue wrapper
    const datepickerElement = wrapper.element.parentElement.parentElement.parentElement.querySelector('.p-datepicker');
    expect(datepickerElement).to.not.equal(null);

    const dayButton = datepickerElement.querySelector('tbody tr td span:not([class^="p-disabled"])');

    await dayButton.click();
    await flushPromises();

    expect(wrapper.emitted()).to.have.property('update:from');
    expect(wrapper.emitted()['update:from']).to.have.length(1);
  });

  it('emits date to date', async () => {
    // separate test is needed since we need to unmount to get rid of the calendar popup element
    const testDates = [
      { tmDate: '2021-03-09T08:28:27.150Z', expectedInputValue: '03/09/2021 09:28' },
      { tmDate: '2022-04-10T09:28:27.150Z', expectedInputValue: '04/10/2022 11:28' },
    ];
    wrapper = mount(DateRange, {
      props: { from: testDates[0]['tmDate'], to: testDates[1]['tmDate'] },
    });
    await flushPromises();

    const toInput = wrapper.find('.tm-calendar[data-test="date-to"] .input input');
    expect(toInput.element.value).to.equal(testDates[1]['expectedInputValue']);
    await toInput.trigger('focus');

    // get datepicker directly using querySelector since it is attached to the body and therefore can not be obtained via vue wrapper
    const datepickerElement = wrapper.element.parentElement.parentElement.parentElement.querySelector('.p-datepicker');
    expect(datepickerElement).to.not.equal(null);

    const dayButton = datepickerElement.querySelector('tbody tr td span:not([class^="p-disabled"])');

    await dayButton.click();
    await flushPromises();

    expect(wrapper.emitted()).to.have.property('update:to');
    expect(wrapper.emitted()['update:to']).to.have.length(1);
  });

  it('is optionally disabled', async () => {
    const testDates = [
      { tmDate: '2021-03-09T08:28:27.150Z', expectedInputValue: '03/09/2021 09:28' },
      { tmDate: '2022-04-10T09:28:27.150Z', expectedInputValue: '04/10/2022 11:28' },
    ];
    wrapper = mount(DateRange, {
      props: { from: testDates[0]['tmDate'], to: testDates[1]['tmDate'], disabled: true },
    });
    await flushPromises();

    const disabledInputs = wrapper.findAll('.tm-calendar .p-disabled');
    expect(disabledInputs).to.have.lengthOf(2);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
