import Heading from '@components/core/Heading.vue';
import { mount, globalStoreMock, globalRouterMock } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

describe('Heading.spec.js', () => {
  it('displays heading text', async () => {
    const testText = 'test-text';

    wrapper = mount(Heading, { props: { text: testText } });
    await flushPromises();

    const textElement = wrapper.find('.heading > span');
    expect(textElement.exists()).to.equal(true);
    expect(textElement.text()).to.equal(testText);

    const backButton = wrapper.find('.heading button[type=back]');
    expect(backButton.exists()).to.equal(false);

    const closeButton = wrapper.find('.heading button.close-icon');
    expect(closeButton.exists()).to.equal(false);
  });

  it('displays back button', async () => {
    const testRoute = 'test-route-name';

    wrapper = mount(Heading, { props: { backToRoute: testRoute } });
    await flushPromises();

    const textElement = wrapper.find('.heading > span');
    expect(textElement.exists()).to.equal(false);

    const closeButton = wrapper.find('.heading button.close-icon');
    expect(closeButton.exists()).to.equal(false);

    const backButton = wrapper.find('.heading button[data-type=back]');
    expect(backButton.exists()).to.equal(true);
  });

  it('redirects on back button click', async () => {
    const testRoute = 'test-route-name';

    wrapper = mount(Heading, { props: { backToRoute: testRoute } });
    await flushPromises();

    const backButton = wrapper.find('.heading button[data-type=back]');
    expect(backButton.exists()).to.equal(true);

    await backButton.trigger('click');
    expect(globalRouterMock.push.lastCall.args).to.deep.equal([{ name: testRoute }]);
  });

  it('changes text on prop update', async () => {
    const testText = 'test-text';
    const updatedText = 'updated-test-text';

    wrapper = mount(Heading, { props: { text: testText } });
    await flushPromises();

    const textElement = wrapper.find('.heading > span');
    expect(textElement.exists()).to.equal(true);
    expect(textElement.text()).to.equal(testText);

    await wrapper.setProps({ text: updatedText });
    await flushPromises();

    const updatedTextElement = wrapper.find('.heading > span');
    expect(updatedTextElement.exists()).to.equal(true);
    expect(updatedTextElement.text()).to.equal(updatedText);
  });

  it('displays default slot', async () => {
    const testSLot = 'test slot';

    wrapper = mount(Heading, {
      slots: { default: () => testSLot }, // TODO: this is probably a bug in current vue-test-utils - slots should not need to be a function
    });
    await flushPromises();

    const heading = wrapper.find('.heading');
    expect(heading.text()).to.equal(testSLot);
  });

  it('displays close icon for widget', async () => {
    const testWidget = 'widget-name';

    wrapper = mount(Heading, { props: { widgetName: testWidget } });
    await flushPromises();

    const textElement = wrapper.find('.heading > span');
    expect(textElement.exists()).to.equal(false);

    const backButton = wrapper.find('.heading button[data-type=back]');
    expect(backButton.exists()).to.equal(false);

    const closeButton = wrapper.find('.heading button.close-icon');
    expect(closeButton.exists()).to.equal(true);
  });

  it('emits close event', async () => {
    const testWidget = 'widget-name';

    wrapper = mount(Heading, { props: { widgetName: testWidget } });
    await flushPromises();

    const closeButton = wrapper.find('.heading button.close-icon');
    expect(closeButton.exists()).to.equal(true);

    await closeButton.trigger('click');

    expect(globalStoreMock.dispatch.getCall(-1).args).to.deep.equal(['map/hideWidget', { name: testWidget }]);
    expect(wrapper.emitted()).to.have.property('widget:closed');
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
