import Tabs from '@components/core/Tabs.vue';
import TabPanel from '@components/core/TabPanel.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

describe('Tabs.spec.js', () => {
  it('displays tab navigation and panels', async () => {
    const testSLot = 'test slot';

    wrapper = mount(Tabs, {
      slots: { default: () => testSLot }, // TODO: this is probably a bug in current vue-test-utils - slots should not need to be a function
    });
    await flushPromises();

    const nav = wrapper.find('.tm-tabs .tm-tabs-nav');
    expect(nav.exists()).to.equal(true);

    const panels = wrapper.find('.tm-tabs .tm-tabs-panels');
    expect(panels.exists()).to.equal(true);
  });

  it('displays tab navigation and panel wrappers', async () => {
    // TODO: how to test with customized components (with custom props) inside slots
    // const tabPanel1 = mount(TabPanel, { props: { header: 'tp1', filled: true } });
    // const tabPanel2 = mount(TabPanel, { isShallow: true, props: { header: 'tp2', filled: true } });

    wrapper = mount(Tabs, {
      slots: { default: [TabPanel, TabPanel] },
    });
    await flushPromises();

    const nav = wrapper.find('.tm-tabs .tm-tabs-nav');
    expect(nav.exists()).to.equal(true);

    const navTabs = nav.findAll('li');
    expect(navTabs).to.have.lengthOf(2);

    const panels = wrapper.find('.tm-tabs .tm-tabs-panels');
    expect(panels.exists()).to.equal(true);

    const innerPanels = panels.findAll('.tm-tabs-panel');
    expect(innerPanels).to.have.lengthOf(2);
  });

  it('can switch between active tabs', async () => {
    wrapper = mount(Tabs, {
      slots: { default: [TabPanel, TabPanel] },
    });
    await flushPromises();

    const nav = wrapper.find('.tm-tabs .tm-tabs-nav');
    expect(nav.exists()).to.equal(true);

    const navTabs = nav.findAll('li a.tm-tabs-nav-link');
    expect(navTabs).to.have.lengthOf(2);

    const firstTabButton = navTabs[0];
    const secondTabButton = navTabs[1];

    const activeTab = wrapper.find('.tm-tabs-nav li.p-highlight');
    expect(activeTab.exists()).to.equal(false);

    await firstTabButton.trigger('click');
    await flushPromises();

    const activeTab1 = wrapper.find('.tm-tabs-nav li:first-child.p-highlight');
    expect(activeTab1.exists()).to.equal(true);

    await firstTabButton.trigger('click');
    await flushPromises();

    const activeTab3 = wrapper.find('.tm-tabs-nav li:first-child.p-highlight');
    expect(activeTab3.exists()).to.equal(false);

    await secondTabButton.trigger('click');
    await flushPromises();

    const activeTab4 = wrapper.find('.tm-tabs-nav li:nth-child(2).p-highlight');
    expect(activeTab4.exists()).to.equal(true);

    const activeTab5 = wrapper.find('.tm-tabs-nav li:first-child.p-highlight');
    expect(activeTab5.exists()).to.equal(false);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
