export * from './Dialog.spec.js';
export * from './WidgetCalendar.spec.js';
export * from './WidgetComparison.spec.js';
export * from './WidgetLegend.spec.js';
export * from './WidgetEventList.spec.js';
export * from './Navigation.spec.js';
