import WidgetCalendar from '@components/layouts/widgets/WidgetCalendar.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';
import { getStoreMock } from '../../helpers.js';

let wrapper, testStore;
const jsDate = new Date();
const today = jsDate.toISOString();
const changeDateSpy = sinon.spy();

describe('WidgetCalendar.spec.js', () => {
  before(async () => {
    testStore = getStoreMock();
    testStore.getters['map/getCalendarDate'] = () => today;
    testStore.dispatch = changeDateSpy;
    wrapper = mount(WidgetCalendar, {
      props: { mapMode: 'modelDTA' },
      mocks: { store: testStore },
    });
    await flushPromises();
  });

  it('displays heading', async () => {
    const heading = wrapper.find('[data-test="heading"]');
    expect(heading.exists()).to.equal(true);
    expect(heading.text()).to.contain('widgets.calendar');
  });

  it('displays calendar', async () => {
    const calendar = wrapper.find('div.tm-calendar');
    expect(calendar.exists()).to.equal(true);
  });

  it('displays time slider', async () => {
    const slider = wrapper.find('div.timeslider');
    expect(slider.exists()).to.equal(true);
  });

  it('displays buttons', () => {
    const expectedButtontypes = ['create'];

    expectedButtontypes.forEach((type) => {
      expect(wrapper.find(`button[data-type=${type}]`).exists()).to.equal(true);
    });
  });

  it('displays default date', async () => {
    const date = wrapper.vm.selectedDate;
    expect(date).to.equal(today);
  });

  it.skip('changes date on click', async () => {
    const calendar = wrapper.find('.p-datepicker-calendar');
    const dayButton = calendar.find('tbody tr:nth-child(3) td:nth-child(4) span');
    await dayButton.trigger('click');

    await flushPromises();

    expect(changeDateSpy.calledOnce).to.equal(true);
    expect(changeDateSpy.args[0][0]).to.equal('map/setCalendarDate');
  });

  after(() => {
    wrapper.unmount();
  });
});
