import WidgetEventList from '@components/layouts/widgets/WidgetEventList.vue';
import { mount, globalRouterMock } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';
import { mockStartEditModeEndpoint } from '../../mockAxios.js';
import { activateScenario, deactivateScenario } from '../../scenarioSetup.js';
import { getStoreMock } from '../../helpers.js';

const testEvents = [
  {
    id: 3,
    name: 'Aktualni udalost',
    description: 'popis',
    dateFrom: '2020-02-20T00:00:00.000Z',
    dateTo: '2022-03-20T00:00:00.000Z',
    authorUserId: 1,
    included: true,
  },
  {
    id: 4,
    name: 'Stara udalost',
    description: 'popis',
    dateFrom: '2020-02-20T00:00:00.000Z',
    dateTo: '2020-03-20T00:00:00.000Z',
    authorUserId: 2,
    included: true,
  },
  {
    id: 5,
    name: 'Budouci udalost',
    description: 'popis',
    dateFrom: '2022-02-20T00:00:00.000Z',
    dateTo: '2022-03-20T00:00:00.000Z',
    authorUserId: 2,
    included: false,
  },
];

const testScenario = {
  id: 1,
  name: 'Soukromy scenario',
  description: 'test desc',
  note: 'test note',
  level: 'editor',
  dateFrom: '2020-02-20T00:00:00.000Z',
  dateTo: '2022-03-20T00:00:00.000Z',
  events: testEvents,
  hasModelSectionsValid: true,
};

let wrapper, testStore;

describe('WidgetEventList.spec.js', () => {
  describe('With active scenario', () => {
    before(async () => {
      mockStartEditModeEndpoint({ response: { editSession: {}, status: null }, scenarioId: testScenario.id });
      testStore = getStoreMock();
      await activateScenario(testScenario, { localStore: testStore });
      wrapper = mount(WidgetEventList, { props: { date: testScenario.dateFrom }, mocks: { store: testStore } });
      await flushPromises();
    });

    it('displays heading', async () => {
      const heading = wrapper.find('[data-test="heading"]');
      expect(heading.exists()).to.equal(true);
      expect(heading.text()).to.contain('widgets.eventList widget');
    });

    it('displays no buttons', () => {
      expect(wrapper.find(`button`).exists()).to.equal(true);
    });

    it('displays current events with active scenario', async () => {
      expect(wrapper.vm.scenario.id).to.deep.equal(testScenario.id);
      expect(wrapper.vm.currentEvents.length).to.equal(2);

      const eventItems = wrapper.findAll('.p-panel');
      expect(eventItems.length).to.equal(2);
    });

    it('updates current events when date changes', async () => {
      await wrapper.setProps({ date: '2021-02-20T00:00:00.000Z' });
      await flushPromises();

      expect(wrapper.vm.scenario.id).to.deep.equal(testScenario.id);
      expect(wrapper.vm.currentEvents.length).to.equal(1);

      const eventItems = wrapper.findAll('.p-panel');
      expect(eventItems.length).to.equal(1);
    });

    it('jumps to event on click', async () => {
      await wrapper.setProps({ date: '2021-02-20T00:00:00.000Z' });
      await flushPromises();

      expect(wrapper.vm.scenario.id).to.deep.equal(testScenario.id);
      expect(wrapper.vm.currentEvents.length).to.equal(1);

      const eventItem = wrapper.find('.item-heading');
      eventItem.trigger('click');
      await flushPromises();

      expect(testStore.dispatch.calledWith('map/activateItem')).to.equal(true);
      expect(testStore.dispatch.calledWith('layout/startLoading')).to.equal(true);
      expect(testStore.dispatch.calledWith('layout/finishLoading')).to.equal(true);
      expect(testStore.dispatch.calledWith('scenarios/startEditMode')).to.equal(true);

      expect(globalRouterMock.push.lastCall.args).to.deep.equal([
        { name: 'scenarios.events.modifications', params: { id: 1, evId: 3 } },
      ]);
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('Without active scenario', () => {
    before(async () => {
      await deactivateScenario();
      mockStartEditModeEndpoint({ response: { editSession: {}, status: null }, scenarioId: testScenario.id });
      wrapper = mount(WidgetEventList, { props: { date: testScenario.dateFrom } });
      await flushPromises();
    });

    it('displays heading', async () => {
      const heading = wrapper.find('[data-test="heading"]');
      expect(heading.exists()).to.equal(true);
      expect(heading.text()).to.contain('widgets.eventList widget');
    });

    it('displays no buttons', () => {
      expect(wrapper.find('.tm-button').exists()).to.equal(false);
    });

    it('displays no events without active scenario', async () => {
      const eventList = wrapper.find('.event-list');
      expect(eventList.exists()).to.equal(true);

      const eventItem = wrapper.find('.p-panel');
      expect(eventItem.exists()).to.equal(false);

      expect(wrapper.vm.scenario).to.deep.equal({});
      expect(wrapper.vm.currentEvents).to.deep.equal([]);
    });

    after(() => {
      wrapper.unmount();
    });
  });
});
