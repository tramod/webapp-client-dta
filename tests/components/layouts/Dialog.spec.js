import Dialog from '@components/layouts/Dialog.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

describe('Dialog.spec.js', () => {
  before(async () => {
    wrapper = mount(Dialog);
    await flushPromises();
  });

  it('is not visible by default', async () => {
    expect(wrapper.vm.displayed).to.equal(false);

    // get dialog directly using querySelector since it is attached to the body and therefore can not be obtained via vue wrapper
    const dialogElement =
      wrapper.element.parentElement.parentElement.parentElement.querySelector('.p-dialog[role=dialog]');
    expect(dialogElement).to.equal(null);
  });

  it('has elements when displayed', async () => {
    wrapper.vm.displayed = true;
    await flushPromises();

    const dialogElement =
      wrapper.element.parentElement.parentElement.parentElement.querySelector('.p-dialog[role=dialog]');
    expect(dialogElement).to.not.equal(null);

    const dialogHeaderElement = dialogElement.querySelector('.p-dialog-header');
    expect(dialogHeaderElement).to.not.equal(null);
    const dialogContentElement = dialogElement.querySelector('.content');
    expect(dialogContentElement).to.not.equal(null);
    const dialogActionsElement = dialogElement.querySelector('.actions');
    expect(dialogActionsElement).to.not.equal(null);
  });

  it('displays custom options', async () => {
    const testHeader = 'test header';
    const testMessage = 'test message';

    wrapper.vm.displayed = true;
    wrapper.vm.dialog.header = testHeader;
    wrapper.vm.dialog.message = testMessage;
    await flushPromises();

    const dialogElement =
      wrapper.element.parentElement.parentElement.parentElement.querySelector('.p-dialog[role=dialog]');
    expect(dialogElement).to.not.equal(null);

    const dialogHeaderElement = dialogElement.querySelector('.p-dialog-header');
    expect(dialogHeaderElement.textContent).to.equal(testHeader);
    const dialogContentElement = dialogElement.querySelector('.content');
    expect(dialogContentElement.textContent).to.equal(testMessage);
  });

  it('can be closed on click', async () => {
    const testAction = sinon.spy();

    wrapper.vm.displayed = true;
    wrapper.vm.dialog.message = 'test';
    await flushPromises();

    const dialogElement =
      wrapper.element.parentElement.parentElement.parentElement.querySelector('.p-dialog[role=dialog]');
    expect(dialogElement).to.not.equal(null);

    const closeButtonElement = dialogElement.querySelector('button[data-type=cancel]');
    expect(testAction.calledOnce).to.equal(false);
    closeButtonElement.click();
    await flushPromises();
    expect(wrapper.vm.displayed).to.equal(false);
  });

  after(() => {
    wrapper.unmount();
  });
});
