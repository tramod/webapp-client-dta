import Navigation from '@components/layouts/Navigation.vue';
import { mount, globalStoreMock, globalRouterMock } from '../../mountComponent.js';
import { mockModelsEndpoint, axiosMock, API_HOST } from '../../mockAxios.js';
import { flushPromises } from '@vue/test-utils';
import { expect } from 'chai';
import { getStoreMock } from '../../helpers.js';
import { mount as mountComposable } from '../../mountComposable';
import useModels from '@composables/useModels';

let wrapper, modelsWrapper, testStore;

const defaultRouteLinks = [
  { name: 'user', icon: 'ri-user-line' },
  { name: 'scenarios', icon: 'ri-file-list-line' },
  { name: 'models', icon: 'ri-flow-chart' },
  { name: 'settings', icon: 'ri-settings-3-line' },
];

const defaultDataModes = [
  { name: 'model', icon: 'ri-send-plane-2-line' },
  { name: 'comparison', icon: 'ri-tools-line' },
];

const defaultWidgets = [
  { name: 'legend', icon: 'ri-list-check' },
  { name: 'mapSettings', icon: 'ri-map-line' },
  { name: 'mapPopup', icon: 'ri-information-line' },
  { name: 'legend', icon: 'ri-list-check' },
  { name: 'mapSettings', icon: 'ri-map-line' },
  { name: 'mapPopup', icon: 'ri-information-line' },
];

const customRouteLinks = [
  { name: 'models', icon: 'custom-icon-1' },
  { name: 'user', icon: 'custom-icon-2' },
  { name: 'disbled-route-1', icon: 'custom-icon-3' },
  { name: 'disbled-route-2', icon: 'custom-icon-4' },
];

const customRouteLinksEnabled = [customRouteLinks[0], customRouteLinks[1]];

const customRouteLinksDisabled = [customRouteLinks[2], customRouteLinks[3]];

const customDataModesEnabled = [{ name: 'model', icon: 'custom-icon-4' }];

const customDataModesDisabled = [
  { name: 'custom-name-5', icon: 'custom-icon-5' },
  { name: 'custom-name-6', icon: 'custom-icon-6' },
];

const customDataModes = [...customDataModesEnabled, ...customDataModesDisabled];

const defaultMapModeByDataMode = {
  model: 'modelDTA',
  comparison: 'modelDTA-comparison',
};

const testModels = [
  {
    id: 1,
    name: 'DTA-test',
    type: 'DTA',
    isDefault: true,
  },
];

describe('Navigation.spec.js', () => {
  before(async () => {
    mockModelsEndpoint({ response: { trafficModels: testModels } });
    modelsWrapper = mountComposable(useModels);
  });

  describe('Default display', () => {
    before(async () => {
      wrapper = mount(Navigation);
      await flushPromises();
    });

    it('Requests models from server', async () => {
      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}models`);
    });

    it('Displays default route buttons', async () => {
      const routeLinks = wrapper.find('[data-test="route-links"]');
      expect(routeLinks.exists()).to.equal(true);

      const routeLinkButtons = routeLinks.findAll('.tm-button');
      expect(routeLinkButtons.length).to.equal(defaultRouteLinks.length);

      defaultRouteLinks.forEach(({ name, icon }) => {
        const routeLinkButton = wrapper.find(`[data-test="route-link-${name}"]`);
        expect(routeLinkButton.exists()).to.equal(true);

        const routeLinkIcon = wrapper.find(`span.${icon}`);
        expect(routeLinkIcon.exists()).to.equal(true);
      });
    });

    it('No route is active by default', async () => {
      const activeRoute = wrapper.find('[data-test="route-links"] .tm-button.active');
      expect(activeRoute.exists()).to.equal(false);
    });

    it('Displays default data mode buttons', async () => {
      const dataModes = wrapper.find('[data-test="data-modes"]');
      expect(dataModes.exists()).to.equal(true);

      const dataModeButtons = dataModes.findAll('.tm-button');
      expect(dataModeButtons.length).to.equal(defaultDataModes.length);

      defaultDataModes.forEach(({ name, icon }) => {
        const dataModeButton = wrapper.find(`[data-test="data-mode-${name}"]`);
        expect(dataModeButton.exists()).to.equal(true);

        const dataModeIcon = wrapper.find(`i.${icon}`);
        expect(dataModeIcon.exists()).to.equal(true);
      });
    });

    it('Model data mode is active by default', async () => {
      const activeMode = wrapper.find('[data-test="data-modes"] .tm-button.active');
      expect(activeMode.exists()).to.equal(true);
      expect(activeMode.attributes('data-test')).to.equal('data-mode-model');
    });

    it('Displays map controls with only zoom', async () => {
      const controlsCard = wrapper.find('div.controls');
      expect(controlsCard.exists()).to.equal(true);

      const controls = wrapper.findAll('div.controls > div');
      expect(controls.length).to.equal(1);

      const zoomControl = wrapper.find('div.controls div.zoom');
      expect(zoomControl.exists()).to.equal(true);
    });

    it('Displays widgets associated with model data mode', async () => {
      const widgets = wrapper.find('[data-test="widgets"]');
      expect(widgets.exists()).to.equal(true);

      const widgetButtons = widgets.findAll('.tm-button');
      expect(widgetButtons.length).to.equal(defaultWidgets.length);

      defaultWidgets.forEach(({ name, icon }) => {
        const widgetButton = wrapper.find(`[data-test="widget-${name}"]`);
        expect(widgetButton.exists()).to.equal(true);

        const widgetIcon = wrapper.find(`span.${icon}`);
        expect(widgetIcon.exists()).to.equal(true);
      });
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('Custom display', () => {
    before(async () => {
      testStore = getStoreMock();
      testStore.state.layout.panels.right = 'closed';
      wrapper = mount(Navigation, {
        mocks: { store: testStore },
        props: {
          routeLinks: customRouteLinks,
          dataModes: customDataModes,
        },
      });
      await flushPromises();
    });

    it('Displays custom route buttons (enabled only)', async () => {
      const routeLinks = wrapper.find('[data-test="route-links"]');
      expect(routeLinks.exists()).to.equal(true);

      const routeLinkButtons = routeLinks.findAll('.tm-button');
      expect(routeLinkButtons.length).to.equal(customRouteLinksEnabled.length);

      customRouteLinksEnabled.forEach(({ name, icon }) => {
        const routeLinkButton = wrapper.find(`[data-test="route-link-${name}"]`);
        expect(routeLinkButton.exists()).to.equal(true);

        const routeLinkIcon = wrapper.find(`span.${icon}`);
        expect(routeLinkIcon.exists()).to.equal(true);
      });

      customRouteLinksDisabled.forEach(({ name }) => {
        const routeLinkButton = wrapper.find(`[data-test="route-link-${name}"]`);
        expect(routeLinkButton.exists()).to.equal(false);
      });
    });

    it('Displays custom data mode button (enabled only)', async () => {
      const dataModes = wrapper.find('[data-test="data-modes"]');
      expect(dataModes.exists()).to.equal(true);

      const dataModeButtons = dataModes.findAll('.tm-button');
      expect(dataModeButtons.length).to.equal(customDataModesEnabled.length);

      customDataModesEnabled.forEach(({ name, icon }) => {
        const dataModeButton = wrapper.find(`[data-test="data-mode-${name}"]`);
        expect(dataModeButton.exists()).to.equal(true);

        const dataModeIcon = wrapper.find(`i.${icon}`);
        expect(dataModeIcon.exists()).to.equal(true);
      });

      customDataModesDisabled.forEach(({ name }) => {
        const dataModeButton = wrapper.find(`[data-test="data-mode-${name}"]`);
        expect(dataModeButton.exists()).to.equal(false);
      });
    });

    it('Displays no widgets (due to closed panel)', async () => {
      const widgets = wrapper.find('[data-test="widgets"]');
      expect(widgets.exists()).to.equal(false);
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('Toggle displayed components', () => {
    before(async () => {
      wrapper = mount(Navigation);
      await flushPromises();
    });

    it('Can toggle route buttons display', async () => {
      await wrapper.setProps({ showRoutes: true });
      await flushPromises();

      const routeLinks = wrapper.find('[data-test="route-links"]');
      expect(routeLinks.exists()).to.equal(true);

      await wrapper.setProps({ showRoutes: false });
      await flushPromises();

      const routeLinksAfterUpdate = wrapper.find('[data-test="route-links"]');
      expect(routeLinksAfterUpdate.exists()).to.equal(false);
    });

    it('Can toggle data mode buttons display (also affecting widgets)', async () => {
      await wrapper.setProps({ showDataModes: true });
      await wrapper.setProps({ showWidgets: true });
      await flushPromises();

      const dataModes = wrapper.find('[data-test="data-modes"]');
      expect(dataModes.exists()).to.equal(true);
      const widgets = wrapper.find('[data-test="widgets"]');
      expect(widgets.exists()).to.equal(true);

      await wrapper.setProps({ showDataModes: false });
      await flushPromises();

      const dataModesAfterUpdate = wrapper.find('[data-test="data-modes"]');
      expect(dataModesAfterUpdate.exists()).to.equal(false);
      const widgetsAfterUpdate = wrapper.find('[data-test="widgets"]');
      expect(widgetsAfterUpdate.exists()).to.equal(false);
    });

    it('Can toggle map controls display', async () => {
      await wrapper.setProps({ showMapControls: true });
      await flushPromises();

      const mapControls = wrapper.find('div.controls');
      expect(mapControls.exists()).to.equal(true);

      await wrapper.setProps({ showMapControls: false });
      await flushPromises();

      const mapControlsAfterUpdate = wrapper.find('div.controls');
      expect(mapControlsAfterUpdate.exists()).to.equal(false);
    });

    it('Can toggle widgets buttons display (not affecting data modes)', async () => {
      await wrapper.setProps({ showDataModes: true });
      await wrapper.setProps({ showWidgets: true });
      await flushPromises();

      const dataModes = wrapper.find('[data-test="data-modes"]');
      expect(dataModes.exists()).to.equal(true);
      const widgets = wrapper.find('[data-test="widgets"]');
      expect(widgets.exists()).to.equal(true);

      await wrapper.setProps({ showWidgets: false });
      await flushPromises();

      const dataModesAfterUpdate = wrapper.find('[data-test="data-modes"]');
      expect(dataModesAfterUpdate.exists()).to.equal(true);
      const widgetsAfterUpdate = wrapper.find('[data-test="widgets"]');
      expect(widgetsAfterUpdate.exists()).to.equal(false);
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('Button interactions', () => {
    before(async () => {
      wrapper = mount(Navigation);
      await flushPromises();
    });

    it('Tries to redirect on route button click', async () => {
      for (const { name } of defaultRouteLinks) {
        const routeLinkButton = wrapper.find(`[data-test="route-link-${name}"] button`);
        expect(routeLinkButton.exists()).to.equal(true);

        await routeLinkButton.trigger('click');

        expect(wrapper.emitted()).to.have.property('navigated');
        expect(globalRouterMock.push.lastCall.args).to.deep.equal([{ name }]);
      }
    });

    it('Tries to activate widget on widget button click', async () => {
      for (const { name } of defaultWidgets) {
        const widgetButton = wrapper.find(`[data-test="widget-${name}"] button`);
        expect(widgetButton.exists()).to.equal(true);

        await widgetButton.trigger('click');

        expect(globalStoreMock.dispatch.getCall(-1).args).to.deep.equal(['map/toggleWidget', { name }]);
      }
    });

    it('Tries to change data mode on data mode button click', async () => {
      for (const { name } of defaultDataModes) {
        const dataModeButton = wrapper.find(`[data-test="data-mode-${name}"] button`);
        expect(dataModeButton.exists()).to.equal(true);

        await dataModeButton.trigger('click');

        expect(wrapper.emitted()).to.have.property('navigated');

        // click on active data mode button instead tries to toggle the entire widget panel
        if (name === 'model') {
          expect(globalStoreMock.dispatch.getCall(-1).args).to.deep.equal([
            'layout/togglePanel',
            { panelSide: 'right' },
          ]);
        } else {
          expect(globalStoreMock.dispatch.getCall(-1).args).to.deep.equal([
            'map/setMapMode',
            { mode: defaultMapModeByDataMode[name], openWidgetPanel: true },
          ]);
        }
      }
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('Broken data mode', () => {
    const brokenMapMode = 'modelDTA';
    const brokenDataMode = 'model';

    before(async () => {
      testStore = getStoreMock();
      testStore.state.map.brokenMapModes = [brokenMapMode];
      wrapper = mount(Navigation, { mocks: { store: testStore } });
    });

    it('Displays refresh button amongst data mode buttons in case of broken mode', async () => {
      const dataModes = wrapper.find('[data-test="data-modes"]');
      expect(dataModes.exists()).to.equal(true);

      const dataModeButtons = dataModes.findAll('.tm-button');
      expect(dataModeButtons.length).to.equal(defaultDataModes.length);

      defaultDataModes.forEach(({ name, icon }) => {
        if (name === brokenDataMode) {
          const dataModeButton = wrapper.find(`[data-test="data-mode-${name}"]`);
          expect(dataModeButton.exists()).to.equal(false);

          const brokenModeButton = wrapper.find(`[data-test="broken-mode-${name}"]`);
          expect(brokenModeButton.exists()).to.equal(true);

          const brokenModeIcon = wrapper.find(`span.pi-refresh`);
          expect(brokenModeIcon.exists()).to.equal(true);
        } else {
          const dataModeButton = wrapper.find(`[data-test="data-mode-${name}"]`);
          expect(dataModeButton.exists()).to.equal(true);

          const dataModeIcon = wrapper.find(`i.${icon}`);
          expect(dataModeIcon.exists()).to.equal(true);
        }
      });
    });

    it('Tries to refresh page on refresh button click', async () => {
      const dataModes = wrapper.find('[data-test="data-modes"]');
      expect(dataModes.exists()).to.equal(true);

      const refreshButton = dataModes.find(`.tm-button[data-test="broken-mode-${brokenDataMode}"]`);
      expect(refreshButton.exists()).to.equal(true);

      await refreshButton.trigger('click');
      expect(testStore.dispatch.getCall(-1).args).to.deep.equal(['refreshApp']);
    });

    after(() => {
      modelsWrapper.invalidateModels();
      wrapper.unmount();
    });
  });
});
