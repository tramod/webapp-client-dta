import WidgetLegend from '@components/layouts/widgets/WidgetLegend.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

describe('WidgetLegend.spec.js', () => {
  describe('compare map modes', () => {
    before(async () => {
      wrapper = mount(WidgetLegend, { props: { mapMode: 'modelDTA-comparison' } });
      await flushPromises();
    });

    it('displays heading', async () => {
      const heading = wrapper.find('[data-test="heading"]');
      expect(heading.exists()).to.equal(true);
      expect(heading.text()).to.contain('widgets.legend');
    });

    it('displays text content', async () => {
      const content = wrapper.find('div.content');
      expect(content.exists()).to.equal(true);
      const text = content.text();
      expect(text).to.contain('widgets.increase and decrease');
      expect(text).to.contain('widgets.large decrease');
      expect(text).to.contain('widgets.medium decrease');
      expect(text).to.contain('widgets.small decrease');
      expect(text).to.contain('widgets.minimal decrease');
      expect(text).to.contain('widgets.small increase');
      expect(text).to.contain('widgets.medium increase');
      expect(text).to.contain('widgets.large increase');
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('other map modes', () => {
    before(async () => {
      wrapper = mount(WidgetLegend, { props: { mapMode: 'modelDTA-viewer' } });
      await flushPromises();
    });

    it('displays heading', async () => {
      const heading = wrapper.find('[data-test="heading"]');
      expect(heading.exists()).to.equal(true);
      expect(heading.text()).to.contain('widgets.legend');
    });

    it('displays text content', async () => {
      const content = wrapper.find('div.content');
      expect(content.exists()).to.equal(true);
      const text = content.text();
      expect(text).to.contain('widgets.traffic');
      expect(text).to.contain('widgets.traffic level');
      expect(text).to.contain('widgets.fluent traffic');
      expect(text).to.contain('widgets.fluent traffic with groups');
      expect(text).to.contain('widgets.continuous traffic');
      expect(text).to.contain('widgets.traffic jams emerging');
      expect(text).to.contain('widgets.traffic jam');
    });

    after(() => {
      wrapper.unmount();
    });
  });
});
