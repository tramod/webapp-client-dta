import WidgetComparison from '@components/layouts/widgets/WidgetComparison.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';
import { mockScenariosEndpoint, axiosMock, API_HOST } from '../../mockAxios.js';
import { activateScenario } from '../../scenarioSetup.js';
import { getStoreMock } from '../../helpers.js';

const testScenarios = [
  {
    id: 1,
    name: 'Scenario 1',
  },
  {
    id: 2,
    name: 'Scenario 2',
  },
  {
    id: 3,
    name: 'Scenario 3',
  },
  {
    id: 4,
    name: 'Scenario 4',
  },
  {
    id: 5,
    name: 'Scenario 5',
  },
];

const testScenario = {
  id: 6,
  name: 'Scenario 6',
};

let wrapper, testStore, mapMode;
const jsDate = new Date();
const today = jsDate.toISOString();

describe('WidgetComparison.spec.js', () => {
  before(() => mockScenariosEndpoint({ response: { scenarios: testScenarios } }));

  describe('model DTA comparison', () => {
    before(async () => {
      mapMode = 'modelDTA-comparison';

      testStore = getStoreMock();
      testStore.getters['map/getCalendarDate'] = () => today;
      wrapper = mount(WidgetComparison, { props: { mapMode }, mocks: { store: testStore } });
      await flushPromises();
    });

    it('sets comparison & fetches scenarios on mount', async () => {
      expect(testStore.dispatch.callCount).to.equal(3);
      expect(testStore.dispatch.args[0][0]).to.equal('layout/startLoading');
      expect(testStore.dispatch.args[1][0]).to.equal('layout/finishLoading');
      expect(testStore.dispatch.args[2][0]).to.equal('map/setComparedScenarios');

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios`);
    });

    it('displays heading', async () => {
      const heading = wrapper.find('[data-test="heading"]');
      expect(heading.exists()).to.equal(true);
      expect(heading.text()).to.contain(`widgets.comparison widget`);
    });

    it('displays two scenarios selectors', async () => {
      const autocompletes = wrapper.findAll('.p-autocomplete-dd');
      expect(autocompletes.length).to.equal(2);

      const dropdownButton = wrapper.find('.p-autocomplete-dropdown');
      await dropdownButton.trigger('click');
      await flushPromises();

      const scenariosOptions =
        wrapper.element.parentElement.parentElement.parentElement.querySelectorAll('.p-autocomplete-item');
      expect(scenariosOptions.length).to.equal(testScenarios.length + 1);

      const scenarios = wrapper.vm.filteredScenarios;
      expect(scenarios.length).to.equal(testScenarios.length + 1);
    });

    it('displays two calendars', async () => {
      const calendars = wrapper.findAll('div.tm-calendar');
      expect(calendars.length).to.equal(2);
    });

    it('displays two time sliders', async () => {
      const sliders = wrapper.findAll('div.timeslider');
      expect(sliders.length).to.equal(2);
    });

    it('displays default dates', async () => {
      const date = wrapper.vm.sourceScenarioDate;
      expect(date).to.equal(today);

      const date2 = wrapper.vm.targetScenarioDate;
      expect(date2).to.equal(today);
    });

    it.skip('changes date on click (inline)', async () => {
      // this test is for the inline calendar version only - remove if it is decided popup is the default

      const calendar = wrapper.find('.p-datepicker-calendar');
      const dayButton = calendar.find('tbody tr:nth-child(3) td:nth-child(4) span');
      await dayButton.trigger('click');

      await flushPromises();

      expect(testStore.dispatch.calledOnce).to.equal(true);
      expect(testStore.dispatch.args[0][0]).to.equal('map/setCalendarDate');
    });

    it.skip('changes date on click (popup)', async () => {
      const input = wrapper.find('.p-calendar .p-inputtext');
      expect(input.exists()).to.equal(true);
      await input.trigger('focus');

      // get datepicker directly using querySelector since it is attached to the body and therefore can not be obtained via vue wrapper
      const datepickerElement =
        wrapper.element.parentElement.parentElement.parentElement.querySelector('.p-datepicker');
      expect(datepickerElement).to.not.equal(null);

      const dayButton = datepickerElement.querySelector('tbody tr td span:not([class^="p-disabled"])');

      await dayButton.click();
      await flushPromises();

      expect(testStore.dispatch.calledOnce).to.equal(true);
      expect(testStore.dispatch.args[0][0]).to.equal('map/setCalendarDate');
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('model DTA comparison with active public scenario not in the list', () => {
    before(async () => {
      await activateScenario(testScenario);
      mapMode = 'modelDTA-comparison';

      testStore = getStoreMock();
      testStore.getters['map/getActiveItemId'] = () => testScenario.id;
      testStore.getters['map/getCalendarDate'] = () => today;
      wrapper = mount(WidgetComparison, { props: { mapMode }, mocks: { store: testStore } });
      await flushPromises();
    });

    it('sets comparison & fetches scenarios on mount', async () => {
      expect(testStore.dispatch.callCount).to.equal(3);
      expect(testStore.dispatch.args[0][0]).to.equal('layout/startLoading');
      expect(testStore.dispatch.args[1][0]).to.equal('layout/finishLoading');
      expect(testStore.dispatch.args[2][0]).to.equal('map/setComparedScenarios');

      expect(axiosMock.history.get.length).to.equal(2);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
      expect(axiosMock.history.get[1].url).to.equal(`${API_HOST}scenarios`);
    });

    it('adds displayed (active) scenario to the selection', async () => {
      const dropdownButton = wrapper.find('.p-autocomplete-dropdown');
      await dropdownButton.trigger('click');
      await flushPromises();

      const scenariosOptions =
        wrapper.element.parentElement.parentElement.parentElement.querySelectorAll('.p-autocomplete-item');
      expect(scenariosOptions.length).to.equal(testScenarios.length + 2);

      const scenarios = wrapper.vm.filteredScenarios;
      expect(scenarios.length).to.equal(testScenarios.length + 2);
    });

    after(() => {
      wrapper.unmount();
    });
  });
});
