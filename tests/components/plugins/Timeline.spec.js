import Timeline from '@components/timeline/Timeline.vue';
import { mount, globalComponentStubs } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

const testEvents = [
  {
    id: 3,
    name: 'Aktualni udalost',
    description: 'popis',
    dateFrom: '2020-02-20T00:00:00.000Z',
    dateTo: '2022-03-20T00:00:00.000Z',
    authorUserId: 1,
    included: true,
  },
  {
    id: 4,
    name: 'Stara udalost',
    description: 'popis',
    dateFrom: '2020-02-20T00:00:00.000Z',
    dateTo: '2020-03-20T00:00:00.000Z',
    authorUserId: 2,
    included: true,
  },
  {
    id: 5,
    name: 'Budouci udalost',
    description: 'popis',
    dateFrom: '2022-02-20T00:00:00.000Z',
    dateTo: '2022-03-20T00:00:00.000Z',
    authorUserId: 2,
    included: false,
  },
];

describe('Timeline.spec.js', () => {
  before(async () => {
    wrapper = mount(Timeline, {
      props: { data: testEvents },
      stubs: globalComponentStubs.filter((s) => s !== 'TmTimeline'),
    });
    await flushPromises();
  });

  it('displays timeline wrapper', () => {
    const timeline = wrapper.get('#timeline');
    expect(timeline.exists()).to.equal(true);
  });

  it('displays svg graph', () => {
    const graph = wrapper.get('svg g.graph');
    expect(graph.exists()).to.equal(true);
  });

  it('displays current date cursor', () => {
    const cursor = wrapper.get('svg g.graph g.cursor');
    expect(cursor.exists()).to.equal(true);
  });

  it('displays axis', () => {
    const axis = wrapper.get('svg g.graph g.axe');
    expect(axis.exists()).to.equal(true);
  });

  it('displays events with names inside the graph', () => {
    const graph = wrapper.get('svg g.graph');
    expect(graph.findAll('g.event')).to.have.lengthOf(testEvents.length);

    testEvents.forEach((event) => {
      const span = graph.find(`[data-test="${event.id}"]`);
      expect(span.text()).to.contain(event.name);
    });
  });

  after(() => {
    wrapper.unmount();
  });
});
