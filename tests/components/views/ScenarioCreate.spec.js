import ScenarioCreate from '@views/scenarios/ScenarioCreate.vue';
import { mount, globalToastMock, globalRouterMock } from '../../mountComponent.js';
import { mockCreateScenarioEndpoint, axiosMock } from '../../mockAxios.js';
import { flushPromises } from '@vue/test-utils';
import { getStoreMock } from '../../helpers.js';

const testCreateScenario = {
  name: 'exp name',
  description: 'some exp desc',
  note: '',
  modelType: 'DTA', // this is the default mode
};
const testCreateScenarioResp = {
  id: 99,
  name: 'exp name',
  description: 'some exp desc',
  note: '',
  dateFrom: null,
  dateTo: null,
  authorUserId: 1,
  eventCount: 0,
  hasModelSectionsValid: true,
};
let wrapper;

describe('ScenarioCreate.spec.js', () => {
  describe('on good request', () => {
    before(async () => {
      mockCreateScenarioEndpoint({ response: { scenario: testCreateScenarioResp } }); // axios mock
      wrapper = mount(ScenarioCreate, {
        props: { scenarioId: null }, // empty id prop = 'create mode'
      });
      await flushPromises();
    });

    it('requests nothing from server', async () => {
      expect(axiosMock.history.get.length).to.equal(0);
    });

    it('displays inputs and buttons', () => {
      const input = wrapper.get('input[name=name]');
      expect(input.attributes('placeholder')).to.equal('scenarios.name');

      const textarea1 = wrapper.get('textarea[name=description]');
      expect(textarea1.attributes('placeholder')).to.equal('scenarios.note');

      const textarea2 = wrapper.get('textarea[name=note]');
      expect(textarea2.attributes('placeholder')).to.equal('scenarios.internal note');

      const confirmButton = wrapper.get('button[data-type="confirm"]');
      expect(confirmButton.exists()).to.equal(true);

      const cancelButton = wrapper.get('button[data-type="cancel"]');
      expect(cancelButton.exists()).to.equal(true);
    });

    it('redirects on cancel button click', async () => {
      const cancelButton = wrapper.get('button[data-type="cancel"]');

      await cancelButton.trigger('click');
      expect(globalRouterMock.push.lastCall.args).to.deep.equal([{ name: 'scenarios' }]);
    });

    it('shows message and redirects on successful create', async () => {
      const input = wrapper.get('input[name=name]');
      const textarea = wrapper.get('textarea[name=description]');
      const confirmButton = wrapper.get('button[data-type="confirm"]');

      await input.setValue(testCreateScenario.name);
      await textarea.setValue(testCreateScenario.description);
      await flushPromises(); // needed for validation

      expect(confirmButton.element.disabled).to.equal(false);

      await confirmButton.trigger('click');
      await flushPromises(); // needed for axios & router call

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].data).to.deep.equal(JSON.stringify(testCreateScenario));

      expect(globalRouterMock.push.lastCall.args).to.deep.equal([
        { name: 'scenarios.events', params: { id: testCreateScenarioResp.id } },
      ]);

      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('success');
    });

    it('can not be submited with invalid input values', async () => {
      const input = wrapper.get('input[name=name]');
      const textarea = wrapper.get('textarea[name=description]');
      const inputError = wrapper.get('#help-input-name');
      const textareaError = wrapper.get('#help-input-description');
      const confirmButton = wrapper.get('button[data-type="confirm"]');

      expect(inputError.text()).to.have.length(0);
      expect(textareaError.text()).to.have.length(0);

      await input.setValue('');
      await textarea.setValue('asd');
      await flushPromises();

      expect(confirmButton.element.disabled).to.equal(true);
      // expect(textareaError.text()).to.have.length.above(1);
      expect(inputError.text()).to.have.length.above(1);
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('on bad requests', () => {
    it('displays message on create mode server error', async () => {
      mockCreateScenarioEndpoint({ code: 400 });
      wrapper = mount(ScenarioCreate, {
        props: { scenarioId: null }, // empty id prop = 'create mode'
      });
      await flushPromises();

      const input = wrapper.get('input[name=name]');
      const textarea = wrapper.get('textarea[name=description]');
      const confirmButton = wrapper.get('button[data-type="confirm"]');
      await input.setValue(testCreateScenario.name);
      await textarea.setValue(testCreateScenario.description);
      await flushPromises();

      expect(confirmButton.element.disabled).to.equal(false);

      await confirmButton.trigger('click');
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].data).to.deep.equal(JSON.stringify(testCreateScenario));
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('error');

      wrapper.unmount();
    });
  });

  describe('in DTA mode', () => {
    before(async () => {
      mockCreateScenarioEndpoint({ response: { scenario: testCreateScenarioResp } });
      const testStore = getStoreMock();
      testStore.state.scenarios.model.type = 'DTA';
      wrapper = mount(ScenarioCreate, {
        props: { scenarioId: null },
        mocks: { store: testStore },
      });
      await flushPromises();
    });

    it('displays message on create mode server error', async () => {
      const input = wrapper.get('input[name=name]');
      const textarea = wrapper.get('textarea[name=description]');
      const confirmButton = wrapper.get('button[data-type="confirm"]');
      await input.setValue(testCreateScenario.name);
      await textarea.setValue(testCreateScenario.description);
      await flushPromises();

      expect(confirmButton.element.disabled).to.equal(false);

      await confirmButton.trigger('click');
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].data).to.deep.equal(
        JSON.stringify({
          ...testCreateScenario,
          modelType: 'DTA',
        }),
      );
    });

    after(() => {
      wrapper.unmount();
    });
  });
});
