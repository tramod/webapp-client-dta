import EventCreate from '@views/scenarios/EventCreate.vue';
import { mount, globalToastMock, globalRouterMock } from '../../mountComponent.js';
import { mockCreateEventEndpoint, axiosMock, API_HOST } from '../../mockAxios.js';
import { flushPromises } from '@vue/test-utils';
import { activateScenario } from '../../scenarioSetup.js';

const testEvent = {
  id: 1,
  name: 'Test event',
  description: 'Test event description',
  note: '',
  type: 'link',
  dateFrom: '2021-03-09T08:28:27.150Z',
  dateTo: '2021-03-09T08:28:27.150Z',
};
const testCreateEvent = {
  name: 'new event name',
  description: 'some event desc',
  note: '',
  type: 'link',
};
const testCreateEventResp = {
  id: 99,
  name: 'new event name',
  description: 'some event desc',
  note: '',
  type: 'link',
  dateFrom: null,
  dateTo: null,
};
const testScenario = {
  id: 1,
  events: [testEvent],
};

let wrapper;

describe('EventCreate.spec.js', () => {
  describe('on good request', () => {
    before(async () => {
      await activateScenario(testScenario);
      mockCreateEventEndpoint({ response: { event: testCreateEventResp }, scenarioId: testScenario.id });
      wrapper = mount(EventCreate, {
        props: { scenarioId: testScenario.id, eventId: null },
      });
      await flushPromises();
    });

    it('requests only scenario from server', async () => {
      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
    });

    it('displays inputs and buttons', () => {
      const input = wrapper.get('input[name=name]');
      expect(input.attributes('placeholder')).to.equal('scenarios.name');

      const textarea1 = wrapper.get('textarea[name=description]');
      expect(textarea1.attributes('placeholder')).to.equal('scenarios.note');

      const textarea2 = wrapper.get('textarea[name=note]');
      expect(textarea2.attributes('placeholder')).to.equal('scenarios.internal note');

      const confirmButton = wrapper.get('button[data-type="confirm"]');
      expect(confirmButton.exists()).to.equal(true);

      const cancelButton = wrapper.get('button[data-type="cancel"]');
      expect(cancelButton.exists()).to.equal(true);
    });

    it('redirects on cancel button click', async () => {
      const cancelButton = wrapper.get('button[data-type="cancel"]');

      await cancelButton.trigger('click');
      expect(globalRouterMock.push.lastCall.args).to.deep.equal([{ name: 'scenarios.events' }]);
    });

    it('shows message and redirects on successful create', async () => {
      const input = wrapper.get('input[name=name]');
      const textarea = wrapper.get('textarea[name=description]');
      const confirmButton = wrapper.get('button[data-type="confirm"]');

      // expect(confirmButton.element.disabled).to.equal(true);

      await input.setValue(testCreateEvent.name);
      await textarea.setValue(testCreateEvent.description);
      await flushPromises();

      expect(confirmButton.element.disabled).to.equal(false);

      await confirmButton.trigger('click');
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].data).to.deep.equal(JSON.stringify(testCreateEvent));

      expect(globalRouterMock.push.lastCall.args).to.deep.equal([
        { name: 'scenarios.events.modifications', params: { evId: testCreateEventResp.id } },
      ]);

      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('success');
    });

    it('can not be submited with invalid input values', async () => {
      const input = wrapper.get('input[name=name]');
      const textarea = wrapper.get('textarea[name=description]');
      const inputError = wrapper.get('#help-input-name');
      const textareaError = wrapper.get('#help-input-description');
      const confirmButton = wrapper.get('button[data-type="confirm"]');

      expect(inputError.text()).to.have.length(0);
      expect(textareaError.text()).to.have.length(0);

      await input.setValue('');
      await textarea.setValue('sadsa');
      await flushPromises();

      expect(confirmButton.element.disabled).to.equal(true);
      // expect(textareaError.text()).to.have.length.above(1);
      expect(inputError.text()).to.have.length.above(1);
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('On bad requests', () => {
    it('displays message on create mode server error', async () => {
      mockCreateEventEndpoint({ code: 400 });
      wrapper = mount(EventCreate, {
        props: { scenarioId: testScenario.id, eventId: null },
      });
      await flushPromises();

      const input = wrapper.get('input[name=name]');
      const textarea = wrapper.get('textarea[name=description]');
      const confirmButton = wrapper.get('button[data-type="confirm"]');
      await input.setValue(testCreateEvent.name);
      await textarea.setValue(testCreateEvent.description);
      await flushPromises();

      expect(confirmButton.element.disabled).to.equal(false);

      await confirmButton.trigger('click');
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].data).to.deep.equal(JSON.stringify(testCreateEvent));
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('error');

      wrapper.unmount();
    });
  });
});
