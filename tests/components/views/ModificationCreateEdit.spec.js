import ModificationCreateEdit from '@views/scenarios/ModificationCreateEdit.vue';
import ModificationLink from '@components/views/ModificationLink.vue';
import { mount, globalToastMock, globalRouterMock } from '../../mountComponent.js';
import useLayerSource from '@composables/useLayerSourceData';
import { LID_DTA_LINKS, LID_DTA_NODES, LID_DTA_GENERATORS } from '@keys/index';
import { mount as mountComposable } from '../../mountComposable';
import {
  mockUpdateModificationEndpoint,
  mockCreateModificationEndpoint,
  mockModelLinksSourceEndpoint,
  mockModelNodesSourceEndpoint,
  mockModelGeneratorsSourceEndpoint,
  axiosMock,
  resetAxiosRequestHistory,
  API_HOST,
} from '../../mockAxios.js';
import { flushPromises } from '@vue/test-utils';
import { activateScenario } from '../../scenarioSetup.js';

const testModification = {
  id: 1,
  dateFrom: '2020-02-20T00:00:00.000Z',
  dateTo: '2022-03-20T00:00:00.000Z',
  type: 'link',
  speed: 100,
  capacity: 1000,
  name: 'test name',
  description: 'test description',
  note: 'test note',
};
const testCreateModificationResp = {
  id: 99,
  dateFrom: '2020-04-20T00:00:00.000Z',
  dateTo: '2022-05-20T00:00:00.000Z',
  speed: 20,
  capacity: 300,
};
const testEvent = {
  id: 1,
  modifications: [testModification],
};
const testScenario = {
  id: 1,
  level: 'editor',
  events: [testEvent],
};

const testBaseModel = {
  [LID_DTA_LINKS]: [
    {
      type: 'Feature',
      properties: {
        edge_id: 1,
        source: 1,
        target: 2,
        capacity: 1000.0,
        cost: 0.01,
        speed: 5.0,
        type: 0,
      },
      geometry: {
        type: 'LineString',
        coordinates: [
          [100.0, 100.0],
          [200.0, 200.0],
        ],
      },
    },
  ],
  [LID_DTA_NODES]: [],
  [LID_DTA_GENERATORS]: [],
};

const stubs = {
  MapInteractionWrapper: true,
  TmBreadcrumb: true,
  RouterLink: true,
  ModificationLink,
};

let wrapper;

describe('ModificationCreateEdit.spec.js', () => {
  const lsUnmounts = [];

  // Setup mock modelData once for whole test suite
  before(async () => {
    const endpointMocks = {
      [LID_DTA_LINKS]: mockModelLinksSourceEndpoint,
      [LID_DTA_NODES]: mockModelNodesSourceEndpoint,
      [LID_DTA_GENERATORS]: mockModelGeneratorsSourceEndpoint,
    };
    const types = Object.keys(endpointMocks);
    await Promise.all(
      types.map(async (modelType) => {
        endpointMocks[modelType]({ response: { features: testBaseModel[modelType] } });
        wrapper = mountComposable(() => useLayerSource(modelType));
        wrapper.clearSourceData();
        await wrapper.fetchSourceData();
        lsUnmounts.push(wrapper.unmount);
      }),
    );
    resetAxiosRequestHistory();
  });

  describe('CREATE mode', () => {
    before(async () => {
      await activateScenario(testScenario);
      mockCreateModificationEndpoint({
        response: { modification: testCreateModificationResp },
        scenarioId: testScenario.id,
        eventId: testEvent.id,
      });
      wrapper = mount(ModificationCreateEdit, {
        props: {
          scenarioId: testScenario.id,
          eventId: testEvent.id,
          modificationId: null,
          modType: 'link',
          modMode: 'existing',
        },
        stubs,
      });
      await flushPromises();
    });

    it('requests scenario and event from server', async () => {
      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
      // expect(axiosMock.history.get[1].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}`);
    });

    it('displays buttons', () => {
      const splitButtonDefault = wrapper.get('button[data-test="split-button-default"]');
      expect(splitButtonDefault.exists()).to.equal(true);

      const splitButtonMenu = wrapper.get('button[data-test="split-button-menu"]');
      expect(splitButtonMenu.exists()).to.equal(true);

      const cancelButton = wrapper.get('button[data-type="back"]');
      expect(cancelButton.exists()).to.equal(true);
    });

    it('redirects on cancel button click', async () => {
      const splitButton = wrapper.get('button[data-test="split-button-menu"]');
      await splitButton.trigger('click');

      // get menu directly using querySelector since the menu is attached to the body and therefore can not be obtained via vue wrapper
      const testMenu = wrapper.element.parentElement.parentElement.parentElement.querySelector('#menu-overlay');
      const testItem = testMenu.querySelector('.p-menuitem-link');
      testItem.click();

      await flushPromises();

      // const splitButtonMenu = wrapper.get('#menu-overlay'); // ! bevare ! since the menu is attached-to body - the component can not be unmounted unitl closed (child node error)
      // expect(splitButtonMenu.exists()).to.equal(true);
      // const cancelButton = wrapper.findAll('.p-menuitem-link')[0];
      // await cancelButton.trigger('click');
      expect(globalRouterMock.push.lastCall.args).to.deep.equal([{ name: 'scenarios.events.modifications' }]);
    });

    it('shows message and redirects on successful create', async () => {
      const confirmButton = wrapper.get('button[data-test="split-button-default"]');
      await confirmButton.trigger('click');
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(globalRouterMock.push.lastCall.args).to.deep.equal([{ name: 'scenarios.events.modifications' }]);
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('success');
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('EDIT mode', () => {
    before(async () => {
      await activateScenario(testScenario);
      mockUpdateModificationEndpoint({
        response: { modification: testCreateModificationResp },
        scenarioId: testScenario.id,
        eventId: testEvent.id,
        modificationId: testModification.id,
      });

      wrapper = mount(ModificationCreateEdit, {
        props: {
          scenarioId: testScenario.id,
          eventId: testEvent.id,
          modificationId: testModification.id,
          modType: 'link',
          modMode: 'existing',
        },
        stubs,
      });
      await flushPromises();
    });

    it('requests scenario, event and modification from server', async () => {
      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
      // expect(axiosMock.history.get[1].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}`);
      // expect(axiosMock.history.get[2].url).to.equal(
      //   `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}/modifications/${testModification.id}`,
      // );
    });

    it('displays header form', () => {
      const input = wrapper.get('input[name=name]');
      expect(input.attributes('placeholder')).to.equal('scenarios.name');

      const textarea1 = wrapper.get('textarea[name=description]');
      expect(textarea1.attributes('placeholder')).to.equal('scenarios.note');

      const textarea2 = wrapper.get('textarea[name=note]');
      expect(textarea2.attributes('placeholder')).to.equal('scenarios.internal note');
    });

    it('shows message on successful update', async () => {
      // TODO Stubs (or something else) seems to enough for full tree load here
      const subtypeButton = wrapper.findAll('div[role="radio"]')[0];
      await subtypeButton.trigger('click');
      await flushPromises();

      const confirmButton = wrapper.get('button[data-test="split-button-default"]');
      expect(confirmButton.element.disabled).to.equal(false);
      await confirmButton.trigger('click');
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(globalRouterMock.push.lastCall.args).to.deep.equal([{ name: 'scenarios.events.modifications' }]);
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('success');
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('On bad requests', () => {
    it('displays message on create mode server error', async () => {
      await activateScenario(testScenario);
      mockCreateModificationEndpoint({ code: 400 });
      wrapper = mount(ModificationCreateEdit, {
        props: {
          scenarioId: testScenario.id,
          eventId: testEvent.id,
          modificationId: null,
          modType: 'link',
          modMode: 'existing',
        },
        stubs,
      });
      await flushPromises();

      const confirmButton = wrapper.get('button[data-test="split-button-default"]');
      await confirmButton.trigger('click');
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('error');

      wrapper.unmount();
    });

    it('displays message on edit mode server error', async () => {
      await activateScenario(testScenario);
      mockUpdateModificationEndpoint({ code: 400 });

      wrapper = mount(ModificationCreateEdit, {
        props: {
          scenarioId: testScenario.id,
          eventId: testEvent.id,
          modificationId: testModification.id,
          modType: 'link',
          modMode: 'existing',
        },
        stubs: stubs,
      });
      // TODO stub not working, have to await async loader of dynamic component and flush for full tree load
      await wrapper.vm.modificationTypeComponent.__asyncLoader();
      await flushPromises();
      const subtypeButton = wrapper.findAll('div[role="radio"]')[0];
      await subtypeButton.trigger('click');
      await flushPromises();

      const confirmButton = wrapper.get('button[data-test="split-button-default"]');
      expect(confirmButton.element.disabled).to.equal(false);
      await confirmButton.trigger('click');
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('error');

      wrapper.unmount();
    });
  });

  describe('editing via header form', () => {
    it('displays scenario data inside inputs', async () => {
      await activateScenario(testScenario);
      wrapper = mount(ModificationCreateEdit, {
        props: {
          scenarioId: testScenario.id,
          eventId: testEvent.id,
          modificationId: testModification.id,
          modType: 'link',
          modMode: 'existing',
        },
        stubs,
      });
      await flushPromises();

      const name = wrapper.get('input[name=name]');
      expect(name.element.value).to.equal(testModification.name);

      const description = wrapper.get('textarea[name=description]');
      expect(description.element.value).to.equal(testModification.description);

      const note = wrapper.get('textarea[name=note]');
      expect(note.element.value).to.equal(testModification.note);
    });

    it('shows message on successful update', async () => {
      mockUpdateModificationEndpoint({
        response: { modification: testCreateModificationResp },
        scenarioId: testScenario.id,
        eventId: testEvent.id,
        modificationId: testModification.id,
      });
      await activateScenario(testScenario);
      wrapper = mount(ModificationCreateEdit, {
        props: {
          scenarioId: testScenario.id,
          eventId: testEvent.id,
          modificationId: testModification.id,
          modType: 'link',
          modMode: 'existing',
        },
        stubs,
      });
      await flushPromises();

      const name = wrapper.get('input[name=name]');
      const description = wrapper.get('textarea[name=description]');
      const note = wrapper.get('textarea[name=note]');

      const testModificationData = {
        name: 'test asd',
        description: 'test fgh',
        note: 'test jkl',
      };

      await name.setValue(testModificationData.name);
      await description.setValue(testModificationData.description);
      await note.setValue(testModificationData.note);
      await flushPromises();

      const confirmButton = wrapper.get('button[data-test="split-button-default"]');
      expect(confirmButton.element.disabled).to.equal(false);
      await confirmButton.trigger('click');
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(globalRouterMock.push.lastCall.args).to.deep.equal([{ name: 'scenarios.events.modifications' }]);
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('success');
    });

    it('displays message on server error', async () => {
      mockUpdateModificationEndpoint({ code: 400 });
      await activateScenario(testScenario);
      wrapper = mount(ModificationCreateEdit, {
        props: {
          scenarioId: testScenario.id,
          eventId: testEvent.id,
          modificationId: testModification.id,
          modType: 'link',
          modMode: 'existing',
        },
        stubs,
      });
      await flushPromises();

      const name = wrapper.get('input[name=name]');
      const description = wrapper.get('textarea[name=description]');
      const note = wrapper.get('textarea[name=note]');

      const testModificationData = {
        name: 'test asd',
        description: 'test fgh',
        note: 'test jkl',
      };

      await name.setValue(testModificationData.name);
      await description.setValue(testModificationData.description);
      await note.setValue(testModificationData.note);
      await flushPromises();

      const confirmButton = wrapper.get('button[data-test="split-button-default"]');
      expect(confirmButton.element.disabled).to.equal(false);
      await confirmButton.trigger('click');
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('error');
    });

    afterEach(() => {
      wrapper.unmount();
    });
  });

  after(async () => {
    for (const lsUnmount of lsUnmounts) {
      lsUnmount();
    }
  });
});
