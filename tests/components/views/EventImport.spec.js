import EventImport from '@views/scenarios/EventImport.vue';
import { mount, globalToastMock, globalRouterMock } from '../../mountComponent.js';
import { mockImportableEventsEndpoint, mockImportEventsEndpoint, axiosMock, API_HOST } from '../../mockAxios.js';
import { flushPromises } from '@vue/test-utils';
import { activateScenario, fsToast } from '../../scenarioSetup.js';

const yesterday = new Date(Date.now() - 24 * 60 * 60 * 1000);
const tomorrow = new Date(Date.now() + 24 * 60 * 60 * 1000);
const testEvents = [
  {
    id: 3,
    name: 'Aktualni udalost',
    description: 'popis',
    dateFrom: yesterday,
    dateTo: tomorrow,
    authorUserId: 1,
    included: true,
  },
  {
    id: 4,
    name: 'Stara udalost',
    description: 'popis',
    dateFrom: yesterday,
    dateTo: yesterday,
    authorUserId: 2,
    included: true,
  },
  {
    id: 5,
    name: 'Budouci udalost',
    description: 'popis',
    dateFrom: tomorrow,
    dateTo: tomorrow,
    authorUserId: 2,
    included: false,
  },
];

const testScenario = {
  id: 1,
  name: 'Soukromy scenario',
  description: 'test desc',
  note: 'test note',
  level: 'editor',
  events: testEvents,
};

let wrapper;

describe('EventImport.spec.js', () => {
  describe('on good request', () => {
    before(async () => {
      await activateScenario(testScenario);
      mockImportableEventsEndpoint({ response: { events: testEvents }, scenarioId: testScenario.id });
      mockImportEventsEndpoint({ response: { events: testEvents }, scenarioId: testScenario.id });
      wrapper = mount(EventImport, { props: { scenarioId: testScenario.id } });
      await flushPromises();
    });

    it('requests scenario from server', async () => {
      expect(axiosMock.history.get.length).to.equal(2);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
      expect(axiosMock.history.get[1].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/importable-events`);
    });

    it('displays heading', () => {
      const heading = wrapper.find('[data-test="heading"]');

      expect(heading.exists()).to.equal(true);
      expect(heading.text()).to.contain('scenarios.import events');
    });

    it('displays importable events', () => {
      expect(wrapper.findAll('[data-test="imports"]')).to.have.lengthOf(3);

      testEvents.forEach((event) => {
        const eventPanel = wrapper.find(`[data-test-id="${event.id}"]`);
        expect(eventPanel.text()).to.contain(event.name);
      });
    });

    it('displays panel checkboxes', () => {
      testEvents.forEach((event) => {
        const eventPanel = wrapper.get(`[data-test-id="${event.id}"]`);
        const checkbox = eventPanel.find('input[type=checkbox]');
        expect(checkbox.exists()).to.equal(true);
      });
    });

    it('displays filters', () => {
      const expectedFilters = ['sharing', 'dating', 'inclusion', 'sorting'];

      expectedFilters.forEach((type) => {
        expect(wrapper.find(`[data-test=${type}]`).exists()).to.equal(true);
      });
    });

    it('displays search input', () => {
      const searchInput = wrapper.find('input[name=search]');
      expect(searchInput.exists()).to.equal(true);
    });

    it('displays buttons', () => {
      const expectedButtontypes = ['split-button-default', 'split-button-menu'];

      expectedButtontypes.forEach((type) => {
        expect(wrapper.find(`button[data-test=${type}]`).exists()).to.equal(true);
      });
    });

    it('filters events', async () => {
      expect(wrapper.findAll('[data-test="imports"]')).to.have.lengthOf(3);
      wrapper.vm.selectedDateFilters = [{ name: 'past', label: 'scenarios.past' }];
      await flushPromises();
      expect(wrapper.findAll('[data-test="imports"]')).to.have.lengthOf(1);
      wrapper.vm.selectedInclusionFilters = [
        { name: 'included', field: 'included', value: true, label: 'scenarios.included' },
      ];
      await flushPromises();
      expect(wrapper.findAll('[data-test="imports"]')).to.have.lengthOf(1);
      wrapper.vm.selectedShareFilters = [{ name: 'own', field: 'authorUserId', value: 1, label: 'scenarios.own' }];
      await flushPromises();
      expect(wrapper.findAll('[data-test="imports"]')).to.have.lengthOf(0);
      wrapper.vm.selectedShareFilters = [];
      await flushPromises();
      expect(wrapper.findAll('[data-test="imports"]')).to.have.lengthOf(1);
      wrapper.vm.selectedInclusionFilters = [];
      await flushPromises();
      expect(wrapper.findAll('[data-test="imports"]')).to.have.lengthOf(1);
      wrapper.vm.selectedDateFilters = [];
      await flushPromises();
      expect(wrapper.findAll('[data-test="imports"]')).to.have.lengthOf(3);
    });

    it('redirects on import button click', async () => {
      const createButton = wrapper.get('button[data-test=split-button-default]');
      expect(createButton.element.disabled).to.equal(true);
      const testEvent = testEvents[0];
      const eventPanel = wrapper.get(`[data-test-id="${testEvent.id}"]`);
      const checkbox = eventPanel.find('input[type=checkbox]');
      await checkbox.trigger('click');
      await flushPromises();
      expect(createButton.element.disabled).to.equal(false);
      await createButton.trigger('click');
      await flushPromises();
      expect(globalRouterMock.push.args[0][0].name).to.deep.equal('scenarios.events');
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('on bad scenario request', () => {
    before(async () => {
      await activateScenario(testScenario, { code: 404 });
    });

    beforeEach(async () => {
      wrapper = mount(EventImport, { props: { scenarioId: testScenario.id } });
      await flushPromises();
    });

    it('sends alert messages', () => {
      expect(fsToast.add.args[0][0].severity).to.deep.equal('error');
    });

    it('displays no events', () => {
      expect(wrapper.findAll('[data-test="imports"]')).to.have.lengthOf(0);
    });

    afterEach(() => {
      wrapper.unmount();
    });
  });

  describe('on bad events request', () => {
    before(async () => {
      await activateScenario(testScenario);
      mockImportableEventsEndpoint({ code: 404 });
      mockImportEventsEndpoint({ code: 404 });
      wrapper = mount(EventImport, { props: { scenarioId: testScenario.id } });
      await flushPromises();
    });

    it('sends alert messages', () => {
      expect(globalToastMock.add.args[0][0].severity).to.deep.equal('info');
    });

    it('displays no events', () => {
      expect(wrapper.findAll('[data-test="imports"]')).to.have.lengthOf(0);
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('on bad import request', () => {
    before(async () => {
      await activateScenario(testScenario);
      mockImportableEventsEndpoint({ response: { events: testEvents }, scenarioId: testScenario.id });
      mockImportEventsEndpoint({ code: 404 });
      wrapper = mount(EventImport, { props: { scenarioId: testScenario.id } });
      await flushPromises();
    });

    it('sends alert messages after trying to import', async () => {
      expect(wrapper.findAll('[data-test="imports"]')).to.have.lengthOf(3);

      const createButton = wrapper.get('button[data-test=split-button-default]');
      expect(createButton.element.disabled).to.equal(true);
      const testEvent = testEvents[0];
      const eventPanel = wrapper.get(`[data-test-id="${testEvent.id}"]`);
      const checkbox = eventPanel.find('input[type=checkbox]');
      await checkbox.trigger('click');
      await flushPromises();
      expect(createButton.element.disabled).to.equal(false);
      await createButton.trigger('click');
      await flushPromises();
      expect(globalToastMock.add.args[0][0].severity).to.deep.equal('error');
    });

    after(() => {
      wrapper.unmount();
    });
  });
});
