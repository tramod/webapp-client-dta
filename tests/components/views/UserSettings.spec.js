import UserSettings from '@components/views/UserSettings.vue';
import { mount, globalToastMock } from '../../mountComponent.js';
import {
  mockUserEndpoint,
  mockOrganizationsEndpoint,
  mockCreateUserEndpoint,
  mockUpdateUserEndpoint,
  mockRequestPasswordResetEndpoint,
  axiosMock,
  API_HOST,
} from '../../mockAxios.js';
import { flushPromises } from '@vue/test-utils';
import { mount as mountComposable } from '../../mountComposable';
import useUsers from '@composables/useUsers';

const testUser = {
  id: 1,
  username: 'frantapepa',
  email: 'franta@pepa.com',
  roles: [100],
  organization: 'Roadtwin',
};
const testOrganizations = [
  { id: 1, name: 'org 1' },
  { id: 2, name: 'org 2' },
  { id: 3, name: 'org 3' },
];
let wrapper, useUsersWrapper;

describe('UserSettings.spec.js', () => {
  describe('CREATE mode', () => {
    before(async () => {
      mockCreateUserEndpoint({ response: { user: testUser } });
      mockRequestPasswordResetEndpoint();
      mockUserEndpoint({ response: { user: testUser }, userId: testUser.id });
      mockOrganizationsEndpoint({ response: { organizations: testOrganizations } });
      wrapper = mount(UserSettings, {
        props: { userId: null }, // empty userId = 'create mode'
      });
      useUsersWrapper = mountComposable(useUsers);
      await flushPromises();
    });

    it('requests organizations from server', async () => {
      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}users/organizations`);
    });

    it('displays inputs and buttons', () => {
      const username = wrapper.get('input[name=username]');
      expect(username.exists()).to.equal(true);

      const email = wrapper.get('input[name=email]');
      expect(email.exists()).to.equal(true);

      const organizations = wrapper.get('.p-autocomplete-input');
      expect(organizations.exists()).to.equal(true);

      const role = wrapper.get('.p-checkbox');
      expect(role.exists()).to.equal(true);

      const confirmButton = wrapper.get('button[data-type="confirm"]');
      expect(confirmButton.exists()).to.equal(true);
    });

    it('shows message on successful create', async () => {
      const username = wrapper.get('input[name=username]');
      const email = wrapper.get('input[name=email]');
      const role = wrapper.get('.p-checkbox');
      const confirmButton = wrapper.get('button[data-type="confirm"]');

      await username.setValue(testUser.username);
      await email.setValue(testUser.email);
      await role.trigger('click'); // check admin role
      await flushPromises(); // needed for validation

      await confirmButton.trigger('click');
      await flushPromises(); // needed for axios & router call

      expect(axiosMock.history.post.length).to.equal(2);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}users`);
      expect(axiosMock.history.post[1].url).to.equal(`${API_HOST}users/password-reset`);
      const requestBody = Object.assign({}, testUser, { id: undefined });
      expect(axiosMock.history.post[0].data).to.deep.equal(JSON.stringify(requestBody));
      expect(globalToastMock.add.calledTwice).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
      expect(globalToastMock.add.getCall(-2).args[0].severity).to.equal('success');
    });

    it('can not be submited with invalid input values', async () => {
      const email = wrapper.get('input[name=email]');
      const inputError = wrapper.get('#help-input-email');
      const confirmButton = wrapper.get('button[data-type="confirm"]');

      expect(inputError.text()).to.have.length(0);

      await email.setValue('');
      await confirmButton.trigger('click');
      await flushPromises();

      expect(inputError.text()).to.have.length.above(1);
    });

    after(() => {
      useUsersWrapper.invalidateOrganizations();
      useUsersWrapper.invalidateUsers();
      wrapper.unmount();
    });
  });

  describe('EDIT mode', () => {
    before(async () => {
      mockUpdateUserEndpoint({ response: { user: testUser } });
      mockUserEndpoint({ response: { user: testUser }, userId: testUser.id });
      mockOrganizationsEndpoint({ response: { organizations: testOrganizations } });
      wrapper = mount(UserSettings, {
        props: { userId: testUser.id },
      });
      await flushPromises();
    });

    it('requests user and organizations from server', async () => {
      expect(axiosMock.history.get.length).to.equal(2);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}users/organizations`);
      expect(axiosMock.history.get[1].url).to.equal(`${API_HOST}users/${testUser.id}`);
    });

    it('displays user data inside inputs', () => {
      const username = wrapper.get('input[name=username]');
      expect(username.element.value).to.equal(testUser.username);

      const email = wrapper.get('input[name=email]');
      expect(email.element.value).to.equal(testUser.email);

      const checkedRole = wrapper.get('.p-checkbox-checked');
      expect(checkedRole.exists()).to.equal(true);

      const organizations = wrapper.get('.p-autocomplete-input');
      expect(organizations.element.value).to.equal(testUser.organization);
    });

    it('shows message on successful update', async () => {
      const username = wrapper.get('input[name=username]');
      const confirmButton = wrapper.get('button[data-type="confirm"]');

      const changedUsername = 'asdf';
      await username.setValue(changedUsername);
      await flushPromises();

      await confirmButton.trigger('click');
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      const requestBody = Object.assign({}, testUser, { username: changedUsername });
      expect(axiosMock.history.patch[0].data).to.deep.equal(JSON.stringify(requestBody));
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('success');
    });

    after(() => {
      useUsersWrapper.invalidateOrganizations();
      useUsersWrapper.invalidateUsers();
      wrapper.unmount();
    });
  });

  describe('On bad requests', () => {
    it('displays message on create mode server error', async () => {
      mockCreateUserEndpoint({ code: 403 });
      mockOrganizationsEndpoint({ code: 403 });
      mockRequestPasswordResetEndpoint({ code: 403 });
      wrapper = mount(UserSettings, {
        props: { userId: null },
      });
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}users/organizations`);
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('error');

      const email = wrapper.get('input[name=email]');
      const username = wrapper.get('input[name=username]');
      const confirmButton = wrapper.get('button[data-type="confirm"]');

      await email.setValue(testUser.email);
      await username.setValue(testUser.username);
      await flushPromises();

      await confirmButton.trigger('click');
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}users`);
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('error');
    });

    it('displays message on edit mode server error', async () => {
      mockUpdateUserEndpoint({ code: 403, userId: testUser.id });
      mockUserEndpoint({ userId: testUser.id, response: { user: testUser } });
      mockOrganizationsEndpoint({ code: 403 });
      wrapper = mount(UserSettings, {
        props: { userId: testUser.id },
      });
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(2);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}users/organizations`);
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('error');
      expect(axiosMock.history.get[1].url).to.equal(`${API_HOST}users/${testUser.id}`);

      const email = wrapper.get('input[name=email]');
      const username = wrapper.get('input[name=username]');
      const confirmButton = wrapper.get('button[data-type="confirm"]');

      await email.setValue(testUser.email);
      await username.setValue(testUser.username);
      await flushPromises();

      await confirmButton.trigger('click');
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(axiosMock.history.patch[0].url).to.equal(`${API_HOST}users/${testUser.id}`);
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('error');
    });

    afterEach(() => {
      useUsersWrapper.invalidateOrganizations();
      useUsersWrapper.invalidateUsers();
      wrapper.unmount();
    });

    after(() => {
      useUsersWrapper.unmount();
    });
  });
});
