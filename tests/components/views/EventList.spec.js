import EventList from '@views/scenarios/EventList.vue';
import { mount, globalToastMock, globalRouterMock } from '../../mountComponent.js';
import {
  mockDeleteEventEndpoint,
  mockUpdateScenarioEndpoint,
  mockModelLinksSourceEndpoint,
  mockModelNodesSourceEndpoint,
  mockModelGeneratorsSourceEndpoint,
  axiosMock,
  API_HOST,
} from '../../mockAxios.js';
import { flushPromises } from '@vue/test-utils';
import { fsToast, activateScenario } from '../../scenarioSetup.js';

const yesterday = new Date(Date.now() - 24 * 60 * 60 * 1000);
const tomorrow = new Date(Date.now() + 24 * 60 * 60 * 1000);
const testEvents = [
  {
    id: 3,
    name: 'Aktualni udalost',
    description: 'popis',
    dateFrom: yesterday,
    dateTo: tomorrow,
    authorUserId: 1,
    included: true,
  },
  {
    id: 4,
    name: 'Stara udalost',
    description: 'popis',
    dateFrom: yesterday,
    dateTo: yesterday,
    authorUserId: 2,
    included: true,
  },
  {
    id: 5,
    name: 'Budouci udalost',
    description: 'popis',
    dateFrom: tomorrow,
    dateTo: tomorrow,
    authorUserId: 2,
    included: false,
  },
];

const testScenario = {
  id: 1,
  name: 'Soukromy scenario',
  description: 'test desc',
  note: 'test note',
  level: 'editor',
  events: testEvents,
  hasModelSectionsValid: true,
};

let wrapper;

describe('EventList.spec.js', () => {
  describe('on good request', () => {
    before(async () => {
      await activateScenario(testScenario);
      mockDeleteEventEndpoint({
        scenarioId: testScenario.id,
        eventId: testEvents[0].id,
        response: { deletedModifications: [] },
      });
      // because of imported useScenarioItemMapFit, we also should mock these
      // (but the requests wont fire if this component is not tested alone)
      mockModelLinksSourceEndpoint({ response: {} });
      mockModelNodesSourceEndpoint({ response: {} });
      mockModelGeneratorsSourceEndpoint({ response: {} });
      wrapper = mount(EventList, {
        props: { scenarioId: testScenario.id },
      });
      await flushPromises();
    });

    it('requests scenario from server', async () => {
      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
    });

    it('displays header form', () => {
      const input = wrapper.get('input[name=name]');
      expect(input.attributes('placeholder')).to.equal('scenarios.name');

      const textarea1 = wrapper.get('textarea[name=description]');
      expect(textarea1.attributes('placeholder')).to.equal('scenarios.note');

      const textarea2 = wrapper.get('textarea[name=note]');
      expect(textarea2.attributes('placeholder')).to.equal('scenarios.internal note');
    });

    it('displays events', () => {
      expect(wrapper.findAll('[data-test="events"]')).to.have.lengthOf(3);

      testEvents.forEach((event) => {
        const eventPanel = wrapper.find(`[data-test-id="${event.id}"]`);
        expect(eventPanel.text()).to.contain(event.name);
      });
    });

    it('displays action icons', () => {
      expect(wrapper.findAll('[data-test="events"]')).to.have.lengthOf(3);

      testEvents.forEach((event) => {
        const eventPanel = wrapper.find(`[data-test-id="${event.id}"]`);

        const expectedActions = ['enter', 'delete', 'copy'];
        expectedActions.forEach((type) => {
          expect(eventPanel.find(`i[data-type=${type}]`).exists()).to.equal(true);
        });
      });
    });

    it('displays panel switch inputs with default values', () => {
      testEvents.forEach((event) => {
        const eventPanel = wrapper.get(`[data-test-id="${event.id}"]`);
        const switchInput = eventPanel.find('input[role=switch]');
        expect(switchInput.exists()).to.equal(true);

        if (event.included === true) {
          expect(switchInput.attributes('aria-checked')).to.equal('true');
        } else {
          expect(switchInput.attributes('aria-checked')).to.equal('false');
        }
      });
    });

    it('displays filters', () => {
      const expectedFilters = ['sharing', 'dating', 'inclusion', 'sorting'];

      expectedFilters.forEach((type) => {
        expect(wrapper.find(`[data-test=${type}]`).exists()).to.equal(true);
      });
    });

    it('displays buttons', () => {
      const expectedButtontypes = ['create', 'back'];

      expectedButtontypes.forEach((type) => {
        expect(wrapper.find(`button[data-type=${type}]`).exists()).to.equal(true);
      });
    });

    it('filters events', async () => {
      expect(wrapper.findAll('[data-test="events"]')).to.have.lengthOf(3);
      wrapper.vm.selectedDateFilters = [{ name: 'past', label: 'scenarios.past' }];
      await flushPromises();
      expect(wrapper.findAll('[data-test="events"]')).to.have.lengthOf(1);
      wrapper.vm.selectedInclusionFilters = [
        { name: 'included', field: 'included', value: true, label: 'scenarios.included' },
      ];
      await flushPromises();
      expect(wrapper.findAll('[data-test="events"]')).to.have.lengthOf(1);
      wrapper.vm.selectedShareFilters = [{ name: 'own', field: 'authorUserId', value: 1, label: 'scenarios.own' }];
      await flushPromises();
      expect(wrapper.findAll('[data-test="events"]')).to.have.lengthOf(0);
      wrapper.vm.selectedShareFilters = [];
      await flushPromises();
      expect(wrapper.findAll('[data-test="events"]')).to.have.lengthOf(1);
      wrapper.vm.selectedInclusionFilters = [];
      await flushPromises();
      expect(wrapper.findAll('[data-test="events"]')).to.have.lengthOf(1);
      wrapper.vm.selectedDateFilters = [];
      await flushPromises();
      expect(wrapper.findAll('[data-test="events"]')).to.have.lengthOf(3);
    });

    it('redirects on create button click', async () => {
      const createButton = wrapper.get('button[data-type=create]');

      await createButton.trigger('click');
      expect(globalRouterMock.push.args).to.deep.equal([[{ name: 'scenarios.events.create' }]]);
    });

    it('removes event on delete action click', async () => {
      const testEvent = testEvents[0];
      const eventPanel = wrapper.get(`[data-test-id="${testEvent.id}"]`);
      expect(eventPanel.text()).to.contain(testEvent.name);
      const deleteActionIcon = eventPanel.get('i[data-type=delete]');
      await deleteActionIcon.trigger('click');
      await flushPromises();

      expect(axiosMock.history.delete.length).to.equal(1);
      expect(axiosMock.history.delete[0].url).to.equal(
        `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}`,
      );
      expect(wrapper.find(`[data-test-id="${testEvent.id}"]`).exists()).to.equal(false);
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('on bad request', () => {
    before(async () => {
      await activateScenario(testScenario, { code: 404 });
    });

    beforeEach(async () => {
      wrapper = mount(EventList, {
        props: { scenarioId: testScenario.id },
      });
      await flushPromises();
    });

    it('sends alert messages', () => {
      expect(fsToast.add.args[0][0].severity).to.deep.equal('error');
      // expect(globalToastMock.add.args[1][0].severity).to.deep.equal('info');
    });

    it('displays no events', () => {
      expect(wrapper.findAll('[data-test="events"]')).to.have.lengthOf(0);
    });

    afterEach(() => {
      wrapper.unmount();
    });
  });

  describe('editing via header form', () => {
    before(async () => {
      await activateScenario(testScenario);
    });

    it('displays scenario data inside inputs', async () => {
      wrapper = mount(EventList, { props: { scenarioId: testScenario.id } });
      await flushPromises();

      const name = wrapper.get('input[name=name]');
      expect(name.element.value).to.equal(testScenario.name);

      const description = wrapper.get('textarea[name=description]');
      expect(description.element.value).to.equal(testScenario.description);

      const note = wrapper.get('textarea[name=note]');
      expect(note.element.value).to.equal(testScenario.note);
    });

    it('shows message on successful update', async () => {
      mockUpdateScenarioEndpoint({ response: { scenario: testScenario } });
      wrapper = mount(EventList, { props: { scenarioId: testScenario.id } });
      await flushPromises();

      const name = wrapper.get('input[name=name]');
      const description = wrapper.get('textarea[name=description]');
      const note = wrapper.get('textarea[name=note]');

      const testScenarioData = {
        name: 'test asd',
        description: 'test fgh',
        note: 'test jkl',
      };

      await name.setValue(testScenarioData.name);
      await description.setValue(testScenarioData.description);
      await note.setValue(testScenarioData.note);

      name.trigger('blur'); // force blur on any of the inputs
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(axiosMock.history.patch[0].data).to.deep.equal(JSON.stringify(testScenarioData));
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('success');
    });

    it('displays message on server error', async () => {
      mockUpdateScenarioEndpoint({ code: 400 });
      wrapper = mount(EventList, { props: { scenarioId: testScenario.id } });
      await flushPromises();

      const name = wrapper.get('input[name=name]');
      const description = wrapper.get('textarea[name=description]');
      const note = wrapper.get('textarea[name=note]');

      const testScenarioData = {
        name: 'test asd',
        description: 'test fgh',
        note: 'test jkl',
      };

      await name.setValue(testScenarioData.name);
      await description.setValue(testScenarioData.description);
      await note.setValue(testScenarioData.note);

      name.trigger('blur'); // force blur on any of the inputs
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(axiosMock.history.patch[0].data).to.deep.equal(JSON.stringify(testScenarioData));
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('error');
    });

    afterEach(() => {
      wrapper.unmount();
    });
  });
});
