import UsersOverview from '@views/admin/Users.vue';
import { mount, globalToastMock } from '../../mountComponent.js';
import { mockUsersEndpoint, axiosMock, API_HOST } from '../../mockAxios.js';
import { flushPromises } from '@vue/test-utils';

const testUsers = [
  {
    id: 1,
    username: 'franta',
    email: 'franta@franta.com',
    roles: [100, 1],
    organization: 'roadtwin',
  },
  {
    id: 2,
    username: 'pepa',
    email: 'pepa@pepa.com',
    roles: [],
    organization: null,
  },
  {
    id: 3,
    username: 'lojza',
    email: 'lojza@lojza.com',
    roles: [],
    organization: null,
  },
];

let wrapper;

describe('Users.spec.js', () => {
  describe('on good request', () => {
    before(async () => {
      mockUsersEndpoint({ response: { users: testUsers } });
      wrapper = mount(UsersOverview);
      await flushPromises();
    });

    it('requests users from server', async () => {
      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}users`);
    });

    it('displays heading', () => {
      const heading = wrapper.find('[data-test="heading"]');

      expect(heading.exists()).to.equal(true);
      expect(heading.text()).to.contain('admin.user management');
    });

    it('displays users', () => {
      const userRows = wrapper.findAll('tbody tr');
      expect(userRows).to.have.lengthOf(testUsers.length);

      let userIndex = 0;
      userRows.forEach((row) => {
        const rowText = row.text();
        expect(rowText).to.contain(testUsers[userIndex].email);
        if (testUsers[userIndex].username) expect(rowText).to.contain(testUsers[userIndex].username);
        if (testUsers[userIndex].organization) expect(rowText).to.contain(testUsers[userIndex].organization);
        if (testUsers[userIndex].roles && testUsers[userIndex].roles.includes(100))
          expect(rowText).to.contain('admin.administrator');
        userIndex++;
      });
    });

    it('displays buttons', () => {
      const expectedButtontypes = ['create'];

      expectedButtontypes.forEach((type) => {
        expect(wrapper.find(`button[data-type=${type}]`).exists()).to.equal(true);
      });

      const logoutIcon = wrapper.find('.pi-sign-out');
      expect(logoutIcon.exists()).to.equal(true);

      const userRows = wrapper.findAll('tbody tr');
      userRows.forEach((row) => {
        const editIcon = row.find('.pi-pencil');
        expect(editIcon.exists()).to.equal(true);

        const deleteIcon = row.find('.pi-trash');
        expect(deleteIcon.exists()).to.equal(true);
      });
    });

    after(() => {
      wrapper.vm.invalidateUsers();
      wrapper.unmount();
    });
  });

  describe('on bad request', () => {
    before(async () => {
      mockUsersEndpoint({ code: 404 });
      wrapper = mount(UsersOverview);
      await flushPromises();
    });

    it('sends alert message', async () => {
      expect(globalToastMock.add.args[0][0].severity).to.deep.equal('info');
    });

    it('displays no users', async () => {
      const userRows = wrapper.findAll('tbody tr');
      expect(userRows).to.have.lengthOf(1);
    });

    after(() => {
      wrapper.vm.invalidateUsers();
      wrapper.unmount();
    });
  });
});
