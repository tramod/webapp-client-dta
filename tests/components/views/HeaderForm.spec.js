import HeaderForm from '@components/views/HeaderForm.vue';
import { mount, globalRouterMock } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

const testData = {
  name: 'test name',
  description: 'test desc',
  note: 'test note',
};

let wrapper;

describe('HeaderForm.spec.js', () => {
  it('displays inputs', async () => {
    wrapper = mount(HeaderForm, {
      props: { id: testData.id, resource: testData },
    });
    await flushPromises();

    const input = wrapper.find('input[name=name]');
    expect(input.exists()).to.equal(true);

    const description = wrapper.find('textarea[name=description]');
    expect(description.exists()).to.equal(true);

    const note = wrapper.find('textarea[name=note]');
    expect(note.exists()).to.equal(true);

    const cancelButton = wrapper.find('button[data-type="back"]');
    expect(cancelButton.exists()).to.equal(false);
  });

  it('displays back button if set', async () => {
    const backToRouteTest = 'asd';
    wrapper = mount(HeaderForm, {
      props: { id: testData.id, resource: testData, backToRoute: backToRouteTest },
    });
    await flushPromises();

    const input = wrapper.find('input[name=name]');
    expect(input.exists()).to.equal(true);

    const description = wrapper.find('textarea[name=description]');
    expect(description.exists()).to.equal(true);

    const note = wrapper.find('textarea[name=note]');
    expect(note.exists()).to.equal(true);

    const cancelButton = wrapper.find('button[data-type="back"]');
    expect(cancelButton.exists()).to.equal(true);

    await cancelButton.trigger('click');
    expect(globalRouterMock.push.lastCall.args).to.deep.equal([{ name: backToRouteTest }]);
  });

  it('displays skeleton if not ready', async () => {
    wrapper = mount(HeaderForm, {
      props: { id: testData.id, resource: testData, ready: false },
    });
    await flushPromises();

    const input = wrapper.find('input[name=name]');
    expect(input.exists()).to.equal(false);

    const description = wrapper.find('textarea[name=description]');
    expect(description.exists()).to.equal(false);

    const note = wrapper.find('textarea[name=note]');
    expect(note.exists()).to.equal(false);

    const skeleton = wrapper.find('.p-skeleton');
    expect(skeleton.exists()).to.equal(true);
  });

  it('displays resource data inside inputs', async () => {
    wrapper = mount(HeaderForm, {
      props: { id: testData.id, resource: testData },
    });
    await flushPromises();

    const name = wrapper.get('input[name=name]');
    expect(name.element.value).to.equal(testData.name);

    const description = wrapper.get('textarea[name=description]');
    expect(description.element.value).to.equal(testData.description);

    const note = wrapper.get('textarea[name=note]');
    expect(note.element.value).to.equal(testData.note);
  });

  it('hides note and disables editing without editorAccess', async () => {
    wrapper = mount(HeaderForm, {
      props: { id: testData.id, resource: testData, hasEditorAccess: false },
    });
    await flushPromises();

    const input = wrapper.find('input[name=name]');
    expect(input.exists()).to.equal(true);
    expect(input.attributes('readonly')).to.equal(''); // not undefined

    const description = wrapper.find('textarea[name=description]');
    expect(description.exists()).to.equal(true);
    expect(input.attributes('readonly')).to.equal(''); // not undefined

    const note = wrapper.find('textarea[name=note]');
    expect(note.exists()).to.equal(false);
  });

  it('accepts and enforces validation rules', async () => {
    const testRules = { name: 'required|email', description: 'required' };
    wrapper = mount(HeaderForm, {
      props: { id: testData.id, resource: testData, rules: testRules },
    });
    await flushPromises();

    const name = wrapper.find('input[name=name]');
    expect(name.element.value).to.equal(testData.name);
    const description = wrapper.get('textarea[name=description]');
    expect(description.element.value).to.equal(testData.description);

    const nameError = wrapper.find('small#help-input-name');
    expect(nameError.exists()).to.equal(true);
    expect(nameError.text()).to.equal('Pole název musí být validní email'); // since vue 3.2.37 the initial value validation is triggered
    const descriptionError = wrapper.find('small#help-input-description');
    expect(descriptionError.exists()).to.equal(true);
    expect(descriptionError.text()).to.equal('');

    name.setValue('');
    await flushPromises();
    expect(nameError.text()).to.equal(`Pole název je povinné`);

    name.setValue('asd');
    await flushPromises();
    expect(nameError.text()).to.equal(`Pole název musí být validní email`);

    name.setValue('test@email.com');
    await flushPromises();
    expect(nameError.text()).to.equal('');

    description.setValue('');
    await flushPromises();
    expect(descriptionError.text()).to.equal(`Pole popis je povinné`);

    description.setValue('asd');
    await flushPromises();
    expect(descriptionError.text()).to.equal('');
  });

  it('updates and emits valid state and resource on input', async () => {
    wrapper = mount(HeaderForm, {
      props: { id: testData.id, resource: testData, rules: { name: 'required' } },
    });
    await flushPromises();

    const changedName = 'adasdsdads';
    const input = wrapper.find(`input[name=name]`);
    expect(input.element.value).to.equal(testData.name);

    input.setValue(changedName);
    await flushPromises();

    expect(input.element.value).to.equal(changedName);
    expect(wrapper.emitted()).to.have.property('update:resource');
    expect(wrapper.emitted()).to.have.property('update:valid');
    expect(wrapper.emitted()['update:valid'][0][0]).to.deep.equal(true); // initial emit
    expect(wrapper.emitted()['update:valid'][1][0]).to.deep.equal(true); // updated emit
    expect(wrapper.emitted()['update:resource'][0][0].name).to.deep.equal(testData.name); // initial emit
    expect(wrapper.emitted()['update:resource'][1][0].name).to.deep.equal(changedName); // updated emit

    input.setValue('');
    await flushPromises();
    expect(wrapper.emitted()['update:valid'][2][0]).to.deep.equal(false);
    expect(wrapper.emitted()['update:resource'][2][0].name).to.deep.equal('');
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
