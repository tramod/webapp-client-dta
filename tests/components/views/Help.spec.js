import Help from '@views/settings/Help.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

describe('Help.spec.js', () => {
  before(async () => {
    wrapper = mount(Help);
    await flushPromises();
  });

  it('displays heading', async () => {
    const heading = wrapper.find('[data-test="heading"]');
    expect(heading.exists()).to.equal(true);
    expect(heading.text()).to.contain('help.help');
  });

  it('displays text content', async () => {
    const content = wrapper.find('div.content');
    expect(content.exists()).to.equal(true);
    const text = content.text();
    expect(text).to.contain('help.user');
    expect(text).to.contain('help.scenarios');
    expect(text).to.contain('help.settings');
    expect(text).to.contain('help.map');
  });

  after(() => {
    wrapper.unmount();
  });
});
