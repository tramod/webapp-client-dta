import ModificationList from '@views/scenarios/ModificationList.vue';
import { mount, globalToastMock, globalRouterMock } from '../../mountComponent.js';
import { mockUpdateEventEndpoint, mockDeleteModificationEndpoint, axiosMock, API_HOST } from '../../mockAxios.js';
import { flushPromises } from '@vue/test-utils';
import { fsToast, activateScenario } from '../../scenarioSetup.js';

const testModifications = [
  {
    id: 1,
    dateFrom: '2020-02-20T00:00:00.000Z',
    dateTo: '2022-03-20T00:00:00.000Z',
    type: 'link',
    mode: 'existing',
    speed: 100,
    capacity: 1000,
  },
  {
    id: 2,
    dateFrom: '2020-02-20T00:00:00.000Z',
    dateTo: '2022-03-20T00:00:00.000Z',
    type: 'link',
    mode: 'new',
    speed: 50,
    capacity: 500,
  },
  {
    id: 3,
    dateFrom: '2020-02-20T00:00:00.000Z',
    dateTo: '2022-03-20T00:00:00.000Z',
    type: 'node',
    mode: 'existing',
  },
];

const testEvent = {
  id: 1,
  name: 'Test event',
  description: 'test desc',
  note: 'test note',
  modifications: testModifications,
};

const testScenario = {
  id: 1,
  level: 'editor',
  events: [testEvent],
  hasModelSectionsValid: true,
};

let wrapper;

describe('ModificationList.spec.js', () => {
  describe('on good request', () => {
    before(async () => {
      await activateScenario(testScenario);
      mockDeleteModificationEndpoint({
        response: { deletedModifications: [testModifications[0].id] },
        scenarioId: testScenario.id,
        eventId: testEvent.id,
        modificationId: testModifications[0].id,
      });
      wrapper = mount(ModificationList, {
        props: { scenarioId: testScenario.id, eventId: testEvent.id },
      });
      await flushPromises();
    });

    it('requests scenario from server', async () => {
      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
      // expect(axiosMock.history.get[1].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}`);
      // expect(axiosMock.history.get[2].url).to.equal(
      //   `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}/modifications`,
      // );
    });

    it('displays header form', () => {
      const input = wrapper.get('input[name=name]');
      expect(input.attributes('placeholder')).to.equal('scenarios.name');

      const textarea1 = wrapper.get('textarea[name=description]');
      expect(textarea1.attributes('placeholder')).to.equal('scenarios.note');

      const textarea2 = wrapper.get('textarea[name=note]');
      expect(textarea2.attributes('placeholder')).to.equal('scenarios.internal note');
    });

    it('displays modifications', () => {
      expect(wrapper.findAll('[data-test="modifications"]')).to.have.lengthOf(3);

      testModifications.forEach((modification) => {
        const modificationPanel = wrapper.find(`[data-test-id="${modification.id}"]`);
        expect(modificationPanel.text()).to.contain(modification.id);
      });
    });

    it('displays action icons', () => {
      expect(wrapper.findAll('[data-test="modifications"]')).to.have.lengthOf(3);

      testModifications.forEach((event) => {
        const modificationPanel = wrapper.find(`[data-test-id="${event.id}"]`);

        const expectedActions = ['enter', 'delete', 'copy'];
        expectedActions.forEach((type) => {
          expect(modificationPanel.find(`i[data-type=${type}]`).exists()).to.equal(true);
        });
      });
    });

    it('displays buttons', () => {
      const expectedButtontypes = ['cancel', 'back', 'create'];

      expectedButtontypes.forEach((type) => {
        expect(wrapper.find(`button[data-type=${type}]`).exists()).to.equal(true);
      });

      const createButtons = wrapper.findAll(`button[data-type=create]`);
      expect(createButtons.length).to.equal(9); // matrix filtered out
    });

    it('redirects on create (link/node) button click', async () => {
      const createLinkButton = wrapper.get('.tm-button[data-test=create-link]');
      await createLinkButton.trigger('click');

      const createNodeButton = wrapper.get('.tm-button[data-test=create-node]');
      await createNodeButton.trigger('click');

      expect(globalRouterMock.push.args).to.deep.equal([
        [{ name: 'scenarios.events.modifications.create', params: { modMode: 'new', modType: 'link' } }],
        [{ name: 'scenarios.events.modifications.create', params: { modMode: 'new', modType: 'node' } }],
      ]);
    });

    it('removes modification on delete action click', async () => {
      const testModification = testModifications[0];
      const modificationPanel = wrapper.get(`[data-test-id="${testModification.id}"]`);
      expect(modificationPanel.text()).to.contain(testModification.id);
      const deleteActionIcon = modificationPanel.get('i[data-type=delete]');
      await deleteActionIcon.trigger('click');
      await flushPromises();

      expect(axiosMock.history.delete.length).to.equal(1);
      expect(axiosMock.history.delete[0].url).to.equal(
        `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}/modifications/${testModification.id}`,
      );
      expect(wrapper.find(`[data-test-id="${testModification.id}"]`).exists()).to.equal(false);
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('editing via header form', () => {
    before(async () => await activateScenario(testScenario));

    it('requests scenario from server', async () => {
      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
    });

    it('displays scenario data inside inputs', async () => {
      wrapper = mount(ModificationList, { props: { scenarioId: testScenario.id, eventId: testEvent.id } });
      await flushPromises();

      const name = wrapper.get('input[name=name]');
      expect(name.element.value).to.equal(testEvent.name);

      const description = wrapper.get('textarea[name=description]');
      expect(description.element.value).to.equal(testEvent.description);

      const note = wrapper.get('textarea[name=note]');
      expect(note.element.value).to.equal(testEvent.note);
    });

    it('shows message on successful update', async () => {
      mockUpdateEventEndpoint({ response: { event: testEvent } });
      wrapper = mount(ModificationList, { props: { scenarioId: testScenario.id, eventId: testEvent.id } });
      await flushPromises();

      const name = wrapper.get('input[name=name]');
      const description = wrapper.get('textarea[name=description]');
      const note = wrapper.get('textarea[name=note]');

      const testEventData = {
        name: 'test asd',
        description: 'test fgh',
        note: 'test jkl',
      };

      await name.setValue(testEventData.name);
      await description.setValue(testEventData.description);
      await note.setValue(testEventData.note);

      name.trigger('blur'); // force blur on any of the inputs
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(axiosMock.history.patch[0].data).to.deep.equal(JSON.stringify(testEventData));
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('success');
    });

    it('displays message on server error', async () => {
      mockUpdateEventEndpoint({ code: 400 });
      wrapper = mount(ModificationList, { props: { scenarioId: testScenario.id, eventId: testEvent.id } });
      await flushPromises();

      const name = wrapper.get('input[name=name]');
      const description = wrapper.get('textarea[name=description]');
      const note = wrapper.get('textarea[name=note]');

      const testEventData = {
        name: 'test asd',
        description: 'test fgh',
        note: 'test jkl',
      };

      await name.setValue(testEventData.name);
      await description.setValue(testEventData.description);
      await note.setValue(testEventData.note);

      name.trigger('blur'); // force blur on any of the inputs
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(axiosMock.history.patch[0].data).to.deep.equal(JSON.stringify(testEventData));
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('error');
    });

    afterEach(() => {
      wrapper.unmount();
    });
  });

  describe('on bad request', () => {
    before(async () => {
      await activateScenario(testScenario, { code: 404 });
      wrapper = mount(ModificationList, {
        props: { scenarioId: testScenario.id, eventId: testEvent.id },
      });
      await flushPromises();
    });

    it('requests scenario from server & receives error', async () => {
      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);

      expect(fsToast.add.args[0][0].severity).to.deep.equal('error'); // no scenario error
      // expect(globalToastMock.add.args[1][0].severity).to.deep.equal('error'); // no event error
      // expect(globalToastMock.add.args[2][0].severity).to.deep.equal('info'); // no modifications info
    });

    it('displays no modifications', () => {
      expect(wrapper.findAll('[data-test="modifications"]')).to.have.lengthOf(0);
    });

    after(() => {
      wrapper.unmount();
    });
  });
});
