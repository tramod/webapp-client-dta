import Models from '@views/models/Models.vue';
import { mount, globalStoreMock } from '../../mountComponent.js';
import { mockModelsEndpoint, axiosMock, API_HOST } from '../../mockAxios.js';
import { flushPromises } from '@vue/test-utils';

const testModels = [
  {
    id: 1,
    name: 'DTA-test',
    type: 'DTA',
    isDefault: true,
  },
  {
    id: 2,
    name: 'ABC-test',
    type: 'ABC',
    isDefault: true,
  },
];

let wrapper;

describe('Models.spec.js', () => {
  before(async () => {
    mockModelsEndpoint({ response: { trafficModels: testModels } });
    wrapper = mount(Models);
    await flushPromises();
  });

  it('requests models from server', async () => {
    expect(axiosMock.history.get.length).to.equal(1);
    expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}models`);
  });

  it('displays heading', async () => {
    const heading = wrapper.find('[data-test="heading"]');
    expect(heading.exists()).to.equal(true);
    expect(heading.text()).to.contain('navigation.models');
  });

  it('displays radio buttons', async () => {
    const abcButton = wrapper.find('div[aria-label="models.ABC"]');
    expect(abcButton.exists()).to.equal(true);
    expect(abcButton.attributes('aria-checked')).to.equal('false');
  });

  it('switches model type state on button click', async () => {
    const abcButton = wrapper.find('div[aria-label="models.ABC"]');
    await abcButton.trigger('click');
    await flushPromises();

    // TODO Discuss Wouldnt be better to test with real store and check final state/changed UI?
    expect(globalStoreMock.commit.getCall(-1).args[0]).to.equal('scenarios/SET_MODEL');
    expect(globalStoreMock.commit.getCall(-1).args[1].type).to.equal('ABC');
    expect(globalStoreMock.dispatch.getCall(-1).args[0]).to.equal('map/activateItem');
    expect(globalStoreMock.dispatch.getCall(-1).args[1]).to.deep.equal({});
  });

  after(() => {
    wrapper.unmount();
  });
});
