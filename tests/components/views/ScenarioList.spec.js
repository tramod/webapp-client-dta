import ScenarioList from '@views/scenarios/ScenarioList.vue';
import { mount, globalToastMock, globalRouterMock } from '../../mountComponent.js';
import { mockScenariosEndpoint, axiosMock, API_HOST } from '../../mockAxios.js';
import { flushPromises } from '@vue/test-utils';
import { getStoreMock } from '../../helpers.js';

const testScenarios = [
  {
    id: 1,
    name: 'Vlastni scenario',
    dateFrom: '2021-02-18T09:05:58.731Z',
    dateTo: '2021-02-18T09:05:58.731Z',
    authorUserId: 1,
    isPublic: false,
    isShared: false,
  },
  {
    id: 2,
    name: 'Vlastni a verejny scenario',
    dateFrom: '2021-02-18T09:05:58.731Z',
    dateTo: '2021-02-18T09:05:58.731Z',
    authorUserId: 1,
    isPublic: true,
    isShared: false,
  },
  {
    id: 3,
    name: 'Sdileny scenario',
    dateFrom: '2021-02-18T09:05:58.731Z',
    dateTo: '2021-02-18T09:05:58.731Z',
    authorUserId: 2,
    isPublic: false,
    isShared: true,
  },
  {
    id: 4,
    name: 'Veřejný scenario',
    dateFrom: '2021-02-18T09:05:58.731Z',
    dateTo: '2021-02-18T09:05:58.731Z',
    authorUserId: 2,
    isPublic: true,
    isShared: false,
  },
  {
    id: 5,
    name: 'Veřejný a sdílený scenario',
    dateFrom: '2021-02-18T09:05:58.731Z',
    dateTo: '2021-02-18T09:05:58.731Z',
    authorUserId: 2,
    isPublic: true,
    isShared: true,
  },
];

let wrapper;

describe('ScenarioList.spec.js', () => {
  describe('on good request', () => {
    before(async () => {
      mockScenariosEndpoint({ response: { scenarios: testScenarios } }); // axios mock
      wrapper = mount(ScenarioList);
      await flushPromises(); // resolve any unresolved promises from non-Vue dependencies (axios)
    });

    it('requests scenarios & edit session overview from server', async () => {
      expect(axiosMock.history.get.length).to.equal(2);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios`);
      expect(axiosMock.history.get[1].url).to.equal(`${API_HOST}scenarios/edit/overview`);
    });

    it('displays heading', () => {
      const heading = wrapper.find('[data-test="heading"]');

      expect(heading.exists()).to.equal(true);
      expect(heading.text()).to.contain('scenarios.scenarios');
    });

    it('displays scenarios', () => {
      expect(wrapper.findAll('[data-test="scenarios"]')).to.have.lengthOf(2);

      const privateScenarios = testScenarios.filter((sc) => sc.authorUserId == 1);
      privateScenarios.forEach((scenario) => {
        expect(wrapper.find(`[data-test-id="${scenario.id}"]`).text()).to.contain(scenario.name);
      });
    });

    it('displays filters', () => {
      const expectedFilters = ['sharing', 'dating', 'sorting'];

      expectedFilters.forEach((type) => {
        expect(wrapper.find(`[data-test=${type}]`).exists()).to.equal(true);
      });
    });

    it('displays buttons', () => {
      const expectedButtontypes = ['create', 'back'];

      expectedButtontypes.forEach((type) => {
        expect(wrapper.find(`button[data-type=${type}]`).exists()).to.equal(true);
      });
    });

    it('filters scenarios', async () => {
      expect(wrapper.findAll('[data-test="scenarios"]')).to.have.lengthOf(2);
      wrapper.vm.selectedShareFilters = [
        { name: 'own', field: 'authorUserId', value: 1, label: 'scenarios.private' },
        { name: 'shared', field: 'isShared', value: true, label: 'scenarios.shared' },
      ];
      await flushPromises();
      expect(wrapper.findAll('[data-test="scenarios"]')).to.have.lengthOf(4);
      wrapper.vm.selectedShareFilters = [
        { name: 'own', field: 'authorUserId', value: 1, label: 'scenarios.private' },
        { name: 'shared', field: 'isShared', value: true, label: 'scenarios.shared' },
        { name: 'public', field: 'isPublic', value: true, label: 'scenarios.public' },
      ];
      await flushPromises();
      expect(wrapper.findAll('[data-test="scenarios"]')).to.have.lengthOf(5);
      wrapper.vm.selectedShareFilters = [
        { name: 'shared', field: 'isShared', value: true, label: 'scenarios.shared' },
        { name: 'public', field: 'isPublic', value: true, label: 'scenarios.public' },
      ];
      await flushPromises();
      expect(wrapper.findAll('[data-test="scenarios"]')).to.have.lengthOf(4);
      wrapper.vm.selectedShareFilters = [{ name: 'public', field: 'isPublic', value: true, label: 'scenarios.public' }];
      await flushPromises();
      expect(wrapper.findAll('[data-test="scenarios"]')).to.have.lengthOf(3);
      wrapper.vm.selectedShareFilters = [];
      await flushPromises();
      expect(wrapper.findAll('[data-test="scenarios"]')).to.have.lengthOf(5);

      wrapper.vm.selectedDateFilters = [{ name: 'past', label: 'scenarios.past' }];
      await flushPromises();
      expect(wrapper.findAll('[data-test="scenarios"]')).to.have.lengthOf(5);
      wrapper.vm.selectedDateFilters = [
        { name: 'past', label: 'scenarios.past' },
        { name: 'current', label: 'scenarios.current' },
      ];
      await flushPromises();
      expect(wrapper.findAll('[data-test="scenarios"]')).to.have.lengthOf(5);
      wrapper.vm.selectedDateFilters = [{ name: 'current', label: 'scenarios.current' }];
      await flushPromises();
      expect(wrapper.findAll('[data-test="scenarios"]')).to.have.lengthOf(0);
      wrapper.vm.selectedDateFilters = [
        { name: 'current', label: 'scenarios.current' },
        { name: 'future', label: 'scenarios.future' },
      ];
      await flushPromises();
      expect(wrapper.findAll('[data-test="scenarios"]')).to.have.lengthOf(0);
      wrapper.vm.selectedDateFilters = [
        { name: 'past', label: 'scenarios.past' },
        { name: 'current', label: 'scenarios.current' },
        { name: 'future', label: 'scenarios.future' },
      ];
      await flushPromises();
      expect(wrapper.findAll('[data-test="scenarios"]')).to.have.lengthOf(5);
    });

    it('redirects on create button click', async () => {
      const createButton = wrapper.get('button[data-type=create]');

      await createButton.trigger('click');
      expect(globalRouterMock.push.args).to.deep.equal([[{ name: 'scenarios.create' }]]);
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('on bad request', () => {
    beforeEach(async () => {
      mockScenariosEndpoint({ code: 404 });
      wrapper = mount(ScenarioList);
      await flushPromises();
    });

    it('displays no scenarios', async () => {
      expect(wrapper.findAll('[data-test="scenarios"]')).to.have.lengthOf(0);
    });

    it('sends alert message', async () => {
      expect(globalToastMock.add.args[0][0].severity).to.deep.equal('info');
    });

    afterEach(() => {
      wrapper.unmount();
    });
  });

  describe('with unauthenticated user', () => {
    before(async () => {
      mockScenariosEndpoint({ response: { scenarios: testScenarios } });
      const testStore = getStoreMock();
      testStore.getters['auth/isLogged'] = false; // simulate unauthenticated user
      wrapper = mount(ScenarioList, { mocks: { store: testStore } });
      await flushPromises();
    });

    it('displays only two filters', () => {
      const expectedFilters = ['dating', 'sorting'];
      const unexpectedFilters = ['sharing'];

      expectedFilters.forEach((type) => {
        expect(wrapper.find(`[data-test=${type}]`).exists()).to.equal(true);
      });

      unexpectedFilters.forEach((type) => {
        expect(wrapper.find(`[data-test=${type}]`).exists()).to.equal(false);
      });
    });

    it('displays all unfiltered scenarios', () => {
      expect(wrapper.findAll('[data-test="scenarios"]')).to.have.lengthOf(5);
    });

    after(() => {
      wrapper.unmount();
    });
  });

  describe('in DTA mode', () => {
    before(async () => {
      mockScenariosEndpoint({ response: { scenarios: testScenarios } });
      const testStore = getStoreMock();
      testStore.state.scenarios.model.type = 'DTA';
      wrapper = mount(ScenarioList, { mocks: { store: testStore } });
      await flushPromises();
    });

    it('displays heading with DTA flag', () => {
      const heading = wrapper.find('[data-test="heading"]');

      expect(heading.exists()).to.equal(true);
      expect(heading.text()).to.contain('scenarios.scenarios');
      expect(heading.text()).to.contain('(DTA)');
    });

    after(() => {
      wrapper.unmount();
    });
  });
});
