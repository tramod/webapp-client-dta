import About from '@views/settings/About.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

describe('About.spec.js', () => {
  before(async () => {
    wrapper = mount(About);
    await flushPromises();
  });

  it('displays heading', async () => {
    const heading = wrapper.find('[data-test="heading"]');
    expect(heading.exists()).to.equal(true);
    expect(heading.text()).to.contain('about.about');
  });

  it('displays text content', async () => {
    const content = wrapper.find('div.content');
    expect(content.exists()).to.equal(true);
    const text = content.text();
    expect(text).to.contain('About app text CS'); // text is defined in the env.test file
  });

  after(() => {
    wrapper.unmount();
  });
});
