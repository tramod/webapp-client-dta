import MapInfoSigns from '@components/map/MapInfoSigns.vue';
import { mount, globalRouterMock } from '../../mountComponent.js';
import { types as modTypes } from '@composables/useModifications';
import { activateScenario } from '../../scenarioSetup.js';
import { shallowReactive } from 'vue';
import { getStoreMock } from '../../helpers.js';

let wrapper, testStore;

const TEST_SCENARIO_ID = 1;
const TEST_EVENT_ID = 2;
const TEST_MOD_ID = 3;

const testScenarioData = {
  id: TEST_SCENARIO_ID,
  name: 'Test scenario',
  type: 'private',
  dateFrom: null,
  dateTo: null,
  authorUserId: 1,
};
const testEventData = {
  id: TEST_EVENT_ID,
  name: 'Test event',
  description: 'popis',
  dateFrom: null,
  dateTo: null,
  authorUserId: 1,
  owner: 'test owner',
  included: true,
};
const testModData = {
  id: TEST_MOD_ID,
  dateFrom: null,
  dateTo: '2021-02-18T09:05:58.731Z',
  updatedAt: '2021-02-18T09:05:58.731Z',
  type: 'node',
  mode: 'new',
  node: [300.0, 300.0],
  name: 'Test mod',
};
const testScenario = {
  ...testScenarioData,
  events: [
    {
      ...testEventData,
      modifications: [
        {
          ...testModData,
        },
      ],
    },
  ],
};

const nodeNewIcon = modTypes.find((item) => item.type === 'node' && item.mode === 'new').icon;

const selectPager = shallowReactive({
  listIndex: 0,
  listLength: 1,
  setItem: sinon.spy(),
});

function setupTestStore() {
  const testStore = getStoreMock();
  testStore.state.map.item = [testScenario.id];
  testStore.getters['map/getActiveItemId'] = () => 1;
  testStore.state.map.mapMode = 'modelDTA'; // Model mode expected to display signs info
  return testStore;
}

describe('MapInfoSigns.spec.js', () => {
  beforeEach(async () => {
    await activateScenario(testScenario);
    testStore = setupTestStore();
  });

  it('Renders with modification content and modification name heading', async () => {
    wrapper = mount(MapInfoSigns, {
      props: {
        selectedData: {
          properties: { eventId: TEST_EVENT_ID, modId: TEST_MOD_ID },
        },
        selectPager,
      },
      mocks: { store: testStore },
    });

    expect(wrapper.text()).to.have.string(testScenario.events[0].modifications[0].name);

    const modIcon = wrapper.find(`.${nodeNewIcon}`); // other way or not test specific icon?
    expect(modIcon.exists()).to.equal(true);

    const expectedContent = [
      { key: 'widgets.duration', value: 'scenarios.unlimited' },
      { key: 'widgets.event', value: testScenario.events[0].name },
      { key: 'widgets.event owner', value: testScenario.events[0].owner },
      { key: 'scenarios.last edit', value: testScenario.events[0].modifications[0].updatedAt },
    ];
    const contentRows = wrapper.findAll('.popup-row');

    for (let i = 0; i < contentRows.length; i++) {
      const contentRow = contentRows[i];
      expect(contentRow.text()).to.have.string(expectedContent[i].key);
      expect(contentRow.text()).to.have.string(expectedContent[i].value);
    }
  });

  it('Renders different content for other typeMode modification', async () => {
    const otherModId = TEST_MOD_ID + 1;
    const otherScenario = {
      ...testScenarioData,
      events: [
        {
          ...testEventData,
          modifications: [
            {
              id: otherModId,
              dateFrom: null,
              dateTo: null,
              type: 'link',
              mode: 'existing',
              linkId: [100],
              speed: 50,
              capacity: 1000,
              name: 'Different mod',
            },
          ],
        },
      ],
    };

    await activateScenario(otherScenario);
    wrapper = mount(MapInfoSigns, {
      props: {
        selectedData: {
          properties: { eventId: TEST_EVENT_ID, modId: otherModId },
        },
        selectPager,
      },
      mocks: { store: testStore },
    });

    expect(wrapper.text()).not.to.have.string(testScenario.events[0].modifications[0].name);

    const modIcon = wrapper.find(`.${nodeNewIcon}`); // other way or not test specific icon?
    expect(modIcon.exists()).to.equal(false);

    const linkExistingIcon = modTypes.find((item) => item.type === 'link' && item.mode === 'existing').icon;
    const correctModIcon = wrapper.find(`.${linkExistingIcon}`); // other way or not test specific icon?
    expect(correctModIcon.exists()).to.equal(true);
  });

  it('Renders with data not found when modification is invalid', async () => {
    wrapper = mount(MapInfoSigns, {
      props: {
        selectedData: {
          properties: { eventId: TEST_EVENT_ID, modId: TEST_MOD_ID + 1 },
        },
        selectPager,
      },
      mocks: { store: testStore },
    });

    expect(wrapper.text()).to.have.string('map.data not found');

    const modIcon = wrapper.find('.ri-git-commit-fill');
    expect(modIcon.exists()).to.equal(false);

    const redirectBtn = wrapper.find('[data-test="redirect-button"]');
    expect(redirectBtn.exists()).to.equal(false);
  });

  it('Renders redirect button which can send clicked to close & redirect', async () => {
    wrapper = mount(MapInfoSigns, {
      props: {
        selectedData: {
          properties: { eventId: TEST_EVENT_ID, modId: TEST_MOD_ID },
        },
        selectPager,
      },
      mocks: { store: testStore },
    });

    const redirectBtn = wrapper.find('[data-test="redirect-button"]');
    expect(redirectBtn.exists()).to.equal(true);

    await redirectBtn.trigger('click');
    expect(globalRouterMock.push.args).to.deep.equal([
      [
        {
          name: 'scenarios.events.modifications.edit',
          params: { modType: 'node', modMode: 'new', id: TEST_SCENARIO_ID, evId: TEST_EVENT_ID, modId: TEST_MOD_ID },
        },
      ],
    ]);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
