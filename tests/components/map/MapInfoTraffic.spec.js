import MapInfoTraffic from '@components/map/MapInfoTraffic.vue';
import { expect } from 'chai';
import { mount, globalStoreMock } from '../../mountComponent.js';
import { shallowReactive } from 'vue';
import { getStoreMock } from '../../helpers.js';

let wrapper;

const VOLUME = 100;
const CAPACITY = 200;
const MODEL_MOCKED_DATA = {
  extraData: {
    context: { volume: VOLUME, capacity: CAPACITY },
  },
  properties: {
    lanes: 1,
  },
};

const selectPager = shallowReactive({
  listIndex: 0,
  listLength: 1,
  setItem: sinon.spy(),
});

describe('MapInfoTraffic.spec.js', () => {
  beforeEach(() => {});

  it('Renders with default map mode and no data', async () => {
    wrapper = mount(MapInfoTraffic, {
      props: { selectedData: {}, selectPager },
    });

    console.log(wrapper.text());

    expect(wrapper.text()).to.have.string(`map.${globalStoreMock.state.map.mapMode} traffic intensity`);
    expect(wrapper.text()).to.have.string('map.data not found');
  });

  it('Renders date for model mode', async () => {
    const testStore = getStoreMock();
    testStore.state.map.mapMode = 'modelDTA';
    testStore.getters['map/getCalendarDate'] = () => '2022-03-22';
    wrapper = mount(MapInfoTraffic, {
      props: { selectedData: MODEL_MOCKED_DATA, selectPager },
      mocks: { store: testStore },
    });

    expect(wrapper.text()).to.have.string('map.modelDTA traffic intensity');
    expect(wrapper.text()).to.have.string('map.day');
    expect(wrapper.text()).to.have.string('2022-03-22');
  });

  it('Renders with zero volume/capacity', async () => {
    const testStore = getStoreMock();
    testStore.state.map.mapMode = 'modelDTA';
    testStore.getters['map/getCalendarDate'] = () => '2022-03-22T00:00:00.000Z';
    const zeroData = {
      extraData: {
        context: { volume: 0, capacity: 0 },
      },
      properties: { lanes: 1 },
    };
    const expectedContent = [
      { key: 'map.day', value: '2022-03-22' },
      { key: 'map.time', value: 0 },
      { key: 'map.traffic', value: 0 },
      { key: null, value: Math.round(0 / 60) },
      { key: 'map.capacity', value: 0 },
      { key: 'map.used capacity', value: 0 },
    ];

    wrapper = mount(MapInfoTraffic, {
      props: { selectedData: zeroData, selectPager },
      mocks: { store: testStore },
    });
    const contentRows = wrapper.findAll('.popup-row');
    expect(contentRows.length).to.equal(expectedContent.length);
    for (let i = 0; i < contentRows.length; i++) {
      const contentRow = contentRows[i];
      if (expectedContent[i].key) expect(contentRow.text()).to.have.string(expectedContent[i].key);
      expect(contentRow.text()).to.have.string(expectedContent[i].value);
    }
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
