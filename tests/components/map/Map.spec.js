import Map from '@components/map/Map.vue';
import { mount } from '../../mountComponent.js';
import { flushPromises } from '@vue/test-utils';
import { getStoreMock } from '../../helpers.js';

let wrapper;

describe('Map.spec.js', () => {
  before(async () => {});

  it('mounts with mocked vuemap and tries to set mapMode', async () => {
    const FALSY_MAP_STATE = 'none';
    const testStore = getStoreMock();
    testStore.state.map.mapMode = FALSY_MAP_STATE;
    testStore.getters['map/getLayerSource'] = sinon.spy();

    wrapper = mount(Map, {
      mocks: {
        store: testStore,
      },
    });

    // TODO improve vuemap mock access
    const setMapStateSpy = wrapper.vm.map.manager.setMapState;
    expect(setMapStateSpy.calledOnce).to.equal(true);
    expect(setMapStateSpy.args).to.deep.equal([[FALSY_MAP_STATE]]);

    await flushPromises();
  });

  after(() => {
    wrapper.unmount();
  });
});
