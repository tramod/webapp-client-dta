import MapInfo from '@components/map/MapInfo.vue';
import { expect } from 'chai';
import { shallowReactive } from 'vue';
import { mount } from '../../mountComponent.js';

let wrapper;

const testData = [
  { key: 'first', value: 111 },
  { key: 'second', value: 22, unit: 'test' },
  { key: 'third', value: 3 },
  { key: 'fourth', value: 0.4, unit: 'test' },
];
const selectPager = shallowReactive({
  listIndex: 0,
  listLength: 1,
  setItem: sinon.spy(),
});

describe('MapInfo.spec.js', () => {
  it('Displays header with closing click button', async () => {
    wrapper = mount(MapInfo, {
      props: { contentData: testData, selectPager },
    });

    const title = wrapper.find('.widget-title');
    expect(title.exists()).to.equal(true);

    const closeButton = wrapper.find('.close-icon');
    expect(closeButton.exists()).to.equal(true);
  });

  it('Displays slotted heading and toolbar', async () => {
    const headingSlot = 'Heading slot';
    const toolbarSlot = 'Toolbar slot';

    wrapper = mount(MapInfo, {
      props: { contentData: testData, selectPager },
      slots: { heading: () => headingSlot, 'heading-toolbar': () => toolbarSlot }, // TODO: this is probably a bug in current vue-test-utils - slots should not need to be a function
    });

    const heading = wrapper.find('.heading.widget');
    expect(heading.text()).to.have.string(headingSlot);
    expect(heading.text()).to.have.string(toolbarSlot);
  });

  it('Displays provided content data', async () => {
    wrapper = mount(MapInfo, {
      props: { contentData: testData, selectPager },
    });

    const contentRows = wrapper.findAll('.popup-row');
    expect(contentRows).to.have.lengthOf(testData.length);

    for (let i = 0; i < contentRows.length; i++) {
      const row = contentRows[i];
      expect(row.text()).to.have.string(testData[i].key);
      expect(row.text()).to.have.string(testData[i].value);
      if (testData[i].units) expect(row.text()).to.have.string(testData[i].units);
    }
  });

  it('Dont display pager when only one item is present', async () => {
    wrapper = mount(MapInfo, {
      props: { contentData: testData, selectPager },
    });
    const pager = wrapper.find('[data-test-id="map-info-pager"]');
    expect(pager.exists()).to.equal(false);
  });

  it('Display pager and allow clickable actions', async () => {
    wrapper = mount(MapInfo, {
      props: {
        contentData: testData,
        selectPager: shallowReactive({
          ...selectPager,
          listIndex: 0,
          listLength: 3,
        }),
      },
    });
    const pager = wrapper.find('[data-test-id="map-info-pager"]');
    expect(pager.exists()).to.equal(true);

    expect(pager.text()).to.have.string(1);
    expect(pager.text()).to.have.string(3);

    const buttons = pager.findAll('button');
    expect(buttons).to.have.lengthOf(2);

    for (let i = 0; i < buttons.length; i++) {
      const button = buttons[i];
      expect(button.attributes().disabled === '').to.equal(i === 0);
    }

    expect(selectPager.setItem.notCalled).to.equal(true);
    await buttons[1].trigger('click');
    expect(selectPager.setItem.called).to.equal(true);
  });

  it('Displays slotted actions in footer', async () => {
    const actionsSlot = 'Actions slot';

    wrapper = mount(MapInfo, {
      props: { contentData: testData, selectPager },
      slots: { actions: () => actionsSlot }, // TODO: this is probably a bug in current vue-test-utils - slots should not need to be a function
    });

    const footer = wrapper.find('.popup-footer');
    expect(footer.text()).to.have.string(actionsSlot);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
