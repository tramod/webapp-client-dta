import {
  MAP_ID,
  LID_DTA_LINKS,
  LID_DTA_NODES,
  LID_DTA_GENERATORS,
  LID_SIGNS,
  MP_MODEL_DTA_COMPARISON,
} from '@keys/index';
import { ref } from 'vue';

export const isRanByVitest = () => typeof window === 'object' && window.vitest === true;

export const getToastMock = () => ({ add: sinon.spy() });

export const getConfirmMock = () => ({
  require: (obj) => obj.accept(), // auto accept confirm dialog
});

export const getRouteMock = () => ({
  name: 'mock.route.name',
  params: sinon.spy(),
});

export const getRouterMock = () => ({
  push: sinon.spy(),
  currentRoute: { value: { name: '', params: { id: null } } },
  options: { history: { state: { back: null } } },
  mocked: true,
});

export const getAbilityMock = () => ({
  ability: sinon.spy(),
  watchAbilityRulesChange: sinon.spy(),
  can: () => {
    return true; // allow everything to simplify testing
  },
});

export const getCookieConsentMock = () => ({
  onConsentChange: sinon.spy(),
});

export const getMapMock = (id) => ({
  id,
  olObject: {
    addInteraction: sinon.spy(),
    addLayer: sinon.spy(),
    removeInteraction: sinon.spy(),
    removeLayer: sinon.spy(),
    on: sinon.spy(),
    un: sinon.spy(),
    getSize: sinon.spy(() => [1, 1]),
  },
  element: {},
  setElement: sinon.spy(),
  view: { state: { zoom: null, center: null, rotation: null }, olObject: { fit: sinon.spy() } },
  isLoading: false,
  isReady: true,
  manager: {
    addBaseMap: sinon.spy(),
    setBaseMap: sinon.spy(),
    setMapState: sinon.spy(),
    forceRender: sinon.spy(),
    state: {
      loading: false,
    },
  },
});

export const getLayerMock = (id) => ({
  id,
  source: {},
  olObject: {},
  isLoading: false,
  isReady: true,
  context: { add: sinon.spy(), set: sinon.spy(), getByFeature: sinon.spy(), has: sinon.spy() },
});

export const setupVueMapMock = (
  vuemapMock,
  {
    mapId = MAP_ID,
    layerIds = [LID_DTA_LINKS, LID_DTA_NODES, LID_DTA_GENERATORS, LID_SIGNS],
    mapMockFn = getMapMock,
    layerMockFn = getLayerMock,
  } = {},
) => {
  vuemapMock.mockState({
    map: mapMockFn(mapId),
    layers: layerIds.map((lId) => layerMockFn(lId)),
  });
};

export const getTimelineMock = () => ({
  timeline: sinon.spy(),
  d3: sinon.spy(),
});

export const getI18nMock = ({ mockedKey = 'mocked-i18n-key-since-the-real-one-is-not-exposed' } = {}) => [
  mockedKey,
  {
    mode: 'composition',
    global: {
      t: translateMock,
      d: dateFormatMock,
      locale: ref('cs'),
    },
    install: (app) => {
      // we need to set the key here - the same way it is set in the vue-i18n install function
      // so we can use the exported useI18n with our mocked functions (passed in the global object)
      app.__VUE_I18N_SYMBOL__ = mockedKey;
    },
  },
];

export const translateMock = (phrase) => phrase;
export const dateFormatMock = (phrase) => phrase;
export const isMobileMock = () => false;

export const getStoreMock = () => ({
  commit: sinon.spy(),
  dispatch: sinon.spy(),
  getters: {
    'auth/isLogged': true,
    'auth/isAdmin': true,
    'scenarios/isToBeComputed': () => false,
    'scenarios/isToBeSaved': () => false,
    'scenarios/isCacheExpired': () => false,
    'scenarios/isWatchModeActive': () => false,
    'scenarios/isScenarioToBeSaved': () => false,
    'scenarios/isScenarioToBeComputed': () => false,
    'scenarios/isOpenEditSession': () => true,
    'scenarios/isUsingChangedScenario': () => true,
    'scenarios/isScenarioUnresolved': () => false,
    'scenarios/isScenarioBeingComputed': () => false,
    'scenarios/isInsideComputation': () => false,
    'scenarios/isWithComputation': () => false,
    'scenarios/getScenarioComputationTimestamp': () => null,
    'scenarios/hasScenarioComputationReady': () => false,
    'scenarios/hasScenarioComputationError': () => false,
    'map/getActiveItemId': () => null,
    'map/getCalendarDate': () => new Date().toISOString(),
    'map/getWidgets': () => [],
    'map/getDataMode': 'model',
  },
  subscribe: sinon.spy(),
  state: {
    locale: { lang: 'cs' },
    auth: { user: { id: 1 } },
    layout: {
      analyticsEnabled: false,
      panels: {
        left: 'opened',
        right: 'opened',
      },
    },
    map: {
      mapMode: 'modelDTA',
      calendar: [],
      widgets: [],
      traffic: {},
      item: [],
      center: null,
      zoom: null,
      sourceLoaded: {},
      base: 'OSM',
      mapStyle: {
        general: {
          vehiclesCountDisplayed: false,
          trafficSignsStyle: false,
        },
        [MP_MODEL_DTA_COMPARISON]: {
          color: 'outFlowDiff',
          width: 'outFlowDiff',
        },
      },
    },
    scenarios: {
      model: { type: 'DTA', isValid: true },
      editMode: {
        scenarioId: null,
        isOpenSession: false,
        isUsingChangedScenario: true,
      },
      progress: {},
      filters: {
        scenarios: {},
        events: {},
        eventsImport: {},
        nodeTransit: {},
      },
      sorting: {
        scenarios: {},
        events: {},
      },
      cache: {
        scenarios: { enabled: false },
        events: { enabled: false },
        modifications: { enabled: false },
        nodeTransit: { enabled: false },
        scenariosOverview: { enabled: false },
      },
    },
  },
});
