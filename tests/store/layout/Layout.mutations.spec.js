import { mutations } from '@store/modules/layout';

const { SET_LOADING } = mutations;

describe('Layout.mutations.spec.js', () => {
  it('SET_LOADING', () => {
    const state = {
      loading: {},
    };

    const testPayload = {
      type: 'custom-loading-type',
      loading: true,
    };

    SET_LOADING(state, testPayload);
    expect(state.loading[testPayload.type]).to.equal(testPayload.loading);
  });
});
