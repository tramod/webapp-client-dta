import { getters } from '@store/modules/layout';

const { isLoading } = getters;
let testState;
beforeEach(() => {
  testState = { loading: {} };
});

describe('Layout.getters.spec.js', () => {
  it('is not loading without counter', () => {
    expect(isLoading(testState)()).to.equal(false);
    expect(isLoading(testState)({ type: 'default' })).to.equal(false);
    expect(isLoading(testState)({ type: 'custom' })).to.equal(false);

    testState = { loading: { default: 0, custom: 0 } };

    expect(isLoading(testState)()).to.equal(false);
    expect(isLoading(testState)({ type: 'default' })).to.equal(false);
    expect(isLoading(testState)({ type: 'custom' })).to.equal(false);
  });

  it('is loading default', () => {
    testState = { loading: { default: 3 } };

    expect(isLoading(testState)()).to.equal(true);
    expect(isLoading(testState)({ type: 'default' })).to.equal(true);
    expect(isLoading(testState)({ type: 'custom' })).to.equal(false);
  });

  it('is loading custom', () => {
    testState = { loading: { custom: 3 } };
    expect(isLoading(testState)()).to.equal(false);
    expect(isLoading(testState)({ type: 'default' })).to.equal(false);
    expect(isLoading(testState)({ type: 'custom' })).to.equal(true);
  });
});
