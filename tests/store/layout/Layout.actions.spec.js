import { actions } from '@store/modules/layout';

const { startLoading, finishLoading } = actions;

let testState;
beforeEach(() => {
  testState = { loading: {} };
});

describe('Layout.actions.spec.js', () => {
  it('starts default loading', async () => {
    const commit = sinon.spy();

    await startLoading({ commit, state: testState });
    expect(commit.args).to.deep.equal([['SET_LOADING', { loading: 1, type: 'default' }]]);
  });

  it('ends default loading', async () => {
    const commit = sinon.spy();
    testState = { loading: { default: 1 } };

    await finishLoading({ commit, state: testState });
    expect(commit.args).to.deep.equal([['SET_LOADING', { loading: 0, type: 'default' }]]);
  });

  it('starts custom loading', async () => {
    const commit = sinon.spy();

    await startLoading({ commit, state: testState }, { type: 'custom' });
    expect(commit.args).to.deep.equal([['SET_LOADING', { loading: 1, type: 'custom' }]]);
  });

  it('ends custom loading', async () => {
    const commit = sinon.spy();
    testState = { loading: { custom: 1 } };

    await finishLoading({ commit, state: testState }, { type: 'custom' });
    expect(commit.args).to.deep.equal([['SET_LOADING', { loading: 0, type: 'custom' }]]);
  });

  it('starts n-th loading', async () => {
    const commit = sinon.spy();
    testState = { loading: { default: 5 } };

    await startLoading({ commit, state: testState });
    expect(commit.calledOnce).to.equal(true);
    expect(commit.args).to.deep.equal([['SET_LOADING', { loading: 6, type: 'default' }]]);
  });

  it('ends n-th loading', async () => {
    const commit = sinon.spy();

    testState = { loading: { default: 6 } };

    await finishLoading({ commit, state: testState });
    expect(commit.calledOnce).to.equal(true);
    expect(commit.args).to.deep.equal([['SET_LOADING', { loading: 5, type: 'default' }]]);
  });
});
