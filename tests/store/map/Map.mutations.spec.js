import { mutations } from '@store/modules/map';

// destructure assign `mutations`
const {
  SET_MAP_MODE,
  SET_CALENDAR,
  SET_ITEM,
  SET_CENTER,
  SET_ZOOM,
  SET_BASE,
  SET_COMPARISON,
  SET_BROKEN_MODES,
  SET_MAP_STYLE,
  SET_SOURCE_LOADED,
} = mutations;

describe('Map.mutations.spec.js', () => {
  it('SET_MAP_MODE', () => {
    const state = {
      mapMode: 'abc',
    };
    const testMode = 'modelDTA';

    SET_MAP_MODE(state, { mode: testMode });
    expect(state.mapMode).to.eql(testMode);
  });

  it('SET_CALENDAR', () => {
    const state = {
      mapMode: 'modelDTA',
      calendar: [{ date: '2022-05-03T00:00:00.000Z', modelType: 'DTA', mapMode: 'modelDTA' }],
    };
    const testDate = '2021-06-02T00:00:00.000Z';

    SET_CALENDAR(state, { tmDate: testDate, mapMode: 'modelDTA', modelType: 'DTA' });
    expect(state.calendar[0].date).to.eql(testDate);
  });

  it('SET_ITEM', () => {
    const state = {
      item: [],
    };
    const testItems = { scenarioId: 1, eventId: 2, modificationId: 3 };

    SET_ITEM(state, testItems);
    expect(state.item).to.eql([1, 2, 3]);
  });

  it('SET_CENTER', () => {
    const state = {
      center: 1,
    };
    const testCenter = 2;

    SET_CENTER(state, { center: testCenter });
    expect(state.center).to.eql(testCenter);
  });

  it('SET_ZOOM', () => {
    const state = {
      zoom: 1,
    };
    const testZoom = 2;

    SET_ZOOM(state, { zoom: testZoom });
    expect(state.zoom).to.eql(testZoom);
  });

  it('SET_BASE', () => {
    const state = {
      base: 1,
    };
    const testBase = 2;

    SET_BASE(state, { baseId: testBase });
    expect(state.base).to.eql(testBase);
  });

  it('SET_COMPARISON', () => {
    const state = {
      comparison: [1, 2],
    };
    const testComparison = [3, 4];

    SET_COMPARISON(state, testComparison);
    expect(state.comparison).to.deep.eql(testComparison);
  });

  it('SET_BROKEN_MODES', () => {
    const state = {
      brokenMapModes: ['abc'],
    };
    const testModes = ['abc', 'efd'];

    SET_BROKEN_MODES(state, testModes);
    expect(state.brokenMapModes).to.deep.eql(testModes);
  });

  it('SET_MAP_STYLE', () => {
    const state = {
      mapStyle: {
        main: {
          mapStateA: false,
          mapStateB: true,
        },
      },
    };

    SET_MAP_STYLE(state, { newState: true, property: 'mapStateA', group: 'main' });
    expect(state.mapStyle.main.mapStateA).to.deep.eql(true);
    expect(state.mapStyle.main.mapStateB).to.deep.eql(true);

    SET_MAP_STYLE(state, { newState: false, property: 'mapStateA', group: 'main' });
    expect(state.mapStyle.main.mapStateA).to.deep.eql(false);
    expect(state.mapStyle.main.mapStateB).to.deep.eql(true);
  });

  it('SET_SOURCE_LOADED', () => {
    const state = {
      sourceLoaded: { model: true },
    };
    const testState = { model: true, abc: true };

    SET_SOURCE_LOADED(state, { sourceKey: 'abc', status: true });
    expect(state.sourceLoaded).to.deep.eql(testState);
  });
});
