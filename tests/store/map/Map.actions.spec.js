import { actions } from '@store/modules/map';

const {
  setMapMode,
  setCalendarDate,
  activateItem,
  setPosition,
  zoomIn,
  zoomOut,
  setBase,
  setComparedScenarios,
  addBrokenMapMode,
  addSourceLoaded,
} = actions;

describe('Map.actions.spec.js', () => {
  describe('setMapMode', () => {
    it('sets map mode state', async () => {
      const commit = sinon.spy();
      const testState = { mapMode: 'modelDTA', calendar: {} };
      const testRootState = { layout: { panels: { right: 'closed' } } };

      await setMapMode({ commit, state: testState, rootState: testRootState }, { mode: testState.mode });
      expect(commit.args).to.deep.equal([['SET_MAP_MODE', { mode: testState.mode }]]);
    });
  });

  describe('setCalendarDate', () => {
    it('sets map calendar state for default map mode', async () => {
      const commit = sinon.spy();
      const testState = { mapMode: 'modelDTA', calendar: { modelDTA: '2021-06-01T00:00:00.000Z' } };
      const testRootState = { scenarios: { model: { type: 'DTA' } } };
      const testDate = '2022-05-03T00:00:00.000Z';

      await setCalendarDate({ commit, state: testState, rootState: testRootState }, { tmDate: testDate });
      expect(commit.args).to.deep.equal([
        ['SET_CALENDAR', { mapMode: 'modelDTA', tmDate: testDate, modelType: 'DTA' }],
      ]);
    });
  });

  describe('activateItem', () => {
    it('sets items', async () => {
      const commit = sinon.spy();
      const testState = { mapMode: 'modelDTA', calendar: { modelDTA: '2021-06-01T00:00:00.000Z' }, item: [] };
      const testRootState = { scenarios: { model: { type: 'DTA' } } };
      const testItems = { scenarioId: 1, eventId: 2, modificationId: 3 };

      await activateItem({ commit, state: testState, rootState: testRootState }, testItems);
      expect(commit.args[1]).to.deep.equal(['SET_ITEM', testItems]);
    });

    it('sets comparison with active scenario', async () => {
      const commit = sinon.spy();
      const testState = { mapMode: 'modelDTA', calendar: { modelDTA: '2021-06-01T00:00:00.000Z' }, item: [] };
      const testRootState = { scenarios: { model: { type: 'DTA' } } };
      const testItems = { scenarioId: 1, eventId: 2, modificationId: 3 };

      await activateItem({ commit, state: testState, rootState: testRootState }, testItems);
      expect(commit.args[2]).to.deep.equal(['SET_COMPARISON', [1, 'base']]);
    });

    it('sets comparison with no active scenario', async () => {
      const commit = sinon.spy();
      const testState = { mapMode: 'modelDTA', calendar: { modelDTA: '2021-06-01T00:00:00.000Z' }, item: [] };
      const testRootState = { scenarios: { model: { type: 'DTA' } } };
      const testItems = {};

      await activateItem({ commit, state: testState, rootState: testRootState }, testItems);
      expect(commit.args[2]).to.deep.equal(['SET_COMPARISON', ['base', 'base']]);
    });

    it('updates calendar date if set', async () => {
      const commit = sinon.spy();
      const testState = { mapMode: 'modelDTA', calendar: { modelDTA: '2021-06-01T00:00:00.000Z' }, item: [] };
      const testRootState = { scenarios: { model: { type: 'DTA' } } };
      const testDate = '2022-05-03T00:00:00.000Z';

      await activateItem({ commit, state: testState, rootState: testRootState }, { date: testDate });
      expect(commit.args[3]).to.deep.equal([
        'SET_CALENDAR',
        { mapMode: 'modelDTA', tmDate: testDate, modelType: 'DTA' },
      ]);
    });
  });

  describe('setPosition', () => {
    it('sets map center', async () => {
      const commit = sinon.spy();
      const testState = { center: [123, 345], zoom: null };

      await setPosition({ commit, state: testState }, { center: testState.center, zoom: testState.zoom });
      expect(commit.args[0]).to.deep.equal(['SET_CENTER', { center: testState.center }]);
    });

    it('converts center to numbers', async () => {
      const commit = sinon.spy();
      const testState = { center: ['123', '345.678'], zoom: null };
      const expectedCall = { center: [123, 345.678] };

      await setPosition({ commit, state: testState }, { center: testState.center, zoom: testState.zoom });
      expect(commit.args[0]).to.deep.equal(['SET_CENTER', expectedCall]);
    });

    it('sets map zoom', async () => {
      const commit = sinon.spy();
      const testState = { center: null, zoom: 456 };

      await setPosition({ commit, state: testState }, { center: testState.center, zoom: testState.zoom });
      expect(commit.args[1]).to.deep.equal(['SET_ZOOM', { zoom: testState.zoom }]);
    });

    it('converts zoom to numbers', async () => {
      const commit = sinon.spy();
      const testState = { center: null, zoom: '456.789' };
      const expectedCall = { zoom: 456.789 };

      await setPosition({ commit, state: testState }, { center: testState.center, zoom: testState.zoom });
      expect(commit.args[1]).to.deep.equal(['SET_ZOOM', expectedCall]);
    });
  });

  describe('zoomIn', () => {
    it('increases map zoom', async () => {
      const commit = sinon.spy();
      const testState = { zoom: 1 };

      await zoomIn({ commit, state: testState });
      expect(commit.args[0]).to.deep.equal(['SET_ZOOM', { zoom: testState.zoom + 1 }]);
    });
  });

  describe('zoomOut', () => {
    it('decreases map zoom', async () => {
      const commit = sinon.spy();
      const testState = { zoom: 1 };

      await zoomOut({ commit, state: testState });
      expect(commit.args[0]).to.deep.equal(['SET_ZOOM', { zoom: testState.zoom - 1 }]);
    });
  });

  describe('setBase', () => {
    it('sets map base layer', async () => {
      const commit = sinon.spy();
      const testState = { base: 'asd' };
      const testBase = 'sdf';

      await setBase({ commit, state: testState }, { baseId: testBase });
      expect(commit.args[0]).to.deep.equal(['SET_BASE', { baseId: testBase }]);
    });
  });

  describe('setComparedScenarios', () => {
    it('sets comparison state', async () => {
      const commit = sinon.spy();
      const testState = { mapMode: 'modelDTA', comparison: [1, 2], calendar: { 'modelDTA-comparison': 1 } };
      const testRootState = { layout: { panels: { right: 'closed' } } };
      const testComparison = [2, 3];

      await setComparedScenarios({ commit, state: testState, rootState: testRootState }, { scenarios: testComparison });
      expect(commit.args[0]).to.deep.equal(['SET_COMPARISON', testComparison]);
    });
  });

  describe('addBrokenMapMode', () => {
    it('adds map mode to the list of broken modes', async () => {
      const commit = sinon.spy();
      const testState = { brokenMapModes: [] };
      const addedBrokenMode = 'modelDTA';
      const expectedBrokenModes = ['modelDTA'];

      await addBrokenMapMode({ commit, state: testState }, { mapModeKey: addedBrokenMode });
      expect(commit.args[0]).to.deep.equal(['SET_BROKEN_MODES', expectedBrokenModes]);
    });

    it('does not add already existing broken mode', async () => {
      const commit = sinon.spy();
      const testState = { brokenMapModes: ['modelDTA'] };
      const addedBrokenMode = 'modelDTA';
      const expectedBrokenModes = ['modelDTA'];

      await addBrokenMapMode({ commit, state: testState }, { mapMode: addedBrokenMode });
      expect(commit.args[0]).to.deep.equal(['SET_BROKEN_MODES', expectedBrokenModes]);
    });
  });

  describe('addSourceLoaded', () => {
    it('adds source to the list of loaded sources', async () => {
      const commit = sinon.spy();
      const testState = { sourceLoaded: { model: true } };
      const testSource = { sourceKey: 'model', status: true };

      await addSourceLoaded(
        { commit, state: testState },
        { sourceKey: testSource.sourceKey, status: testSource.status },
      );
      expect(commit.args[0]).to.deep.equal(['SET_SOURCE_LOADED', testSource]);
    });
  });
});
