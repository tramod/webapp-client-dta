import { getters } from '@store/modules/map';

const { getCalendarDate, getActiveItemId } = getters;

describe('Map.getters.spec.js', () => {
  describe('getCalendarDate', () => {
    it('gets default calendar date', () => {
      const testDate = '2021-06-01T00:00:00.000Z';
      const testState = {
        mapMode: 'modelDTA',
        calendar: [{ date: testDate, modelType: 'DTA', mapMode: 'modelDTA' }],
      };
      const testRootState = { scenarios: { model: { type: 'DTA' } } };
      expect(getCalendarDate(testState, null, testRootState)()).to.equal(testDate);
    });

    it('gets specific calendar date', () => {
      const testDate = '2021-06-01T00:00:00.000Z';
      const testState = {
        mapMode: 'modelDTA',
        calendar: [
          { date: testDate, modelType: 'DTA', mapMode: 'modelDTA' },
          { date: testDate, modelType: 'DTA', mapMode: 'abcd' },
        ],
      };
      const testRootState = { scenarios: { model: { type: 'DTA' } } };
      expect(getCalendarDate(testState, null, testRootState)({ mapMode: 'abcd' })).to.equal(testDate);
    });
  });

  describe('getActiveItemId', () => {
    it('gets active scenario id', () => {
      const testState = {
        item: [1, 2, 3],
      };

      expect(getActiveItemId(testState)({ itemLevel: 'scenario' })).to.equal(testState.item[0]);
    });

    it('gets active event id', () => {
      const testState = {
        item: [1, 2, 3],
      };

      expect(getActiveItemId(testState)({ itemLevel: 'event' })).to.equal(testState.item[1]);
    });

    it('gets active modification id', () => {
      const testState = {
        item: [1, 2, 3],
      };

      expect(getActiveItemId(testState)({ itemLevel: 'modification' })).to.equal(testState.item[2]);
    });
  });
});
