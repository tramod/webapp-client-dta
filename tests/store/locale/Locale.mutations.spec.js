import { mutations } from '@store/modules/locale';

const { SET_LANG } = mutations;

describe('Locale.mutations.spec.js', () => {
  it('SET_LANG', () => {
    const testState = { lang: 'cs' };

    const testPayload = { lang: 'en' };

    SET_LANG(testState, testPayload);
    expect(testState.lang).to.equal(testPayload.lang);
  });
});
