import { actions } from '@store/modules/auth';
// destructure assign `actions`
const { login, logout } = actions;

const testUser = {
  id: 1,
  username: 'franta',
  password: '1234',
  roles: [100, 1],
};

describe('Auth.actions.spec.js', () => {
  describe('login', () => {
    it('sets user state & exits edit mode', async () => {
      const commit = sinon.spy();
      const dispatch = sinon.spy();

      await login({ commit, dispatch }, testUser);

      expect(commit.args).to.deep.equal([['SET_AUTH', testUser]]);
      expect(dispatch.args).to.deep.equal([['scenarios/exitEditMode', {}, { root: true }]]);
    });
  });

  describe('logout', () => {
    it('purges user state & exits edit mode', async () => {
      const commit = sinon.spy();
      const dispatch = sinon.spy();

      await logout({ commit, dispatch });

      expect(commit.args).to.deep.equal([['PURGE_AUTH']]);
      expect(dispatch.args).to.deep.equal([['scenarios/exitEditMode', {}, { root: true }]]);
    });
  });
});
