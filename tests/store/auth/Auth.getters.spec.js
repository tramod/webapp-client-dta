import { getters } from '@store/modules/auth';

const { hasRole, isLogged, isAdmin } = getters;

describe('Auth.getters.spec.js', () => {
  it('hasRole', () => {
    const state = {
      user: { id: 1 },
      permissions: [],
      roles: [1],
    };
    expect(hasRole(state)(1)).to.equal(true);
    expect(hasRole(state)(2)).to.equal(false);
  });

  it('isLogged', () => {
    const loggedUserState = {
      user: { id: 1 },
    };

    const unloggedUserState = {
      user: {},
    };
    expect(isLogged(loggedUserState)).to.equal(true);
    expect(isLogged(unloggedUserState)).to.equal(false);
  });

  it('isAdmin', () => {
    const adminUserState = {
      user: { id: 1 },
      roles: [100],
    };

    const nonAdminUserState = {
      user: {},
      roles: [],
    };
    expect(isAdmin(adminUserState)).to.equal(true);
    expect(isAdmin(nonAdminUserState)).to.equal(false);
  });
});
