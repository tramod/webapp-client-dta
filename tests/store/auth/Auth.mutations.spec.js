import { mutations } from '@store/modules/auth';

// destructure assign `mutations`
const { SET_AUTH, PURGE_AUTH, UPDATE_AUTH } = mutations;

describe('Auth.mutations.spec.js', () => {
  it('PURGE_AUTH', () => {
    const state = {
      user: { id: 1, username: 'test' },
      roles: [101],
      casl: { rules: [], aliases: {} },
    };

    PURGE_AUTH(state);
    expect(state.user).to.be.empty;
    expect(state.roles).to.be.empty;
    expect(state.casl.rules).to.be.empty;
  });

  it('SET_AUTH', () => {
    const state = {
      user: {},
      roles: [],
      casl: { rules: [], aliases: {} },
    };

    const testUser = {
      id: 1,
      username: 'franta',
      roles: [100, 1],
      rights: { rules: [], aliases: {} },
    };

    SET_AUTH(state, testUser);
    expect(state.user).to.eql({ id: testUser.id, username: testUser.username });
    expect(state.roles).to.eql([100, 1]);
    expect(state.casl).to.deep.eql(testUser.rights);
  });

  it('UPDATE_AUTH', () => {
    const state = {
      user: { id: 1, username: 'test', email: 'franta@test.test' },
      roles: [1],
    };

    const updatedState = {
      username: 'test2',
      email: 'franta2@test.test',
      roles: [2],
    };

    UPDATE_AUTH(state, updatedState);
    expect(state.user.username).to.eql(updatedState.username);
    expect(state.user.email).to.eql(updatedState.email);
    expect(state.roles).to.eql(updatedState.roles);
  });
});
