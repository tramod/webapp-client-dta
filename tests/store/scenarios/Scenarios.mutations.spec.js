import { mutations } from '@store/modules/scenarios';

// destructure assign `mutations`
const { SET_EDIT_MODE, SET_MODEL, SET_PROGRESS, SET_FILTERS, SET_SORTING, RESET_CACHE_TIMER } = mutations;

describe('Scenarios.mutations.spec.js', () => {
  it('SET_EDIT_MODE', () => {
    const state = {
      editMode: {
        scenarioId: null,
        isUsingChangedScenario: false,
        isOpenSession: false,
      },
    };
    const testState = {
      scenarioId: 1,
      isOpenSession: true,
      isUsingChangedScenario: false,
    };

    SET_EDIT_MODE(state, testState);
    expect(state.editMode).to.deep.eql(testState);
  });

  it('SET_MODEL', () => {
    const state = { model: { type: 'ABC', timestamp: 0, isValid: false } };
    const testTypeState = 'DTA';
    const testTimestampState = 123;
    const testIsValidState = true;

    SET_MODEL(state, {
      type: testTypeState,
      timestamp: testTimestampState,
      isValid: testIsValidState,
    });

    expect(state.model.type).to.eql(testTypeState);
    expect(state.model.timestamp).to.eql(testTimestampState);
    expect(state.model.isValid).to.eql(testIsValidState);
  });

  it('SET_PROGRESS', () => {
    const testScenarioId = 1;
    const state = { progress: { [testScenarioId]: {} } };
    const testComputingState = true;
    const testToBeComputedState = true;
    const testToBeSavedState = true;

    SET_PROGRESS(state, {
      scenarioId: testScenarioId,
      isBeingComputed: testComputingState,
      isToBeSaved: testToBeSavedState,
      isToBeComputed: testToBeComputedState,
    });

    expect(state.progress[testScenarioId].isBeingComputed).to.eql(testComputingState);
    expect(state.progress[testScenarioId].isToBeSaved).to.eql(testToBeComputedState);
    expect(state.progress[testScenarioId].isToBeComputed).to.eql(testToBeSavedState);
  });

  it('SET_FILTERS', () => {
    const state = {
      filters: { scenarios: { date: [{ id: 1 }, { id: 2 }] } },
    };
    const testState = { cacheKey: 'scenarios', group: 'date', selection: [{ id: 1 }] };

    SET_FILTERS(state, testState);
    expect(state.filters.scenarios.date).to.deep.eql(testState.selection);
  });

  it('SET_SORTING', () => {
    const state = {
      sorting: { scenarios: 'name' },
    };
    const testState = { cacheKey: 'scenarios', sortBy: 'id' };

    SET_SORTING(state, testState);
    expect(state.sorting.scenarios).to.deep.eql(testState.sortBy);
  });

  it('RESET_CACHE_TIMER', () => {
    const state = {
      cache: { scenarios: { timer: 12345 } },
    };
    const testState = { cacheKey: 'scenarios' };

    RESET_CACHE_TIMER(state, testState);

    const isCloseEnough = (n1, n2) => Math.abs(n1 - n2) <= 5;
    expect(isCloseEnough(state.cache.scenarios.timer, Date.now())).to.be.true;
  });
});
