import { getters } from '@store/modules/scenarios';

const { isCacheExpired, isWatchModeActive } = getters;

describe('Scenarios.getters.spec.js', () => {
  describe('isCacheExpired', () => {
    it('gets cache expired state', () => {
      const testState = {
        cache: { scenarios: { timer: 123 } },
      };
      expect(isCacheExpired(testState)('scenarios')).to.equal(true);
    });

    it('gets cache non-expired state', () => {
      const testState = {
        cache: { scenarios: { timer: Date.now() + 10000000 } },
      };
      expect(isCacheExpired(testState)('scenarios')).to.equal(false);
    });
  });

  describe('isWatchModeActive', () => {
    it('gets active watch mode state', () => {
      const testScenarioId = 1;
      let testState, testGetters;

      testState = {
        progress: { [testScenarioId]: { isBeingComputed: true } },
        editModeInProgress: false,
        model: { isValid: false },
      };
      testGetters = {
        isScenarioUnresolved: () => false,
        isOpenEditSession: () => true,
      };
      expect(isWatchModeActive(testState, testGetters)(testScenarioId)).to.equal(true);

      testState = {
        progress: { [testScenarioId]: { isBeingComputed: false } },
        editModeInProgress: false,
      };
      expect(isWatchModeActive(testState, testGetters)(testScenarioId)).to.equal(true);

      testState = {
        progress: { [testScenarioId]: { isBeingComputed: true } },
        editModeInProgress: testScenarioId,
      };
      expect(isWatchModeActive(testState, testGetters)(testScenarioId)).to.equal(true);

      testGetters = {
        isScenarioUnresolved: () => true,
        isOpenEditSession: () => true,
      };
      expect(isWatchModeActive(testState, testGetters)(testScenarioId)).to.equal(true);
    });

    it('gets non-active watch mode state', () => {
      const testScenarioId = 1;
      let testState, testGetters;

      testState = {
        progress: { [testScenarioId]: { isBeingComputed: false } },
        editModeInProgress: testScenarioId,
        model: { isValid: true },
      };
      testGetters = {
        isScenarioUnresolved: () => false,
        isOpenEditSession: () => true,
      };
      expect(isWatchModeActive(testState, testGetters)(testScenarioId)).to.equal(false);
    });
  });
});
