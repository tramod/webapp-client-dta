import { actions } from '@store/modules/scenarios';
import { expect } from 'chai';

const { startEditMode, exitEditMode } = actions;
const testScenarioId = 1;

describe('Scenarios.actions.spec.js', () => {
  describe('startEditMode', () => {
    it('sets active edit mode', async () => {
      const commit = sinon.spy();

      startEditMode({ commit }, { scenarioId: testScenarioId });

      expect(commit.args.length).to.equal(1);
      expect(commit.args[0]).to.deep.equal([
        'SET_EDIT_MODE',
        { scenarioId: testScenarioId, isUsingChangedScenario: false, isOpenSession: true },
      ]);
    });
  });

  describe('exitEditMode', () => {
    it('deactivates edit mode', async () => {
      const commit = sinon.spy();

      exitEditMode({ commit });
      expect(commit.args.length).to.equal(2);
      expect(commit.args[0]).to.deep.equal([
        'SET_EDIT_MODE',
        { scenarioId: null, isUsingChangedScenario: false, isOpenSession: false },
      ]);
      expect(commit.args[1]).to.deep.equal(['CLEAR_PROGRESS']);
    });

    it('sets scenario as not to be computed & not to be saved', async () => {
      const commit = sinon.spy();

      exitEditMode({ commit }, { scenarioId: testScenarioId });
      expect(commit.args.length).to.equal(2);
      expect(commit.args[0]).to.deep.equal([
        'SET_EDIT_MODE',
        { scenarioId: null, isUsingChangedScenario: false, isOpenSession: false },
      ]);
      expect(commit.args[1]).to.deep.equal([
        'SET_PROGRESS',
        { scenarioId: testScenarioId, isToBeComputed: false, isToBeSaved: false },
      ]);
    });
  });
});
