import sinon from 'sinon';
import { restoreAxiosMock, resetAxiosRequestHistory } from './mockAxios';
import { fsMount, fsUnmount } from './scenarioSetup.js';
import { beforeAll, afterAll } from 'vitest';

// add globals
global.sinon = sinon;
global.sleep = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};
global.before = beforeAll; // mocha compat
global.after = afterAll; // mocha compat
// vuemap required globals
window.URL.createObjectURL = () => {};
global.URL.createObjectURL = () => {};
global.Worker = class {
  constructor() {}
  addEventListener() {}
};
global.vitest = true; // helper env flag

beforeAll(async () => {
  // mount useFullScenario before all tests
  // so we can observe and force the active scenario fetching in tests
  await fsMount();
});

afterEach(() => {
  // clear axios mock request history after each test
  // (not before since some requests are made on mount)
  resetAxiosRequestHistory();
  // reset history of all spies
  sinon.resetHistory();
});

afterAll(() => {
  // unmount useFullScenario
  fsUnmount();
  // restore axios mock after all tests
  restoreAxiosMock();
});
