import App from '../src/App.vue';
import { mount } from './mountComponent.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;

describe('App.spec.js', () => {
  before(async () => {
    wrapper = mount(App);
    await flushPromises();
  });

  it('has stubbed router view', () => {
    expect(wrapper.vm.layout).to.equal(undefined);
    expect(wrapper.html()).to.contain('<router-view-stub></router-view-stub>');
  });

  after(() => {
    wrapper.unmount();
  });
});
