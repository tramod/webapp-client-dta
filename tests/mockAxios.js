import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

export const API_HOST = import.meta.env.VITE_API_HOST;
export const API_MODEL_DTA = import.meta.env.VITE_API_MODEL_DTA; // both source and data
export const MODEL_NAME_DTA = import.meta.env.VITE_MODEL_NAME_DTA;
const TRAFFIC_MODEL_PREFIX = import.meta.env.VITE_MODEL_CACHE_PREFIX;
export const TRAFFIC_MODEL_DEFAULT_CODE = `${TRAFFIC_MODEL_PREFIX}-default`;
export const axiosMock = new MockAdapter(axios, { delayResponse: 0, onNoMatch: 'throwException' });

// TM API MOCKS
export const mockLoginEndpoint = ({ response = {}, code = 200 } = {}) => {
  axiosMock.onPost(API_HOST + 'auth/login').reply(code, response);
};
export const mockVerifyUserEndpoint = ({ response = {}, code = 200 } = {}) => {
  axiosMock.onGet(API_HOST + 'auth/user').reply(code, response);
};
export const mockLogoutEndpoint = ({ response = {}, code = 200 } = {}) => {
  axiosMock.onPost(API_HOST + 'auth/logout').reply(code, response);
};
export const mockUsersEndpoint = ({ response = {}, code = 200 } = {}) => {
  axiosMock.onGet(API_HOST + 'users').reply(code, response);
};
export const mockUserEndpoint = ({ response = {}, code = 200, userId = 1 } = {}) => {
  axiosMock.onGet(API_HOST + `users/${userId}`).reply(code, response);
};
export const mockCreateUserEndpoint = ({ response = {}, code = 200 } = {}) => {
  axiosMock.onPost(API_HOST + 'users').reply(code, response);
};
export const mockUpdateUserEndpoint = ({ response = {}, code = 200, userId = 1 } = {}) => {
  axiosMock.onPatch(API_HOST + `users/${userId}`).reply(code, response);
};
export const mockDeleteUserEndpoint = ({ response = {}, code = 200, userId = 1 } = {}) => {
  axiosMock.onDelete(API_HOST + `users/${userId}`).reply(code, response);
};
export const mockRequestPasswordResetEndpoint = ({ response = {}, code = 200 } = {}) => {
  axiosMock.onPost(API_HOST + 'users/password-reset').reply(code, response);
};
export const mockResetPasswordEndpoint = ({
  response = {},
  code = 200,
  userId = 1,
  token = 'mtxouivec9ass8t918mvx',
} = {}) => {
  axiosMock.onPost(API_HOST + `users/${userId}/password-reset/${token}`).reply(code, response);
};
export const mockOrganizationsEndpoint = ({ response = {}, code = 200 } = {}) => {
  axiosMock.onGet(API_HOST + 'users/organizations').reply(code, response);
};
export const mockCloseEditSessionEndpoint = ({ response = {}, code = 200, scenarioId = 1 } = {}) => {
  axiosMock.onPost(API_HOST + `scenarios/${scenarioId}/edit/close`).reply(code, response);
};
export const mockScenariosEndpoint = ({ response = {}, code = 200 } = {}) => {
  axiosMock.onGet(API_HOST + 'scenarios').reply(code, response);
};
export const mockCreateScenarioEndpoint = ({ response = {}, code = 200 } = {}) => {
  axiosMock.onPost(API_HOST + 'scenarios').reply(code, response);
};
export const mockUpdateScenarioEndpoint = ({ response = {}, code = 200, scenarioId = 1 } = {}) => {
  axiosMock.onPatch(API_HOST + `scenarios/${scenarioId}`).reply(code, response);
};
export const mockScenarioEndpoint = ({ response = {}, code = 200, scenarioId = 1 } = {}) => {
  axiosMock.onGet(API_HOST + `scenarios/${scenarioId}`).reply(code, response);
};
export const mockScenarioEndpointAsPromise = ({ response = {}, code = 200, scenarioId = 1 } = {}) => {
  axiosMock.onGet(API_HOST + `scenarios/${scenarioId}`).reply(() => {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve([code, response]);
      }, 100);
    });
  });
};
export const mockDeleteScenarioEndpoint = ({ response = {}, code = 200, scenarioId = 1 } = {}) => {
  axiosMock.onDelete(API_HOST + `scenarios/${scenarioId}`).reply(code, response);
};
export const mockEventsEndpoint = ({ response = {}, code = 200, scenarioId = 1 } = {}) => {
  axiosMock.onGet(API_HOST + `scenarios/${scenarioId}/events`).reply(code, response);
};
export const mockCreateEventEndpoint = ({ response = {}, code = 200, scenarioId = 1 } = {}) => {
  axiosMock.onPost(API_HOST + `scenarios/${scenarioId}/events`).reply(code, response);
};
export const mockUpdateEventEndpoint = ({ response = {}, code = 200, scenarioId = 1, eventId = 1 } = {}) => {
  axiosMock.onPatch(API_HOST + `scenarios/${scenarioId}/events/${eventId}`).reply(code, response);
};
export const mockEventEndpoint = ({ response = {}, code = 200, scenarioId = 1, eventId = 1 } = {}) => {
  axiosMock.onGet(API_HOST + `scenarios/${scenarioId}/events/${eventId}`).reply(code, response);
};
export const mockDeleteEventEndpoint = ({ response = {}, code = 200, scenarioId = 1, eventId = 1 } = {}) => {
  axiosMock.onDelete(API_HOST + `scenarios/${scenarioId}/events/${eventId}`).reply(code, response);
};
export const mockImportableEventsEndpoint = ({ response = {}, code = 200, scenarioId = 1 } = {}) => {
  axiosMock.onGet(API_HOST + `scenarios/${scenarioId}/importable-events`).reply(code, response);
};
export const mockImportEventsEndpoint = ({ response = {}, code = 201, scenarioId = 1 } = {}) => {
  axiosMock.onPost(API_HOST + `scenarios/${scenarioId}/import-events`).reply(code, response);
};
export const mockModificationsEndpoint = ({ response = {}, code = 200, scenarioId = 1, eventId = 1 } = {}) => {
  axiosMock.onGet(API_HOST + `scenarios/${scenarioId}/events/${eventId}/modifications`).reply(code, response);
};
export const mockCreateModificationEndpoint = ({ response = {}, code = 200, scenarioId = 1, eventId = 1 } = {}) => {
  axiosMock.onPost(API_HOST + `scenarios/${scenarioId}/events/${eventId}/modifications`).reply(code, response);
};
export const mockUpdateModificationEndpoint = ({
  response = {},
  code = 200,
  scenarioId = 1,
  eventId = 1,
  modificationId = 1,
} = {}) => {
  axiosMock
    .onPatch(API_HOST + `scenarios/${scenarioId}/events/${eventId}/modifications/${modificationId}`)
    .reply(code, response);
};
export const mockModificationEndpoint = ({
  response = {},
  code = 200,
  scenarioId = 1,
  eventId = 1,
  modificationId = 1,
} = {}) => {
  axiosMock
    .onGet(API_HOST + `scenarios/${scenarioId}/events/${eventId}/modifications/${modificationId}`)
    .reply(code, response);
};
export const mockDeleteModificationEndpoint = ({
  response = {},
  code = 200,
  scenarioId = 1,
  eventId = 1,
  modificationId = 1,
} = {}) => {
  axiosMock
    .onDelete(API_HOST + `scenarios/${scenarioId}/events/${eventId}/modifications/${modificationId}`)
    .reply(code, response);
};
export const mockStartEditModeEndpoint = ({ response = {}, code = 200, scenarioId = 1 }) => {
  axiosMock.onPost(API_HOST + `scenarios/${scenarioId}/edit/enter`).reply(code, response);
};
export const mockCloseEditModeEndpoint = ({ response = {}, code = 200, scenarioId = 1 }) => {
  axiosMock.onPost(API_HOST + `scenarios/${scenarioId}/edit/leave`).reply(code, response);
};
export const mockCancelEditModeEndpoint = ({ response = {}, code = 200, scenarioId = 1 }) => {
  axiosMock.onPost(API_HOST + `scenarios/${scenarioId}/edit/cancel`).reply(code, response);
};
export const mockGetEditModeEndpoint = ({ response = {}, code = 200, scenarioId = 1 }) => {
  axiosMock.onGet(API_HOST + `scenarios/${scenarioId}/edit`).reply(code, response);
};
export const mockComputeEditModeEndpoint = ({ response = {}, code = 200, scenarioId = 1 }) => {
  axiosMock.onPost(API_HOST + `scenarios/${scenarioId}/edit/compute`).reply(code, response);
};
export const mockSaveEditModeEndpoint = ({ response = {}, code = 200, scenarioId = 1 }) => {
  axiosMock.onPost(API_HOST + `scenarios/${scenarioId}/edit/save`).reply(code, response);
};
export const mockGetEditOverview = ({ response = {}, code = 200 }) => {
  axiosMock.onGet(API_HOST + `scenarios/edit/overview`).reply(code, response);
};
export const mockGetExtraFeaturesEndpoint = ({ response = {}, code = 200, scenarioId = 1 }) => {
  axiosMock.onGet(API_HOST + `scenarios/${scenarioId}/extra-feature-modifications`).reply(code, response);
};
export const mockModelsEndpoint = ({ response = {}, code = 200 } = {}) => {
  axiosMock.onGet(API_HOST + 'models').reply(code, response);
};

// EXTERNAL API MOCKS
export const mockFutureTrafficDataEndpoint = ({ response = {}, code = 200 }) => {
  axiosMock.onGet(`${API_MODEL_DTA}/caches/${MODEL_NAME_DTA}/${TRAFFIC_MODEL_DEFAULT_CODE}`).reply(code, response);
};
export const mockModelLinksSourceEndpoint = ({ response = {}, code = 200 }) => {
  axiosMock.onGet(`${API_MODEL_DTA}/edges/${MODEL_NAME_DTA}`).reply(code, response);
};

export const mockModelNodesSourceEndpoint = ({ response = {}, code = 200 }) => {
  axiosMock.onGet(`${API_MODEL_DTA}/nodes/${MODEL_NAME_DTA}`).reply(code, response);
};

export const mockModelGeneratorsSourceEndpoint = ({ response = {}, code = 200 }) => {
  axiosMock.onGet(`${API_MODEL_DTA}/zones/${MODEL_NAME_DTA}`).reply(code, response);
};

// HELPERS
export const resetAxiosRequestHistory = () => {
  axiosMock.resetHistory();
};

export const restoreAxiosMock = () => {
  axiosMock.restore();
};
