import { mount } from '@vue/test-utils';
import { PrimeVueToastSymbol } from 'primevue/usetoast';
import { PrimeVueConfirmSymbol } from 'primevue/useconfirm';
import PrimeVue from 'primevue/config';
import { routerKey, routeLocationKey } from 'vue-router';
import { VueMapKey, getVueMapMock } from 'vuemap';
import tmDate from '@plugins/tm-date';
import TmButton from '@components/core/Button.vue';
import TmInput from '@components/core/Input.vue';
import TmTextarea from '@components/core/Textarea.vue';
import TmCalendar from '@components/core/Calendar.vue';
import TmSlider from '@components/core/Slider.vue';
import TmSelection from '@components/core/Selection.vue';
import TmSplitButton from '@components/core/SplitButton.vue';
import TmSwitch from '@components/core/Switch.vue';
import TmDivider from '@components/core/Divider.vue';
import TmDateRange from '@components/core/DateRange.vue';
import TmHeading from '@components/core/Heading.vue';
import TmCard from '@components/core/Card.vue';
import TmTabs from '@components/core/Tabs.vue';
import TmTabPanel from '@components/core/TabPanel.vue';
import PrimeSkeleton from 'primevue/skeleton';
import { configureVeeValidate } from '@plugins/vee-validate/config';
import { pluginKey as cookieConsentKey } from '@plugins/cookie-consent';
import { createMetaManager } from 'vue-meta'; // no way to mock this atm
import { pluginKey as tmMobileKey } from '@plugins/tm-mobile';
import {
  isRanByVitest,
  getStoreMock,
  getToastMock,
  getConfirmMock,
  getRouterMock,
  getRouteMock,
  translateMock,
  dateFormatMock,
  isMobileMock,
  getAbilityMock,
  getI18nMock,
  getCookieConsentMock,
  setupVueMapMock,
} from './helpers.js';

// use real vee-validate configuration (this applies real rules and locales files)
configureVeeValidate('cs');

const globalToastMock = getToastMock();
const globalConfirmMock = getConfirmMock();
const globalRouterMock = getRouterMock();
const globalRouteMock = getRouteMock();
const globalStoreMock = getStoreMock();
const globalAbilityMock = getAbilityMock();
const [globalI18nMockKey, globalI18nMock] = getI18nMock();
const globalCookieConsentMock = getCookieConsentMock();
const globalVueMapMock = getVueMapMock();
setupVueMapMock(globalVueMapMock); // update the mock retrieved from vuemap

const globalComponents = {
  TmTabPanel,
  TmButton,
  TmInput,
  TmTextarea,
  TmCalendar,
  TmDivider,
  TmSlider,
  TmSelection,
  TmSplitButton,
  TmSwitch,
  TmDateRange,
  TmHeading,
  TmCard,
  TmTabs,
  PrimeSkeleton,
};

const globalComponentStubs = [
  'TmBreadcrumb',
  'OlVectorTileLocal',
  'RouterView',
  'RouterLink',
  'OlSelectPopup',
  'TmInputSelector',
  'i18n-t',
  'metainfo',
];

const setupGlobalMocks = ({
  router = globalRouterMock,
  route = globalRouteMock,
  store = globalStoreMock,
  ability = globalAbilityMock,
  vueMap = globalVueMapMock,
  cookieConsent = globalCookieConsentMock,
}) => ({
  plugins: [
    PrimeVue, // primeVue config - used when changing locale, does not export its Symbol
    globalI18nMock, // mocked install is needed so we can mock and setup the key symbol
    ...(router.install ? [router] : []), // in case the real router is used
    ...(store.install ? [store] : []), // in case the real store is used
    ...(ability.install ? [ability] : []), // in case the real ability plugin is used
    tmDate, // no need to mock this I think..
    createMetaManager(), // no way to mock this atm
  ],
  mocks: {
    // used inside template
    ...(!router.install ? { $router: router, $route: route } : {}),
    ...(!ability.install ? { $tmability: ability } : {}),
    $toast: globalToastMock,
    $confirm: globalConfirmMock,
    $t: translateMock,
    $d: dateFormatMock,
    $cc: cookieConsent,
    $isMobile: isMobileMock,
  },
  provide: {
    // used inside setup()
    ...(!router.install ? { [routerKey]: router, [routeLocationKey]: route } : {}),
    ...(!store.install ? { store: store } : {}),
    ...(!ability.install ? { tmability: ability } : {}),
    [PrimeVueToastSymbol]: globalToastMock,
    [PrimeVueConfirmSymbol]: globalConfirmMock,
    [VueMapKey]: vueMap,
    [globalI18nMockKey]: globalI18nMock, // includes t() and d() function mocks,
    [cookieConsentKey]: cookieConsent,
    [tmMobileKey]: isMobileMock,
  },
});

const mountComponent = (
  component,
  {
    props = {},
    components = {},
    mocks = {},
    slots = {},
    stubs = globalComponentStubs,
    isShallow = false,
    withGlobalComponents = true,
  } = {},
) => {
  return mount(component, {
    attachTo: isRanByVitest() ? document.body : document.getElementById('test-app'),
    props,
    slots,
    global: {
      components: {
        ...components,
        ...(withGlobalComponents ? globalComponents : {}),
      },
      ...setupGlobalMocks(mocks),
      stubs,
      directives: { tooltip: {} },
    },
    shallow: isShallow,
  });
};

export { mountComponent as mount, globalToastMock, globalRouterMock, globalStoreMock, globalComponentStubs };
