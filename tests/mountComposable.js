import { PrimeVueToastSymbol } from 'primevue/usetoast';
import { PrimeVueConfirmSymbol } from 'primevue/useconfirm';
import { routerKey, routeLocationKey } from 'vue-router';
import { createApp } from 'vue';
import tmDate from '@plugins/tm-date';
import { pluginKey as tmMobileKey } from '@plugins/tm-mobile';
import {
  getStoreMock,
  getToastMock,
  getConfirmMock,
  getRouterMock,
  getRouteMock,
  isMobileMock,
  getI18nMock,
} from './helpers.js';

const globalToastMock = getToastMock();
const globalConfirmMock = getConfirmMock();
const globalRouterMock = getRouterMock();
const globalRouteMock = getRouteMock();
const globalStoreMock = getStoreMock();
const [globalI18nMockKey, globalI18nMock] = getI18nMock();

const mountComposable = (
  composable,
  { router = globalRouterMock, route = globalRouteMock, store = globalStoreMock, mobile = isMobileMock } = {},
) => {
  const App = {
    setup() {
      const result = composable();
      const wrapper = () => result;
      return { wrapper };
    },
    render() {},
  };
  const root = document.createElement('div');
  const app = createApp(App);

  app.use(tmDate);
  app.use(globalI18nMock);

  app.provide(PrimeVueToastSymbol, globalToastMock);
  app.provide(PrimeVueConfirmSymbol, globalConfirmMock);
  app.provide(tmMobileKey, mobile);
  app.provide(globalI18nMockKey, globalI18nMock);

  if (store.install) app.use(store);
  else app.provide('store', store);

  if (router.install) app.use(router);
  else {
    app.provide(routerKey, router);
    app.provide(routeLocationKey, route);
  }

  const vm = app.mount(root);
  const wrapper = vm.wrapper();
  const unmount = () => app.unmount();

  return {
    ...wrapper,
    unmount,
  };
};

export { mountComposable as mount, globalToastMock, globalRouterMock, globalStoreMock };
