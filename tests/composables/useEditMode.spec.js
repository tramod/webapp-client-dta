import useEditMode from '@composables/useEditMode';
import { mount, globalToastMock, globalStoreMock } from '../mountComposable';
import {
  mockStartEditModeEndpoint,
  mockCloseEditModeEndpoint,
  mockCancelEditModeEndpoint,
  mockGetEditModeEndpoint,
  mockComputeEditModeEndpoint,
  mockSaveEditModeEndpoint,
  mockScenarioEndpoint,
  mockScenariosEndpoint,
  mockGetEditOverview,
  axiosMock,
  API_HOST,
} from './../mockAxios.js';
import { flushPromises } from '@vue/test-utils';
import { getStoreMock } from '../helpers.js';

const testScenario = {
  id: 1,
  name: 'Private scenario',
  type: 'private',
  dateFrom: '2021-02-18T09:05:58.731Z',
  dateTo: '2021-02-18T09:05:58.731Z',
  level: 'editor',
  authorUserId: 1,
  hasModelSectionsValid: true,
};

let wrapper, testStore;

describe('useEditMode.spec.js', () => {
  before(() => {
    testStore = getStoreMock();
    testStore.state.scenarios.editMode.scenarioId = testScenario.id;
  });

  beforeEach(() => {
    mockScenarioEndpoint({ response: { scenario: testScenario }, scenarioId: testScenario.id });
    mockScenariosEndpoint({ response: { scenarios: [testScenario] } });
  });

  describe('on good requests', () => {
    it('starts new edit mode', async () => {
      wrapper = mount(() => useEditMode(testScenario.id));
      mockStartEditModeEndpoint({
        response: {
          editSession: {},
          status: null,
        },
        scenarioId: testScenario.id,
      });

      await wrapper.enterEditSession(testScenario);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/edit/enter`);

      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');

      expect(globalStoreMock.dispatch.calledWith('layout/startLoading')).to.equal(true);
      expect(globalStoreMock.dispatch.calledWith('layout/finishLoading')).to.equal(true);
      expect(
        globalStoreMock.dispatch.calledWith('scenarios/startEditMode', {
          scenarioId: testScenario.id,
          withChanges: true,
        }),
      ).to.equal(true);
      expect(globalStoreMock.dispatch.calledWith('scenarios/updateScenarioProgress')).to.equal(true);
    });

    it('resumes unfinished edit mode', async () => {
      wrapper = mount(() => useEditMode(testScenario.id));
      mockStartEditModeEndpoint({
        response: {
          editSession: {
            hasChange: true,
            hasComputableChange: true,
          },
          status: 'EDIT_ENTERED',
        },
        scenarioId: testScenario.id,
      });

      await wrapper.enterEditSession(testScenario);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/edit/enter`);

      expect(globalToastMock.add.calledTwice).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].summary).to.equal('scenarios.changes resumed');
      expect(globalToastMock.add.getCall(-2).args[0].summary).to.equal('scenarios.start edit mode');

      expect(globalStoreMock.dispatch.calledWith('layout/startLoading')).to.equal(true);
      expect(globalStoreMock.dispatch.calledWith('layout/finishLoading')).to.equal(true);
      expect(
        globalStoreMock.dispatch.calledWith('scenarios/startEditMode', {
          scenarioId: testScenario.id,
          withChanges: true,
        }),
      ).to.equal(true);
      expect(globalStoreMock.dispatch.calledWith('scenarios/updateScenarioProgress')).to.equal(true);
    });

    it('closes edit mode', async () => {
      wrapper = mount(() => useEditMode(testScenario.id));
      mockCloseEditModeEndpoint({
        scenarioId: testScenario.id,
      });

      await wrapper.exitEditSession(testScenario.id);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/edit/leave`);

      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('info');

      expect(globalStoreMock.dispatch.calledWith('layout/startLoading')).to.equal(true);
      expect(globalStoreMock.dispatch.calledWith('layout/finishLoading')).to.equal(true);
      expect(
        globalStoreMock.dispatch.calledWith('scenarios/exitEditMode', {
          scenarioId: testScenario.id,
        }),
      ).to.equal(true);
    });

    it('cancels edit mode', async () => {
      wrapper = mount(() => useEditMode(testScenario.id));
      mockCancelEditModeEndpoint({
        scenarioId: testScenario.id,
        response: { editSession: {} },
      });

      await wrapper.cancelEditSession(testScenario.id);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/edit/cancel`);

      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('info');

      expect(globalStoreMock.dispatch.calledWith('layout/startLoading')).to.equal(true);
      expect(globalStoreMock.dispatch.calledWith('layout/finishLoading')).to.equal(true);
      expect(globalStoreMock.dispatch.calledWith('scenarios/updateScenarioProgress')).to.equal(true);
    });

    it('verifies server edit mode', async () => {
      wrapper = mount(() => useEditMode(testScenario.id), { store: testStore });
      mockGetEditOverview({
        scenarioId: testScenario.id,
        response: [{ scenarioId: testScenario.id, editSession: {} }],
      });

      await wrapper.verifyEditSessions();
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/edit/overview`);

      expect(globalToastMock.add.notCalled).to.equal(true);
    });

    it('computes scenario & tracks the state & re-fetches scenarios', async () => {
      wrapper = mount(() => useEditMode(testScenario.id));

      mockScenarioEndpoint({ response: { scenario: testScenario }, scenarioId: testScenario.id });
      mockComputeEditModeEndpoint({ scenarioId: testScenario.id });
      // also mock this for the synchronous tracking
      mockGetEditModeEndpoint({
        scenarioId: testScenario.id,
        response: {
          editSession: {
            computationState: 'notStarted', // notStarted is the desired computation state if we also want to saveAfter
            state: 'saved',
          },
        },
      });

      await wrapper.computeScenario(testScenario.id, { saveAfter: true });
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/edit/compute`);

      expect(globalToastMock.add.calledThrice).to.equal(true);
      expect(globalToastMock.add.getCall(-3).args[0].summary).to.equal('scenarios.scenario start computing');
      expect(globalToastMock.add.getCall(-2).args[0].summary).to.equal('scenarios.computing finished');
      expect(globalToastMock.add.getCall(-1).args[0].summary).to.equal('scenarios.scenario saved');

      expect(globalStoreMock.commit.getCall(-1).args).to.deep.equal([
        'scenarios/SET_PROGRESS',
        { scenarioId: testScenario.id, isBeingComputed: true },
      ]);

      expect(axiosMock.history.get.length).to.equal(2);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/edit`);
      expect(axiosMock.history.get[1].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);

      expect(globalStoreMock.dispatch.calledWith('scenarios/updateScenarioProgress')).to.equal(true);
      expect(globalStoreMock.dispatch.calledWith('layout/finishLoading')).to.equal(true);
    }); //.timeout(5000);

    it('saves scenario', async () => {
      wrapper = mount(() => useEditMode(testScenario.id));
      mockSaveEditModeEndpoint({
        scenarioId: testScenario.id,
        response: { editSession: {} },
      });

      mockScenarioEndpoint({ response: { scenario: testScenario }, scenarioId: testScenario.id }); // scenario refetch

      await wrapper.saveScenario(testScenario.id);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/edit/save`);

      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');

      expect(globalStoreMock.dispatch.calledWith('layout/startLoading')).to.equal(true);
      expect(globalStoreMock.dispatch.calledWith('layout/finishLoading')).to.equal(true);
      expect(globalStoreMock.dispatch.calledWith('scenarios/updateScenarioProgress')).to.equal(true);
    });
  });

  describe('on bad requests', () => {
    it('does not start edit mode', async () => {
      wrapper = mount(() => useEditMode(testScenario.id));
      mockStartEditModeEndpoint({
        code: 404,
        scenarioId: testScenario.id,
      });

      await wrapper.enterEditSession(testScenario);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/edit/enter`);

      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(globalToastMock.add.getCall(-1).args[0].summary).to.equal('scenarios.failed to start edit mode');
    });

    it('does not close edit mode', async () => {
      wrapper = mount(() => useEditMode(testScenario.id));
      mockCloseEditModeEndpoint({
        code: 404,
        scenarioId: testScenario.id,
      });

      await wrapper.exitEditSession(testScenario.id);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/edit/leave`);

      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
    });

    it('does not cancel edit mode', async () => {
      wrapper = mount(() => useEditMode(testScenario.id));
      mockCancelEditModeEndpoint({
        code: 404,
        scenarioId: testScenario.id,
      });

      await wrapper.cancelEditSession(testScenario.id);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/edit/cancel`);

      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
    });

    it('exits edit mode that is not verified on server', async () => {
      wrapper = mount(() => useEditMode(testScenario.id), { store: testStore });
      mockGetEditOverview({
        code: 404,
        scenarioId: testScenario.id,
        response: [],
      });

      await wrapper.verifyEditSessions();
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(2);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/edit/overview`);
      expect(axiosMock.history.get[1].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`); // re-fetch scenario
      expect(globalToastMock.add.notCalled).to.equal(true);

      expect(testStore.dispatch.calledWith('layout/startLoading')).to.equal(true);
      expect(testStore.dispatch.calledWith('layout/finishLoading')).to.equal(true);
      expect(
        testStore.dispatch.calledWith('scenarios/exitEditMode', {
          scenarioId: testScenario.id,
        }),
      ).to.equal(true);
    });

    it('does not compute scenario', async () => {
      wrapper = mount(() => useEditMode(testScenario.id));
      mockComputeEditModeEndpoint({
        code: 404,
        scenarioId: testScenario.id,
      });

      await wrapper.computeScenario(testScenario.id);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/edit/compute`);

      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
    });

    it('does not save scenario', async () => {
      wrapper = mount(() => useEditMode(testScenario.id));
      mockSaveEditModeEndpoint({
        code: 404,
        scenarioId: testScenario.id,
      });

      await wrapper.saveScenario(testScenario.id);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/edit/save`);

      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
    });
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
