import useAxios from '@composables/useAxios';
import { mount } from '../mountComposable';
import {
  mockScenariosEndpoint,
  mockCreateScenarioEndpoint,
  mockUpdateScenarioEndpoint,
  mockDeleteScenarioEndpoint,
  axiosMock,
  API_HOST,
} from './../mockAxios.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;
const testResponse = { id: 1 };
const methods = [
  {
    name: 'get',
    url: `${API_HOST}scenarios`,
    mock: () => mockScenariosEndpoint({ response: testResponse }),
    mockErr: () => mockScenariosEndpoint({ code: 404 }),
  },
  {
    name: 'post',
    url: `${API_HOST}scenarios`,
    mock: () => mockCreateScenarioEndpoint({ response: testResponse }),
    mockErr: () => mockCreateScenarioEndpoint({ code: 404 }),
  },
  {
    name: 'patch',
    url: `${API_HOST}scenarios/${testResponse.id}`,
    mock: () => mockUpdateScenarioEndpoint({ response: testResponse, scenarioId: testResponse.id }),
    mockErr: () => mockUpdateScenarioEndpoint({ code: 404 }),
  },
  {
    name: 'delete',
    url: `${API_HOST}scenarios/${testResponse.id}`,
    mock: () => mockDeleteScenarioEndpoint({ response: testResponse, scenarioId: testResponse.id }),
    mockErr: () => mockDeleteScenarioEndpoint({ code: 404 }),
  },
];

describe('useAxios.spec.js', () => {
  methods.forEach((method) => {
    describe(method.name.toUpperCase(), () => {
      before(() => {
        method.mock();
      });

      it('makes request', async () => {
        wrapper = mount(useAxios);

        await wrapper.exec({ url: method.url, method: method.name });
        await flushPromises();

        expect(axiosMock.history[method.name].length).to.equal(1);
        expect(axiosMock.history[method.name][0].url).to.equal(method.url);

        await wrapper.response.value;
      });

      it('passes data', async () => {
        wrapper = mount(useAxios);

        const testData = { test: 'test' };

        await wrapper.exec({ url: method.url, method: method.name, data: testData });
        await flushPromises();

        expect(axiosMock.history[method.name][0].data).to.deep.equal(JSON.stringify(testData));

        await wrapper.response.value;
      });

      it('returns valid result on successful request', async () => {
        wrapper = mount(useAxios);

        await wrapper.exec({ url: method.url, method: method.name });
        await flushPromises();

        await wrapper.response.value;

        expect(wrapper.hasError.value).to.equal(false);
        expect(wrapper.response.value.status).to.equal(200);
        expect(wrapper.response.value.data).to.deep.equal(testResponse);
      });

      it('returns error on bad request', async () => {
        method.mockErr();
        wrapper = mount(useAxios);

        await wrapper.exec({ url: method.url, method: method.name });
        await flushPromises();

        await wrapper.response.value;

        expect(wrapper.hasError.value).to.equal(true);
        expect(wrapper.response.value.status).to.equal(404);
      });

      after(() => {
        wrapper.unmount();
      });
    });
  });
});
