import useDataModes from '@composables/useDataModes';
import { mount } from '../mountComposable';
import { getStoreMock } from './../helpers.js';
import { flushPromises } from '@vue/test-utils';

let wrapper, testStore;

const testModes = [
  {
    name: 'model',
    icon: 'ri-send-plane-2-line',
    tooltip: 'model',
    expectedMapModes: [
      'modelDTA',
      'model-generator_new_draw',
      'model-generator_new_nodes_select',
      'model-generator_existing_select',
      'model-generator_existing_nodes_select',
      'model-link_new_nodes_select',
      'model-link_new_modification',
      'model-link_existing_select',
      'model-node_new_draw',
      'model-node_existing_selection',
      'model-node_existing_linking',
    ],
  },
  {
    name: 'comparison',
    icon: 'ri-tools-line',
    tooltip: 'comparison',
    expectedMapModes: ['modelDTA-comparison'],
  },
];

describe('useDataModes.spec.js', () => {
  it('returns all enabled data modes as array of data mode names', async () => {
    wrapper = mount(useDataModes);
    await flushPromises();

    const enabledModes = wrapper.enabledDataModes;
    expect(enabledModes.length).to.equal(testModes.length);
    expect(enabledModes).to.deep.equal(testModes.map((tm) => tm.name));
  });

  it('returns all available data modes as reactive array of objects', async () => {
    wrapper = mount(useDataModes);
    await flushPromises();

    const availableModes = wrapper.availableDataModes.value;
    expect(availableModes.length).to.equal(testModes.length);
    expect(availableModes.map((tm) => tm.name)).to.deep.equal(testModes.map((tm) => tm.name));
  });

  it('adds corresponding map modes to every available data mode object', async () => {
    wrapper = mount(useDataModes);
    await flushPromises();

    const availableModes = wrapper.availableDataModes.value;

    availableModes.forEach((am, index) => {
      expect(am.mapModes).to.deep.equal(testModes[index].expectedMapModes);
    });
  });

  it('marks broken map modes with isBroken prop', async () => {
    const testMode = 'model';
    testStore = getStoreMock();
    testStore.getters['map/getDataMode'] = testMode;
    testStore.state.map.brokenMapModes = ['modelDTA'];
    wrapper = mount(useDataModes, { store: testStore });
    await flushPromises();
    const availableModes = wrapper.availableDataModes.value;

    availableModes.forEach((am) => {
      const isExpectedToBeBroken = (mm) => mm.name == testMode;
      expect(am.isBroken).to.equal(isExpectedToBeBroken(am));
    });
  });

  it('returns current data mode', async () => {
    const testMode = 'model';
    testStore = getStoreMock();
    testStore.getters['map/getDataMode'] = testMode;
    wrapper = mount(useDataModes, { store: testStore });
    await flushPromises();

    const currentMode = wrapper.currentDataMode.value;
    expect(currentMode).to.equal(testMode);
  });

  it('can determine wether data mode object is currently active', async () => {
    const testMode = 'model';
    testStore = getStoreMock();
    testStore.getters['map/getDataMode'] = testMode;
    wrapper = mount(useDataModes, { store: testStore });
    await flushPromises();

    expect(wrapper.isActiveDataMode(testModes[0])).to.equal(true);
    expect(wrapper.isActiveDataMode(testModes[1])).to.equal(false);
  });

  it('can change active data mode by dispatching its default map mode', async () => {
    const testMode = 'model';
    testStore = getStoreMock();
    testStore.getters['map/getDataMode'] = testMode;
    wrapper = mount(useDataModes, { store: testStore });
    await flushPromises();

    const updatedMapMode = 'model';
    const updatedDataModeObject = { ...testModes[1], mapModes: [updatedMapMode] };

    wrapper.setDataMode(updatedDataModeObject);
    await flushPromises();

    expect(testStore.dispatch.calledOnce).to.equal(true);
    expect(testStore.dispatch.args[0][0]).to.equal('map/setMapMode');
    expect(testStore.dispatch.args[0][1].mode).to.equal(updatedMapMode);
  });

  it('tries to toggle widget panel when activating already active data mode', async () => {
    const testMode = 'model';
    testStore = getStoreMock();
    testStore.getters['map/getDataMode'] = testMode;
    wrapper = mount(useDataModes, { store: testStore });
    await flushPromises();

    const updatedMapMode = 'modelDTA';
    const updatedDataModeObject = { ...testModes[0], mapModes: [updatedMapMode] };

    wrapper.setDataMode(updatedDataModeObject);
    await flushPromises();

    expect(testStore.dispatch.calledOnce).to.equal(true);
    expect(testStore.dispatch.args[0][0]).to.equal('layout/togglePanel');
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
