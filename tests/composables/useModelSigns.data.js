import { LID_DTA_LINKS, LID_DTA_NODES, LID_DTA_GENERATORS } from '@keys/index';

/*
Coords transform 3857 -> 4326
 100; 0.0008983
 200; 0.0017967
 300; 0.0026949
 150; 0.0013475
 5000; 0.0449158
*/

export const MOCK_BASE_MODEL = {
  [LID_DTA_LINKS]: [
    {
      type: 'Feature',
      properties: {
        edge_id: 1,
        source: 1,
        target: 2,
        capacity: 1000.0,
        cost: 0.01,
        speed: 5.0,
        type: 0,
      },
      geometry: {
        type: 'LineString',
        coordinates: [
          [0.0008983, 0.0008983],
          [0.0017967, 0.0017967],
        ],
      },
    },
    {
      type: 'Feature',
      properties: {
        edge_id: 2,
        source: 2,
        target: 3,
        capacity: 1000.0,
        cost: 0.01,
        speed: 5.0,
        type: 0,
      },
      geometry: {
        type: 'LineString',
        coordinates: [
          [0.0017967, 0.0017967],
          [0.0026949, 0.0026949],
        ],
      },
    },
    {
      type: 'Feature',
      properties: {
        edge_id: 3,
        source: 3,
        target: 2,
        capacity: 1000.0,
        cost: 0.01,
        speed: 5.0,
        type: 0,
      },
      geometry: {
        type: 'LineString',
        coordinates: [
          [0.0026949, 0.0026949],
          [0.0017967, 0.0017967],
        ],
      },
    },
  ],
  [LID_DTA_NODES]: [
    {
      type: 'Feature',
      properties: { node_id: 1 },
      geometry: { type: 'Point', coordinates: [0.0008983, 0.0008983] },
    },
    {
      type: 'Feature',
      properties: { node_id: 2 },
      geometry: { type: 'Point', coordinates: [0.0017967, 0.0017967] },
    },
  ],
  [LID_DTA_GENERATORS]: [
    {
      type: 'Feature',
      properties: {
        zone_id: 1,
        node_id: 1,
        trips: 10,
        incoming_trips: 10,
        outgoing_trips: 10,
      },
      geometry: { type: 'Point', coordinates: [0.0013475, 0.0013475] },
    },
  ],
};

const _mockScenarioBase = {
  name: 'Test scenario',
  type: 'private',
  dateFrom: null,
  dateTo: null,
  authorUserId: 1,
};

const _mockEventBase = {
  name: 'Test event',
  description: 'popis',
  dateFrom: null,
  dateTo: null,
  authorUserId: 1,
  included: true,
};

const _mockModLinkNew = {
  dateFrom: null,
  dateTo: null,
  type: 'link',
  mode: 'new',
  source: 2,
  target: 1,
  coordinates: [
    [200.0, 200.0],
    [100.0, 100.0],
  ],
  capacity: 900,
  speed: 40,
  twoWay: false,
  twoWayCapacity: 900,
  twoWaySpeed: 40,
};

const _mockModLinkExisting = {
  dateFrom: null,
  dateTo: null,
  type: 'link',
  mode: 'existing',
  linkId: [1, 2],
  capacity: 900,
  speed: 40,
  twoWay: false,
  twoWayCapacity: 900,
  twoWaySpeed: 40,
};

const _mockModNodeNew = {
  dateFrom: null,
  dateTo: null,
  type: 'node',
  mode: 'new',
  node: [300.0, 300.0],
};
const _mockModNodeExisting = {
  dateFrom: null,
  dateTo: null,
  type: 'node',
  mode: 'existing',
  node: 2,
  linkFrom: 1,
  linkTo: 2,
  cost: 60,
};
const _mockModGenNew = {
  dateFrom: null,
  dateTo: null,
  type: 'generator',
  mode: 'new',
  generator: [5000.0, 5000.0],
  nodes: [1],
  inTraffic: 250,
  outTraffic: 250,
};
const _mockModGenExisting = {
  dateFrom: null,
  dateTo: null,
  type: 'generator',
  mode: 'existing',
  generator: 1,
  nodes: [1, 2],
  inTraffic: 250,
  outTraffic: 250,
};

const _mockModClosedLink = {
  dateFrom: null,
  dateTo: null,
  type: 'link',
  mode: 'existing',
  linkId: [1],
  capacity: 0,
  speed: 0,
};

const _mockModRestrictedLink = {
  dateFrom: null,
  dateTo: null,
  type: 'link',
  mode: 'existing',
  linkId: [2],
  capacity: 100,
  speed: 5.0,
};

const _mockModEnlargedLink = {
  dateFrom: null,
  dateTo: null,
  type: 'link',
  mode: 'existing',
  linkId: [3],
  capacity: 5000,
  speed: 500.0,
};

export const MOCK_SCENARIOS = {
  baseTest: {
    id: 1,
    ..._mockScenarioBase,
    events: [
      {
        id: 1,
        ..._mockEventBase,
        modifications: [
          {
            id: 1,
            ..._mockModLinkNew,
          },
        ],
      },
    ],
  },
  multiTest1: {
    id: 1,
    ..._mockScenarioBase,
    events: [
      {
        id: 1,
        ..._mockEventBase,
        modifications: [
          {
            id: 1,
            ..._mockModLinkExisting,
          },
        ],
      },
    ],
  },
  multiTest2: {
    id: 2,
    ..._mockScenarioBase,
    events: [
      {
        id: 2,
        ..._mockEventBase,
        modifications: [
          {
            id: 2,
            ..._mockModLinkExisting,
          },
        ],
      },
    ],
  },
  filterTest: {
    id: 1,
    ..._mockScenarioBase,
    events: [
      {
        id: 1,
        ..._mockEventBase,
        modifications: [
          {
            id: 1,
            ..._mockModLinkNew,
          },
          {
            id: 2,
            ..._mockModLinkNew,
            dateFrom: '2021-01-01T00:00:00.000Z',
            dateTo: '2022-01-01T00:00:00.000Z',
          },
        ],
      },
      {
        id: 2,
        ..._mockEventBase,
        included: false,
        modifications: [
          {
            id: 3,
            ..._mockModLinkNew,
          },
          {
            id: 4,
            ..._mockModLinkNew,
            dateFrom: '2021-01-01T00:00:00.000Z',
            dateTo: '2022-01-01T00:00:00.000Z',
          },
        ],
      },
    ],
  },
  transform: {
    id: 1,
    ..._mockScenarioBase,
    events: [
      {
        id: 1,
        ..._mockEventBase,
        modifications: [
          {
            id: 1,
            ..._mockModLinkNew,
          },
          {
            id: 2,
            ..._mockModLinkExisting,
          },
          {
            id: 3,
            ..._mockModNodeNew,
          },
          {
            id: 4,
            ..._mockModNodeExisting,
          },
          {
            id: 5,
            ..._mockModGenNew,
          },
          {
            id: 6,
            ..._mockModGenExisting,
          },
        ],
      },
    ],
  },
  signsMode: {
    id: 1,
    ..._mockScenarioBase,
    events: [
      {
        id: 1,
        ..._mockEventBase,
        modifications: [
          {
            id: 1,
            ..._mockModClosedLink,
          },
          {
            id: 2,
            ..._mockModRestrictedLink,
          },
          {
            id: 3,
            ..._mockModEnlargedLink,
          },
        ],
      },
    ],
  },
};
