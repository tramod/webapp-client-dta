import useUsers from '@composables/useUsers';
import { mount, globalToastMock } from '../mountComposable';
import {
  mockUsersEndpoint,
  mockUserEndpoint,
  mockCreateUserEndpoint,
  mockUpdateUserEndpoint,
  mockDeleteUserEndpoint,
  mockRequestPasswordResetEndpoint,
  mockResetPasswordEndpoint,
  mockOrganizationsEndpoint,
  axiosMock,
  API_HOST,
} from './../mockAxios.js';
import { flushPromises } from '@vue/test-utils';

const testUsers = [
  {
    id: 1,
    username: 'franta',
    email: 'franta@franta.com',
    roles: [100, 1],
    organization: 'roadtwin',
  },
  {
    id: 2,
    username: 'pepa',
    email: 'pepa@pepa.com',
    roles: [],
    organization: null,
  },
  {
    id: 3,
    username: 'lojza',
    email: 'lojza@lojza.com',
    roles: [],
    organization: null,
  },
];

const testUsersPublic = testUsers.map(({ id, username, organization }) => ({
  id,
  username,
  organization,
}));

const testUser = testUsers[0];

const testOrganizations = [
  { id: 1, name: 'org 1' },
  { id: 2, name: 'org 2' },
  { id: 3, name: 'org 3' },
];

let wrapper;

describe('useUsers.spec.js', () => {
  describe('on good requests', () => {
    it('fetches users data', async () => {
      wrapper = mount(() => useUsers());
      mockUsersEndpoint({ response: { users: testUsersPublic } });

      expect(wrapper.users.value).to.deep.equal([]);

      await wrapper.fetchUsers();
      await flushPromises();
      const users = wrapper.users.value;

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}users`);
      expect(globalToastMock.add.notCalled).to.equal(true);

      testUsersPublic.forEach((user) => {
        expect(users).to.deep.include(user);
      });
    });

    it('fetches users data with hidden fields', async () => {
      wrapper = mount(() => useUsers());
      mockUsersEndpoint({ response: { users: testUsers } });

      expect(wrapper.users.value).to.deep.equal([]);

      await wrapper.fetchUsers(true);
      await flushPromises();
      const users = wrapper.users.value;
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}users`);
      expect(axiosMock.history.get[0].params.hiddenFields).to.equal(true);

      testUsers.forEach((user) => {
        expect(users).to.deep.include(user);
      });
    });

    it('fetches user data', async () => {
      wrapper = mount(() => useUsers(testUser.id));
      mockUserEndpoint({ response: { user: testUser }, userId: testUser.id });
      expect(wrapper.user.value).to.deep.equal({});

      await wrapper.fetchUser();
      await flushPromises();
      const user = wrapper.user.value;

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}users/${testUser.id}`);
      expect(globalToastMock.add.notCalled).to.equal(true);
      expect(user).to.deep.equal(testUser);
    });

    it('fetches organizations data', async () => {
      wrapper = mount(() => useUsers());
      mockOrganizationsEndpoint({ response: { organizations: testOrganizations } });

      await wrapper.fetchOrganizations();
      await flushPromises();
      const organizations = wrapper.organizations.value;

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}users/organizations`);
      expect(globalToastMock.add.notCalled).to.equal(true);

      testOrganizations.forEach((org) => {
        expect(organizations).to.deep.include(org);
      });
    });

    it('creates new user', async () => {
      wrapper = mount(() => useUsers());
      mockCreateUserEndpoint({ response: { user: testUser } });
      mockRequestPasswordResetEndpoint();

      expect(wrapper.user.value).to.deep.equal({});
      expect(wrapper.users.value).to.deep.equal([]);

      const newUser = {
        username: 'test username',
        email: 'test@test.test',
      };

      await wrapper.createUser(newUser);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(2);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}users`);
      expect(axiosMock.history.post[1].url).to.equal(`${API_HOST}users/password-reset`);

      expect(wrapper.users.value[0]).to.deep.equal(testUser);
      expect(globalToastMock.add.calledTwice).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
      expect(globalToastMock.add.getCall(-2).args[0].severity).to.equal('success');
    });

    it('updates user', async () => {
      wrapper = mount(() => useUsers(testUser.id));
      mockUpdateUserEndpoint({ response: { user: testUser }, userId: testUser.id });

      expect(wrapper.user.value).to.deep.equal({});
      expect(wrapper.users.value).to.deep.equal([]);

      const updatedUser = {
        ...testUser,
        username: 'updated username',
      };

      await wrapper.updateUser(updatedUser);
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(axiosMock.history.patch[0].url).to.equal(`${API_HOST}users/${testUser.id}`);
      expect(wrapper.user.value).to.deep.equal(testUser);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
    });

    it('deletes user', async () => {
      wrapper = mount(() => useUsers(testUser.id));
      mockDeleteUserEndpoint({ response: { user: testUser }, userId: testUser.id });

      expect(wrapper.user.value).to.deep.equal({});
      expect(wrapper.users.value).to.deep.equal([]);

      await wrapper.deleteUser(testUser.id);
      await flushPromises();

      expect(axiosMock.history.delete.length).to.equal(1);
      expect(axiosMock.history.delete[0].url).to.equal(`${API_HOST}users/${testUser.id}`);
      expect(wrapper.users.value).to.deep.equal([]);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
    });

    it('requests password reset', async () => {
      wrapper = mount(() => useUsers());
      mockRequestPasswordResetEndpoint();

      await wrapper.requestPasswordReset({ email: testUser.email });
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}users/password-reset`);

      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
    });

    it('resets password', async () => {
      wrapper = mount(() => useUsers());
      mockResetPasswordEndpoint();

      const testPasswordResetBody = {
        userId: 1,
        token: 'mtxouivec9ass8t918mvx',
        email: 'test@test.test',
        password: '1234heslo',
      };

      await wrapper.resetPassword(testPasswordResetBody);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(
        `${API_HOST}users/${testPasswordResetBody.userId}/password-reset/${testPasswordResetBody.token}`,
      );
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
    });
  });

  describe('on bad requests', () => {
    it('fetches no users data & displays error', async () => {
      wrapper = mount(() => useUsers());
      mockUsersEndpoint({ code: 403 });
      expect(wrapper.users.value).to.deep.equal([]);

      await wrapper.fetchUsers();
      await flushPromises();
      const users = wrapper.users.value;

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}users`);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(users).to.deep.equal([]);
    });

    it('fetches no user data & displays error', async () => {
      wrapper = mount(() => useUsers(testUser.id));
      mockUserEndpoint({ code: 403, userId: testUser.id });
      expect(wrapper.user.value).to.deep.equal({});

      await wrapper.fetchUser();
      await flushPromises();
      const user = wrapper.user.value;

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}users/${testUser.id}`);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(user).to.deep.equal({});
    });

    it('fetches no organizations data & displays error', async () => {
      wrapper = mount(() => useUsers());
      mockOrganizationsEndpoint({ code: 403 });

      await wrapper.fetchOrganizations();
      await flushPromises();
      const organizations = wrapper.organizations.value;

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}users/organizations`);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(organizations).to.deep.equal([]);
    });

    it('does not create user and sends error', async () => {
      wrapper = mount(() => useUsers());
      mockCreateUserEndpoint({ code: 404 });
      mockRequestPasswordResetEndpoint({ code: 403 });

      expect(wrapper.user.value).to.deep.equal({});
      expect(wrapper.users.value).to.deep.equal([]);

      const newUser = {
        username: 'test username',
        email: 'test@test.test',
      };

      await wrapper.createUser(newUser);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}users`);
      expect(wrapper.users.value).to.deep.equal([]);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
    });

    it('does not update user and sends error', async () => {
      wrapper = mount(() => useUsers(testUser.id));
      mockUpdateUserEndpoint({ code: 404, userId: testUser.id });

      expect(wrapper.user.value).to.deep.equal({});
      expect(wrapper.users.value).to.deep.equal([]);

      const updatedUser = {
        ...testUser,
        username: 'updated username',
      };

      await wrapper.updateUser(updatedUser);
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(axiosMock.history.patch[0].url).to.equal(`${API_HOST}users/${testUser.id}`);
      expect(wrapper.user.value).to.deep.equal({});
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
    });

    it('does not delete user and sends error', async () => {
      wrapper = mount(() => useUsers(testUser.id));
      mockDeleteUserEndpoint({ code: 404, userId: testUser.id });

      expect(wrapper.user.value).to.deep.equal({});
      expect(wrapper.users.value).to.deep.equal([]);

      await wrapper.deleteUser(testUser.id);
      await flushPromises();

      expect(axiosMock.history.delete.length).to.equal(1);
      expect(axiosMock.history.delete[0].url).to.equal(`${API_HOST}users/${testUser.id}`);
      expect(wrapper.users.value).to.deep.equal([]);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
    });

    it('does not request password reset and sends error', async () => {
      wrapper = mount(() => useUsers());
      mockRequestPasswordResetEndpoint({ code: 403 });

      await wrapper.requestPasswordReset({ email: testUser.email });
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}users/password-reset`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
    });

    it('does not reset password and sends error', async () => {
      wrapper = mount(() => useUsers());
      mockResetPasswordEndpoint({ code: 403 });

      const testPasswordResetBody = {
        userId: 1,
        token: 'mtxouivec9ass8t918mvx',
        email: 'test@test.test',
        password: '1234heslo',
      };

      await wrapper.resetPassword(testPasswordResetBody);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(
        `${API_HOST}users/${testPasswordResetBody.userId}/password-reset/${testPasswordResetBody.token}`,
      );
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
    });
  });

  afterEach(() => {
    wrapper.invalidateUsers();
    wrapper.invalidateOrganizations();
    wrapper.unmount();
  });
});
