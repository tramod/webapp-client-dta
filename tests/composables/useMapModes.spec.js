import useMapModes from '@composables/useMapModes';
import { mount } from '../mountComposable';
import { getStoreMock } from './../helpers.js';
import { flushPromises } from '@vue/test-utils';

let wrapper, testStore;

const testModes = [
  { modelType: 'DTA', dataMode: 'model', expectedMapModeCount: 11 },
  { modelType: 'DTA', dataMode: 'comparison', expectedMapModeCount: 1 },
];

describe('useMapModes.spec.js', () => {
  it('gets relevant map modes for default dataMode and modelType', async () => {
    testStore = getStoreMock();

    for (const { modelType, dataMode, expectedMapModeCount } of testModes) {
      testStore.state.scenarios.model.type = modelType;
      testStore.getters['map/getDataMode'] = dataMode;
      wrapper = mount(useMapModes, { store: testStore });
      await flushPromises();

      const getterModes = wrapper.getMapModes();
      expect(getterModes.length).to.equal(expectedMapModeCount);

      const computedModes = wrapper.mapModes.value;
      expect(computedModes.length).to.equal(expectedMapModeCount);
    }
  });

  it('gets relevant map modes for custom dataMode and modelType', async () => {
    testStore = getStoreMock();
    testStore.state.scenarios.model.type = 'DTA';
    testStore.getters['map/getDataMode'] = 'model';
    wrapper = mount(useMapModes, { store: testStore });
    await flushPromises();

    const mapModes = wrapper.getMapModes('comparison', 'DTA');
    expect(mapModes.length).to.equal(1);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
