import useMainApiRequest from '@composables/useMainApiRequest';
import { mount, globalToastMock, globalStoreMock, globalRouterMock } from '../mountComposable';
import { mockScenarioEndpoint, axiosMock, API_HOST } from './../mockAxios.js';
import { flushPromises } from '@vue/test-utils';

const testResponse = { id: 11, test: 'test' };

let wrapper;

describe('useMainApiRequest.spec.js', () => {
  describe('on good request', () => {
    before(() => mockScenarioEndpoint({ response: testResponse, scenarioId: testResponse.id }));

    beforeEach(async () => {
      wrapper = mount(useMainApiRequest);
      await flushPromises();
    });

    it('controls loading state', async () => {
      await wrapper.makeRequest({
        url: `scenarios/${testResponse.id}`,
        method: 'get',
      });
      await flushPromises();

      expect(globalStoreMock.dispatch.calledTwice).to.equal(true);
      expect(globalStoreMock.dispatch.getCall(-2).args).to.deep.equal(['layout/startLoading']);
      expect(globalStoreMock.dispatch.getCall(-1).args).to.deep.equal(['layout/finishLoading']);
    });

    it('fetches data from server', async () => {
      await wrapper.makeRequest({
        url: `scenarios/${testResponse.id}`,
        method: 'get',
      });
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testResponse.id}`);
    });

    it('performs success action', async () => {
      const successSpy = sinon.spy();
      await wrapper.makeRequest({
        url: `scenarios/${testResponse.id}`,
        method: 'get',
        onSuccess: successSpy,
      });
      await flushPromises();

      expect(successSpy.calledOnce).to.equal(true);
    });

    it('displays no message if not set', async () => {
      await wrapper.makeRequest({
        url: `scenarios/${testResponse.id}`,
        method: 'get',
      });
      await flushPromises();

      expect(globalToastMock.add.notCalled).to.equal(true);
    });

    it('displays success message if set', async () => {
      const testMessage = 'asdf';
      await wrapper.makeRequest({
        url: `scenarios/${testResponse.id}`,
        method: 'get',
        message: { success: testMessage },
      });
      await flushPromises();

      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('success');
    });
  });

  describe('on bad request', () => {
    before(() => {
      mockScenarioEndpoint({ code: 404, scenarioId: testResponse.id });
      wrapper = mount(useMainApiRequest);
    });

    it('displays error message', async () => {
      const successSpy = sinon.spy();
      await wrapper.makeRequest({
        url: `scenarios/${testResponse.id}`,
        method: 'get',
        message: {
          error: 'test error',
        },
        onSuccess: (result) => successSpy(result),
      });
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testResponse.id}`);
      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('error');
      expect(successSpy.notCalled).to.equal(true);
    });

    it('does not perform success action', async () => {
      const successSpy = sinon.spy();
      await wrapper.makeRequest({
        url: `scenarios/${testResponse.id}`,
        method: 'get',
        onSuccess: (result) => successSpy(result),
      });
      await flushPromises();

      expect(successSpy.notCalled).to.equal(true);
    });

    it('logs out after 401', async () => {
      mockScenarioEndpoint({ code: 401, scenarioId: testResponse.id });
      wrapper = mount(useMainApiRequest);

      await wrapper.makeRequest({
        url: `scenarios/${testResponse.id}`,
        method: 'get',
      });
      await flushPromises();

      expect(globalToastMock.add.lastCall.args[0].severity).to.deep.equal('info');
      expect(globalRouterMock.push.calledOnce).to.equal(true);
      expect(globalRouterMock.push.lastCall.args).to.deep.equal([{ name: 'user.login' }]);
    });
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
