import useWidgets from '@composables/useWidgets';
import { mount } from '../mountComposable';
import { getStoreMock } from './../helpers.js';
import { flushPromises } from '@vue/test-utils';
import { MP_LINK_NODES as interactionMode } from '@keys/index';

let wrapper, testStore;

const testWidgets = [
  { modelType: 'DTA', dataMode: 'model', expWidCountDesktop: 6, expWidCountMobile: 6, expWidCountInteraction: 0 },
  { modelType: 'DTA', dataMode: 'comparison', expWidCountDesktop: 3, expWidCountMobile: 3, expWidCountInteraction: 1 },
];

describe('useWidgets.spec.js', () => {
  it('gets relevant widgets for current dataMode and modelType for DESKTOP layout', async () => {
    testStore = getStoreMock();

    for (const { modelType, dataMode, expWidCountDesktop } of testWidgets) {
      testStore.state.scenarios.model.type = modelType;
      testStore.getters['map/getDataMode'] = dataMode;
      wrapper = mount(useWidgets, { store: testStore });
      await flushPromises();

      const widgets = wrapper.widgets.value;
      expect(widgets.length).to.equal(expWidCountDesktop);

      await flushPromises();
    }
  });

  it('gets relevant widgets for current dataMode and modelType for MOBILE layout', async () => {
    testStore = getStoreMock();

    for (const { modelType, dataMode, expWidCountMobile } of testWidgets) {
      testStore.state.scenarios.model.type = modelType;
      testStore.getters['map/getDataMode'] = dataMode;
      wrapper = mount(useWidgets, { store: testStore, mobile: () => true });
      await flushPromises();

      const widgets = wrapper.widgets.value;
      expect(widgets.length).to.equal(expWidCountMobile);

      await flushPromises();
    }
  });

  it('gets relevant widgets for current dataMode and modelType on map INTERACTION', async () => {
    testStore = getStoreMock();

    for (const { modelType, dataMode, expWidCountInteraction } of testWidgets) {
      testStore.state.map.mapMode = interactionMode;
      testStore.state.scenarios.model.type = modelType;
      testStore.getters['map/getDataMode'] = dataMode;
      wrapper = mount(useWidgets, { store: testStore });
      await flushPromises();

      const widgets = wrapper.widgets.value;
      expect(widgets.length).to.equal(expWidCountInteraction);

      await flushPromises();
    }
  });

  it('filters active only widgets', async () => {
    testStore = getStoreMock();
    testStore.state.scenarios.model.type = 'DTA';
    testStore.getters['map/getDataMode'] = 'model';
    testStore.getters['map/getWidgets'] = () => ['legend'];
    wrapper = mount(useWidgets, { store: testStore });
    await flushPromises();

    const widgets = wrapper.activeWidgets.value;
    expect(widgets.length).to.equal(1);

    await flushPromises();
  });

  it('sorts active widgets', async () => {
    testStore = getStoreMock();
    testStore.state.scenarios.model.type = 'DTA';
    testStore.getters['map/getDataMode'] = 'model';
    testStore.getters['map/getWidgets'] = () => ['calendar', 'legend'];
    wrapper = mount(useWidgets, { store: testStore });
    await flushPromises();

    const widgets = wrapper.activeWidgets.value;
    expect(widgets.length).to.equal(2);
    expect(widgets[0].name).to.equal('legend');

    await flushPromises();
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
