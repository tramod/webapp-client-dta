import useExtraFeatures from '@composables/useExtraFeatures';
import useLayerSource from '@composables/useLayerSourceData';
import { LID_DTA_LINKS } from '@keys/index';
import { mount } from '../mountComposable';
import { getStoreMock } from './../helpers.js';
import { computed } from 'vue';
import { mockModelLinksSourceEndpoint } from './../mockAxios.js';

const testModifications = [
  {
    id: 1,
    dateFrom: '2020-02-20T00:00:00.000Z',
    dateTo: '2022-03-20T00:00:00.000Z',
    type: 'link',
    mode: 'new',
    speed: 100,
    capacity: 1000,
    coordinates: [
      [1, 1],
      [2, 2],
    ],
  },
  {
    id: 2,
    dateFrom: '2021-02-20T00:00:00.000Z',
    dateTo: '2022-03-20T00:00:00.000Z',
    type: 'link',
    mode: 'new',
    speed: 50,
    capacity: 500,
    coordinates: [
      [2, 2],
      [3, 3],
    ],
  },
  {
    id: 3,
    dateFrom: '2021-02-20T00:00:00.000Z',
    dateTo: '2022-03-20T00:00:00.000Z',
    type: 'link',
    mode: 'existing', // should be excluded from extra features
  },
];
const testExtraModifications = [testModifications[0], testModifications[1]];
const testExtraModifications2 = [
  {
    id: 10,
    dateFrom: '2021-02-20T00:00:00.000Z',
    dateTo: '2022-03-20T00:00:00.000Z',
    type: 'link',
    mode: 'new',
    speed: 50,
    capacity: 500,
    coordinates: [
      [2, 2],
      [3, 3],
    ],
  },
  {
    id: 11,
    dateFrom: '2021-02-20T00:00:00.000Z',
    dateTo: '2022-03-20T00:00:00.000Z',
    type: 'link',
    mode: 'new',
    speed: 50,
    capacity: 500,
    coordinates: [
      [2, 2],
      [3, 3],
    ],
  },
];
const testActiveScenario = computed(() => ({
  id: 1,
  modifications: testModifications,
}));
const testCalendarDate = '2020-10-20T00:00:00.000Z';

let wrapper;

describe('useExtraFeatures.spec.js', () => {
  beforeEach(() => {
    const testStore = getStoreMock();
    testStore.getters['map/getCalendarDate'] = () => testCalendarDate;
    wrapper = mount(() => useExtraFeatures({ activeScenario: testActiveScenario }), { store: testStore });
  });

  it('parses active scenario modifications', () => {
    expect(wrapper.activeScenarioModifications.value).to.deep.equal(testExtraModifications);
  });

  it('parses extra features from active scenario modifications', () => {
    expect(wrapper.extraFeatures.value.generator).to.deep.equal([]);
    expect(wrapper.extraFeatures.value.node).to.deep.equal([]);
    expect(wrapper.extraFeatures.value.link.length).to.equal(testExtraModifications.length);
    testExtraModifications.forEach((mod, i) => {
      expect(wrapper.extraFeatures.value.link[i].geometry).to.deep.equal({
        coordinates: mod.coordinates,
      });
      expect(wrapper.extraFeatures.value.link[i].properties).to.deep.equal({
        capacity: mod.capacity,
        edge_id: `extra-${mod.id}`,
        id: `extra-${mod.id}`,
        isvalid: true,
        modification: mod,
        from_node_id: undefined,
        free_speed: mod.speed,
        critical_speed: mod.speed / 1.2,
        to_node_id: undefined,
        lanes: undefined,
      });
    });
  });

  it('exports calendarDate filtered extra features', () => {
    const dateFilteredMod = testModifications[0];

    expect(wrapper.dateFilteredExtraFeatures.value.generator).to.deep.equal([]);
    expect(wrapper.dateFilteredExtraFeatures.value.node).to.deep.equal([]);
    expect(wrapper.dateFilteredExtraFeatures.value.link.length).to.equal(1);
    expect(wrapper.extraFeatures.value.link[0].geometry).to.deep.equal({
      coordinates: dateFilteredMod.coordinates,
    });
    expect(wrapper.extraFeatures.value.link[0].properties).to.deep.equal({
      capacity: dateFilteredMod.capacity,
      edge_id: `extra-${dateFilteredMod.id}`,
      id: `extra-${dateFilteredMod.id}`,
      isvalid: true,
      modification: dateFilteredMod,
      from_node_id: undefined,
      free_speed: dateFilteredMod.speed,
      critical_speed: dateFilteredMod.speed / 1.2,
      to_node_id: undefined,
      lanes: undefined,
    });
  });

  it('parses extra features for both compared scenarios and applies them on layer source', () => {
    const nonFilteringDate = '2021-10-20T00:00:00.000Z';
    mockModelLinksSourceEndpoint({ response: {} }); // mock needed for the useLayerSourceData composable
    const lsWrapper = mount(() => useLayerSource(LID_DTA_LINKS));

    wrapper.setModelDiffExtraFeatures({
      sourceOptions: { modifications: testExtraModifications, date: nonFilteringDate },
      targetOptions: { modifications: testExtraModifications2, date: nonFilteringDate },
    });

    const extraModifications = [...testExtraModifications, ...testExtraModifications2];

    expect(lsWrapper.extraFeaturesData.value.length).to.equal(4);
    extraModifications.forEach((mod, i) => {
      expect(lsWrapper.extraFeaturesData.value[i].geometry).to.deep.equal({
        coordinates: mod.coordinates,
      });
      expect(lsWrapper.extraFeaturesData.value[i].properties).to.deep.equal({
        capacity: mod.capacity,
        edge_id: `extra-${mod.id}`,
        id: `extra-${mod.id}`,
        isvalid: true,
        modification: mod,
        from_node_id: undefined,
        free_speed: mod.speed,
        critical_speed: mod.speed / 1.2,
        to_node_id: undefined,
        lanes: undefined,
      });
    });

    lsWrapper.unmount();
  });

  it('applies nothing with no extra modifications (base model)', () => {
    const nonFilteringDate = '2021-10-20T00:00:00.000Z';
    mockModelLinksSourceEndpoint({ response: {} }); // mock needed for the useLayerSourceData composable
    const lsWrapper = mount(() => useLayerSource(LID_DTA_LINKS));

    wrapper.setModelDiffExtraFeatures({
      sourceOptions: { modifications: [], date: nonFilteringDate },
      targetOptions: { modifications: [], date: nonFilteringDate },
    });

    expect(lsWrapper.extraFeaturesData.value.length).to.equal(0);

    lsWrapper.unmount();
  });

  it('merges and filter mods when one scenario diff is used', () => {
    const nonFilteringDate = '2021-10-20T00:00:00.000Z';
    mockModelLinksSourceEndpoint({ response: {} }); // mock needed for the useLayerSourceData composable
    const lsWrapper = mount(() => useLayerSource(LID_DTA_LINKS));

    wrapper.setModelDiffExtraFeatures({
      sourceOptions: { modifications: testExtraModifications, date: nonFilteringDate },
      targetOptions: { modifications: testExtraModifications, date: nonFilteringDate },
    });

    expect(lsWrapper.extraFeaturesData.value.length).to.equal(2);
    testExtraModifications.forEach((mod, i) => {
      expect(lsWrapper.extraFeaturesData.value[i].geometry).to.deep.equal({
        coordinates: mod.coordinates,
      });
      expect(lsWrapper.extraFeaturesData.value[i].properties).to.deep.equal({
        capacity: mod.capacity,
        edge_id: `extra-${mod.id}`,
        id: `extra-${mod.id}`,
        isvalid: true,
        modification: mod,
        from_node_id: undefined,
        free_speed: mod.speed,
        critical_speed: mod.speed / 1.2,
        to_node_id: undefined,
        lanes: undefined,
      });
    });

    lsWrapper.unmount();
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
