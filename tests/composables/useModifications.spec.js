import useModifications from '@composables/useModifications';
import { mount, globalToastMock, globalStoreMock } from '../mountComposable';
import {
  mockModificationEndpoint,
  mockModificationsEndpoint,
  mockCreateModificationEndpoint,
  mockUpdateModificationEndpoint,
  mockDeleteModificationEndpoint,
  axiosMock,
  API_HOST,
} from './../mockAxios.js';
import { flushPromises } from '@vue/test-utils';

const testScenario = { id: 1 };
const testEvent = { id: 1 };
const testModifications = [
  {
    id: 1,
    dateFrom: '2020-02-20T00:00:00.000Z',
    dateTo: '2022-03-20T00:00:00.000Z',
    type: 'link',
    speed: 100,
    capacity: 1000,
  },
  {
    id: 2,
    dateFrom: '2020-02-20T00:00:00.000Z',
    dateTo: '2022-03-20T00:00:00.000Z',
    type: 'link',
    speed: 50,
    capacity: 500,
  },
];
const testModification = testModifications[0];

let wrapper, cacheModification, invalidateModification, invalidateNodeTransit;

describe('useModifications.spec.js', () => {
  beforeEach(() => {
    cacheModification = sinon.spy();
    invalidateModification = sinon.spy();
    invalidateNodeTransit = sinon.spy();
    wrapper = mount(() =>
      useModifications(
        { scenarioId: testScenario.id, eventId: testEvent.id },
        {
          cacheModification,
          invalidateModification,
          invalidateNodeTransit,
        },
      ),
    );
  });

  describe('on good requests', () => {
    it('fetches & caches modification', async () => {
      mockModificationEndpoint({
        response: { modification: testModification },
        scenarioId: testScenario.id,
        eventId: testEvent.id,
        modificationId: testModification.id,
      });

      await wrapper.fetchModification(testModification.id);
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(
        `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}/modifications/${testModification.id}`,
      );
      expect(globalToastMock.add.notCalled).to.equal(true);
      expect(cacheModification.calledOnce).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(cacheModification.calledWith(testModification)).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('fetches & caches modifications', async () => {
      mockModificationsEndpoint({
        response: { modifications: testModifications },
        scenarioId: testScenario.id,
        eventId: testEvent.id,
      });

      await wrapper.fetchModifications();
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(
        `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}/modifications`,
      );
      expect(globalToastMock.add.notCalled).to.equal(true);
      expect(cacheModification.calledOnce).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(cacheModification.calledWith(testModifications)).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('creates & caches new modification', async () => {
      mockCreateModificationEndpoint({
        response: { modification: testModification, editSession: {} },
        scenarioId: testScenario.id,
        eventId: testEvent.id,
      });

      const newModification = {
        name: 'test ev',
        description: 'test desc',
      };

      await wrapper.createModification(newModification);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(
        `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}/modifications`,
      );
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
      expect(cacheModification.calledOnce).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(cacheModification.calledWith(testModification)).to.equal(true);
      expect(globalStoreMock.dispatch.getCall(-1).args[0]).to.equal('scenarios/updateScenarioProgress');
    });

    it('updates & caches modification', async () => {
      mockUpdateModificationEndpoint({
        response: { modification: testModification, editSession: {} },
        scenarioId: testScenario.id,
        eventId: testEvent.id,
        modificationId: testModification.id,
      });

      const updatedModification = {
        ...testModification,
        name: 'test sc',
        description: 'test desc',
      };

      await wrapper.updateModification(updatedModification);
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(axiosMock.history.patch[0].url).to.equal(
        `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}/modifications/${testModification.id}`,
      );
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
      expect(cacheModification.calledOnce).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(cacheModification.calledWith(testModification)).to.equal(true);
      expect(globalStoreMock.dispatch.getCall(-1).args[0]).to.equal('scenarios/updateScenarioProgress');
    });

    it('deletes & invalidates modification', async () => {
      mockDeleteModificationEndpoint({
        response: {
          deletedModifications: [{ ...testModification, evId: testEvent.id, modId: testModification.id }],
          editSession: {},
        },
        scenarioId: testScenario.id,
        eventId: testEvent.id,
        modificationId: testModification.id,
      });

      await wrapper.deleteModification(testModification.id);
      await flushPromises();

      expect(axiosMock.history.delete.length).to.equal(1);
      expect(axiosMock.history.delete[0].url).to.equal(
        `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}/modifications/${testModification.id}`,
      );
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
      expect(cacheModification.notCalled).to.equal(true);
      expect(invalidateModification.calledOnce).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(
        invalidateModification.calledWith({
          scenarioId: testScenario.id,
          eventId: testEvent.id,
          modificationId: testModification.id,
        }),
      ).to.equal(true);
      expect(globalStoreMock.dispatch.getCall(-1).args[0]).to.equal('scenarios/updateScenarioProgress');
    });
  });

  describe('on bad requests', () => {
    it('returns no modification and sends error', async () => {
      mockModificationEndpoint({
        code: 404,
        scenarioId: testScenario.id,
        eventId: testEvent.id,
        modificationId: testModification.id,
      });

      await wrapper.fetchModification(testModification.id);
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(
        `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}/modifications/${testModification.id}`,
      );
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(cacheModification.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('returns no modifications and sends info', async () => {
      mockModificationsEndpoint({ code: 404, scenarioId: testScenario.id, eventId: testEvent.id });

      await wrapper.fetchModifications();
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(
        `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}/modifications`,
      );
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('info');
      expect(cacheModification.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('does not create modification and sends error', async () => {
      mockCreateModificationEndpoint({ code: 404, scenarioId: testScenario.id, eventId: testEvent.id });

      const newModification = {
        name: 'test sc',
        description: 'test desc',
      };

      await wrapper.createModification(newModification);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(
        `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}/modifications`,
      );
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(cacheModification.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('does not update modification and sends error', async () => {
      mockUpdateModificationEndpoint({
        code: 404,
        scenarioId: testScenario.id,
        eventId: testEvent.id,
        modificationId: testModification.id,
      });

      const updatedModification = {
        ...testModification,
        name: 'test sc',
        description: 'test desc',
      };

      await wrapper.updateModification(updatedModification);
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(axiosMock.history.patch[0].url).to.equal(
        `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}/modifications/${testModification.id}`,
      );
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(cacheModification.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('does not delete modification and sends error', async () => {
      mockDeleteModificationEndpoint({
        code: 404,
        scenarioId: testScenario.id,
        eventId: testEvent.id,
        modificationId: testModification.id,
      });

      await wrapper.deleteModification(testModification.id);
      await flushPromises();

      expect(axiosMock.history.delete.length).to.equal(1);
      expect(axiosMock.history.delete[0].url).to.equal(
        `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}/modifications/${testModification.id}`,
      );
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(cacheModification.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });
  });

  afterEach(() => wrapper.unmount());
});
