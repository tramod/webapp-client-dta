import useFullScenario from '@composables/useFullScenario';
import { mount } from '../mountComposable';
import { getStoreMock } from './../helpers.js';
import {
  mockScenarioEndpoint,
  mockEventEndpoint,
  mockModificationEndpoint,
  mockScenarioEndpointAsPromise,
  axiosMock,
  API_HOST,
} from './../mockAxios.js';
import { flushPromises } from '@vue/test-utils';
import { reactive } from 'vue';

const testModification = {
  id: 1,
  dateFrom: '2020-02-20T00:00:00.000Z',
  dateTo: '2022-03-20T00:00:00.000Z',
  type: 'link',
  speed: 100,
  capacity: 1000,
};

const testEvent = {
  id: 2,
  name: 'Aktualni udalost',
  description: 'popis',
  dateFrom: '2020-02-20T00:00:00.000Z',
  dateTo: '2022-03-20T00:00:00.000Z',
  authorUserId: 1,
  included: true,
  modifications: [testModification],
};

const testScenario = {
  id: 1,
  name: 'Soukromy scenario',
  type: 'private',
  dateFrom: '2021-02-18T09:05:58.731Z',
  dateTo: '2021-02-18T09:05:58.731Z',
  authorUserId: 1,
  events: [testEvent],
};

const testItemIds = { scenarioId: testScenario.id, eventId: testEvent.id, modificationId: testModification.id };

let wrapper, testStore;

describe('useFullScenario.spec.js', () => {
  beforeEach(() => {
    testStore = getStoreMock();
  });

  it('fetches only once with multiple simultaneous requests', async () => {
    mockScenarioEndpointAsPromise({ response: { scenario: testScenario }, scenarioId: testScenario.id });
    testStore.state.scenarios.cache.scenarios.enabled = false;
    wrapper = mount(() => useFullScenario(testItemIds), { store: testStore });
    await flushPromises();

    wrapper.fetchScenario(testScenario.id);
    wrapper.fetchScenario(testScenario.id);
    wrapper.fetchScenario(testScenario.id);
    await wrapper.fetchScenario(testScenario.id);

    await flushPromises();

    expect(axiosMock.history.get.length).to.equal(1);
    expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
  });

  it('utilizes cache', async () => {
    mockScenarioEndpoint({ response: { scenario: testScenario }, scenarioId: testScenario.id });
    testStore.state.scenarios.cache.scenarios.enabled = true;
    wrapper = mount(() => useFullScenario(testItemIds), { store: testStore });
    await flushPromises();

    await wrapper.fetchScenario(testScenario.id);
    await flushPromises();

    await wrapper.fetchScenario(testScenario.id);
    await flushPromises();

    expect(axiosMock.history.get.length).to.equal(1);
    expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
  });

  it('computes active scenario', async () => {
    mockScenarioEndpoint({ response: { scenario: testScenario }, scenarioId: testScenario.id });
    testStore.getters['map/getActiveItemId'] = () => testScenario.id;
    wrapper = mount(() => useFullScenario(testItemIds), { store: testStore });
    await flushPromises();

    const eventedModification = { ...testModification, eventId: parseInt(testEvent.id) }; // fullScenario mod contains eventId
    const expectedActiveScenario = {
      ...testScenario,
      events: [{ ...testEvent, modifications: [eventedModification] }],
      modifications: [eventedModification],
    };

    expect(wrapper.activeScenarioId.value).to.equal(testScenario.id);
    expect(wrapper.activeScenario.value).to.deep.equal(expectedActiveScenario);
  });

  it('computes active event', async () => {
    mockScenarioEndpoint({ response: { scenario: testScenario }, scenarioId: testScenario.id });
    testStore.getters['map/getActiveItemId'] = ({ itemLevel }) => {
      switch (itemLevel) {
        case 'scenario':
          return testScenario.id;
        case 'event':
          return testEvent.id;
      }
    };
    wrapper = mount(() => useFullScenario(testItemIds), { store: testStore });
    await flushPromises();

    const eventedModification = { ...testModification, eventId: parseInt(testEvent.id) };
    const expectedActiveEvent = { ...testEvent, modifications: [eventedModification] };

    expect(wrapper.activeEventId.value).to.equal(testEvent.id);
    expect(wrapper.activeEvent.value).to.deep.equal(expectedActiveEvent);
  });

  it('computes active modification', async () => {
    mockScenarioEndpoint({ response: { scenario: testScenario }, scenarioId: testScenario.id });
    testStore.getters['map/getActiveItemId'] = ({ itemLevel }) => {
      switch (itemLevel) {
        case 'scenario':
          return testScenario.id;
        case 'event':
          return testEvent.id;
        case 'modification':
          return testModification.id;
      }
    };
    wrapper = mount(() => useFullScenario(testItemIds), { store: testStore });
    await flushPromises();

    const expectedModification = { ...testModification, eventId: parseInt(testEvent.id) };

    expect(wrapper.activeModificationId.value).to.equal(testModification.id);
    expect(wrapper.activeModification.value).to.deep.equal(expectedModification);
  });

  it('computes independent models', async () => {
    const reactiveItemIds = { scenarioId: testScenario.id, eventId: testEvent.id, modificationId: testModification.id };

    mockScenarioEndpoint({ response: { scenario: testScenario }, scenarioId: testScenario.id });
    testStore.getters['map/getActiveItemId'] = () => testScenario.id;
    wrapper = mount(() => useFullScenario(reactiveItemIds), { store: testStore });
    await flushPromises();

    const expectedScenario = { ...testScenario };
    delete expectedScenario.events;

    const expectedEvent = { ...testEvent };
    delete expectedEvent.modifications;

    const expectedModification = { ...testModification, eventId: parseInt(testEvent.id) };

    expect(wrapper.scenario.value).to.deep.equal(expectedScenario);
    expect(wrapper.event.value).to.deep.equal(expectedEvent);
    expect(wrapper.modification.value).to.deep.equal(expectedModification);
  });

  it('accepts reactive item IDs passed to child composables', async () => {
    const reactiveItemIds = reactive({
      scenarioId: testScenario.id,
      eventId: testEvent.id,
      modificationId: testModification.id,
    });
    mockScenarioEndpoint({ response: { scenario: testScenario }, scenarioId: testScenario.id });
    mockEventEndpoint({ response: { event: testEvent }, scenarioId: testScenario.id, eventId: testEvent.id });
    mockModificationEndpoint({
      response: { modification: testModification },
      scenarioId: testScenario.id,
      eventId: testEvent.id,
      modificationId: testModification.id,
    });

    testStore.getters['map/getActiveItemId'] = () => testScenario.id;
    wrapper = mount(() => useFullScenario(reactiveItemIds), { store: testStore });
    await flushPromises();

    await wrapper.fetchScenario();
    expect(axiosMock.history.get.length).to.equal(2);
    expect(axiosMock.history.get[1].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);

    await wrapper.fetchEvent(testEvent.id);
    expect(axiosMock.history.get.length).to.equal(3);
    expect(axiosMock.history.get[2].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}`);

    await wrapper.fetchModification(testModification.id);
    expect(axiosMock.history.get.length).to.equal(4);
    expect(axiosMock.history.get[3].url).to.equal(
      `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}/modifications/${testModification.id}`,
    );

    const changedScenarioId = '10';
    const changedEventId = '20';
    const changedModificationId = '30';
    reactiveItemIds.scenarioId = changedScenarioId;
    reactiveItemIds.eventId = changedEventId;
    reactiveItemIds.modificationId = changedModificationId;

    await wrapper.fetchScenario();
    expect(axiosMock.history.get.length).to.equal(5);
    expect(axiosMock.history.get[4].url).to.equal(`${API_HOST}scenarios/${changedScenarioId}`);

    await wrapper.fetchEvent(changedEventId);
    expect(axiosMock.history.get.length).to.equal(6);
    expect(axiosMock.history.get[5].url).to.equal(`${API_HOST}scenarios/${changedScenarioId}/events/${changedEventId}`);

    await wrapper.fetchModification(changedModificationId);
    expect(axiosMock.history.get.length).to.equal(7);
    expect(axiosMock.history.get[6].url).to.equal(
      `${API_HOST}scenarios/${changedScenarioId}/events/${changedEventId}/modifications/${changedModificationId}`,
    );
  });

  afterEach(() => {
    wrapper.invalidateFullScenario(testScenario.id);
    wrapper.unmount();
  });
});
