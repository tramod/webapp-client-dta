import useTrafficData, { useModelTrafficData } from '@composables/useTrafficData';
import { mount } from '../mountComposable';
import {
  mockFutureTrafficDataEndpoint,
  axiosMock,
  API_MODEL_DTA,
  MODEL_NAME_DTA,
  TRAFFIC_MODEL_DEFAULT_CODE,
} from './../mockAxios.js';
import { expect } from 'chai';

let wrapper;

const exampleBaseModelResponse = {
  cacheName: TRAFFIC_MODEL_DEFAULT_CODE,
  result: {
    result: [
      { traffic: 0.0, edgeId: 1 },
      { traffic: 10.0, edgeId: 2 },
      { traffic: 20.0, edgeId: 3 },
    ],
    modelInfo: {},
  },
};
const exampleBaseModelParsedData = {
  1: { traffic: 0.0, edgeId: 1 },
  2: { traffic: 10.0, edgeId: 2 },
  3: { traffic: 20.0, edgeId: 3 },
  modelInfo: {
    numOfTimeIntervals: 12,
  },
};

describe('useTrafficData.spec.js', () => {
  describe('faulty initialization', () => {
    it('requires dataKey to be provided', async () => {
      expect(() => useTrafficData()).to.throw('Map mode key not provided');
    });

    it('requires supported dataKey to be provided', async () => {
      const notSupportedKey = 'notSupported';
      expect(() => useTrafficData(notSupportedKey)).to.throw(
        `Data for map mode key ${notSupportedKey} are not supported`,
      );
    });
  });

  describe('successfully initialization', () => {
    describe('basic traffic data fetching', () => {
      it('fetches required traffic data (modelDTA)', async () => {
        wrapper = mount(() => useTrafficData('modelDTA'));

        mockFutureTrafficDataEndpoint({ response: exampleBaseModelResponse });
        await wrapper.fetchTrafficData();
        expect(axiosMock.history.get[0].url).to.equal(
          `${API_MODEL_DTA}/caches/${MODEL_NAME_DTA}/${TRAFFIC_MODEL_DEFAULT_CODE}`,
        );
      });

      it('parses fetched data by type parser (modelDTA) and expose it on traffic property', async () => {
        wrapper = mount(() => useTrafficData('modelDTA'));
        mockFutureTrafficDataEndpoint({ response: exampleBaseModelResponse });
        await wrapper.fetchTrafficData();
        expect(wrapper.trafficData.value).to.deep.equal(exampleBaseModelParsedData);
      });
    });

    describe('custom data save key', () => {
      it('allows to save data fetch under custom key not exposed on trafficData property', async () => {
        wrapper = mount(() => useTrafficData('modelDTA'));
        mockFutureTrafficDataEndpoint({ response: exampleBaseModelResponse });
        wrapper.clearTrafficData();
        await wrapper.fetchTrafficData({ cacheKey: 'customKey' });
        expect(wrapper.trafficData.value).to.be.undefined;
      });

      it('access both non/custom key data with getTrafficDataByKey', async () => {
        wrapper = mount(() => useTrafficData('modelDTA'));
        mockFutureTrafficDataEndpoint({ response: exampleBaseModelResponse });
        wrapper.clearTrafficData();
        await wrapper.fetchTrafficData();
        await wrapper.fetchTrafficData({ cacheKey: 'customKey' });
        expect(wrapper.getTrafficDataByKey('modelDTA')).to.deep.equal(exampleBaseModelParsedData);
        expect(wrapper.getTrafficDataByKey('customKey')).to.deep.equal(exampleBaseModelParsedData);
      });
    });

    describe('areTrafficDataAvailable', () => {
      it('method return data availability (loaded with at least one item)', async () => {
        wrapper = mount(() => useTrafficData('modelDTA'));
        wrapper.clearTrafficData();
        expect(wrapper.areTrafficDataAvailable()).to.equal(false);

        mockFutureTrafficDataEndpoint({
          response: {
            result: {
              result: [],
            },
          },
        });
        await wrapper.fetchTrafficData();
        expect(wrapper.areTrafficDataAvailable()).to.equal(false);

        mockFutureTrafficDataEndpoint({ response: exampleBaseModelResponse });
        await wrapper.fetchTrafficData();
        expect(wrapper.areTrafficDataAvailable()).to.equal(true);
      });
    });

    afterEach(() => {
      wrapper.unmount();
    });
  });
});

describe('useModelTrafficData (DTA)', () => {
  describe('basic traffic data fetching', () => {
    it('fetches required traffic data (model), base use under hood', async () => {
      wrapper = mount(() => useModelTrafficData('DTA'));

      mockFutureTrafficDataEndpoint({ response: exampleBaseModelResponse });
      await wrapper.fetchTrafficData();
      expect(axiosMock.history.get[0].url).to.equal(
        `${API_MODEL_DTA}/caches/${MODEL_NAME_DTA}/${TRAFFIC_MODEL_DEFAULT_CODE}`,
      );
    });
  });

  describe('Codelist getTrafficDataByKey handling', () => {
    it('return original traffic data when empty codelist provided', async () => {
      wrapper = mount(() => useModelTrafficData('DTA'));
      mockFutureTrafficDataEndpoint({ response: exampleBaseModelResponse });
      await wrapper.fetchTrafficData();

      const dataReturn = wrapper.getTrafficDataByKey();
      expect(dataReturn).to.deep.equal(exampleBaseModelParsedData);
    });

    it('return codelisted data with different codelist for same cache', async () => {
      wrapper = mount(() => useModelTrafficData('DTA'));
      mockFutureTrafficDataEndpoint({ response: exampleBaseModelResponse });
      await wrapper.fetchTrafficData();

      const codeList = { 3: 'extra-first' };
      const dataReturn = wrapper.getTrafficDataByKey({ codeList });
      const expectedAddon = {
        'extra-first': {
          edgeId: 3,
          traffic: 20,
        },
        modelInfo: {
          numOfTimeIntervals: 12,
        },
      };
      expect(dataReturn).to.deep.equal({ ...exampleBaseModelParsedData, ...expectedAddon });

      const otherCodeList = { 3: 'extra-next' };
      const dataReturn2 = wrapper.getTrafficDataByKey({ codeList: otherCodeList });
      const expectedAddonNext = {
        'extra-next': {
          edgeId: 3,
          traffic: 20,
        },
        modelInfo: {
          numOfTimeIntervals: 12,
        },
      };
      expect(dataReturn2).to.deep.equal({ ...exampleBaseModelParsedData, ...expectedAddonNext });
    });
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
