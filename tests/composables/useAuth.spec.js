import useAuth from '@composables/useAuth';
import { mount, globalToastMock, globalStoreMock, globalRouterMock } from '../mountComposable';
import { mockLoginEndpoint, mockLogoutEndpoint, mockVerifyUserEndpoint, axiosMock, API_HOST } from './../mockAxios.js';
import { flushPromises } from '@vue/test-utils';

const testUsers = [
  {
    id: 1,
    username: 'franta',
    email: 'franta@franta.com',
    roles: [100, 1],
    organization: 'roadtwin',
  },
  {
    id: 2,
    username: 'pepa',
    email: 'pepa@pepa.com',
    roles: [],
    organization: null,
  },
  {
    id: 3,
    username: 'lojza',
    email: 'lojza@lojza.com',
    roles: [],
    organization: null,
  },
];

const testUser = testUsers[0];

const testCredentials = {
  username: 'franta',
  password: '1234heslo',
};

let wrapper;

describe('useAuth.spec.js', () => {
  describe('on good requests', () => {
    it('logs user in, sets user state', async () => {
      wrapper = mount(() => useAuth());
      mockLoginEndpoint({ response: testUser });

      await wrapper.login(testCredentials);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}auth/login`);
      expect(axiosMock.history.post[0].data).to.deep.equal(
        JSON.stringify({ username: testCredentials.username, password: testCredentials.password }),
      );
      expect(globalStoreMock.dispatch.getCall(-1).args).to.deep.equal(['auth/login', testUser]);
      expect(globalRouterMock.push.calledOnce).to.equal(true);
      expect(globalRouterMock.push.lastCall.args).to.deep.equal([{ name: 'user' }]);

      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('info');
    });

    it('logs user out, purges user state', async () => {
      wrapper = mount(() => useAuth());
      mockLogoutEndpoint();

      await wrapper.logout();
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}auth/logout`);

      expect(globalStoreMock.dispatch.getCall(-1).args).to.deep.equal(['auth/logout']);
      expect(globalRouterMock.push.calledOnce).to.equal(true);
      expect(globalRouterMock.push.lastCall.args).to.deep.equal([{ name: 'user.login' }]);

      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('info');
    });

    it('verifies user data, sets user state', async () => {
      wrapper = mount(() => useAuth());
      mockVerifyUserEndpoint({ response: testUser });

      await wrapper.verify();
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}auth/user`);

      expect(globalStoreMock.commit.calledOnce).to.equal(false);
      // expect(globalStoreMock.commit.getCall(-1).args).to.deep.equal(['auth/SET_AUTH', testUser]);
    });
  });

  describe('on bad requests', () => {
    it('displays error on bad login', async () => {
      wrapper = mount(() => useAuth());
      mockLoginEndpoint({ code: 401 });

      await wrapper.login(testCredentials);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}auth/login`);

      expect(globalStoreMock.commit.notCalled).to.equal(true);
      expect(globalRouterMock.push.notCalled).to.equal(true);

      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
    });

    it('displays error on bad logout', async () => {
      wrapper = mount(() => useAuth());
      mockLogoutEndpoint({ code: 403 });

      await wrapper.logout();
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}auth/logout`);

      expect(globalStoreMock.commit.notCalled).to.equal(true);
      expect(globalRouterMock.push.notCalled).to.equal(true);

      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
    });

    it('purges user data on failed verification', async () => {
      wrapper = mount(() => useAuth());
      mockVerifyUserEndpoint({ code: 401 });

      await wrapper.verify();
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}auth/user`);

      expect(globalStoreMock.commit.calledOnce).to.equal(true);
      expect(globalStoreMock.commit.getCall(-1).args).to.deep.equal(['auth/PURGE_AUTH']);
    });
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
