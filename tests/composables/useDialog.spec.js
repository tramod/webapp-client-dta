import useDialog from '@composables/useDialog';
import { mount } from '../mountComposable';
import { flushPromises } from '@vue/test-utils';

let wrapper;

describe('useDialog.spec.js', () => {
  beforeEach(async () => {
    wrapper = await mount(useDialog);
    await flushPromises();
  });

  it('is not displayed by default', () => {
    expect(wrapper.displayed.value).to.equal(false);
  });

  it('can change displayed state', () => {
    wrapper.show({});
    expect(wrapper.displayed.value).to.equal(true);

    wrapper.close();
    expect(wrapper.displayed.value).to.equal(false);
  });

  it('can change dialog content on show', () => {
    const testContent = { test: 'test' };

    wrapper.show(testContent);
    expect(wrapper.displayed.value).to.equal(true);
    expect(wrapper.dialog.value).to.deep.equal(testContent);
    wrapper.close();
  });

  it('preserves displayed state through mount and unmount', () => {
    wrapper.show({});
    expect(wrapper.displayed.value).to.equal(true);

    wrapper = mount(useDialog);

    expect(wrapper.displayed.value).to.equal(true);
    wrapper.close();
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
