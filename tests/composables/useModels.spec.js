import useModels from '@composables/useModels';
import { mount, globalToastMock, globalStoreMock } from '../mountComposable';
import { flushPromises } from '@vue/test-utils';
import { mockModelsEndpoint, axiosMock, API_HOST } from './../mockAxios.js';

const testModels = [
  {
    id: 1,
    name: 'DTA-test',
    type: 'DTA',
    isDefault: true,
  },
  {
    id: 2,
    name: 'ABC-test',
    type: 'ABC',
    isDefault: true,
  },
];

let wrapper;

describe('useModels.spec.js', () => {
  describe('on good requests', () => {
    beforeEach(async () => {
      wrapper = mount(useModels);
      mockModelsEndpoint({ response: { trafficModels: testModels } });
    });

    it('fetches models data', async () => {
      expect(wrapper.models.value).to.deep.equal([]);

      await wrapper.fetchModels();
      await flushPromises();
      const models = wrapper.models.value;

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}models`);
      expect(globalToastMock.add.notCalled).to.equal(true);

      testModels.forEach((model) => {
        expect(models).to.deep.include(model);
      });
    });

    it('switches model type state', async () => {
      await wrapper.fetchModels();
      await flushPromises();

      await wrapper.switchActiveModel(2); // ABC
      await flushPromises();

      expect(globalStoreMock.commit.getCall(-1).args[0]).to.equal('scenarios/SET_MODEL');
      expect(globalStoreMock.commit.getCall(-1).args[1].type).to.equal('ABC');
      expect(globalStoreMock.dispatch.getCall(-1).args[0]).to.equal('map/activateItem');
      expect(globalStoreMock.dispatch.getCall(-1).args[1]).to.deep.equal({});
    });
  });

  describe('on bad requests', () => {
    it('fetches no models data & displays error', async () => {
      wrapper = mount(useModels);
      mockModelsEndpoint({ code: 403 });
      expect(wrapper.models.value).to.deep.equal([]);

      await wrapper.fetchModels();
      await flushPromises();
      const models = wrapper.models.value;

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}models`);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(models).to.deep.equal([]);
    });
  });

  afterEach(() => {
    wrapper.invalidateModels();
    wrapper.unmount();
  });
});
