import useFilters from '@composables/useFilters';
import { mount, globalStoreMock } from '../mountComposable';
import { getStoreMock } from './../helpers.js';
import { ref } from 'vue';
import { flushPromises } from '@vue/test-utils';

let wrapper, selectedSorting, selectedFilters;

function _getEveryFilterCombination(filters) {
  const combinations = [];
  const fn = (done = [], filters) => {
    for (let i = 0; i < filters.length; i++) {
      const appliedFilters = [...done, filters[i]];
      combinations.push(appliedFilters);
      fn(appliedFilters, filters.slice(i + 1));
    }
  };
  fn([], filters);
  return combinations;
}

function _testFilterFunction(filterFunction, filteredItems, testItems, appliedFilters) {
  // apply composable filter method
  filterFunction(appliedFilters);
  // test every filtered item
  filteredItems.value.forEach((item) => {
    const itemMatch = appliedFilters.some((filter) => item.test === filter.test);
    expect(itemMatch).to.equal(true);
  });
}

function _testSortingFunction(sortingFunction, filteredItems) {
  // apply composable sorting method
  sortingFunction();
  // test order of every item
  filteredItems.value.forEach((item, index) => {
    expect(item.testOrder === index).to.equal(true);
  });
}

describe('useFilters.spec.js', () => {
  describe('FILTERING', () => {
    it('filters by field + value', () => {
      const testItems = ref([
        { id: 1, type: 'A', test: 'type-A' },
        { id: 2, type: 'B', test: 'type-B' },
        { id: 3, type: 'C', test: 'type-C' },
        { id: 4, type: 'A', test: 'type-A' },
        { id: 5, type: 'D', test: 'type-D' },
        { id: 6, type: 'F', test: 'type-F' },
      ]);

      const testFilterGroup = [
        { field: 'type', value: 'A', test: 'type-A' },
        { field: 'type', value: 'B', test: 'type-B' },
        { field: 'type', value: 'C', test: 'type-C' },
        { field: 'type', value: 'D', test: 'type-D' },
      ];

      wrapper = mount(() => useFilters({ items: testItems, cacheKey: 'scenarios', selectedFilters: {} }));

      const filterCombinations = _getEveryFilterCombination(testFilterGroup);
      filterCombinations.forEach((appliedFilters) => {
        _testFilterFunction(wrapper.filterByType, wrapper.filteredItems, testItems, appliedFilters);
      });
    });

    it('filters by boolean field', () => {
      const testItems = ref([
        { id: 1, included: true, test: 'included' },
        { id: 2, included: false, test: 'excluded' },
        { id: 3, included: true, test: 'included' },
        { id: 4, included: false, test: 'excluded' },
      ]);

      const testFilterGroup = [
        { field: 'included', value: true, test: 'included' },
        { field: 'included', value: false, test: 'excluded' },
      ];

      wrapper = mount(() => useFilters({ items: testItems, cacheKey: 'scenarios', selectedFilters: {} }));

      const filterCombinations = _getEveryFilterCombination(testFilterGroup);
      filterCombinations.forEach((appliedFilters) => {
        _testFilterFunction(wrapper.filterByType, wrapper.filteredItems, testItems, appliedFilters);
      });
    });

    it('filters by negated field value', () => {
      const testItems = ref([
        { id: 1, type: 'A', test: 'type-A' },
        { id: 2, type: 'B', test: 'not-type-A' },
        { id: 3, type: 'C', test: 'not-type-A' },
        { id: 4, type: 'D', test: 'not-type-A' },
      ]);

      const testFilterGroup = [
        { field: 'type', value: 'A', test: 'type-A' },
        { field: 'type', value: 'A', operator: 'NEQ', test: 'not-type-A' },
      ];

      wrapper = mount(() => useFilters({ items: testItems, cacheKey: 'scenarios', selectedFilters: {} }));

      const filterCombinations = _getEveryFilterCombination(testFilterGroup);
      filterCombinations.forEach((appliedFilters) => {
        _testFilterFunction(wrapper.filterByType, wrapper.filteredItems, testItems, appliedFilters);
      });
    });

    it('filters by dates', () => {
      const yesterday = new Date(Date.now() - 24 * 60 * 60 * 1000);
      const tomorrow = new Date(Date.now() + 24 * 60 * 60 * 1000);
      const unlimitedDate = null;

      const testItems = ref([
        { id: 1, dateFrom: yesterday, dateTo: yesterday, test: 'past' },
        { id: 2, dateFrom: yesterday, dateTo: tomorrow, test: 'current' },
        { id: 3, dateFrom: tomorrow, dateTo: tomorrow, test: 'future' },
        { id: 4, dateFrom: unlimitedDate, dateTo: yesterday, test: 'past' },
        { id: 5, dateFrom: unlimitedDate, dateTo: tomorrow, test: 'current' },
        { id: 6, dateFrom: unlimitedDate, dateTo: unlimitedDate, test: 'current' },
        { id: 7, dateFrom: yesterday, dateTo: unlimitedDate, test: 'current' },
        { id: 8, dateFrom: tomorrow, dateTo: unlimitedDate, test: 'future' },
      ]);

      const testFilterGroup = [
        { name: 'past', test: 'past' },
        { name: 'current', test: 'current' },
        { name: 'future', test: 'future' },
      ];

      wrapper = mount(() => useFilters({ items: testItems, cacheKey: 'scenarios', selectedFilters: {} }));

      const filterCombinations = _getEveryFilterCombination(testFilterGroup);
      filterCombinations.forEach((appliedFilters) => {
        _testFilterFunction(wrapper.filterByDate, wrapper.filteredItems, testItems, appliedFilters);
      });
    });

    it('filters with default filters', () => {
      const testItems = ref([
        { id: 1, type: 'A', test: 'type-A' },
        { id: 2, type: 'B', test: 'type-B' },
        { id: 3, type: 'C', test: 'type-C' },
        { id: 4, type: 'A', test: 'type-A' },
        { id: 5, type: 'D', test: 'type-D' },
        { id: 6, type: 'F', test: 'type-F' },
      ]);

      const testAppliedFilters = ref([
        { field: 'type', value: 'A', test: 'type-A' },
        { field: 'type', value: 'D', test: 'type-D' },
      ]);

      const testStore = getStoreMock();
      testStore.state.scenarios.filters.scenarios = { type: null, date: null };

      wrapper = mount(
        () =>
          useFilters({
            items: testItems,
            cacheKey: 'scenarios',
            selectedFilters: { type: testAppliedFilters },
          }),
        {
          store: testStore,
        },
      );

      wrapper.filteredItems.value.forEach((item) => {
        const itemMatch = testAppliedFilters.value.some((filter) => item.test === filter.test);
        expect(itemMatch).to.equal(true);
      });
    });

    it('filters with stored filters', () => {
      const testItems = ref([
        { id: 1, type: 'A', test: 'type-A' },
        { id: 2, type: 'B', test: 'type-B' },
        { id: 3, type: 'C', test: 'type-C' },
        { id: 4, type: 'A', test: 'type-A' },
        { id: 5, type: 'D', test: 'type-D' },
        { id: 6, type: 'F', test: 'type-F' },
      ]);

      const storedFilters = [
        { field: 'type', value: 'A', test: 'type-A' },
        { field: 'type', value: 'D', test: 'type-D' },
      ];

      const testStore = getStoreMock();
      testStore.state.scenarios.filters.scenarios = { type: storedFilters, date: null };

      wrapper = mount(
        () =>
          useFilters({
            items: testItems,
            cacheKey: 'scenarios',
            selectedFilters: { type: ref([]) },
          }),
        {
          store: testStore,
        },
      );

      wrapper.filteredItems.value.forEach((item) => {
        const itemMatch = storedFilters.some((filter) => item.test === filter.test);
        expect(itemMatch).to.equal(true);
      });
    });

    it('watches applied filters and stores them', async () => {
      const testItems = ref([
        { id: 1, type: 'A', test: 'type-A' },
        { id: 2, type: 'B', test: 'type-B' },
        { id: 3, type: 'C', test: 'type-C' },
        { id: 4, type: 'A', test: 'type-A' },
        { id: 5, type: 'D', test: 'type-D' },
        { id: 6, type: 'F', test: 'type-F' },
      ]);

      const testAppliedFilters = ref([
        { field: 'type', value: 'A', test: 'type-A' },
        { field: 'type', value: 'D', test: 'type-D' },
      ]);

      wrapper = mount(() =>
        useFilters({
          items: testItems,
          cacheKey: 'scenarios',
          selectedFilters: { type: testAppliedFilters },
        }),
      );

      testAppliedFilters.value = [{ field: 'type', value: 'F', test: 'type-F' }];
      await flushPromises();

      expect(globalStoreMock.commit.calledOnce).to.equal(true);
      expect(globalStoreMock.commit.getCall(-1).args).to.deep.equal([
        'scenarios/SET_FILTERS',
        { cacheKey: 'scenarios', group: 'type', selection: testAppliedFilters.value },
      ]);

      wrapper.filteredItems.value.forEach((item) => {
        const itemMatch = testAppliedFilters.value.some((filter) => item.test === filter.test);
        expect(itemMatch).to.equal(true);
      });
    });

    it('preserves order while filtering', async () => {
      selectedSorting = ref({ name: 'id' });

      const testItems = ref([
        { id: 1, type: 'A', test: 'type-A', testOrder: 5 },
        { id: 2, type: 'B', test: 'type-B', testOrder: 4 },
        { id: 3, type: 'C', test: 'type-C', testOrder: 3 },
        { id: 4, type: 'A', test: 'type-A', testOrder: 2 },
        { id: 5, type: 'D', test: 'type-D', testOrder: 1 },
        { id: 6, type: 'F', test: 'type-F', testOrder: 0 },
      ]);

      const selectedFilters = ref([
        { field: 'type', value: 'A', test: 'type-A' },
        { field: 'type', value: 'D', test: 'type-D' },
      ]);

      wrapper = mount(() =>
        useFilters({
          items: testItems,
          cacheKey: 'scenarios',
          selectedFilters: { type: selectedFilters },
          selectedSorting,
        }),
      );

      selectedFilters.value = [{ field: 'type', value: 'F', test: 'type-F' }];
      await flushPromises();

      wrapper.filteredItems.value.forEach((item, index) => {
        expect(item.testOrder === index).to.equal(true);
      });
    });
  });

  describe('SORTING', () => {
    before(() => {
      selectedSorting = ref({ name: 'name' });
    });

    it('sorts by name property', () => {
      const testItems = ref([
        { id: 1, name: 'A', testOrder: 0 },
        { id: 2, name: 'B', testOrder: 1 },
        { id: 3, name: 'C', testOrder: 2 },
        { id: 4, name: 'F', testOrder: 5 },
        { id: 5, name: 'E', testOrder: 4 },
        { id: 6, name: 'D', testOrder: 3 },
      ]);

      wrapper = mount(() => useFilters({ items: testItems, cacheKey: 'scenarios', selectedSorting }));

      _testSortingFunction(wrapper.sortByName, wrapper.filteredItems);
    });

    it('sorts by id', () => {
      const testItems = ref([
        { id: 1, testOrder: 5 },
        { id: 2, testOrder: 4 },
        { id: 3, testOrder: 3 },
        { id: 4, testOrder: 2 },
        { id: 5, testOrder: 1 },
        { id: 6, testOrder: 0 },
      ]);

      wrapper = mount(() => useFilters({ items: testItems, cacheKey: 'scenarios', selectedSorting }));

      _testSortingFunction(wrapper.sortById, wrapper.filteredItems);
    });

    it('sorts by date', () => {
      const testItems = ref([
        { id: 1, createdAt: '2021-02-18T09:05:58.731Z', testOrder: 2 },
        { id: 2, createdAt: null, testOrder: 4 },
        { id: 3, createdAt: '2021-02-19T09:05:58.731Z', testOrder: 1 },
        { id: 4, createdAt: '2021-02-20T09:05:58.731Z', testOrder: 0 },
        { id: 5, createdAt: null, testOrder: 5 },
        { id: 6, createdAt: '2021-02-01T09:05:58.731Z', testOrder: 3 },
      ]);

      wrapper = mount(() => useFilters({ items: testItems, cacheKey: 'scenarios', selectedSorting }));

      _testSortingFunction(wrapper.sortByDate, wrapper.filteredItems);
    });

    it('applies default sorting', () => {
      selectedSorting = ref({ name: 'name' });

      const testItems = ref([
        { id: 1, name: 'A', testOrder: 0 },
        { id: 2, name: 'B', testOrder: 1 },
        { id: 3, name: 'C', testOrder: 2 },
        { id: 4, name: 'F', testOrder: 5 },
        { id: 5, name: 'E', testOrder: 4 },
        { id: 6, name: 'D', testOrder: 3 },
      ]);

      wrapper = mount(() => useFilters({ items: testItems, cacheKey: 'scenarios', selectedSorting }));

      wrapper.filteredItems.value.forEach((item, index) => {
        expect(item.testOrder === index).to.equal(true);
      });
    });

    it('applies stored sorting as default', () => {
      selectedSorting = ref({ name: 'id' });

      const testItems = ref([
        { id: 1, name: 'A', testOrder: 0 },
        { id: 2, name: 'B', testOrder: 1 },
        { id: 3, name: 'C', testOrder: 2 },
        { id: 4, name: 'F', testOrder: 5 },
        { id: 5, name: 'E', testOrder: 4 },
        { id: 6, name: 'D', testOrder: 3 },
      ]);

      const testStore = getStoreMock();
      testStore.state.scenarios.sorting.scenarios = { name: 'name' };

      wrapper = mount(() => useFilters({ items: testItems, cacheKey: 'scenarios', selectedSorting }), {
        store: testStore,
      });

      wrapper.filteredItems.value.forEach((item, index) => {
        expect(item.testOrder === index).to.equal(true);
      });
    });

    it('watches changes of sorting and stores them', async () => {
      selectedSorting = ref({ name: 'id' });

      const testItems = ref([
        { id: 1, name: 'A', testOrder: 0 },
        { id: 2, name: 'B', testOrder: 1 },
        { id: 3, name: 'C', testOrder: 2 },
        { id: 4, name: 'F', testOrder: 5 },
        { id: 5, name: 'E', testOrder: 4 },
        { id: 6, name: 'D', testOrder: 3 },
      ]);

      wrapper = mount(() => useFilters({ items: testItems, cacheKey: 'scenarios', selectedFilters, selectedSorting }));

      selectedSorting.value = { name: 'name' };
      await flushPromises();

      expect(globalStoreMock.commit.calledOnce).to.equal(true);
      expect(globalStoreMock.commit.getCall(-1).args).to.deep.equal([
        'scenarios/SET_SORTING',
        { cacheKey: 'scenarios', sortBy: selectedSorting.value },
      ]);

      wrapper.filteredItems.value.forEach((item, index) => {
        expect(item.testOrder === index).to.equal(true);
      });
    });

    it('preserves filtering while sorting', async () => {
      selectedSorting = ref({ name: 'name' });

      const testItems = ref([
        { id: 1, type: 'A', name: 'A', test: 'type-A', testOrder: 0 },
        { id: 2, type: 'B', name: 'B', test: 'type-B', testOrder: 1 },
        { id: 3, type: 'C', name: 'C', test: 'type-C', testOrder: 2 },
        { id: 4, type: 'A', name: 'F', test: 'type-A', testOrder: 5 },
        { id: 5, type: 'D', name: 'E', test: 'type-D', testOrder: 4 },
        { id: 6, type: 'F', name: 'D', test: 'type-F', testOrder: 3 },
      ]);

      const selectedFilters = ref([
        { field: 'type', value: 'A', test: 'type-A' },
        { field: 'type', value: 'D', test: 'type-D' },
      ]);

      const testStore = getStoreMock();
      testStore.state.scenarios.filters.scenarios = { type: null, date: null };

      wrapper = mount(
        () =>
          useFilters({
            items: testItems,
            cacheKey: 'scenarios',
            selectedFilters: { type: selectedFilters },
            selectedSorting,
          }),
        { store: testStore },
      );

      selectedSorting.value = 'id';
      await flushPromises();

      wrapper.filteredItems.value.forEach((item) => {
        const itemMatch = selectedFilters.value.some((filter) => item.test === filter.test);
        expect(itemMatch).to.equal(true);
      });
    });
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
