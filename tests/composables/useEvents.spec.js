import useEvents from '@composables/useEvents';
import { mount, globalToastMock, globalStoreMock } from '../mountComposable';
import {
  mockEventEndpoint,
  mockEventsEndpoint,
  mockCreateEventEndpoint,
  mockUpdateEventEndpoint,
  mockDeleteEventEndpoint,
  mockImportableEventsEndpoint,
  mockImportEventsEndpoint,
  axiosMock,
  API_HOST,
} from './../mockAxios.js';
import { flushPromises } from '@vue/test-utils';

const testScenario = { id: 1 };
const testEvents = [
  {
    id: 1,
    name: 'Aktualni udalost',
    description: 'popis',
    dateFrom: '2020-02-20T00:00:00.000Z',
    dateTo: '2022-03-20T00:00:00.000Z',
    authorUserId: 1,
    included: true,
  },
  {
    id: 2,
    name: 'Stara udalost',
    description: 'popis',
    dateFrom: '2020-02-20T00:00:00.000Z',
    dateTo: '2020-03-20T00:00:00.000Z',
    authorUserId: 2,
    included: true,
  },
];
const testEvent = testEvents[0];

let wrapper, cacheEvent, invalidateEvent, updateCachedEvent, invalidateModification, invalidateNodeTransit;

describe('useEvents.spec.js', () => {
  beforeEach(() => {
    cacheEvent = sinon.spy();
    invalidateEvent = sinon.spy();
    updateCachedEvent = sinon.spy();
    invalidateModification = sinon.spy();
    invalidateNodeTransit = sinon.spy();
    wrapper = mount(() =>
      useEvents(
        { scenarioId: testScenario.id },
        {
          cacheEvent,
          invalidateEvent,
          updateCachedEvent,
          invalidateModification,
          invalidateNodeTransit,
        },
      ),
    );
  });

  describe('on good requests', () => {
    it('fetches & caches event', async () => {
      mockEventEndpoint({ response: { event: testEvent }, scenarioId: testScenario.id, eventId: testEvent.id });

      await wrapper.fetchEvent(testEvent.id);
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}`);
      expect(globalToastMock.add.notCalled).to.equal(true);
      expect(cacheEvent.calledOnce).to.equal(true);
      expect(invalidateEvent.notCalled).to.equal(true);
      expect(updateCachedEvent.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(cacheEvent.calledWith(testEvent)).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('fetches & caches events', async () => {
      mockEventsEndpoint({ response: { events: testEvents }, scenarioId: testScenario.id });

      await wrapper.fetchEvents();
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/events`);
      expect(globalToastMock.add.notCalled).to.equal(true);
      expect(cacheEvent.calledOnce).to.equal(true);
      expect(invalidateEvent.notCalled).to.equal(true);
      expect(updateCachedEvent.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(cacheEvent.calledWith(testEvents)).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('creates & caches new event', async () => {
      mockCreateEventEndpoint({ response: { event: testEvent, editSession: {} }, scenarioId: testScenario.id });

      const newEvent = {
        name: 'test ev',
        description: 'test desc',
      };

      await wrapper.createEvent(newEvent);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/events`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
      expect(cacheEvent.calledOnce).to.equal(true);
      expect(invalidateEvent.notCalled).to.equal(true);
      expect(updateCachedEvent.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(cacheEvent.calledWith(testEvent)).to.equal(true);
      expect(globalStoreMock.dispatch.getCall(-1).args[0]).to.equal('scenarios/updateScenarioProgress');
    });

    it('updates & caches event', async () => {
      mockUpdateEventEndpoint({
        response: { event: testEvent, updatedEvents: [], editSession: {} },
        scenarioId: testScenario.id,
        eventId: testEvent.id,
      });

      const updatedEvent = {
        ...testEvent,
        name: 'test sc',
        description: 'test desc',
      };
      delete updatedEvent.included; // included can only be sent when changing inclusion

      await wrapper.updateEvent(updatedEvent);
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(axiosMock.history.patch[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
      expect(cacheEvent.calledOnce).to.equal(true);
      expect(invalidateEvent.notCalled).to.equal(true);
      expect(updateCachedEvent.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(cacheEvent.calledWith(testEvent)).to.equal(true);
      expect(globalStoreMock.dispatch.getCall(-1).args[0]).to.equal('scenarios/updateScenarioProgress');
    });

    it('deletes & invalidates event', async () => {
      mockDeleteEventEndpoint({
        response: { deletedModifications: [], editSession: {} },
        scenarioId: testScenario.id,
        eventId: testEvent.id,
      });

      await wrapper.deleteEvent(testEvent.id);
      await flushPromises();

      expect(axiosMock.history.delete.length).to.equal(1);
      expect(axiosMock.history.delete[0].url).to.equal(
        `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}`,
      );
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
      expect(cacheEvent.notCalled).to.equal(true);
      expect(invalidateEvent.calledOnce).to.equal(true);
      expect(updateCachedEvent.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(invalidateEvent.calledWith({ scenarioId: testScenario.id, eventId: testEvent.id })).to.equal(true);
      expect(globalStoreMock.dispatch.getCall(-1).args[0]).to.equal('scenarios/updateScenarioProgress');
    });

    it('fetches importable events', async () => {
      mockImportableEventsEndpoint({ response: { events: testEvents }, scenarioId: testScenario.id });
      const onSuccess = sinon.spy();
      await wrapper.fetchImportableEvents({ onSuccess });
      await flushPromises();

      expect(onSuccess.calledOnce).to.equal(true);
      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/importable-events`);
      expect(globalToastMock.add.notCalled).to.equal(true);
      expect(cacheEvent.notCalled).to.equal(true);
      expect(invalidateEvent.notCalled).to.equal(true);
      expect(updateCachedEvent.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('imports events', async () => {
      mockImportEventsEndpoint({ response: { events: testEvents, editSession: {} }, scenarioId: testScenario.id });

      const events = [{ eventId: 1, scenarioId: 1 }];

      await wrapper.importEvents(events);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/import-events`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
      expect(cacheEvent.callCount).to.equal(testEvents.length);
      expect(invalidateEvent.notCalled).to.equal(true);
      expect(updateCachedEvent.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(cacheEvent.calledWith(testEvent)).to.equal(true);
      expect(globalStoreMock.dispatch.getCall(-1).args[0]).to.equal('scenarios/updateScenarioProgress');
    });
  });

  describe('on bad requests', () => {
    it('returns no event and sends error', async () => {
      mockEventEndpoint({ code: 404, scenarioId: testScenario.id, eventId: testEvent.id });

      await wrapper.fetchEvent(testEvent.id);
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(cacheEvent.notCalled).to.equal(true);
      expect(invalidateEvent.notCalled).to.equal(true);
      expect(updateCachedEvent.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('returns no events and sends info', async () => {
      mockEventsEndpoint({ code: 404, scenarioId: testScenario.id });

      await wrapper.fetchEvents();
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/events`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('info');
      expect(cacheEvent.notCalled).to.equal(true);
      expect(invalidateEvent.notCalled).to.equal(true);
      expect(updateCachedEvent.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('does not create event and sends error', async () => {
      mockCreateEventEndpoint({ code: 404, scenarioId: testScenario.id });

      const newEvent = {
        name: 'test sc',
        description: 'test desc',
      };

      await wrapper.createEvent(newEvent);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/events`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(cacheEvent.notCalled).to.equal(true);
      expect(invalidateEvent.notCalled).to.equal(true);
      expect(updateCachedEvent.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('does not update event and sends error', async () => {
      mockUpdateEventEndpoint({ code: 404, scenarioId: testScenario.id, eventId: testEvent.id });

      const updatedEvent = {
        ...testEvent,
        name: 'test sc',
        description: 'test desc',
      };

      await wrapper.updateEvent(updatedEvent);
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(axiosMock.history.patch[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(cacheEvent.notCalled).to.equal(true);
      expect(invalidateEvent.notCalled).to.equal(true);
      expect(updateCachedEvent.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('does not delete event and sends error', async () => {
      mockDeleteEventEndpoint({ code: 404, scenarioId: testScenario.id, eventId: testEvent.id });

      await wrapper.deleteEvent(testEvent.id);
      await flushPromises();

      expect(axiosMock.history.delete.length).to.equal(1);
      expect(axiosMock.history.delete[0].url).to.equal(
        `${API_HOST}scenarios/${testScenario.id}/events/${testEvent.id}`,
      );
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(cacheEvent.notCalled).to.equal(true);
      expect(invalidateEvent.notCalled).to.equal(true);
      expect(updateCachedEvent.notCalled).to.equal(true);
      expect(invalidateModification.notCalled).to.equal(true);
      expect(invalidateNodeTransit.notCalled).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });
  });

  afterEach(() => wrapper.unmount());
});
