import useLayerSourceData, { getNodeNeighboringLinksData } from '@composables/useLayerSourceData';
import { LID_DTA_LINKS, LID_DTA_NODES, LID_DTA_GENERATORS } from '@keys/index';
import { expect } from 'chai';
import { mount } from '../mountComposable';
import {
  mockModelLinksSourceEndpoint,
  mockModelNodesSourceEndpoint,
  mockModelGeneratorsSourceEndpoint,
  axiosMock,
  API_MODEL_DTA,
  MODEL_NAME_DTA,
} from './../mockAxios.js';

let wrapper;

const exampleModelLinksResponse = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {
        edge_id: 1,
        from_node_id: 1,
        to_node_id: 2,
        capacity: 500.0,
        cost: 0.06082271784543991,
        speed: 5.0,
        type: 0,
      },
      geometry: {
        type: 'LineString',
        coordinates: [
          [1487982, 6392045],
          [1487639, 6391991],
        ],
      },
    },
    {
      type: 'Feature',
      properties: {
        edge_id: 2,
        from_node_id: 1,
        to_node_id: 3,
        capacity: 1000.0,
        cost: 0.06512484699487686,
        speed: 20.0,
        type: 1,
      },
      geometry: {
        type: 'LineString',
        coordinates: [
          [1487639, 6391991],
          [1488291, 6392071],
        ],
      },
    },
    {
      type: 'Feature',
      properties: {
        edge_id: 3,
        from_node_id: 2,
        to_node_id: 3,
        capacity: 2000.0,
        cost: 0.05625548213720322,
        speed: 50.0,
        type: 2,
      },
      geometry: {
        type: 'LineString',
        coordinates: [
          [1488291, 6392071],
          [1487982, 6392045],
        ],
      },
    },
  ],
};

const getRoundedCoords = (feat) => {
  const coords = feat.geometry.coordinates;
  return coords.map((coord) => [Math.round(coord[0]), Math.round(coord[1])]);
};
const roundFeatureDataCoords = (featureData) => {
  featureData.geometry.coordinates = getRoundedCoords(featureData);
  return featureData;
};
const roundFeaturesDataCoords = (featuresData) => {
  if (featuresData.features)
    return {
      ...featuresData,
      features: featuresData.features.map((fData) => roundFeatureDataCoords(fData)),
    };
  return featuresData.map((fData) => roundFeatureDataCoords(fData));
};

const testSourceCoords = [
  [
    [13.3667697, 49.6858367],
    [13.3636885, 49.6855228],
  ],
  [
    [13.3636885, 49.6855228],
    [13.3695455, 49.6859877],
  ],
  [
    [13.3695455, 49.6859877],
    [13.3667697, 49.6858367],
  ],
];
const testSourceFeatures = {
  type: 'FeatureCollection',
  features: exampleModelLinksResponse.features.map((feat, index) => {
    return {
      ...feat,
      geometry: {
        type: 'LineString',
        coordinates: testSourceCoords[index],
      },
    };
  }),
};
describe('useLayerSourceData.spec.js', () => {
  describe('faulty initialization', () => {
    it('requires sourceKey to be provided', async () => {
      expect(() => useLayerSourceData()).to.throw('SourceKey not provided');
    });

    it('requires supported sourceKey to be provided', async () => {
      const notSupportedKey = 'notSupported';
      expect(() => useLayerSourceData(notSupportedKey)).to.throw(`SourceKey ${notSupportedKey} not supported`);
    });
  });

  describe('successfully initialization', () => {
    describe('basic layer source fetching', () => {
      it('fetches required source data (modelLinks)', async () => {
        const testData = {};
        mockModelLinksSourceEndpoint({ response: testData });
        wrapper = mount(() => useLayerSourceData(LID_DTA_LINKS, { clearOldData: true }));

        await wrapper.fetchSourceData();
        expect(axiosMock.history.get[0].url).to.equal(`${API_MODEL_DTA}/edges/${MODEL_NAME_DTA}`);
      });

      it('fetches required source data (modelNodes)', async () => {
        const testData = {};
        mockModelNodesSourceEndpoint({ response: testData });
        wrapper = mount(() => useLayerSourceData(LID_DTA_NODES, { clearOldData: true }));

        await wrapper.fetchSourceData();
        expect(axiosMock.history.get[0].url).to.equal(`${API_MODEL_DTA}/nodes/${MODEL_NAME_DTA}`);
      });

      it('fetches required source data (modelGenerators)', async () => {
        const testData = {};
        mockModelGeneratorsSourceEndpoint({ response: testData });
        wrapper = mount(() => useLayerSourceData(LID_DTA_GENERATORS, { clearOldData: true }));

        await wrapper.fetchSourceData();
        expect(axiosMock.history.get[0].url).to.equal(`${API_MODEL_DTA}/zones/${MODEL_NAME_DTA}`);
      });

      it('fetches required source data (modelLinks) and expose it on sourceData property', async () => {
        mockModelLinksSourceEndpoint({ response: testSourceFeatures });
        wrapper = mount(() => useLayerSourceData(LID_DTA_LINKS, { clearOldData: true }));
        await wrapper.fetchSourceData();
        const receivedFeatures = roundFeaturesDataCoords(wrapper.sourceData.value);
        expect(exampleModelLinksResponse).to.deep.equal(receivedFeatures);
        expect(axiosMock.history.get[0].params).to.deep.equal({});
      });
    });

    describe('getFeatureData', () => {
      const expectedFeatureData = exampleModelLinksResponse.features[1];
      const requestedFeatureID = 2;
      const notAvailableFeatureID = 4;

      it('expect getFeatureData to throw if source was not correctly fetched', () => {
        wrapper = mount(() => useLayerSourceData(LID_DTA_LINKS, { clearOldData: true }));
        wrapper.clearSourceData();
        expect(() => wrapper.getFeatureData(requestedFeatureID)).to.throw();
      });

      it('returns correct feature based on id (with id matcher) after source was fetched', async () => {
        mockModelLinksSourceEndpoint({ response: testSourceFeatures });
        wrapper = mount(() => useLayerSourceData(LID_DTA_LINKS, { clearOldData: true }));
        await wrapper.fetchSourceData();
        const featureData = wrapper.getFeatureData(requestedFeatureID);
        const receivedFData = roundFeatureDataCoords(featureData);
        expect(expectedFeatureData).to.deep.equal(receivedFData);
      });

      it('returns undefined if feature is not found in source', async () => {
        mockModelLinksSourceEndpoint({ response: testSourceFeatures });
        wrapper = mount(() => useLayerSourceData(LID_DTA_LINKS, { clearOldData: true }));
        await wrapper.fetchSourceData();
        const featureData = wrapper.getFeatureData(notAvailableFeatureID);
        expect(featureData).to.be.undefined;
      });

      it('returns consistent feature if requested twice', async () => {
        mockModelLinksSourceEndpoint({ response: testSourceFeatures });
        wrapper = mount(() => useLayerSourceData(LID_DTA_LINKS, { clearOldData: true }));
        await wrapper.fetchSourceData();
        const firstFData = roundFeatureDataCoords(wrapper.getFeatureData(requestedFeatureID));
        expect(expectedFeatureData).to.deep.equal(firstFData);
        const secondFData = roundFeatureDataCoords(wrapper.getFeatureData(requestedFeatureID));
        expect(expectedFeatureData).to.deep.equal(secondFData);
      });
    });

    describe('getNodeNeighboringLinksData', () => {
      const expectedFeaturesData = [exampleModelLinksResponse.features[1], exampleModelLinksResponse.features[2]];
      const requestedNodeID = 3;
      const notAvailableNodeNeighborsID = 4;

      it('expect getNodeNeighboringLinksData to throw if modelLinks source was fetched', () => {
        wrapper = mount(() => useLayerSourceData(LID_DTA_LINKS, { autoFetch: true }));
        wrapper.clearSourceData();
        expect(() => getNodeNeighboringLinksData(requestedNodeID)).to.throw();
      });

      it('returns correct features based on nodeId after source was fetched', async () => {
        mockModelLinksSourceEndpoint({ response: testSourceFeatures });
        wrapper = mount(() => useLayerSourceData(LID_DTA_LINKS, { clearOldData: true }));
        await wrapper.fetchSourceData();
        const neighborLinksData = getNodeNeighboringLinksData(requestedNodeID);
        const receivedFeatures = roundFeaturesDataCoords(neighborLinksData);
        expect(expectedFeaturesData).to.deep.equal(receivedFeatures);
      });

      it('returns empty array if neighbor features are not found in source', async () => {
        mockModelLinksSourceEndpoint({ response: testSourceFeatures });
        wrapper = mount(() => useLayerSourceData(LID_DTA_LINKS, { clearOldData: true }));
        await wrapper.fetchSourceData();
        const neighborLinksData = getNodeNeighboringLinksData(notAvailableNodeNeighborsID);
        expect(neighborLinksData).to.deep.equal([]);
      });

      it('returns consistent features array if requested twice', async () => {
        mockModelLinksSourceEndpoint({ response: testSourceFeatures });
        wrapper = mount(() => useLayerSourceData(LID_DTA_LINKS, { clearOldData: true }));
        await wrapper.fetchSourceData();
        const firstNeighborLinksData = getNodeNeighboringLinksData(requestedNodeID);
        const firstReceivedFeatures = roundFeaturesDataCoords(firstNeighborLinksData);
        expect(expectedFeaturesData).to.deep.equal(firstReceivedFeatures);
        const secondNeighborLinksData = getNodeNeighboringLinksData(requestedNodeID);
        const secondReceivedFeatures = roundFeaturesDataCoords(secondNeighborLinksData);
        expect(expectedFeaturesData).to.deep.equal(secondReceivedFeatures);
      });
    });

    afterEach(() => {
      wrapper.unmount();
    });
  });
});
