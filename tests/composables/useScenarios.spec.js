import useScenarios from '@composables/useScenarios';
import { mount, globalToastMock, globalStoreMock } from '../mountComposable';
import {
  mockScenarioEndpoint,
  mockScenariosEndpoint,
  mockCreateScenarioEndpoint,
  mockUpdateScenarioEndpoint,
  mockDeleteScenarioEndpoint,
  axiosMock,
  API_HOST,
} from './../mockAxios.js';
import { flushPromises } from '@vue/test-utils';

const testScenarios = [
  {
    id: 1,
    name: 'Soukromy scenario',
    type: 'private',
    dateFrom: '2021-02-18T09:05:58.731Z',
    dateTo: '2021-02-18T09:05:58.731Z',
    authorUserId: 1,
    hasModelSectionsValid: true,
  },
  {
    id: 2,
    name: 'Adminova tvorba',
    type: 'private',
    dateFrom: '2021-02-18T09:05:58.731Z',
    dateTo: '2021-02-18T09:05:58.731Z',
    authorUserId: 1,
    hasModelSectionsValid: true,
  },
];

const testScenario = testScenarios[0];

let wrapper, cacheOverview, invalidateOverview, cacheScenario, invalidateScenario;

describe('useScenarios.spec.js', () => {
  beforeEach(() => {
    cacheOverview = sinon.spy();
    invalidateOverview = sinon.spy();
    cacheScenario = sinon.spy();
    invalidateScenario = sinon.spy();
    wrapper = mount(() => useScenarios({ cacheOverview, invalidateOverview, cacheScenario, invalidateScenario }));
  });

  describe('on good requests', () => {
    it('fetches & caches scenario', async () => {
      mockScenarioEndpoint({ response: { scenario: testScenario }, scenarioId: testScenario.id });

      await wrapper.fetchScenario(testScenario.id);
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
      expect(globalToastMock.add.notCalled).to.equal(true);
      expect(cacheScenario.calledOnce).to.equal(true);
      expect(cacheOverview.notCalled).to.equal(true);
      expect(invalidateOverview.notCalled).to.equal(true);
      expect(invalidateScenario.notCalled).to.equal(true);
      expect(cacheScenario.calledWith(testScenario)).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('fetches & caches scenarios', async () => {
      mockScenariosEndpoint({ response: { scenarios: testScenarios } });

      await wrapper.fetchScenarios();
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios`);
      expect(globalToastMock.add.notCalled).to.equal(true);
      expect(cacheScenario.notCalled).to.equal(true);
      expect(cacheOverview.calledOnce).to.equal(true);
      expect(invalidateOverview.notCalled).to.equal(true);
      expect(invalidateScenario.notCalled).to.equal(true);
      expect(cacheOverview.calledWith(testScenarios)).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('creates & caches new scenario', async () => {
      mockCreateScenarioEndpoint({ response: { scenario: testScenario } });

      const newScenario = {
        name: 'test sc',
        description: 'test desc',
      };

      await wrapper.createScenario(newScenario);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
      expect(cacheScenario.notCalled).to.equal(true);
      expect(cacheOverview.calledOnce).to.equal(true);
      expect(invalidateScenario.notCalled).to.equal(true);
      expect(invalidateOverview.notCalled).to.equal(true);
      expect(cacheOverview.calledWith(testScenario)).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('updates & caches scenario', async () => {
      mockUpdateScenarioEndpoint({
        response: { scenario: testScenario, editSession: {} },
        scenarioId: testScenario.id,
      });

      const updatedScenario = {
        ...testScenario,
        name: 'test sc',
        description: 'test desc',
      };

      await wrapper.updateScenario(updatedScenario);
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(axiosMock.history.patch[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
      expect(cacheScenario.calledOnce).to.equal(true);
      expect(cacheOverview.calledOnce).to.equal(true);
      expect(invalidateScenario.notCalled).to.equal(true);
      expect(invalidateOverview.notCalled).to.equal(true);
      expect(cacheScenario.calledWith(testScenario)).to.equal(true);
      expect(cacheOverview.calledWith(testScenario)).to.equal(true);
      expect(globalStoreMock.dispatch.getCall(-1).args[0]).to.equal('scenarios/updateScenarioProgress');
    });

    it('deletes & invalidates scenario', async () => {
      mockDeleteScenarioEndpoint({ response: { scenario: testScenario }, scenarioId: testScenario.id });

      await wrapper.deleteScenario(testScenario.id);
      await flushPromises();

      expect(axiosMock.history.delete.length).to.equal(1);
      expect(axiosMock.history.delete[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('success');
      expect(cacheScenario.notCalled).to.equal(true);
      expect(cacheOverview.notCalled).to.equal(true);
      expect(invalidateScenario.calledOnce).to.equal(true);
      expect(invalidateScenario.calledWith({ scenarioId: testScenario.id })).to.equal(true);
      expect(invalidateOverview.calledOnce).to.equal(true);
      expect(invalidateOverview.calledWith({ scenarioId: testScenario.id })).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });
  });

  describe('on bad requests', () => {
    it('returns no scenario and sends error', async () => {
      mockScenarioEndpoint({ code: 404, scenarioId: testScenario.id });

      await wrapper.fetchScenario(testScenario.id);
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(cacheScenario.notCalled).to.equal(true);
      expect(cacheOverview.notCalled).to.equal(true);
      expect(invalidateScenario.calledOnce).to.equal(true);
      expect(invalidateOverview.notCalled).to.equal(true);
      expect(invalidateScenario.calledWith({ scenarioId: testScenario.id })).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('returns no scenarios and sends info', async () => {
      mockScenariosEndpoint({ code: 404 });

      await wrapper.fetchScenarios();
      await flushPromises();

      expect(axiosMock.history.get.length).to.equal(1);
      expect(axiosMock.history.get[0].url).to.equal(`${API_HOST}scenarios`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('info');
      expect(cacheScenario.notCalled).to.equal(true);
      expect(cacheOverview.notCalled).to.equal(true);
      expect(invalidateScenario.notCalled).to.equal(true);
      expect(invalidateOverview.calledOnce).to.equal(true);
      expect(invalidateOverview.calledWith()).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('does not create scenario and sends error', async () => {
      mockCreateScenarioEndpoint({ code: 404 });

      const newScenario = {
        name: 'test sc',
        description: 'test desc',
      };

      await wrapper.createScenario(newScenario);
      await flushPromises();

      expect(axiosMock.history.post.length).to.equal(1);
      expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(cacheScenario.notCalled).to.equal(true);
      expect(cacheOverview.notCalled).to.equal(true);
      expect(invalidateScenario.notCalled).to.equal(true);
      expect(invalidateOverview.notCalled).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('does not update scenario and sends error', async () => {
      mockUpdateScenarioEndpoint({ code: 404, scenarioId: testScenario.id });

      const updatedScenario = {
        ...testScenario,
        name: 'test sc',
        description: 'test desc',
      };

      await wrapper.updateScenario(updatedScenario);
      await flushPromises();

      expect(axiosMock.history.patch.length).to.equal(1);
      expect(axiosMock.history.patch[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(cacheScenario.notCalled).to.equal(true);
      expect(cacheOverview.notCalled).to.equal(true);
      expect(invalidateScenario.notCalled).to.equal(true);
      expect(invalidateOverview.notCalled).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });

    it('does not delete scenario and sends error', async () => {
      mockDeleteScenarioEndpoint({ code: 404, scenarioId: testScenario.id });

      await wrapper.deleteScenario(testScenario.id);
      await flushPromises();

      expect(axiosMock.history.delete.length).to.equal(1);
      expect(axiosMock.history.delete[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}`);
      expect(globalToastMock.add.calledOnce).to.equal(true);
      expect(globalToastMock.add.getCall(-1).args[0].severity).to.equal('error');
      expect(cacheScenario.notCalled).to.equal(true);
      expect(cacheOverview.notCalled).to.equal(true);
      expect(invalidateScenario.notCalled).to.equal(true);
      expect(invalidateOverview.notCalled).to.equal(true);
      expect(globalStoreMock.commit.notCalled).to.equal(true);
    });
  });

  afterEach(() => wrapper.unmount());
});
