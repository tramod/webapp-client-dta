import useDate from '@composables/useDate';
import { mount } from '../mountComposable';
import { flushPromises } from '@vue/test-utils';

let wrapper;
const testTmDate = '2021-03-09T08:28:27.150Z'; // proper format of tm-date is iso string
const getRelevantDatePart = (d) => d.slice(0, -5); // strip the .XXXZ suffix from the iso string date

describe('useDate.spec.js', () => {
  beforeEach(async () => {
    wrapper = mount(useDate);
    await flushPromises();
  });

  it('gets current date as iso string (tm-date)', () => {
    const currentDate = new Date();

    const expectedFullDate = currentDate.toISOString();
    const returnedFullDate = wrapper.today({ stripMins: false });

    expect(getRelevantDatePart(returnedFullDate)).to.equal(getRelevantDatePart(expectedFullDate));

    currentDate.setMinutes(0, 0, 0);
    const expectedMinutelessDate = currentDate.toISOString();
    const returnedMinutelessDate = wrapper.today({ stripMins: true });

    expect(getRelevantDatePart(returnedMinutelessDate)).to.equal(getRelevantDatePart(expectedMinutelessDate));

    currentDate.setHours(0, 0, 0, 0);
    const expectedHourlessDate = currentDate.toISOString();
    const returnedHourlessDate = wrapper.today({ stripHours: true });

    expect(getRelevantDatePart(returnedHourlessDate)).to.equal(getRelevantDatePart(expectedHourlessDate));
  });

  it('gets tomorrow date as iso string (tm-date)', () => {
    const tomorrowDate = new Date();
    tomorrowDate.setDate(tomorrowDate.getDate() + 1);

    const expectedFullDate = tomorrowDate.toISOString();
    const returnedFullDate = wrapper.tomorrow({ stripHours: false });

    expect(getRelevantDatePart(returnedFullDate)).to.equal(getRelevantDatePart(expectedFullDate));

    tomorrowDate.setHours(0, 0, 0, 0);
    const expectedHourlessDate = tomorrowDate.toISOString();
    const returnedHourlessDate = wrapper.tomorrow({ stripHours: true });

    expect(getRelevantDatePart(returnedHourlessDate)).to.equal(getRelevantDatePart(expectedHourlessDate));
  });

  it('gets yesterday date as iso string (tm-date)', () => {
    const yesterdayDate = new Date();
    yesterdayDate.setDate(yesterdayDate.getDate() - 1);

    const expectedFullDate = yesterdayDate.toISOString();
    const returnedFullDate = wrapper.yesterday({ stripHours: false });

    expect(getRelevantDatePart(returnedFullDate)).to.equal(getRelevantDatePart(expectedFullDate));

    yesterdayDate.setHours(0, 0, 0, 0);
    const expectedHourlessDate = yesterdayDate.toISOString();
    const returnedHourlessDate = wrapper.yesterday({ stripHours: true });

    expect(getRelevantDatePart(returnedHourlessDate)).to.equal(getRelevantDatePart(expectedHourlessDate));
  });

  it('gets hard coded model monday as tm-date', () => {
    const expectedDate = '2022-01-03T00:00:00.000Z';
    expect(wrapper.getModelMonday()).to.equal(expectedDate);

    // with specific hour
    const expectedDateWithSetHour = `2022-01-03T02:00:00.000Z`; // -1 UTC
    expect(wrapper.getModelMonday({ hour: 3 })).to.equal(expectedDateWithSetHour);
  });

  it('gets hours from tm-date', () => {
    const expectedHours = 9;
    expect(wrapper.getTmDateHours(testTmDate)).to.equal(expectedHours);
  });

  it('sets hours for tm-date', () => {
    const expectedDate = '2021-03-09T10:28:27.150Z';
    expect(wrapper.setTmDateHours(testTmDate, 11)).to.equal(expectedDate);
  });

  it('adds hours to tm-date', () => {
    const expectedDate = '2021-03-09T10:28:27.150Z';
    expect(wrapper.addTmDateHours(testTmDate, 2)).to.equal(expectedDate);

    // with max hour limit
    const expectedDate2 = '2021-03-09T04:00:27.150Z'; // back to minHour (-1 UTC)
    const returnedDate = wrapper.addTmDateHours(testTmDate, 2, { minHour: 5, maxHour: 10 });
    expect(returnedDate).to.equal(expectedDate2);
  });

  it('subtracts hours from tm-date', () => {
    const expectedDate = '2021-03-09T06:28:27.150Z';
    expect(wrapper.subtractTmDateHours(testTmDate, 2)).to.equal(expectedDate);
  });

  it('formats tm-date for API', () => {
    const expectedDate = '2021-03-09T08:00:00';
    expect(wrapper.formatAsApiDate(testTmDate)).to.equal(expectedDate);
  });

  it('transforms tm-date to js-date', () => {
    const expectedDate = new Date(testTmDate);
    expect(wrapper.toJsDate(testTmDate)).to.deep.equal(expectedDate);

    expectedDate.setHours(0, 0, 0, 0);
    const returnedDate = wrapper.toJsDate(testTmDate, { stripHours: true });
    expect(returnedDate).to.deep.equal(expectedDate);
  });

  it('can strip hours while transforming tm-date to js-date', () => {
    const expectedDate = new Date(testTmDate);
    expectedDate.setHours(0, 0, 0, 0);
    expect(wrapper.toJsDate(testTmDate, { stripHours: true })).to.deep.equal(expectedDate);
  });

  it('transforms js date to tm-date', () => {
    expect(wrapper.toTmDate(new Date(testTmDate))).to.equal(testTmDate);
  });

  it('formats as api date', () => {
    const expectedDate = '2021-03-09T08:28:27';
    const returnedDate = wrapper.formatAsApiDate(testTmDate, false);
    expect(returnedDate).to.deep.equal(expectedDate);

    // rounded down
    const testDate = '2021-03-09T08:28:27.150Z';
    const expectedRoundedDate = '2021-03-09T08:00:00';
    const returnedRoundedDate = wrapper.formatAsApiDate(testDate, true);
    expect(returnedRoundedDate).to.deep.equal(expectedRoundedDate);

    // rounded up
    const testDate2 = '2021-03-09T08:31:27.150Z';
    const expectedRoundedDate2 = '2021-03-09T09:00:00';
    const returnedRoundedDate2 = wrapper.formatAsApiDate(testDate2, true);
    expect(returnedRoundedDate2).to.deep.equal(expectedRoundedDate2);
  });

  it('formats travel time', () => {
    const expectedTime = '10h  15m  0s';
    const returnedTime = wrapper.formatTravelTime(10.25);
    expect(returnedTime).to.deep.equal(expectedTime);
  });

  it('gets time as decimal number', () => {
    const testDate = '2021-03-09T08:30:00.000Z';
    const expectedDecimal = 9.5; // 8:30 + 1 UTM
    const returnedDecimal = wrapper.getTmDateHoursNumber(testDate);
    expect(returnedDecimal).to.deep.equal(expectedDecimal);

    const expectedDecimal2 = 9;
    const returnedDecimal2 = wrapper.getTmDateHoursNumber(testDate, { stripMins: true });
    expect(returnedDecimal2).to.deep.equal(expectedDecimal2);
  });

  it('gets the hour part from hour decimal', () => {
    const expectedHour = 9;
    const returnedHour = wrapper.getHoursFromHoursNumber(9.5);
    expect(returnedHour).to.deep.equal(expectedHour);
  });

  it('reports intersecting dates', () => {
    const testDate = '2021-03-09T08:28:27.150Z';

    const isIntersectingWithTestDate = (d1, d2) =>
      wrapper.dateIntervalIntersection(testDate)({
        dateFrom: d1,
        dateTo: d2,
      });

    expect(isIntersectingWithTestDate('2021-03-09T08:28:27.150Z', '2021-03-09T08:28:27.150Z')).to.equal(false);
    expect(isIntersectingWithTestDate('2021-03-09T08:28:27.150Z', '2021-03-10T08:28:27.150Z')).to.equal(true);
    expect(isIntersectingWithTestDate('2021-03-10T08:28:27.150Z', '2021-03-10T08:28:27.150Z')).to.equal(false);
    expect(isIntersectingWithTestDate('2021-03-08T08:28:27.150Z', '2021-03-10T08:28:27.150Z')).to.equal(true);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
