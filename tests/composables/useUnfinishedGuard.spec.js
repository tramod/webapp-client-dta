import { ref } from 'vue';
import useUnfinishedGuard from '@composables/useUnfinishedGuard';
import { mount } from '../mountComposable';
import { getStoreMock, getRouteMock } from './../helpers.js';
import { mockCloseEditSessionEndpoint, axiosMock, API_HOST } from './../mockAxios.js';
import { flushPromises } from '@vue/test-utils';

let wrapper;
const testModel = { id: 1, name: 'test' };
const testSubmitAction = sinon.spy();
const testRoute = { name: 'test' };
const testScenario = { id: 1 };

describe('useUnfinishedGuard.spec.js', () => {
  it('tracks model change', async () => {
    const testNewModel = ref({ ...testModel });
    const testFormerModel = ref({ ...testModel });

    wrapper = mount(() =>
      useUnfinishedGuard({
        newModel: testNewModel,
        formerModel: testFormerModel,
        modelType: 'scenario',
        submitAction: testSubmitAction,
      }),
    );

    expect(wrapper.isModelChanged.value).to.equal(false);
    testNewModel.value.name = 'changed';
    expect(wrapper.isModelChanged.value).to.equal(true);
  });

  it('displays unfinished scenario form dialog', async () => {
    const testNewModel = ref({ ...testModel });
    const testFormerModel = ref({ ...testModel });

    wrapper = mount(() =>
      useUnfinishedGuard({
        newModel: testNewModel,
        formerModel: testFormerModel,
        modelType: 'scenario',
        submitAction: testSubmitAction,
      }),
    );
    // make sure dialog is closed
    wrapper.dialog.close();
    expect(wrapper.dialog.displayed.value).to.equal(false);
    // change model value
    testNewModel.value.name = 'changed';
    // simulate leaving the route
    wrapper.guardRouteLeave(testRoute);

    expect(wrapper.dialog.displayed.value).to.equal(true);
    expect(wrapper.dialog.dialog.value.data.modelType).to.equal('scenario');
  });

  it('displays unfinished event form dialog', async () => {
    const testNewModel = ref({ ...testModel });
    const testFormerModel = ref({ ...testModel });

    wrapper = mount(() =>
      useUnfinishedGuard({
        newModel: testNewModel,
        formerModel: testFormerModel,
        modelType: 'event',
        submitAction: testSubmitAction,
      }),
    );
    // make sure dialog is closed
    wrapper.dialog.close();
    expect(wrapper.dialog.displayed.value).to.equal(false);
    // change model value
    testNewModel.value.name = 'changed';
    // simulate leaving the route
    wrapper.guardRouteLeave(testRoute);

    expect(wrapper.dialog.displayed.value).to.equal(true);
    expect(wrapper.dialog.dialog.value.data.modelType).to.equal('event');
  });

  it('displays unfinished modification form dialog', async () => {
    const testNewModel = ref({ ...testModel });
    const testFormerModel = ref({ ...testModel });

    wrapper = mount(() =>
      useUnfinishedGuard({
        newModel: testNewModel,
        formerModel: testFormerModel,
        modelType: 'modification',
        submitAction: testSubmitAction,
      }),
    );
    // make sure dialog is closed
    wrapper.dialog.close();
    expect(wrapper.dialog.displayed.value).to.equal(false);
    // change model value
    testNewModel.value.name = 'changed';
    // simulate leaving the route
    wrapper.guardRouteLeave(testRoute);

    expect(wrapper.dialog.displayed.value).to.equal(true);
    expect(wrapper.dialog.dialog.value.data.modelType).to.equal('modification');
  });

  it('displays unfinished scenario (edit session) dialog', async () => {
    const testNewModel = ref({ ...testModel });
    const testFormerModel = ref({ ...testModel });

    const testScenarioId = 1;
    const testRoute = getRouteMock();
    testRoute.params.id = testScenarioId;
    const testStore = getStoreMock();
    testStore.getters['scenarios/isEditModeInProgress'] = () => testScenarioId;
    testStore.getters['scenarios/isScenarioToBeSaved'] = () => true;

    wrapper = mount(
      () =>
        useUnfinishedGuard({
          newModel: testNewModel,
          formerModel: testFormerModel,
          modelType: 'scenario',
          submitAction: testSubmitAction,
        }),
      { store: testStore, route: testRoute },
    );
    // make sure dialog is closed
    wrapper.dialog.close();
    expect(wrapper.dialog.displayed.value).to.equal(false);
    // simulate leaving the route
    wrapper.guardRouteLeave(testRoute);

    expect(wrapper.dialog.displayed.value).to.equal(true);
    expect(wrapper.dialog.dialog.value.data).to.equal(undefined);
  });

  it('does not display dialog if nothing has changed', async () => {
    const testNewModel = ref({ ...testModel });
    const testFormerModel = ref({ ...testModel });

    const testScenarioId = 1;
    const testRoute = getRouteMock();
    testRoute.params.id = testScenarioId;
    const testStore = getStoreMock();
    testStore.getters['scenarios/isScenarioToBeSaved'] = () => false;

    wrapper = mount(
      () =>
        useUnfinishedGuard({
          newModel: testNewModel,
          formerModel: testFormerModel,
          modelType: 'scenario',
          submitAction: testSubmitAction,
        }),
      { store: testStore, route: testRoute },
    );
    // make sure dialog is closed
    wrapper.dialog.close();
    expect(wrapper.dialog.displayed.value).to.equal(false);
    // simulate leaving the route
    wrapper.guardRouteLeave(testRoute);
    expect(wrapper.dialog.displayed.value).to.equal(false);
  });

  it('tries to close edit session', async () => {
    mockCloseEditSessionEndpoint({ scenarioId: testScenario.id });

    const testNewModel = ref({ ...testModel });
    const testFormerModel = ref({ ...testModel });

    const testScenarioId = 1;
    const testRoute = getRouteMock();
    testRoute.params.id = testScenarioId;
    const testStore = getStoreMock();
    testStore.getters['scenarios/isEditModeInProgress'] = () => testScenarioId;
    testStore.getters['scenarios/isScenarioToBeSaved'] = () => false;

    wrapper = mount(
      () =>
        useUnfinishedGuard({
          newModel: testNewModel,
          formerModel: testFormerModel,
          modelType: 'scenario',
          submitAction: testSubmitAction,
        }),
      { store: testStore, route: testRoute },
    );
    // make sure dialog is closed
    wrapper.dialog.close();
    expect(wrapper.dialog.displayed.value).to.equal(false);
    // simulate leaving the route
    wrapper.guardRouteLeave(testRoute);
    await flushPromises();

    expect(wrapper.dialog.displayed.value).to.equal(false);
    // expect that it sends request to close edit session
    expect(axiosMock.history.post.length).to.equal(1);
    expect(axiosMock.history.post[0].url).to.equal(`${API_HOST}scenarios/${testScenario.id}/edit/leave`);
  });

  afterEach(() => {
    wrapper.unmount();
  });
});
