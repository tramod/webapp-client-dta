import useModelSigns from '@composables/useModelSigns';
import useLayerSource from '@composables/useLayerSourceData';
import { mount } from '../mountComposable';
import { LID_DTA_LINKS, LID_DTA_NODES, LID_DTA_GENERATORS } from '@keys/index';
import {
  mockModelLinksSourceEndpoint,
  mockModelNodesSourceEndpoint,
  mockModelGeneratorsSourceEndpoint,
} from './../mockAxios.js';
import { MOCK_BASE_MODEL, MOCK_SCENARIOS } from './useModelSigns.data.js';
// TODO Probably should refactor to createStore fn so store is not leaking between tests?
// App would use exported appStore instance so all modules can import same one?
import store from '@store';
import { expect } from 'chai';
import { activateScenario, deactivateScenario } from '../scenarioSetup.js';
import { getStoreMock } from '../helpers.js';

let wrapper, testStore;
const lsUnmounts = [];
const lsWrappers = {};

describe('useModelSigns.spec.js', () => {
  // Setup mock modelData once for whole test suite
  before(async () => {
    testStore = getStoreMock();
    const endpointMocks = {
      [LID_DTA_LINKS]: mockModelLinksSourceEndpoint,
      [LID_DTA_NODES]: mockModelNodesSourceEndpoint,
      [LID_DTA_GENERATORS]: mockModelGeneratorsSourceEndpoint,
    };
    const types = Object.keys(endpointMocks);
    await Promise.all(
      types.map(async (modelType) => {
        endpointMocks[modelType]({ response: { features: MOCK_BASE_MODEL[modelType] } });
        setupTestStore(); // setup store before mounting ls
        const lsWrapper = mount(() => useLayerSource(modelType), { store: testStore });
        lsWrapper.clearSourceData();
        await lsWrapper.fetchSourceData();
        lsWrappers[modelType] = lsWrapper;
        lsUnmounts.push(lsWrapper.unmount);
      }),
    );
  });

  beforeEach(async () => {
    setActiveId(store, null);
    setDate(store, new Date().toISOString());
    setMapModeAndModelType(store);
  });

  it('mounts successfully and returns signs feature (test base)', async () => {
    await activateScenario(MOCK_SCENARIOS.baseTest);
    setActiveId(store, MOCK_SCENARIOS.baseTest.id);
    wrapper = mount(() => useModelSigns(), { store });
    expect(wrapper.signFeatures.value).to.have.lengthOf(1);
  });

  it('does not provide signs until source is loaded', async () => {
    await activateScenario(MOCK_SCENARIOS.baseTest);
    lsWrappers[LID_DTA_LINKS].clearSourceData();
    setActiveId(store, MOCK_SCENARIOS.baseTest.id);
    wrapper = mount(() => useModelSigns(), { store });
    expect(wrapper.signFeatures.value).to.have.lengthOf(0);

    await lsWrappers[LID_DTA_LINKS].fetchSourceData();
    expect(wrapper.signFeatures.value).to.have.lengthOf(1);
  });

  it('provides signs from correct active scenario', async () => {
    await activateScenario(MOCK_SCENARIOS.multiTest1);
    await activateScenario(MOCK_SCENARIOS.multiTest2);
    wrapper = mount(() => useModelSigns(), { store });

    setActiveId(store, MOCK_SCENARIOS.multiTest1.id);
    expect(wrapper.signFeatures.value).to.have.lengthOf(1);
    let feature = wrapper.signFeatures.value[0];
    expect(feature.get('modId')).to.equal(MOCK_SCENARIOS.multiTest1.events[0].modifications[0].id);

    setActiveId(store, MOCK_SCENARIOS.multiTest2.id);
    expect(wrapper.signFeatures.value).to.have.lengthOf(1);
    feature = wrapper.signFeatures.value[0];
    expect(feature.get('modId')).to.equal(MOCK_SCENARIOS.multiTest2.events[0].modifications[0].id);
  });

  it('filters correctly based on calendar date and applyFilters option', async () => {
    await activateScenario(MOCK_SCENARIOS.filterTest);
    wrapper = mount(() => useModelSigns(), { store });
    setActiveId(store, MOCK_SCENARIOS.filterTest.id);

    const testCases = [
      // applied date and event included filter
      {
        date: '2021-06-01T00:00:00.000Z',
        applyFilters: true,
        expectedMods: [1, 2],
      },
      {
        date: '2022-06-01T00:00:00.000Z',
        applyFilters: true,
        expectedMods: [1],
      },
      // no filters applied
      {
        date: '2021-06-01T00:00:00.000Z',
        applyFilters: false,
        expectedMods: [1, 2, 3, 4],
      },
      {
        date: '2022-06-01T00:00:00.000Z',
        applyFilters: false,
        expectedMods: [1, 2, 3, 4],
      },
    ];

    for (const { date, applyFilters, expectedMods } of testCases) {
      setDate(store, date);
      wrapper.applyFilters.value = applyFilters;
      expect(wrapper.signFeatures.value).to.have.lengthOf(expectedMods.length);

      const modIds = wrapper.signFeatures.value.map((feature) => feature.get('modId'));
      expect(modIds).to.deep.equal(expectedMods);
    }
  });

  it('provided features have expected props', async () => {
    await activateScenario(MOCK_SCENARIOS.baseTest);
    setActiveId(store, MOCK_SCENARIOS.baseTest.id);
    wrapper = mount(() => useModelSigns(), { store });
    expect(wrapper.signFeatures.value).to.have.lengthOf(1);
    const featureProps = wrapper.signFeatures.value[0].getProperties();
    expect(featureProps).to.include.all.keys(['eventId', 'modId', 'dateFrom', 'dateTo', 'type', 'mode']);
  });

  it('resulting features have expected point geometry', async () => {
    await activateScenario(MOCK_SCENARIOS.transform);
    setActiveId(store, MOCK_SCENARIOS.transform.id);
    wrapper = mount(() => useModelSigns(), { store });
    expect(wrapper.signFeatures.value).to.have.lengthOf(6);

    const expectedPoints = {
      1: [150.0, 150.0], //center of link
      2: [250.0, 250.0], // center of last link
      3: [300.0, 300.0],
      4: [200.0, 200.0],
      5: [5000.0, 5000.0],
      6: [150.0, 150.0],
    };
    const features = wrapper.signFeatures.value;
    for (const feature of features) {
      const coords = feature.getGeometry().getCoordinates();
      const modId = feature.get('modId');
      const roundedCoords = coords.map((c) => Math.round(c));
      expect(expectedPoints[modId]).to.deep.equal(roundedCoords);
    }
  });

  it('expose all mods also in traffic signs mode', async () => {
    await activateScenario(MOCK_SCENARIOS.transform);
    setActiveId(store, MOCK_SCENARIOS.transform.id);
    setTrafficSignMode(store);
    wrapper = mount(() => useModelSigns(), { store });
    expect(wrapper.signFeatures.value).to.have.lengthOf(6);
  });

  it('expose features with correct props in sign mode', async () => {
    await activateScenario(MOCK_SCENARIOS.signsMode);
    setActiveId(store, MOCK_SCENARIOS.signsMode.id);
    setTrafficSignMode(store);

    const expectedProps = [
      { isClosed: true, isRestricted: false },
      { isClosed: false, isRestricted: true },
      { isClosed: false, isRestricted: false },
    ];

    lsWrappers[LID_DTA_LINKS].clearSourceData();
    wrapper = mount(() => useModelSigns(), { store });
    expect(wrapper.signFeatures.value).to.have.lengthOf(0); // Signs features produced only after source data load
    await lsWrappers[LID_DTA_LINKS].fetchSourceData(); // Works with source load after mount, wrong execution order bug surfaced
    expect(wrapper.signFeatures.value).to.have.lengthOf(expectedProps.length);

    for (let i = 0; i < wrapper.signFeatures.value.length; i++) {
      const feature = wrapper.signFeatures.value[i];
      const { isClosed, isRestricted } = feature.getProperties();
      expect(isClosed).to.equal(expectedProps[i].isClosed);
      expect(isRestricted).to.equal(expectedProps[i].isRestricted);
    }
  });

  afterEach(() => {
    wrapper.unmount();

    setTrafficSignMode(store, false);
  });

  after(async () => {
    await deactivateScenario(MOCK_SCENARIOS.baseTest);
    for (const lsUnmount of lsUnmounts) {
      lsUnmount();
    }
  });
});

function setupTestStore() {
  testStore.state.scenarios.cache = {
    ...testStore.state.scenarios.cache,
    scenarios: { enabled: true },
    events: { enabled: true },
    modifications: { enabled: true },
  };
}

function setActiveId(store, scenarioId) {
  store.commit('map/SET_ITEM', { scenarioId });
}

function setTrafficSignMode(store, status = true) {
  store.commit('map/SET_MAP_STYLE', { group: 'general', newState: status, property: 'trafficSignsStyle' });
}

function setDate(store, dateString) {
  store.commit('map/SET_CALENDAR', { mapMode: 'modelDTA', modelType: 'DTA', tmDate: dateString });
}

function setMapModeAndModelType(store, mapMode = 'modelDTA', modelType = 'DTA') {
  store.commit('scenarios/SET_MODEL', { type: modelType });
  store.commit('map/SET_MAP_MODE', { mode: mapMode });
}
