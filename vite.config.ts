import { UserConfig, defineConfig } from 'vitest/config';
import { loadEnv } from 'vite';
import vue from '@vitejs/plugin-vue';
import VueI18nPlugin from '@intlify/unplugin-vue-i18n/vite';
import { resolve, dirname } from 'path';
import { fileURLToPath } from 'url';
import { ViteAliases } from 'vite-aliases';
import { exec } from 'child_process';
import VitePluginHtmlEnv from 'vite-plugin-html-env';

// __dirname replacement for ES module
// eslint-disable-next-line no-undef
const _dirname: string = typeof __dirname !== 'undefined' ? __dirname : dirname(fileURLToPath(import.meta.url));

const getBranchName = (): Promise<string> =>
  new Promise<string>((resolve, reject) => {
    exec('git rev-parse --abbrev-ref HEAD', (error, stdout) => {
      if (error) reject(error);
      resolve(stdout.trim());
    });
  });

const getLastCommit = (): Promise<string> =>
  new Promise<string>((resolve, reject) => {
    exec('git log -1 --pretty=%B', (error, stdout) => {
      if (error) reject(error);
      resolve(stdout.trim());
    });
  });

export default defineConfig(async ({ command, mode }) => {
  const env = loadEnv(mode, process.cwd() + '/env', 'APP_');
  const envBase = env.APP_BASE;

  const baseConfig: UserConfig = {
    ...(envBase && { base: envBase }),
    define: {
      // eslint-disable-next-line no-undef
      __APP_VERSION__: JSON.stringify(process.env.npm_package_version), // process is not defined in vite as it uses import.meta
      __BUILD_TIME__: JSON.stringify(Date.now()),
      __BRANCH_NAME__: JSON.stringify(await getBranchName()),
      __COMMIT_MESSAGE__: JSON.stringify(await getLastCommit()),
    },
    resolve: {
      dedupe: ['vue'], // prevent duplicate bundle by vuemap
    },
    plugins: [
      vue(),
      ViteAliases(),
      VueI18nPlugin({ include: resolve(_dirname, 'src/locales/messages/**/*') }),
      VitePluginHtmlEnv(), // enables using env variables inside index.html (<{ ... }>)
    ],
    envDir: 'env',
  };

  const devConfig: UserConfig = {
    server: {
      fs: {
        // Allow serving files from project root and vuemap folder one level up to the project root
        allow: ['./', '../vuemap'],
      },
      proxy: {
        '/api': {
          target: 'http://localhost:8088',
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, ''),
        },
      },
    },
  };

  const prodConfig: UserConfig = {
    build: {
      sourcemap: true,
    },
  };

  const testConfig: UserConfig = {
    test: {
      globals: true, // provides vitest API globally (describe(), it(), expect(),..)
      environment: 'jsdom', // 'node' | 'jsdom' | 'happy-dom' (after testing both node and happy-dom, jsdom seems closest to the browser and fixes button click event issues)
      include: ['./tests/**/*.spec.js'],
      setupFiles: ['./tests/vitest.js'],
      deps: {
        inline: [
          'sinon', // sinon import fix since it is ES Module but shipped in a CommonJS package
          'date-fns', //  same here
          '@vue/devtools-api', // same here
        ],
      },
    },
  };

  return {
    ...baseConfig,
    ...(command === 'serve' ? devConfig : prodConfig),
    ...(mode === 'test' && testConfig),
  };
});
