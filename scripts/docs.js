import { parse, ScriptHandlers, TemplateHandlers } from 'vue-docgen-api';
// documentation by Vue Styleguidist
// https://vue-styleguidist.github.io/docs/Docgen.html#api

parse('src/views/playground/Playground.vue', {
  // these handlers are defaults, but can be customized
  scriptHandlers: [ScriptHandlers.componentHandler],
  templateHandlers: [TemplateHandlers.slotHandler],
}).then((ci) => {
  // TODO: we can parse/log/display the docs somehow
  console.log(ci);
});
