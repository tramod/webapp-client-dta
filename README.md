# (TraMod) WebApp Client

Client side component of TraMod WebApp. Tightly coupled to WebApp Backend component and build on top of traffic-modeller modelling server application. Thus main feature is management and display of traffic models (DTA) and traffic scenarios based on these models. Application can also display traffic data from real traffic observations both realtime and historical.

Build on OpenLayers (v7), Vue3 and PrimeVue with help of Vite and TypeScript.

## Requirements

- For development/build, you will need Node.js and npm installed in your environement.
- Recommended (tested) Node version: v16
- Internal vuemap helper package is utilized in project. This package is not published on npm nor orchestrated in monorepo setup so for usage see installation point below.

## Development

1. Download repository, eg. `$ git clone git@gitlab.com:tramod/webapp-client.git`

2. Install dependencies `$ npm install`

3. Build and install vuemap package

- Download repository, eg. `$ git clone git@gitlab.com:tramod/webapp-vuemap.git`
- Expect this folder structure
  ```
  TraMod
    - webapp-client
    - webapp-vuemap
  ```
- CD to webapp-vuemap
- Run `$ npm run build && npm pack`
- CD to webapp-client
- Install vuemap package. Use current vuemap version for {version} eg. _0.2.5_ . `$ npm install ../vuemap/vuemap-{version}.tgz`

4. Adjust configuration if required. See our [config guide](./env/CONFIG.md) for list of options

5. Run dev command `$ npm run dev`. Note that _dev-pilsen_ mode (see config for details) is used by default.

## Build for production

1. Steps 1-3 from development guide.

2. Add config for your production mode (or adjust default mode). See our [config guide](./env/CONFIG.md)

3. Run app build command `$ npm run build` or `$ npm run build -- --mode your-mode` if special configuration is done.

### Environment

Production usage of WebApp is tested with _PM2 process manager_ (relevant for backend) and served with _NGINX_ proxy.
For client nonce attribute placeholder may be noticed on analytics script tag of _dist/index.html_. For proper nonce support this nonce attribute must be manually copied to main script tag after build is done.
